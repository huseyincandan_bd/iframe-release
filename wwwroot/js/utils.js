﻿class utils {
	static getUrlParamList(url) {
		if (!url)
			url = window.location.href;
		var params = {};
		var parser = document.createElement('a');
		parser.href = url;
		var query = parser.search.substring(1);
		var vars = query.split('&');
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split('=');
			params[pair[0]] = decodeURIComponent(pair[1]);
		}
		return params;
	}
	static getUrlParam (paramName) {
		var list = this.getUrlParamList(window.location.href);
		return list[paramName];
	}
	static updateURLParam(url, param, paramVal) {
		var newAdditionalURL = "";
		var tempArray = url.split("?");
		var baseURL = tempArray[0];
		var additionalURL = tempArray[1];
		var temp = "";
		if (additionalURL) {
			tempArray = additionalURL.split("&");
			for (var i = 0; i < tempArray.length; i++) {
				if (tempArray[i].split('=')[0] != param) {
					newAdditionalURL += temp + tempArray[i];
					temp = "&";
				}
			}
		}
		var rows_txt = temp + "" + param + "=" + paramVal;
		return baseURL + "?" + newAdditionalURL + rows_txt;
	}
	static getCookie(name) {
		const value = `; ${document.cookie}`;
		const parts = value.split(`; ${name}=`);
		if (parts.length === 2) return parts.pop().split(';').shift();
	}
	static getFSessionID() {
		return this.getCookie('fSessionID');
	}
	static openPopup(type, title, message, buttons, options) {
		var defaultOptions = {
			onCloseFunction: null,
			size: '',
			customClass: '',
			headerButtons : []
        }
		options = { ...defaultOptions, ...options };
		if (!window.popupIndex)
			window.popupIndex = 0;
		window.popupIndex++;
		var popupID = 'fPopup' + window.popupIndex;
		if (!buttons) {
			buttons = [{
				text: lngHelper.get('generic.ok'),
				class : 'btn-primary',
				onclick: function () {
					$('#' + popupID).modal('hide');
                }
			}]
        }
		var buttonContent = '';
		if (type)
			type = 'bg-' + type;
		if (options.size)
			options.size = 'modal-' + options.size;
		var content = '\
			<div class="modal fade ' + options.customClass + '" id="' + popupID + '" tabindex="-1" role="dialog" aria-labelledby="' + popupID + 'Title" aria-hidden="true" >\
				<div class="modal-dialog modal-dialog-centered ' + options.size + '" role="document">\
					<div class="modal-content">\
						<div class="modal-header ' + type + '">\
							<h5 class="modal-title" id="' + popupID + 'Title">' + title + '</h5>\
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-left:0px">\
								<i aria-hidden="true" class="ki ki-close"></i>\
							</button>\
						</div>\
						<div class="modal-body ' + (options.modalBodyClass ? options.modalBodyClass : '') + '">' + message + '\
						</div>\
						<div class="modal-footer">\
						</div>\
					</div>\
				</div>\
			</div>';
		var contentJQ = $(content);
		var contentFooter = contentJQ.find('.modal-footer');
		if (buttons.length > 0) {
			for (var i = 0; i < buttons.length; i++) {
				var btn = $('<button type="button" class="btn ' + (buttons[i].class || 'btn-primary') + '">' + buttons[i].text + '</button>');
				if (buttons[i].onclick) {
					if (typeof (buttons[i].onclick) == 'string')
						btn.attr('onclick', buttons[i].onclick);
					else
						btn.on('click', buttons[i].onclick);
				} else
					btn.attr('data-dismiss', 'modal');
				contentFooter.append(btn);
			}
		} else
			contentFooter.remove();

		if (options.headerButtons.length > 0) {
			var modalCloseButton = contentJQ.find('.modal-header .close');
			for (var i = 0; i < options.headerButtons.length; i++) {
				var btn = $('\
					<div class="ml-auto">\
						<button type="button" class="close" data-dismiss="modal" style="margin-left:0px;margin-right:10px;">\
							<i aria-hidden="true" class="far fa-window-maximize"></i>\
						</button>\
					</div>');
				if (options.headerButtons[i].onclick) {
					if (typeof (options.headerButtons[i].onclick) == 'string')
						btn.attr('onclick', options.headerButtons[i].onclick);
					else
						btn.on('click', options.headerButtons[i].onclick);
				} else
					btn.attr('data-dismiss', 'modal');
				btn.insertBefore(modalCloseButton);
            }
        }

		$('body').append(contentJQ);
		//$('#' + popupID).modal('show');
		contentJQ.modal({
			keyboard: false,
			backdrop: 'static',
			show:true
		});
		if (options.onCloseFunction) {
			contentJQ.on('hidden.bs.modal', function (e) {
				options.onCloseFunction();
            })
		}
		if (options.draggable) {
			if (contentJQ.draggable) {
				contentJQ.draggable({
					handle: ".modal-header"
				})
            }
        }
		return contentJQ;
	}
	static alertWarning(message) {
		this.openPopup('warning', lngHelper.get('generic.warning'), message);
	}
	static alertInfo(message) {
		this.openPopup('info', lngHelper.get('generic.info'), message);
	}
	static alertError(message) {
		this.openPopup('danger', lngHelper.get('generic.error'), message);
	}
	static alertDanger(message, callBackFunction) {
		var popup = this.openPopup('danger', lngHelper.get('generic.danger'), message, [{
			text: lngHelper.get('generic.ok'),
			onclick: function () {
				popup.modal('hide');
				if (callBackFunction)
					callBackFunction();
			}
		}]);
	}
	static alertConfirm(message, callBackFunction) {
		var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), message, [{
			text: lngHelper.get('generic.yes'),
			onclick: function () {
				popup.modal('hide');
				if (callBackFunction)
					callBackFunction();
			}
		}, {
			text: lngHelper.get('generic.no')
		}])
	}
	static confirmDelete(callBackFunction) {
		utils.alertConfirm(lngHelper.get('generic.confirmDelete'), callBackFunction);
	}
	static openPage(url, onCloseFunction, options) {
		if (!options)
			options = {};
		if (!options.fOpenerWindow)
			options.fOpenerWindow = window;
		if (self != top && parent.getUtilsObject) {
			if (!parent.preventParentPopupOnThisPage || !parent.preventParentPopupOnThisPage()) {
				return parent.getUtilsObject().openPage(url, onCloseFunction, options);
			}
		}
		url = encodeURI(url);
		var defaultHeaderButtons = [{
			content: '&#x21E7;',
			onclick: function () {
				var openedWindow = window.open($(this).parents('.popup-page').find('iframe').attr('src'));
				openedWindow.addEventListener('DOMContentLoaded', function () {
					openedWindow.fOpenerWindow = options.fOpenerWindow;
				}, false);
			}
		}];
		if (options && options.noOpenInNewTab) {
			defaultHeaderButtons = [];
		}
		options = Object.assign({
			onCloseFunction: onCloseFunction,
			size: 'xxl',
			customClass: 'popup-page',
			headerButtons: defaultHeaderButtons
		}, options);
		options.modalBodyClass = 'modal-body-h100';
		var content = '<div class="page-container"></div>'
		var popup = utils.openPopup('', '', content, [], options);
		var iframe = $(document.createElement('iframe'));
		iframe.ready(function () {
			iframe[0].contentWindow.fOpenerWindow = options.fOpenerWindow;
		})

		iframe.prop('src', url).prop('width', '100%').prop('height', '100vh').css('width', '100%').css('height', '100%').css('display:block');
		iframe.on('load', () => {
			if (options.onLoad)
				options.onLoad(iframe);
			if (options.callBackFunction) {
				if (iframe[0].contentWindow.setSelectForCallerCallBackFunction)
					iframe[0].contentWindow.setSelectForCallerCallBackFunction(options.callBackFunction);
            }
		})
		popup.find('.page-container').append(iframe);
		return popup;
	}
	static closePage(modalID) {
		$('[id=' + modalID + ']').modal('hide');
	}
	static closePageFromInside(windowObj, isRecursiveCall) {
		if (!windowObj)
			windowObj = window;

		if (!isRecursiveCall && self == top)
			window.close();

		var bFound = false;
		var iframes = $('iframe');
		for (var i = 0; i < iframes.length; i++) {
			if (iframes[i].contentWindow == windowObj) {
				utils.closePage($(iframes[i]).parents('.modal').attr('id'));
				bFound = true;
				break;
			}
		}
		if (!bFound && self != top && parent && parent.getUtilsObject) {
			parent.getUtilsObject().closePageFromInside(windowObj, true);
			return;
		}
	}
	//TODO: childTitleChanged yerine bu fonksiyon kullanılmalı  
	static changeTitleFromInside(title, windowObj) {
		if (!windowObj) {
			windowObj = window;
			document.title = title;
        }

		var bFound = false;
		var iframes = $('iframe');
		for (var i = 0; i < iframes.length; i++) {
			if (iframes[i].contentWindow == windowObj) {
				$(iframes[i]).parents('.modal-content').find('.modal-header .modal-title').html(title);
				bFound = true;
				break;
			}
		}
		if (!bFound && self != top && parent && parent.getUtilsObject) {
			parent.getUtilsObject().changeTitleFromInside(title, windowObj);
			return;
		}
    }
	static getOuterHtml(elem) {
		return $('<div>').append(elem.clone()).html();
	}
	static htmlEscape(str) {
		if (!str)
			return str;
		return str.toString().replace(/"/g, '&#34;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	}
	static getEventSender(e) {
		if (!e) e = window.event;
		return $(e.srcElement || e.target);
	}
	static overlay(op, id) {
		if (op == 'showSingle') {
			var overlays = $('.foverlay');
			if (overlays && overlays.length > 0)
				return null;
        }
		if (op == 'show' || op == 'showSingle') {
			var newID = 1;
			$('.foverlay').each((index, item) => {
				if ($(item).attr('id')) {
					var objID = parseInt($(item).attr('id').split('-')[1]);
					if (objID >= newID)
						newID = objID + 1;
				}
			})
			newID = 'overlay-' + newID.toString();
			var overlayElem = '<div id="' + newID + '" class="foverlay"></div>';
			$('body').append(overlayElem);
			return newID;
		} else {
			if (op == 'hide') {
				if (id)
					$('#' + id).remove();
				else {
					$('.foverlay').remove();
				}
			}
		}
	}
	static progress(op, id) {
		if (op == 'show' || op == 'showSingle') {
			var elemID = this.overlay(op, id);
			if (elemID) {
				var content = '<div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status"><span class="sr-only">Loading...</span></div>';
				$('#' + elemID).append(content);
			}
			return elemID;
		} else {
			this.overlay(op, id);
		}
	}
	static fillSelectOptions(elem, options, noTitleOption, titleOptionValue) {
		elem.html('');
		var html = '';
		if (titleOptionValue == undefined)
			titleOptionValue = '0';
		if (!noTitleOption)
			html = '<option value="' + titleOptionValue + '">-' + lngHelper.get('generic.select') + '-</option>';
		for (var i = 0; i < options.length; i++) {
			var opt = options[i];
			html += '<option value="' + opt.value + '"';
			if (opt.attributes) {
				for (const [key, value] of Object.entries(opt.attributes)) {
					html += ' ' + key + '="' + utils.htmlEscape(value) + '"';
				}
			}
			html += '>' + utils.htmlEscape(opt.text) + '</option>';
		}
		elem.html(html);
	}
	static fillSelectOptions2(elem, options, noTitleOption) {
		utils.fillSelectOptions(elem, options, noTitleOption, utils.emptyGuid);
	}
	static fillSelectOptionGroups(elem, optionGroups, noTitleOption, titleOptionValue) {
		elem.html('');
		var html = '';
		if (titleOptionValue == undefined)
			titleOptionValue = '0';
		if (!noTitleOption)
			html = '<option value="' + titleOptionValue + '">-' + lngHelper.get('generic.select') + '-</option>';
		for (var i = 0; i < optionGroups.length; i++) {
			var og = optionGroups[i];
			html += '<optgroup label="' + utils.htmlEscape(og.label) + '" ' + (og.id ? 'id="' + utils.htmlEscape(og.id) + '"' : '');
			if (og.attributes) {
				for (const [key, value] of Object.entries(og.attributes)) {
					html += ' ' + key + '="' + value + '"';
				}
			}
			html += '>';
			for (var j = 0; j < og.options.length; j++) {
				var o = og.options[j];
				html += '<option value="' + o.value + '"';
				if (o.attributes) {
					for (const [key, value] of Object.entries(o.attributes)) {
						html += ' ' + key + '="' + value + '"';
					}
				}
				html += '>' + utils.htmlEscape(o.text) + '</option>';
			}
			html += '</optgroup>';
        }
		elem.html(html);
	}
	static selectObjectItemFromTable(objectID, tableID, callBackFunction, extParam) {
		var url = '../html/tableDataList.html?op=select';
		if (extParam)
			url += '&' + extParam;
		if (objectID)
			url += '&objectID=' + objectID;
		if (tableID)
			url += '&tableID=' + tableID;
		var selectObjectItemFromTable_Page = utils.openPage(url, null, {
			headerButtons: [], callBackFunction: function (recID, params) {
				callBackFunction(recID, params);
				selectObjectItemFromTable_Page.modal('hide');
			}
		});
		return selectObjectItemFromTable_Page;
	}
	static redirectOnRoot(url) {
		if (self != top && parent.getUtilsObject)
			window.parent.getUtilsObject().redirectOnRoot(url);
		else
			window.location.href = url;
	}
	static getRootUrlForRedirectAfterLogin() {
		if (self != top && parent.getUtilsObject)
			return window.parent.getUtilsObject().getRootUrlForRedirectAfterLogin();
		else {
			if (location.pathname == '/html/main.html') {
				if ($('#mainFrame').attr('src'))
					return location.pathname + '?openPage=' + $('#mainFrame').attr('src');
				else
					return location.pathname;
			} else {
				return location.pathname + location.search;
            }
        }
    }
	static formValidate(form) {
		if (!form.checkValidity()) {
			form.reportValidity();
			return false;
		}
		return true;
	}
    static downloadFile(url, params) {
		if (params) {
			var form = $('<form></form>').attr('action', url).attr('method', 'post');
			for (var i = 0; i < params.length; i++) {
				form.append($("<input></input>").attr('type', 'hidden').attr('name', params[i].name).attr('value', params[i].value));
			}
			form.appendTo('body').submit().remove();
		} else {
			var iframe = $('<iframe src="' + url + '" style="display:none"></iframe>');
			iframe.appendTo('body');
        }
	};
	static copyTextToClipboard(str, containerElem) {
		if (!containerElem)
			containerElem = document.body;
		const el = document.createElement('textarea');
		el.value = str;
		containerElem.appendChild(el);
		el.select();
		el.setSelectionRange(0, 99999); /* For mobile devices */
		var result = document.execCommand('copy');
		containerElem.removeChild(el);
		return result;
	}
	static showToast(type, message, options) {
		if (toastr) {
			options = Object.assign({
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": false,
				"positionClass": "toast-top-right",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "3000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}, options);
			toastr.options = options;

			toastr[type](message);
		}
	}
	static callChildIFrameFunction(onRoot, functionName, params) {
		if (onRoot && self != top && parent.getUtilsObject)
			window.parent.getUtilsObject().callChildIFrameFunction(onRoot, functionName, params);
		else {
			var iframes = $('iframe');
			for (var i = 0; i < iframes.length; i++) {
				var item = iframes[i];
				if (item.contentWindow && item.contentWindow[functionName]) {
					try {
						item.contentWindow[functionName](params);
					}
					catch (err) { }
				}
            }
        }
	}
	static preventClickTwice(elem) {
		if (elem instanceof PointerEvent)
			elem = utils.getEventSender(elem);
		if (!elem)
			return;
		elem.attr('disabled', true);
		setTimeout(function () {
			elem.attr('disabled', false);
		}, 500);
    }
}
utils.emptyGuid = '00000000-0000-0000-0000-000000000000';

window.getUtilsObject = function () {
	return utils;
}

window.tableDataSelected = function (recID, params) {
	utils.tableDataSelected(recID, params)
}

class dataTableHelper {
	constructor(elemID, dtParams, options) {
		this.elemID = elemID;
		this.elem = $('#' + elemID);
		this.dtParams = dtParams;
		this.options = options;
		this.dtParams.language = {
			url: '../lib/datatables/' + lngHelper.currentLanguage.dataTablesName
		}
		if (!this.dtParams.hasOwnProperty('processing'))
			this.dtParams.processing = true;
		if (!this.dtParams.hasOwnProperty('dom'))
			this.dtParams.dom = 'rt<"bottom"<pi>l><"clear">';
		if (this.options && this.options.defaultActionButtons) {
			this.options.buttons = [{
				text: lngHelper.get('generic.edit'),
				clickFunction: 'btnEditClick'
			}, {
				text: lngHelper.get('generic.delete'),
				clickFunction: 'btnDeleteClick'
				}];
			this.dblClickFunction = 'btnEditClick';
		}
		if (dtParams.ajax) {
			if (typeof dtParams.ajax == 'string') {
				dtParams.ajax = {
					url: dtParams.ajax
				}
			}
			dtParams.ajax.error = function (xhr, error, code) {
				if (xhr.status == 401 || xhr.status == 403)
					service.processSessionTimeOut();
				else {
					var content = '<p>' + code.toString() + '</p>';
					content += '<div style="width:100%"><a onclick="var mDiv = this.parentNode.parentNode.querySelector(\'.error-details\'); mDiv.style.display = (mDiv.style.display == \'none\' ? \'block\' : \'none\')" style="cursor:pointer;color:#007bff">' + lngHelper.get('generic.errorDetails') + '</a></div>';
					if (xhr.responseText)
						content += '<div class="error-details" style="max-height:150px;overflow:scroll;font-size:10pt;width:100%;margin-top:20px;display:none;border:1px solid lightgray;padding:10px">' + xhr.responseText + '</div>';
					if (xhr.responseJSON && xhr.responseJSON.detail && xhr.responseJSON.detail.indexOf('##') == 0)
						content = lngHelper.get(xhr.responseJSON.detail.substring(2));
					utils.openPopup('danger', lngHelper.get('generic.error'), content);
                }
			}
		}
		if (this.options && this.options.buttons) {
			var rowMenu = '<div class="table-action-button-container"><div class="dropdown dropdown-inline">\
                    <button type="button" class="btn btn-light-primary btn-icon btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                        <i class="ki ki-bold-more-hor"></i>\
                    </button>\
                    <div class="dropdown-menu">';
			for (var i = 0; i < this.options.buttons.length; i++) {
				var curButton = this.options.buttons[i];
				if (curButton.isSeperator) {
					rowMenu += '<hr/>';
				} else {
					rowMenu += '<a class="dropdown-item" href="#" onclick="' + curButton.clickFunction + '(event)"';
					if (curButton.attributes) {
						for (const [key, value] of Object.entries(curButton.attributes)) {
							rowMenu += ' ' + key + '="' + utils.htmlEscape(value) + '"';
						}
					}
					rowMenu += '>' + curButton.text + '</a>';
                }
			}
			rowMenu += '</div>\
                </div></div>';
			for (var i = 0; i < dtParams.columns.length; i++)
				this.dtParams.columns[i].data = i;
			var actionColumnVisible = true;
			if (!this.options.buttons || this.options.buttons.length == 0)
				actionColumnVisible = false;
			this.dtParams.columns.unshift({ title: '', data: null, orderable: false, searchable: false, visible: actionColumnVisible, defaultContent: rowMenu, width: 31, className: 'action-column' });
			if (!this.dtParams.order)
				this.dtParams.order = [[2, "asc"]];
		}
		this.dtParams.pageLength = dataTableHelper.getDefaultPageLength();
		if (window.lngHelper && window.lngHelper.currentLanguage && window.lngHelper.currentLanguage.name == 'tr') {
			if (!this.dtParams.search)
				this.dtParams.search = {};
			this.dtParams.search.regex = true;
		}
		if (options.fSelect) {
			this.dtParams = Object.assign(this.dtParams, {
				select: {
					style: 'os',
					selector: 'td:first-child'
				}
			});
			this.dtParams.columns.unshift({
				title: '',
				name: '_fselect',
				data: null,
				orderable: false,
				searchable: false,
				visible: false,
				className: 'select-checkbox',
				defaultContent: ''
			});
			if (options.fSelectMenuElem)
				this.initSelectMenu();
		}
		//this.dtParams.responsive = true;
		this.dtParams.language = lngHelper.getDataTableLanguageObject();
		this.dtObject = this.elem.DataTable(this.dtParams);
		if (this.options && this.options.dblClickFunction) {
			var self = this;
			this.elem.find('tbody').on('dblclick', 'tr', function (e) {
				if (event && event.target && event.target.tagName.toUpperCase() == 'INPUT')
					return;
				if (window[self.options.dblClickFunction])
					window[self.options.dblClickFunction](event);
			});
		}
		$('[search-target=' + this.elemID + ']').on('input', (e) => {
			var sender = utils.getEventSender(e);
			var searchText = sender.val();
			if (typeof searchText === 'string' && window.lngHelper && window.lngHelper.currentLanguage && window.lngHelper.currentLanguage.name == 'tr') {
				searchText = searchText.replace(/[-[/\]{}()*+?.,\\^$|#\s]/g, '\\$&').replace(/i/g, '(İ|i)').replace(/İ/g, '(İ|i)').replace(/ı/g, '(I|ı)').replace(/I/g, '(I|ı)');
				searchText = searchText.replace(/\\ /g, ' ');

            }
			//searchText = '(.*)';
			$('#' + sender.attr('search-target')).DataTable().search(searchText).draw();
		});
		this.elem.on('length.dt', function (e, settings, len) {
			dataTableHelper.setDefaultPageLength(len);
		});
		var self = this;
		this.elem.on('draw.dt', function (e) {
			var pageInfo = self.dtObject.page.info();
			if (pageInfo.recordsTotal <= 10)
				self.elem.parents('.dataTables_wrapper').find('.dataTables_length').hide();
			else
				self.elem.parents('.dataTables_wrapper').find('.dataTables_length').show();
			if (pageInfo.pages == 1)
				self.elem.parents('.dataTables_wrapper').find('.dataTables_paginate').hide();
			else
				self.elem.parents('.dataTables_wrapper').find('.dataTables_paginate').show();
        })
	}
	setSelectionVisible(visible) {
		this.dtObject.column(0).visible(visible);
		if (visible)
			this.elem.closest('.dataTables_wrapper').addClass('selection-visible');
		else
			this.elem.closest('.dataTables_wrapper').removeClass('selection-visible');
		this.elem.trigger("selectionVisibleChange");
    }
	initSelectMenu() {
		var self = this;
		this.options.fSelectMenuElem.html('');
		var toggleSelection = $('<a class="dropdown-item" href="#">' + lngHelper.get('generic.tableSelectMenu_ToggleSelection') + '</a>');
		this.options.fSelectMenuElem.append(toggleSelection);
		toggleSelection.on('click', function () {
			var visible = !self.dtObject.column(0).visible();
			self.setSelectionVisible(visible);
		});
		var selectAll = $('<a class="dropdown-item" href="#">' + lngHelper.get('generic.tableSelectMenu_SelectAll') + '</a>');
		this.options.fSelectMenuElem.append(selectAll);
		selectAll.on('click', function () {
			self.setSelectionVisible(true)
			self.dtObject.rows().select();
		});
		if (this.options.fSelectMenuItems && this.options.fSelectMenuItems.length) {
			this.options.fSelectMenuElem.append($('<div class="dropdown-divider"></div>'));
			for (var i = 0; i < this.options.fSelectMenuItems.length; i++) {
				var item = this.options.fSelectMenuItems[i];
				var menuElem = $('<a class="dropdown-item action-button disabled" href="#">' + item.displayName + '</a>');
				menuElem.data('clickFunction', item.onclick);
				this.options.fSelectMenuElem.append(menuElem);
				menuElem.on('click', function (e) {
					utils.getEventSender(e).data('clickFunction')();
                })
			}
			this.elem.on('select.dt', function (e, dt, type, indexes) {
				self.selectionChanged();
			})
			this.elem.on('deselect.dt', function (e, dt, type, indexes) {
				self.selectionChanged();
			})
        }
	}
	selectionChanged() {
		var count = this.dtObject.rows({ selected: true }).count();
		if (this.options.fSelectMenuElem.length) {
			if (count > 0)
				this.options.fSelectMenuElem.find('.action-button').removeClass('disabled');
			else
				this.options.fSelectMenuElem.find('.action-button').addClass('disabled');
        }
    }
	reload(resetPaging = true, callBack) {
		this.dtObject.ajax.reload(callBack, resetPaging);
	}
	updateData(dataSet) {
		this.dtObject.clear();
		this.dtObject.rows.add(dataSet);
		this.dtObject.draw();
    }
	destroy() {
		this.dtObject.destroy();
		this.elem.empty();
	}
	static setDefaultPageLength(length) {
		localStorage.setItem('fTablePageLength', length);
	}
	static getDefaultPageLength() {
		var len = localStorage.getItem('fTablePageLength');
		if (len) {
			try {
				len = parseInt(len);
			} catch {
				len = 10;
			}
		} else
			len = 10;
		return len;
    }
	static getRowFromElem(elem) {
		if (elem instanceof MouseEvent)
			elem = utils.getEventSender(elem);
		var rowElem = elem;
		if (rowElem.prop('tagName') != 'TR')
			rowElem = elem.parents('tr');
		if (!rowElem)
			return;
		var tableElem = rowElem.parents('table');
		var tableObj = tableElem.DataTable();
		return tableObj.row(rowElem); 
    }
	static setClickEvents(dataTable, noClickEvent) {
		var table = $(dataTable.table().node());
		if (!noClickEvent) {
			table.find('tbody').on('click', 'tr', function (e) {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				}
				else {
					dataTable.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});
		}
		if (table.attr('onrowdblclick')) {
			table.find('tbody').on('dblclick', 'tr', function (e) {
				if (event && event.target && event.target.tagName.toUpperCase() == 'INPUT')
					return;
				if (table.attr('onrowdblclick')) {
					dataTable.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
					eval(table.attr('onrowdblclick'));
				}
			});
		}
	}
	static getSelectedRow(dataTable, alertIfNull) {
		var selectedRow = dataTable.row('.selected');
		if (selectedRow && selectedRow.length == 0) {
			if (alertIfNull)
				utils.alertWarning(lngHelper.get('generic.selectRecord'));
			return null;
        }
		return selectedRow;
	}
	static getSelectedID(dataTable) {
		var selectedRow = this.getSelectedRow(dataTable, true);
		if (selectedRow)
			return parseInt(selectedRow.data()[0]);
		return -1;
    }
}

class menuHelper {
	static findMenuInList(recID, menuList) {
		for (var i = 0; i < menuList.length; i++) {
			if (menuList[i].recID == recID)
				return menuList[i];
		}
		return null;
    }
    static sortMenu(menuArray) {
		menuArray.sort(function (a, b) {
			if (a.recOrder > b.recOrder)
				return 1;
			if (a.recOrder < b.recOrder)
				return -1;
			return 0;
		})
		for (var i = 0; i < menuArray.length; i++) {
			if (menuArray[i].children)
				menuHelper.sortMenu(menuArray[i].children);
		}
	}
	static findMenuInTree(recID, menuArray) {
		for (var i = 0; i < menuArray.length; i++) {
			if (menuArray[i].recID == recID)
				return menuArray[i];
			if (menuArray[i].children && menuArray[i].children.length > 0) {
				var item = menuHelper.findMenuInTree(recID, menuArray[i].children);
				if (item)
					return item;
			}
		}
		return null;
	}
	static menuListToTree(menuList) {
		var menuTree = [];
		for (var i = 0; i < menuList.length; i++)
			menuList[i].found = false;
		for (var i = 0; i < menuList.length; i++) {
			if (menuList[i].parentID == utils.emptyGuid) {
				menuTree.push(menuList[i]);
				menuList[i].found = true;
				menuList[i].children = [];
			}
		}
		var counter = 0;
		while (true) {
			var allFound = true;
			for (var i = 0; i < menuList.length; i++) {
				if (!menuList[i].found) {
					var item = menuHelper.findMenuInTree(menuList[i].parentID, menuTree);
					if (item) {
						item.children.push(menuList[i]);
						menuList[i].found = true;
						menuList[i].children = [];
					} else
						allFound = false;
				}
			}
			if (allFound)
				break;
			counter++;
			if (counter > 3)  //parenti olmayan menu olmamalı, varsa da sonsuz döngüye girmeyelim
				break;
		}
		menuHelper.sortMenu(menuTree);
		return menuTree;
	}
}

class queryHelper{
	static async getDataQuery(objectID, propertyArray, query, queryOptions, callBackFunction) {
		const FObjectRelationType = { OneToMany: 0, OneToOne: 1, ManyToMany: 2 };

		if (!propertyArray)
			propertyArray = [];
		var objectPropertyDetails = await service.call('fObject/getPropertyDetails?fObjectID=' + objectID, 'POST', propertyArray);

		queryOptions = Object.assign({
			copyToClipboardButtonVisible: true,
			okButtonText: lngHelper.get('generic.ok'),
			isForObjectDataAuthorization: false,
			title: lngHelper.get('generic.query')
		}, queryOptions);

		var operatorDefault = '<select class="form-control query-operator query-operator-string" onchange="queryHelper.getDataQuery_OperatorChange()"><option value="LIKE">≈</option><option value="=">=</option><option value=">">&gt;</option><option value="<">&lt;</option><option value="<>">!=</option><option value="><">&gt;&lt;</option><option value="\'\'">\'\'</option><option value="!\'\'">!\'\'</option></select>';
		var operatorSelect = '<select class="form-control query-operator query-operator-select" onchange="queryHelper.getDataQuery_OperatorChange()"><option value="IN">=</option><option value="NOT IN">!=</option><option value="\'\'">\'\'</option><option value="!\'\'">!\'\'</option></select>';
		var operatorCheckbox = '<select class="form-control query-operator query-operator-checkbox" onchange="queryHelper.getDataQuery_OperatorChange()"><option value="-1">&#8722;</option><option value="1">&#10003;</option><option value="0">&#10539;</option></select>';
		var operatorNumeric = '<select class="form-control query-operator" onchange="queryHelper.getDataQuery_OperatorChange()"><option value="=">=</option><option value=">">&gt;</option><option value="<">&lt;</option><option value="<>">!=</option><option value="><">&gt;&lt;</option><option value="\'\'">\'\'</option><option value="!\'\'">!\'\'</option></select>';

		var operatorAuthDefault = '<select class="form-control query-operator" onchange="queryHelper.getDataQuery_OperatorChange()"><option value="=">=</option><option value=">=">&gt;=</option><option value="<=">&lt;=</option><option value="<>">!=</option></select>';

		var html = '<div class="data-query-popup">';

		var objectPropertyGroupArray = [];
		for (var i = 0; i < objectPropertyDetails.length; i++) {
			var propertyDetail = objectPropertyDetails[i];
			var relationID = utils.emptyGuid;
			var index = propertyDetail.propertyFullID.lastIndexOf('_');
			if (index > 0) {
				relationID = propertyDetail.propertyFullID.substring(0, index);
			}
			var objectPropertyGroup = null;
			for (var j = 0; j < objectPropertyGroupArray.length; j++) {
				if (objectPropertyGroupArray[j].relationID == relationID) {
					objectPropertyGroup = objectPropertyGroupArray[j];
					break;
                }
			}
			if (objectPropertyGroup == null) {
				objectPropertyGroup = {
					relationID : relationID,
					displayName: propertyDetail.relationFullName,
					objectPropertyDetailArray : []
				}
				objectPropertyGroupArray.push(objectPropertyGroup);
			}
			objectPropertyGroup.objectPropertyDetailArray.push(propertyDetail);
        }

		if (queryOptions.isForObjectDataAuthorization) {
			var selectDefault = $('<select></select>');
			var optArray = ['userUserName', 'departmentName', 'companyName', 'userGroupName'];
			var options = [];
			for (var i = 0; i < optArray.length; i++) {
				options.push({
					value: '{{' + optArray[i] + '}}',
					text: lngHelper.get('authorization.objectDataAuthorization_' + optArray[i])
				})
			}
			utils.fillSelectOptions(selectDefault, options);

			for (var i = 0; i < objectPropertyGroupArray.length; i++) {
				var objectPropertyGroup = objectPropertyGroupArray[i];
				if (i != 0) {
					html += '<div class="query-section">' + utils.htmlEscape(objectPropertyGroup.displayName) + '</div><hr/>';
                }
				for (var j = 0; j < objectPropertyGroup.objectPropertyDetailArray.length; j++) {
					var objectPropertyDetail = objectPropertyGroup.objectPropertyDetailArray[j];
					var objectProperty = objectPropertyDetail.property;
					var htmlPart = '<div class="form-group row" property-id="' + objectPropertyDetail.propertyFullID + '">\
								<label for="query-' + objectPropertyDetail.propertyFullID + '-1" class="col-sm-4 col-form-label">' + objectProperty.displayName + '</label>\
								<div class="col-sm-8 input-container">';
					var bSkip = false;
					switch (objectProperty.editorType) {
						case window.fEnums.FObjectPropertyEditorType.Checkbox:
							bSkip = true;
							break;
						default:
							htmlPart += operatorAuthDefault + '<div class="select-container"><select id="query-' + objectPropertyDetail.propertyFullID + '-1" class="form-control property-select-authorization">';
							htmlPart += selectDefault.html();
							htmlPart += '</select></div>';
					}
					if (bSkip)
						continue;
					htmlPart += '</div></div>';
					html += htmlPart;
                }
            }
		} else {

			for (var i = 0; i < objectPropertyGroupArray.length; i++) {
				var objectPropertyGroup = objectPropertyGroupArray[i];
				if (i != 0) {
					html += '<div class="query-section">' + utils.htmlEscape(objectPropertyGroup.displayName) + '</div><hr/>';
				}
				for (var j = 0; j < objectPropertyGroup.objectPropertyDetailArray.length; j++) {
					var objectPropertyDetail = objectPropertyGroup.objectPropertyDetailArray[j];
					var objectProperty = objectPropertyDetail.property;
					html += '<div class="form-group row" property-id="' + objectPropertyDetail.propertyFullID + '">\
								<label for="query-' + objectPropertyDetail.propertyFullID + '-1" class="col-sm-4 col-form-label">' + objectProperty.displayName + '</label>\
								<div class="col-sm-8 input-container">';
					switch (objectProperty.editorType) {
						case window.fEnums.FObjectPropertyEditorType.RadioButton:
						case window.fEnums.FObjectPropertyEditorType.Select:
							html += operatorSelect + '<div class="select-container"><select id="query-' + objectPropertyDetail.propertyFullID + '-1" class="form-control property-select" multiple>';
							var options = await service.call('fOptionListItem/getList', 'GET', { fOptionListID: objectProperty.fOptionListID });
							for (var k = 0; k < options.length; k++)
								html += '<option value="' + options[k].recID + '">' + utils.htmlEscape(options[k].displayName) + '</option>';
							html += '</select></div>';
							break;
						case window.fEnums.FObjectPropertyEditorType.Checkbox:
							html += operatorCheckbox;
							break;
						default:
							var inputType = 'text';
							var operator = operatorDefault;
							switch (objectProperty.editorType) {
								case window.fEnums.FObjectPropertyEditorType.Date: inputType = 'date'; operator = operatorNumeric; break;
								case window.fEnums.FObjectPropertyEditorType.DateTime: inputType = 'datetime-local'; operator = operatorNumeric; break;
								case window.fEnums.FObjectPropertyEditorType.Time: inputType = 'time'; operator = operatorNumeric; break;
								case window.fEnums.FObjectPropertyEditorType.InputNumber:
								case window.fEnums.FObjectPropertyEditorType.InputDecimal: inputType = 'number'; operator = operatorNumeric; break;
							}
							html += operator + '<input type="' + inputType + '" class="form-control property-input" id="query-' + objectPropertyDetail.propertyFullID + '-1">\
										<input type="' + inputType + '" class="form-control property-input property-input2" id="query-' + objectPropertyDetail.propertyFullID + '-2">';
							break;
					}
					html += '</div></div>';

				}
			}
        }

		html += '</div>'
		var buttons = [];
		if (queryOptions.copyToClipboardButtonVisible) {
			buttons.push({
				text: lngHelper.get('generic.copyQueryObjectToClipboard'),
				onclick: 'queryHelper.getDataQuery_CopyToClipboardClick()',
				class: 'btn-primary'
			})
		};
		buttons.push({
			text: lngHelper.get('generic.clear'),
			onclick: 'queryHelper.getDataQuery_ClearClick()',
			class: 'btn-primary'
		});
		buttons.push({
			text: queryOptions.okButtonText,
			onclick: 'queryHelper.getDataQuery_QueryClick()',
			class: 'btn-primary'
		});

		window.currentDataQueryPopup = utils.openPopup(null, queryOptions.title, html, buttons, { size: 'xl' });

		if (query && query.columnFilters) {
			for (var i = 0; i < query.columnFilters.length; i++) {
				for (var j = 0; j < objectPropertyDetails.length; j++) {
					var objectPropertyDetail = objectPropertyDetails[j];
					var objectProperty = objectPropertyDetail.property;
					if (query.columnFilters[i].name == objectPropertyDetail.propertyFullID) {
						var operator = query.columnFilters[i].operator;
						var operatorOption = operator;
						if (operator == '>=' || operator == '<=')
							operatorOption = '><';
						var formGroup = window.currentDataQueryPopup.find('.form-group[property-id=' + objectPropertyDetail.propertyFullID + ']');
						if (formGroup) {
							var operatorElem = formGroup.find('.query-operator');

							if (objectProperty.editorType == window.fEnums.FObjectPropertyEditorType.Checkbox) {
								var value = query.columnFilters[i].value;
								if (value.toString() == '1') {
									operatorElem.val('1');
								} else {
									if (value.toString() == '0')
										operatorElem.val('0');
									else
										operatorElem.val('-1');
								}
							}
							else {
								operatorElem.val(operatorOption);
								this.getDataQuery_OperatorChange({ srcElement: operatorElem[0] });

								if (queryOptions.isForObjectDataAuthorization) {
									var input = formGroup.find('.property-select-authorization');
									var value = query.columnFilters[i].value;
									input.val(value);
								} else {
									var inputs = formGroup.find('.property-input');
									switch (objectProperty.editorType) {
										case window.fEnums.FObjectPropertyEditorType.RadioButton:
										case window.fEnums.FObjectPropertyEditorType.Select:
											var input = formGroup.find('.property-select');
											var values = query.columnFilters[i].value.substring(1, query.columnFilters[i].value.length - 1).split(',');
											var options = input.find('option');
											var valueArray = [];
											for (var k = 0; k < values.length; k++) {
												var valueStr = values[k].substring(1, values[k].length - 1);
												for (var t = 0; t < options.length; t++) {
													if ($(options[t]).text() == valueStr) {
														valueArray.push($(options[t]).val());
														break;
													}
												}
											}
											input.selectpicker('val', valueArray);
											break;
										default:
											var value = query.columnFilters[i].value;
											if (objectProperty.editorType == window.fEnums.FObjectPropertyEditorType.DateTime)
												value = value.replace(/ /, 'T');
											if (value.indexOf && value) {
												if (value.indexOf('%') == 0)
													value = value.substring(1);
												if (value.indexOf('%') == value.length - 1)
													value = value.substring(0, value.length - 1);
											}
											switch (query.columnFilters[i].operator) {
												case '<=':
													$(inputs[1]).val(value);
													break;
												default:
													$(inputs[0]).val(value);
													break;
											}
											break;
									}
								}
							}
						}
						break;
					}
				}
			}
		}
		
		window.currentDataQueryPopup.data('objectPropertyDetails', objectPropertyDetails);
		window.currentDataQueryPopup.data('queryOptions', queryOptions);
		window.currentDataQueryPopup.find('.property-select').selectpicker();
		window.currentDataQueryCallBackFunction = callBackFunction;
	}
	static getDataQuery_OperatorChange(e) {
		var sender = utils.getEventSender(e);
		if (sender.val() == '\'\'' || sender.val() == '!\'\'')
			sender.parents('.input-container').find('.property-input').hide();
		else
			sender.parents('.input-container').find('.property-input').show();

		if (sender.val() == '><')
			sender.parents('.input-container').find('.property-input2').show();
		else
			sender.parents('.input-container').find('.property-input2').hide();
	}
	static getDataQuery_ClearClick() {
		window.currentDataQueryPopup.find('.form-group .property-input').val('');
		window.currentDataQueryPopup.find('.form-group select.property-select').selectpicker('val', '');
		window.currentDataQueryPopup.find('.form-group select.property-select-authorization').val('0');
		window.currentDataQueryPopup.find('.form-group .property-checkbox').prop('checked', false);
		window.currentDataQueryPopup.find('.form-group .query-operator').val('=');
		window.currentDataQueryPopup.find('.form-group .query-operator-string').val('LIKE');
		window.currentDataQueryPopup.find('.form-group .query-operator-checkbox').val('-1');
		window.currentDataQueryPopup.find('.form-group .query-operator-select').val('IN');
	}
	static getDataQuery_CopyToClipboardClick() {
		var objectDataQuery = queryHelper.getDataQuery_GetQuery();
		if (objectDataQuery)
			objectDataQuery = objectDataQuery.columnFilters;
		var result = utils.copyTextToClipboard(encodeURIComponent(JSON.stringify(objectDataQuery)), window.currentDataQueryPopup.find('.modal-body')[0]);
		if (result)
			utils.alertInfo(lngHelper.get('generic.copiedToClipboard'));
		else
			utils.alertWarning(lngHelper.get('generic.couldNotCopyToClipboard'));
	}
	static getDataQuery_GetQuery() {
		var objectPropertyDetails = window.currentDataQueryPopup.data('objectPropertyDetails');
		var queryOptions = window.currentDataQueryPopup.data('queryOptions');
		var objectDataQuery = {
			columnFilters: [],
			orders: []
			//startIndex: 1,
			//endIndex: 10
		}

		for (var i = 0; i < objectPropertyDetails.length; i++) {
			var objectPropertyDetail = objectPropertyDetails[i];
			var objectProperty = objectPropertyDetail.property;
			var formGroup = window.currentDataQueryPopup.find('.form-group[property-id=' + objectPropertyDetail.propertyFullID + ']');
			if (formGroup && formGroup.length > 0) {
				var operator = formGroup.find('.query-operator').val();
				var val1 = null;
				var val2 = null;
				if (queryOptions.isForObjectDataAuthorization) {
					val1 = formGroup.find('select.property-select-authorization').val();
					if (val1 == '0')
						val1 = null;
				}
				else {
					switch (objectProperty.editorType) {
						case window.fEnums.FObjectPropertyEditorType.RadioButton:
						case window.fEnums.FObjectPropertyEditorType.Select:
							var value = formGroup.find('select.property-select').selectpicker('val');
							if (value && value.length > 0) {
								var val1 = '';
								for (var j = 0; j < value.length; j++) {
									if (val1 != '')
										val1 += ',';
									val1 += "'" + formGroup.find('select.property-select option[value=' + value[j] + ']').text() + "'";
								}
								val1 = '(' + val1 + ')';
							}
							break;
						case window.fEnums.FObjectPropertyEditorType.Checkbox:
							var value = formGroup.find('.query-operator-checkbox').val();
							if (value != '-1') {
								val1 = value == '1' ? 1 : 0;
								operator = '=';
							}
							break;
						default:
							var inputs = formGroup.find('.property-input');
							if (operator == '\'\'' || operator == '!\'\'') {
								val1 = '-';
								break;
                            }
							if (operator == 'LIKE') {
								val1 = $(inputs[0]).val();
								if (val1) {
									if (val1.indexOf('*') == -1 && val1.indexOf('%') == -1)
										val1 = '%' + val1 + '%';
									else
										val1 = val1.replace(/\*/g, '%');
								}
							} else {
								val1 = $(inputs[0]).val();
								val2 = $(inputs[1]).val();
								if (objectProperty.editorType == window.fEnums.FObjectPropertyEditorType.DateTime) {
									val1 = val1.replace(/T/, ' ');
									val2 = val2.replace(/T/, ' ');
								}
							}
							if (!val1) val1 = null;
							if (!val2) val2 = null;
							break;
					}
                }

				if (operator == '><') {
					if (val1 != null) {
						objectDataQuery.columnFilters.push({
							name: objectPropertyDetail.propertyFullID,
							operator: '>=',
							value: val1
						})
					}
					if (val1 != null) {
						objectDataQuery.columnFilters.push({
							name: objectPropertyDetail.propertyFullID,
							operator: '<=',
							value: val2
						})
					}

				} else {
					if (val1 != null) {
						objectDataQuery.columnFilters.push({
							name: objectPropertyDetail.propertyFullID,
							operator: operator,
							value: val1
						})
					}
				}
			}
		}
		return objectDataQuery;
    }
	static getDataQuery_QueryClick() {
		var objectDataQuery = queryHelper.getDataQuery_GetQuery();
		if (window.currentDataQueryCallBackFunction)
			window.currentDataQueryCallBackFunction(objectDataQuery);
		window.currentDataQueryPopup.modal('hide');
	}
}
//queryHelper.FObjectPropertyEditorType = { InputText: 0, TextArea: 1, InputNumber: 2, InputDecimal: 3, Date: 4, Select: 5, Checkbox: 6, RadioButton: 7, DateTime: 8, Time: 9 };

class lngHelperClass {
	constructor() {
		this.supportedLanguages = [{
			name: 'en',
			icon: 'usa.svg',
			displayName: 'English',
			dataTablesName : 'English.json'
		},{
			name: 'tr',
			icon: 'turkey.svg',
			displayName : 'Türkçe',
			dataTablesName : 'Turkish.json'
		}];
		this.currentLanguage = this.supportedLanguages[0];
		this.setMomentLocale();
    }
	init() {
		var self = this;
		var supportedLngs = [];
		for (var i = 0; i < this.supportedLanguages.length; i++)
			supportedLngs.push(this.supportedLanguages[i].name);
		return new Promise((resolve, reject) => {
			lngHelper.loadCurrentLanguage();
			i18next
				.use(i18nextXHRBackend)
				.init({
					load: [self.currentLanguage.name],
					preload: [self.currentLanguage.name],
					fallbackLng: 'en',
					ns: 'default',
					defaultNS: 'default',
					supportedLngs: supportedLngs,
					backend: {
						loadPath: '/locales/{{lng}}/{{ns}}.json'
					},
					getAsync: false
				}, function (t) {
					jqueryI18next.init(i18next, $);
					lngHelper.setDocumentLanguage();
					resolve();
				});
		})
	}
	formatDateTime(value, format) {
		if (!format)
			format = 'L';
		if (format == 'dateTime')
			format = 'L LT';
		return moment(value).format(format);
	}
	dataTableDateTimeColumnRender = function(format) {
		if (!format)
			format = 'L';
		if (format == 'dateTime')
			format = 'L LT';
		if (format == 'time')
			format = 'LT'
		if (format == 'date')
			format = 'L'
		return function (d, type, row) {
			if (!d) {
				return type === 'sort' || type === 'type' ? 0 : d;
			}
			if (!window.moment)
				return d;
			var m = format == 'LT' ? window.moment.utc(d, 'HH:mm') : window.moment(d);
			return m.format(type === 'sort' || type === 'type' ? 'x' : format);
        }
	}
	dataTableNumberRender = function (minimumFractionDigits) {
		return function (d, type, row) {
			if (!d) {
				return type === 'sort' || type === 'type' ? 0 : d;
			}
			return (type === 'sort' || type === 'type') ? d : lngHelper.formatNumber(d, minimumFractionDigits);
		}
	}
	setMomentLocale() {
		if (window.moment)
			window.moment.locale(this.currentLanguage.name);
	}
	saveCurrentLanguage() {
		localStorage.setItem('flanguage', JSON.stringify(this.currentLanguage));
    }
	loadCurrentLanguage() {
		var tmp = JSON.parse(localStorage.getItem('flanguage'));
		if (tmp) {
			this.currentLanguage = JSON.parse(localStorage.getItem('flanguage'));
			this.setMomentLocale();
		}
	}
	setDocumentLanguage() {
		i18next.changeLanguage(this.currentLanguage.name);
		$(document).localize();
		if (window.parent && window.parent.childTitleChanged)
			window.parent.childTitleChanged();
		if (window.parent && window.parent.iFrameTitleChanged)
			window.parent.iFrameTitleChanged();		
    }
	setCurrentLanguage(lng) {
		return new Promise((resolve, reject) => {
			if (this.currentLanguage.name == lng)
				resolve();

			var bFound = false;
			for (var i = 0; i < this.supportedLanguages.length; i++) {
				if (this.supportedLanguages[i].name == lng) {
					this.currentLanguage = this.supportedLanguages[i];
					this.setMomentLocale();
					bFound = true;
					break;
				}
			}
			if (!bFound) {
				reject();
				return;
			}
			this.saveCurrentLanguage();
			i18next.loadLanguages([lng]).then(() => {
				this.setDocumentLanguage();
				resolve();
			})
		})
	}
	get(str) {
		return i18next.t(str);
	}
	formatNumber(val, minimumFractionDigits) {
		return i18next.t('{{val, number}}', { 'val': val, 'minimumFractionDigits': minimumFractionDigits });
	}
	getDataTableLanguageObject() {
		return i18next.getDataByLanguage(this.currentLanguage.name).default.dataTables;
    }
}
lngHelper = new lngHelperClass();
window.lng = lngHelper;
$(document).ready(function () {
	lngHelper.init().then(() => {
		$(document).localize();
		$(document).trigger('initcompleted');
		if (window.parent.childTitleChanged) {
			window.setTimeout(() => {
				window.parent.childTitleChanged();
			}, 300);
        }
	});

	//IF-12 
	if ($.fn.dataTable && lng.currentLanguage) {
		$.fn.dataTable.ext.order.intl = function (locales, options) {
			if (window.Intl) {
				var collator = new window.Intl.Collator(locales, options);
				var types = $.fn.dataTable.ext.type;

				delete types.order['string-pre'];
				types.order['string-asc'] = collator.compare;
				types.order['string-desc'] = function (a, b) {
					return collator.compare(a, b) * -1;
				};
			}
		};
		$.fn.dataTable.ext.order.intl(lng.currentLanguage.name);
	}

})

class bProcessHelper {
    static redirectToPage(url) {
		window.open(url, '_self', 'copyhistory=yes');
	}

	static async handleBProcessRunResult(runResult) {
		var processed = false;
		if (runResult.bProcessElement && runResult.bProcessElement.elementType == 0 && runResult.assignedToUser && runResult.bProcessElement.props && runResult.bProcessRun) {
			var elemProps = JSON.parse(runResult.bProcessElement.props);
			switch (elemProps.userActionType) {
				case window.fEnums.FBProcessUserActionType.OpenRecord:
					if (elemProps.openRecords) {
						for (var i = 0; i < elemProps.openRecords.length; i++) {
							switch (elemProps.openRecords[i].type) {
								case window.fEnums.FBProcessUserActionOpenRecordType.CreatedRecord:
									for (var j = 0; j < runResult.bProcessRunCreatedRecords.length; j++) {
										var bProcessRunCreatedRecord = runResult.bProcessRunCreatedRecords[j];
										if (bProcessRunCreatedRecord.elementNumber == elemProps.openRecords[i].createElementNumber.toString()) {
											processed = true;
											bProcessHelper.redirectToPage('./formDataEdit.html?formID=' + elemProps.openRecords[i].fFormID + '&recID=' + bProcessRunCreatedRecord.objectRecID + '&bProcessRunElementID=' + runResult.bProcessRunElement.recID);
											break;
										}
									}
									break;
								case window.fEnums.FBProcessUserActionOpenRecordType.AnyRecord:
									var parameter = await service.call('fBProcessRunParameter/get', 'GET', { fBProcessRunID: runResult.bProcessRun.recID, parameterNumber: elemProps.openRecords[i].dataRecIDParamNumber });
									processed = true;
									bProcessHelper.redirectToPage('./formDataEdit.html?formID=' + elemProps.openRecords[i].fFormID + '&recID=' + parameter.valueStr + '&bProcessRunElementID=' + runResult.bProcessRunElement.recID);
									break;
							}
						}
					}
					break;
				case window.fEnums.FBProcessUserActionType.SelectRecord:
					processed = true;
					bProcessHelper.redirectToPage('./tableDataList.html?op=select&tableID=' + elemProps.fTableID + '&bProcessRunElementID=' + runResult.bProcessRunElement.recID);
					break;
			}
		}
		if (!processed)
			bProcessHelper.redirectToPage('./bProcessRunHistory.html?bProcessRunID=' + runResult.bProcessRunElement.bProcessRunID);
    }
}

function iFrameTitleChanged(e) {
	$('.popup-page iframe').each((index, item) => {
		$(item).parents('.popup-page').find('.modal-header .modal-title').html(item.contentDocument.title);
	})
}

window.fEnums = {
	AuthObjectType: { Undefined: 0, UserCreate: 1, UserEdit: 2, UserDelete: 3, UserGroupCreate: 4, UserGroupEdit: 5, UserGroupDelete: 6, ObjectCreate: 7, FormCreate: 8, TableCreate: 9, MenuCreate: 10, OptionListEdit: 11, CompanyCreate: 12, CompanyEdit: 13, CompanyDelete: 14, DepartmentCreate: 15, DepartmentEdit: 16, DepartmentDelete: 17, MenuRead: 18, MenuEdit: 19, MenuDelete: 20, ObjectRead: 21, ObjectEdit: 22, ObjectDelete: 23, ObjectDataRead: 24, ObjectDataCreate: 25, ObjectDataEdit: 26, ObjectDataDelete: 27, FormRead: 28, FormEdit: 29, FormDelete: 30, TableRead: 31, TableEdit: 32, TableDelete: 33, BusinessProcessEdit: 34, RepositoryEdit: 35, ReportRead: 36, ReportEdit: 37, ReportDelete: 38, DashboardRead: 39, DashboardEdit: 40, LogView: 41, ExternalDatabaseEdit: 42, DynamicAssemblyEdit: 43, ScheduledTaskEdit: 44, ModuleEdit: 45, AuthorizationEdit: 46, UserTitleEdit: 47, AppSettingsEdit: 48, ApiDocumentationView: 49, TrashView: 50, ApiTokenEdit:51 },
	AuthStatusType: { Denied: 0, Allowed: 1 },
	FLogType: { AppError: 0, AppWarning: 1, AppInfo: 2, Custom: 3, LogIn: 4, LongInError: 5, LogOut: 6, ObjectData: 7, BProcess: 8, Dashboard: 9, Attachment: 10 },
	FLogOpType: { None: 0, Create: 1, Read: 2, Update: 3, Delete: 4, ReadListOrTable: 5, Export: 6, Undelete: 7 },
	//FBProcessParameterType: { StaticValue: 0, CreatedRecordID: 1, UrlParameter: 2, QueryResult: 3, ProcessStartTime: 4, Now: 5, StarterUserID: 6, CurrentUserID: 7, JavascriptEval: 8, CSharpEval: 9, BProcessRunID: 10, StarterUserName: 11 },   
	FSoftDeleteStatus: { OnlyNotDeleted: 0, OnlyDeleted: 1, All: 2 },
	FBProcessSetParameterValueType : {
		Static: 0,
		UrlParameter: 1,
		StarterUserID: 2,
		StarterUserName: 3,
		StarterUserSurname: 4,
		StarterUserUserName: 5,
		ProcessStartTime: 6,
		ProcessRunID: 7,
		ActionTime: 8,
		CreateRecordID: 9,
		CreateRecordPropertyValue: 10,
		StarterUserEmailAddress: 11,
		ProcessRunStateDisplayName: 12
	},
	FBProcessSetParameterValueTypeGroup: {
		Generic: 0,
		StarterUserInfo: 1,
		ProcessInfo : 2,
		RecordInfo : 3
	},
	FBProcessActionNotificationType: {
		Email: 0,
		InApp: 1,
		SMS: 2
	},
	FBProcessRoleType: {
		User: 0,
		UserGroup: 1,
		ProcessParameter: 2,
		RunElementUser: 3
	},
	FBProcessAssignListItemType: {
		BProcessDisplayName: 0,
		BProcessVersionDisplayName: 1,
		BProcessCurrentStateDisplayName: 2,
		BProcessRunElementTime: 3,
		BProcessRunElementAssigneeName: 4,
		BProcessRunParameterValue: 5
	},
	MailSecureSocketOptions: {
		None: 0,
		Auto: 1,
		SslOnConnect: 2,
		StartTls: 3,
		StartTlsWhenAvailable: 4
	},
	FAppSettingType: {
		//AppInfo: 0,
		Generic: 0,
		Mail: 1,
		Log: 2,
		LDAP: 3
	},
	FDatabaseType: {
		MSSQLServer: 0,
		Oracle: 1,
		PostgreSQL: 2,
		MySQL: 3
	},
	FObjectTableType: {
		Table: 0,
		View: 1,
		SQL: 2
	},
	FDashboardChartSerieOrder: {
		LabelAsc: 0,
		LabelDesc: 1,
		ValueAsc: 2,
		ValueDesc: 3
	},
	FObjectPropertyEditorType: { InputText: 0, TextArea: 1, InputNumber: 2, InputDecimal: 3, Date: 4, Select: 5, Checkbox: 6, RadioButton: 7, DateTime: 8, Time: 9, GeoWKT: 10 },
	FormExecutableCodeType: { ServerSide: 0, ClientSide: 1 },
	FObjectRelationType: { OneToMany: 0, OneToOne: 1, ManyToMany: 2 },
	FExternalServiceType: { Rest: 0, Soap: 1 },
	FBProcessUserActionType: { OpenRecord: 0, SelectRecord: 1 },
	FBProcessUserActionOpenRecordType: { CreatedRecord: 0, AnyRecord: 1 },
	FScheduledTaskPeriodType: { Daily: 0, Weekly: 1, Monthly: 2 },
	WeekDay: { Sunday: 0, Monday: 1, Tuesday: 2, Wednesday: 3, Thursday: 4, Friday: 5, Saturday: 6 },
	Month: { January: 0, February: 1, March: 2, April: 3, May: 4, June: 5, July: 6, August: 7, September: 8, October: 9, November: 10, December: 11 },
	FScheduledTaskMonthlyRepeatType: { MonthDays: 0, WeekDays: 1 },
	FScheduledTaskMonthlyOnWeek: { First: 0, Second: 1, Third: 2, Fourth: 3, Last: 4 },
	FScheduledTaskStatus: { Active: 0, Passive: 1 },
	FScheduledTaskActionType: { BProcess: 0, ExecuteCode: 1 },
	FMapBaseLayers: { BingRoad: 0, BingAerialWithLabels: 1, BingAerialWithLabelsOnDemand: 2, BingCanvasDark: 3, OpenStreetMap: 4},
	QueryResultAggregateFunction: { ColumnValue: 0, Count: 1, CountDistinct: 2, Sum: 3, Maximum: 4, Minimum: 5, Average: 6 },
	FReportType: { Excel: 0, Html: 1, Html2Pdf: 2, Html2Excel: 3 },
	FBProcessElementType: {
		UserAction: 0,
		Notification: 1,
		CreateRecord: 2,
		ChangeState: 3,
		Start: 4,
		End: 5,
		Gateway: 6,
		UpdateRecord: 7,
		DeleteRecord: 8,
		SetParameter: 9,
		ExecuteFunction: 10
	},
	FOptionListSortType: { RowNumber: 0, DisplayName: 1 },
	FFormParentSourceParameterValueType: { Static: 0, PropertyValue: 1 },
	FTableColumnAggregateFunctionScope: { All: 0, Page: 1 }
}