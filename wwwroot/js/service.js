﻿class service {
    static getTimeOutLoginUrl() {
        var url = '/html/login.html';
        var redirectUrl = utils.getRootUrlForRedirectAfterLogin();
        if (redirectUrl)
            url += '?redirectUrl=' + encodeURIComponent(redirectUrl);
        return url;
    }
    static processSessionTimeOut() {
        if (!utils.getFSessionID()) {
            utils.redirectOnRoot(service.getTimeOutLoginUrl());
            return;
        }
        var content = '<div>' + lngHelper.get('generic.sessionTimeOut') + '</div>';
        var buttons = [{
            text: lngHelper.get('generic.ok'),
            class: 'btn-primary',
            onclick: function () {
                window.sessionTimeOutPopup = null;
                utils.redirectOnRoot(service.getTimeOutLoginUrl());
            }
        }];
        if (!window.sessionTimeOutPopup)
            window.sessionTimeOutPopup = utils.openPopup('danger', lngHelper.get('generic.error'), content, buttons);
    }
    static call(serviceName, method, data, noErrorAlert, noProgress, options) {
        return new Promise(function (resolve, reject) {
            var progressID = null;
            if (!noProgress && utils.progress)
                progressID = utils.progress('showSingle');
            var contentType = 'application/json';
            var dataType = 'json';
            if (options && options.contentType != 'undefined' && typeof (options.contentType) != 'undefined')
                contentType = options.contentType;
            if (options && options.dataType != 'undefined' && typeof (options.dataType) != 'undefined')
                dataType = options.dataType;
            method = method || 'GET';
            if (method.toUpperCase() == 'POST' && contentType == 'application/json' && (!options || !options.noStringify))
                data = JSON.stringify(data);
            var processData = true;
            if (options && options.processData != 'undefined' && typeof (options.processData) != 'undefined')
                processData = options.processData;
            $.ajax({
                'method': method,
                'url': '/' + serviceName,
                'data': data,
                'contentType': contentType,
                'dataType': dataType,
                'processData': processData,
                'success': function (result, status, xhr) {
                    if (!noProgress && progressID)
                        utils.progress('hide', progressID);
                    resolve(result);
                },
                'error': function (xhr, status, error) {
                    if (!noProgress && progressID)
                        utils.progress('hide', progressID);
                    if (utils && utils.openPopup && !noErrorAlert) {
                        switch (xhr.status) {
                            case 401:
                            case 403:
                                if (xhr.responseText && xhr.responseText.startsWith('##'))
                                    utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get(xhr.responseText.substring(2)));
                                else
                                    service.processSessionTimeOut();
                                break;
                            case 402:
                                utils.redirectOnRoot('/html/license.html');
                                break;
                            default:
                                if (xhr.status == 400 && xhr.responseJSON && xhr.responseJSON.detail == 'duplicateDisplayName') {
                                    utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.duplicateDisplayName'));
                                } else {
                                    var content = '<p>' + error.toString() + '</p>';
                                    content += '<div style="width:100%"><a onclick="var mDiv = this.parentNode.parentNode.querySelector(\'.error-details\'); mDiv.style.display = (mDiv.style.display == \'none\' ? \'block\' : \'none\')" style="cursor:pointer;color:#007bff">' + lngHelper.get('generic.errorDetails') + '</a></div>';
                                    if (xhr.responseText)
                                        content += '<div class="error-details" style="max-height:150px;overflow:scroll;font-size:10pt;width:100%;margin-top:20px;display:none;border:1px solid lightgray;padding:10px">' + xhr.responseText + '</div>';
                                    if (xhr.responseJSON && xhr.responseJSON.detail && xhr.responseJSON.detail.indexOf('##') == 0)
                                        content = lngHelper.get(xhr.responseJSON.detail.substring(2));
                                    utils.openPopup('danger', lngHelper.get('generic.error'), content);
                                }
                                break;
                        }
                    }
                    if (options && options.onError) {
                        options.onError(xhr, status, error);
                    }
                    reject(error);
                },
                'beforeSend': function (xhrObj) {
                    if (lngHelper && lngHelper.currentLanguage)
                        xhrObj.setRequestHeader("Client-Language", lngHelper.currentLanguage.name);
                }
            });
        })
    }
}
