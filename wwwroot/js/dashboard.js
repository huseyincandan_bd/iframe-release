﻿const FDashboardWidgetType = { Chart: 0, ObjectTable: 1, ExternalPage: 2, StaticContent: 3, Map: 4 };
const ChartType = { Bar: 0, Line: 1, Pie: 2, Area: 3, Doughnut: 4, Radar: 5, PolarArea: 6 };
const ChartLegendPosition = { None: 0, Top: 1, Right: 2, Bottom: 3, Left: 4 };
const ChartLegendAlign = { Start: 0, Center: 1, End: 2 };

function widgetContentObjectChange() {
    var fObjectID = $('#widgetContentObject').val();
    $('#widgetContentTable option').each((index, item) => {
        var item = $(item);
        var value = item.val();
        if (value == '0' || value.split('|')[0] == fObjectID)
            item.show();
        else
            item.hide();
    })
}
async function fillWidgetContentTypeOptions() {
    var objects = $('#widgetContentObject option');
    if (!objects || objects.length == 0) {
        objects = await service.call('fObject/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < objects.length; i++) {
            options.push({
                value: objects[i].recID,
                text: objects[i].displayName
            });
        }
        utils.fillSelectOptions($('#widgetContentObject'), options);
    }
    var tables = $('#widgetContentTable option');
    if (!tables || tables.length == 0) {
        var tables = await service.call('fTable/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < tables.length; i++) {
            options.push({
                value: tables[i].fObjectID + '|' + tables[i].recID,
                text: tables[i].displayName
            })
        }
        utils.fillSelectOptions($('#widgetContentTable'), options);
        $('#widgetContentTable option').hide();
        $('#widgetContentTable option[value=0]').show();
    }
}

function chartSerieQueryClick() {
    var serie = $('#chartDataSerie').data('serie');
    var fObjectID = $('#chartSerieObject').val();
    if (fObjectID) {
        queryHelper.getDataQuery(fObjectID, null, serie.query, null, function (query) {
            serie.query = query;
            chartDataSerieParamChanged();
        });
    }
}
function selectChartSerieColorPaletteClick() {
    $('#chartSerieColorPaletteSelect').modal('show');
}
function chartDataSerieParamChanged() {
    var widget = $('#modalEditWidget').data('widget');
    var serie = $('#chartDataSerie').data('serie');

    updateSerieProps(serie);

    widget = JSON.parse(JSON.stringify(widget));
    var bFound = false;
    for (var i = 0; i < widget.chart.series.length; i++) {
        if (widget.chart.series[i].id == serie.id) {
            widget.chart.series[i] = serie;
            bFound = true;
            break;
        }
    }
    if (!bFound)
        widget.chart.series.push(serie);

    drawChartWidget(widget);
}
async function drawChartWidget(widget) {
    if (!widget.chart || !widget.chart.series || widget.chart.series.length == 0)
        return;

    var elem = $('.grid-stack .grid-stack-item[widget-index=' + widget.index + '] .card-body');
    elem.html('<div class="spinner-border text-primary" role="status"><span class="sr-only">Loading...</span></div>');

    var yAxes = [{
        type: 'linear',
        display: true,
        id: 'left',
        position: 'left',
        ticks: {
            callback: function (value, index, values) {
                return value.toLocaleString(lngHelper.currentLanguage.name);
            }
        }
    }]

    var datasets = [];
    for (var i = 0; i < widget.chart.series.length; i++) {
        var serie = widget.chart.series[i];
        if (!serie.labelColumnFObjectPropertyID || !serie.valueColumnFObjectPropertyID)
            continue;
        if (serie.yAxis == 'right') {
            if (yAxes.length == 1) {
                yAxes.push({
                    type: 'linear',
                    display: true,
                    id: 'right',
                    position: 'right',
                    ticks: {
                        callback: function (value, index, values) {
                            return value.toLocaleString(lngHelper.currentLanguage.name);
                        }
                    }
                })
            }
        }
        if (serie.labelColumnFObjectPropertyID == '0' || serie.valueColumnFObjectPropertyID == '0')
            return;
        var dataQuery = serie.query ? serie.query : {};
        dataQuery.resultColumns = [{
            fObjectPropertyRecID: serie.labelColumnFObjectPropertyID,
            aggregateFunction: 0
        }, {
            fObjectPropertyRecID: serie.valueColumnFObjectPropertyID,
            aggregateFunction: serie.aggregateFunction
        }];
        if (!dataQuery.resultColumns[0].fObjectPropertyRecID || dataQuery.resultColumns[0].fObjectPropertyRecID == utils.emptyGuid) { //etiket yoksa grafik yok  
            elem.html('<div style="width:100%;height:100%;display:flex;align-items:center;justify-content:center">' + lngHelper.get('dashboard.noLabelColumn') + '</div>');
            return;
        }
        if (dataQuery.resultColumns[0].fObjectPropertyRecID == dataQuery.resultColumns[1].fObjectPropertyRecID) {//etiket ve seri aynı ise grafik çizmiyoruz, bunu düzeltebiliriz. 
            elem.html('<div style="width:100%;height:100%;display:flex;align-items:center;justify-content:center">' + lngHelper.get('dashboard.labelAndValueAreSame') + '</div>');
            return;
        }
        switch (serie.order) {
            case window.fEnums.FDashboardChartSerieOrder.LabelAsc:
                dataQuery.orders = [{
                    name: serie.labelColumnFObjectPropertyID,
                    direction: 'asc'
                }];
                break;
            case window.fEnums.FDashboardChartSerieOrder.LabelDesc:
                dataQuery.orders = [{
                    name: serie.labelColumnFObjectPropertyID,
                    direction: 'desc'
                }];
                break;
            case window.fEnums.FDashboardChartSerieOrder.ValueAsc:
                dataQuery.orders = [{
                    name: serie.valueColumnFObjectPropertyID,
                    direction: 'asc'
                }];
                break;
            case window.fEnums.FDashboardChartSerieOrder.ValueDesc:
                dataQuery.orders = [{
                    name: serie.valueColumnFObjectPropertyID,
                    direction: 'asc'
                }];
                break;
        }
        var result = await service.call('fObjectData/query?fObjectID=' + serie.fObjectID.toString(), 'POST', dataQuery, false, true);
        var labels = [];
        var data = [];
        if (result.length > 0) {
            var resultColumns = [];
            for (const [key, value] of Object.entries(result[0])) {
                resultColumns.push(key);
            }
            for (var j = 0; j < result.length; j++) {
                var label = result[j][resultColumns[0]];
                if (label.hasOwnProperty('ticks')) {
                    var date = new Date(label.ticks);
                    label = date.toLocaleTimeString();
                }
                if (serie.labelColumnFObjectPropertyEditorType == window.fEnums.FObjectPropertyEditorType.DateTime ||
                    serie.labelColumnFObjectPropertyEditorType == window.fEnums.FObjectPropertyEditorType.Time) {
                    label = window.lngHelper.formatDateTime(label, 'dateTime');
                }
                labels.push(label);
                var value = result[j][resultColumns[1]];
                if (serie.valueColumnFObjectPropertyEditorType == window.fEnums.FObjectPropertyEditorType.DateTime ||
                    serie.valueColumnFObjectPropertyEditorType == window.fEnums.FObjectPropertyEditorType.Time) {
                    value = window.lngHelper.formatDateTime(value, 'dateTime');
                }
                data.push(value);
            }
        }
        var dataset = {
            type: 'bar',
            label: serie.displayName,
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: data,
            fill: false,
            yAxisID: (serie.yAxis ? serie.yAxis : 'left')
        };
        if (serie.colorPalette && window.chartColorPalettes[serie.colorPalette - 1]) {
            dataset.backgroundColor = window.chartColorPalettes[serie.colorPalette - 1];
            dataset.borderColor = window.chartColorPalettes[serie.colorPalette - 1];
        }

        datasets.push(dataset);
        for (const [key, value] of Object.entries(ChartType)) {
            if (value == serie.type) {
                switch (value) {
                    case ChartType.Bar:
                        dataset.type = 'bar';
                        dataset.backgroundColor = dataset.backgroundColor[0];
                        dataset.borderColor = dataset.borderColor[0];
                        break;
                    case ChartType.Line:
                        dataset.type = 'line';
                        dataset.borderColor = dataset.borderColor[0];
                        dataset.fill = false;
                        break;
                    case ChartType.Pie:
                        dataset.type = 'pie';
                        break;
                    case ChartType.Area:
                        dataset.type = 'line';
                        dataset.borderColor = dataset.borderColor[0];
                        dataset.fill = true;
                        break;
                    case ChartType.Radar:
                        dataset.type = 'radar';
                        break;
                    case ChartType.Doughnut:
                        dataset.type = 'doughnut';
                        break;
                    case ChartType.PolarArea:
                        dataset.type = 'polarArea';
                        break;
                }
                break;
            }
        }
    }
    var options = {
        responsive: true,
        tooltips: {
            callbacks: {
                label: function (tooltipItems, data) {
                    return data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index].toLocaleString(lngHelper.currentLanguage.name);
                },
                title: function (tooltipItems, data) {
                    if (tooltipItems.length == 1) {
                        return data.labels[tooltipItems[0].index];
                    }
                }
            }
        },
        scales: {
            yAxes: yAxes
        },
        legend: {
            display: widget.chart.legendPosition != ChartLegendPosition.None
        },
        layout: {
            padding: { left: 15, top: 15, right: 15, bottom: 15 }
        }
    }
    if (serie.type == ChartType.Pie || serie.type == ChartType.Doughnut)
        delete options.scales;
    switch (widget.chart.legendPosition) {
        case ChartLegendPosition.Top:
            options.legend.position = 'top';
            break;
        case ChartLegendPosition.Left:
            options.legend.position = 'left';
            break;
        case ChartLegendPosition.Right:
            options.legend.position = 'right';
            break;
        case ChartLegendPosition.Bottom:
            options.legend.position = 'bottom';
            break;
    }
    switch (widget.chart.legendAlign) {
        case ChartLegendAlign.Center:
            options.legend.align = 'center';
            break;
        case ChartLegendAlign.Start:
            options.legend.align = 'start';
            break;
        case ChartLegendAlign.End:
            options.legend.align = 'end';
            break;
    }

    elem.html('<canvas style="width:100%;height:100%;"></canvas>');
    if (datasets.length > 0) {
        var ctx = elem.find('canvas')[0].getContext('2d');
        var chart = new Chart(ctx, {
            type: datasets[0].type,
            data: {
                labels: labels,
                datasets: datasets
            },
            options: options
        });
    }
}
function fillChartDataSerieTable() {
    var widget = $('#modalEditWidget').data('widget');

    var dataSet = [];
    if (widget && widget.chart) {
        var series = widget.chart.series;
        for (var i = 0; i < series.length; i++) {
            dataSet.push([series[i].id, series[i].fObjectDisplayName, series[i].labelColumnFObjectPropertyDisplayName, series[i].valueColumnFObjectPropertyDisplayName]);
        }
    }
    window.chartDataSerieDT.dtObject.clear();
    window.chartDataSerieDT.dtObject.rows.add(dataSet);
    window.chartDataSerieDT.dtObject.draw();
}
function updateSerieProps(serie) {
    serie.fObjectID = $('#chartSerieObject').val();
    serie.fObjectDisplayName = $('#chartSerieObject option:selected').text();
    serie.labelColumnFObjectPropertyID = $('#chartSerieLabelColumn').val();
    serie.labelColumnFObjectPropertyDisplayName = $('#chartSerieLabelColumn option:selected').text();
    var editorType = $('#chartSerieLabelColumn option:selected').attr('editorType');
    if (editorType)
        serie.labelColumnFObjectPropertyEditorType = parseInt(editorType);
    serie.displayName = $('#chartSerieDisplayName').val();
    serie.type = parseInt($('#chartSerieChartType').val());
    serie.yAxis = $('#chartSerieYAxis').val();
    serie.aggregateFunction = parseInt($('#chartSerieValueAggregateFunction').val());
    serie.valueColumnFObjectPropertyID = $('#chartSerieValueColumn').val();
    serie.valueColumnFObjectPropertyDisplayName = $('#chartSerieValueColumn option:selected').text();
    if (serie.displayName == '')
        serie.displayName = serie.valueColumnFObjectPropertyDisplayName;
    var editorType = $('#chartSerieValueColumn option:selected').attr('editorType');
    if (editorType)
        serie.valueColumnFObjectPropertyEditorType = parseInt(editorType);
    serie.colorPalette = $('#chartSerieColorPalette').val() ? parseInt($('#chartSerieColorPalette').val()) : 1;
    serie.order = parseInt($('#chartSerieOrder').val());
}
function btnSaveChartDataSerieClick() {
    var serie = $('#chartDataSerie').data('serie');
    var widget = $('#modalEditWidget').data('widget');

    if (!widget) {
        widget = {
            chart: {
                series: []
            }
        };
    }
    updateSerieProps(serie);
    var bFound = false;
    for (var i = 0; i < widget.chart.series.length; i++) {
        if (widget.chart.series[i].id == serie.id) {
            widget.chart.series[i] = serie;
            bFound = true;
            break;
        }
    }
    if (!bFound)
        widget.chart.series.push(serie);
    fillChartDataSerieTable();
    $('#chartDataSerie').modal('hide');
}
async function getChartDataSerieObjectList() {
    if ($('#chartSerieObject option').length == 0) {
        var objects = await service.call('fObject/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < objects.length; i++) {
            options.push({
                text: objects[i].displayName,
                value: objects[i].recID
            })
        }
        utils.fillSelectOptions($('#chartSerieObject'), options);
    }
}
async function btnAddChartDataSerieClick() {
    var widget = $('#modalEditWidget').data('widget');
    await getChartDataSerieObjectList();
    $('#chartSerieDisplayName').val('');
    var serie = {};
    serie.id = 1;
    for (var i = 0; i < widget.chart.series.length; i++) {
        if (widget.chart.series[i].id >= serie.id)
            serie.id = widget.chart.series[i].id + 1;
    }
    $('#chartDataSerie').data('serie', serie);
    $('#chartDataSerie').modal('show');
}
async function btnEditChartDataSerieClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    await getChartDataSerieObjectList();
    var widget = $('#modalEditWidget').data('widget');
    var serie = null;
    for (var i = 0; i < widget.chart.series.length; i++) {
        if (widget.chart.series[i].id == row.data()[0]) {
            serie = widget.chart.series[i];
            break;
        }
    }
    serie = JSON.parse(JSON.stringify(serie));
    $('#chartDataSerie').data('serie', serie);
    $('#chartSerieObject').val(serie.fObjectID);
    try { await chartSerieObjectChange(); }
    catch { }  //TODO : try-catch kaldırılacak..

    $('#chartSerieLabelColumn').val(serie.labelColumnFObjectPropertyID);
    $('#chartSerieValueColumn').val(serie.valueColumnFObjectPropertyID);
    $('#chartSerieValueAggregateFunction').val(serie.aggregateFunction);
    $('#chartSerieDisplayName').val(serie.displayName);
    $('#chartSerieChartType').val(serie.type);
    $('#chartSerieYAxis').val(serie.yAxis);
    $('#chartSerieColorPalette').val(serie.colorPalette);
    $('#chartSerieOrder').val(serie.order);
    $('#chartDataSerie').modal('show');
}
function btnDeleteChartDataSerieClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    utils.confirmDelete(() => {
        var widget = $('#modalEditWidget').data('widget');
        for (var i = 0; i < widget.chart.series.length; i++) {
            if (widget.chart.series[i].id == row.data()[0]) {
                widget.chart.series.splice(i, 1);
                break;
            }
        }
        drawChartWidget(widget);
        fillChartDataSerieTable();
    })
}
async function chartSerieObjectChange(clearQuery) {
    if (clearQuery) {
        var serie = $('#chartDataSerie').data('serie');
        if (serie)
            serie.query = null;
    }

    var fObjectID = $('#chartSerieObject').val();
    var fPropertyList = await service.call('fObjectProperty/getList', 'GET', { fObjectID: fObjectID });
    var fObjectRelationList = await service.call('fObject/getRelations', 'GET', { fObjectID: fObjectID });
    var parentObjectList = [];
    for (var i = 0; i < fObjectRelationList.length; i++) {
        if (fObjectRelationList[i].childFObjectID == fObjectID) {
            parentObjectList.push(fObjectRelationList[i].parentFObjectID);
        }
    }
    if (parentObjectList.length == 0) {
        var options = [];
        for (var i = 0; i < fPropertyList.length; i++) {
            options.push({
                value: fPropertyList[i].recID,
                text: fPropertyList[i].displayName,
                attributes: {
                    editorType: fPropertyList[i].editorType
                }
            })
        }
        utils.fillSelectOptions2($('#chartSerieLabelColumn'), options);
        utils.fillSelectOptions2($('#chartSerieValueColumn'), options);
    } else {
        var fObject = await service.call('fObject/get', 'GET', { recID: fObjectID });
        var optionGroups = [];
        var options = [];
        for (var i = 0; i < fPropertyList.length; i++) {
            options.push({
                value: fPropertyList[i].recID,
                text: fPropertyList[i].displayName,
                attributes: {
                    editorType: fPropertyList[i].editorType
                }
            })
        }
        optionGroups.push({
            label: fObject.displayName,
            options: options
        });
        for (var i = 0; i < parentObjectList.length; i++) {
            var fPropertyList = await service.call('fObjectProperty/getList', 'GET', { fObjectID: parentObjectList[i] });
            var fObject = await service.call('fObject/get', 'GET', { recID: parentObjectList[i] });
            var options = [];
            for (var j = 0; j < fPropertyList.length; j++) {
                options.push({
                    value: fPropertyList[j].recID,
                    text: fPropertyList[j].displayName,
                    attributes: {
                        editorType: fPropertyList[i].editorType
                    }
                })
            }
            optionGroups.push({
                label: fObject.displayName,
                options: options
            });
        }
        utils.fillSelectOptionGroups($('#chartSerieLabelColumn'), optionGroups);
        utils.fillSelectOptionGroups($('#chartSerieValueColumn'), optionGroups);
    }
}
function btnEditModeOpenClick() {
    $('.edit-mode-opener').hide();
    $('.edit-mode-container').css('display', 'flex');
    $('.grid-stack .card-header button').css('display', 'inline-block');
    window.grid.enableResize(true);
    window.grid.enableMove(true);
}
function btnEditModeCloseClick() {
    $('.edit-mode-opener').show();
    $('.edit-mode-container').css('display', 'none');
    $('.grid-stack .card-header button').css('display', 'none');
    window.grid.enableResize(false);
    window.grid.enableMove(false);
}
function btnDeleteDashboardClick() {
    if (window.currentDashboard.recID == 0) {
        utils.alertWarning(lngHelper.get('dashboard.notSavedYet'));
        return;
    }
    utils.confirmDelete(async () => {
        var result = await service.call('fDashboard/delete', 'GET', { recID: window.currentDashboard.recID });
        $('body > div').hide();
        $('body > nav').hide();
        if (parent.initDashboardMenu) {
            parent.initDashboardMenu();
        }
        utils.alertWarning(lngHelper.get('generic.deleted'));
    })
}
function widgetTypeChange(e) {
    var sender = utils.getEventSender(e);

    $('.edit-widget-internal-page').hide();
    $('.edit-widget-external-page').hide();
    $('.edit-widget-static-content').hide();
    $('.edit-widget-chart').hide();
    $('.edit-widget-map').hide();
    $('#widgetContentObject').parents('.form-group').hide();
    $('#widgetContentTable').parents('.form-group').hide();
    switch (parseInt($('#widgetType').val())) {
        case FDashboardWidgetType.Chart:
            $('.edit-widget-chart').show();
            break;
        case FDashboardWidgetType.ObjectTable:
            //$('.edit-widget-internal-page').show();
            $('#widgetContentObject').parents('.form-group').show();
            $('#widgetContentTable').parents('.form-group').show();
            break;
        case FDashboardWidgetType.ExternalPage:
            $('.edit-widget-external-page').show();
            break;
        case FDashboardWidgetType.StaticContent:
            $('.edit-widget-static-content').show();
            break;
        case FDashboardWidgetType.Map:
            $('.edit-widget-map').show();
            break;
    }
}
function btnSaveDashboardClick() {
    $('#dashboardDisplayName').val(window.currentDashboard.displayName);
    $('#dashboardRecorder').val(window.currentDashboard.recOrder);
    $('#modalSaveDashboard').modal('show');
}
async function btnSaveDashboardOKClick() {
    var gridSaveResult = window.grid.save(false);
    for (var i = 0; i < gridSaveResult.length; i++)
        delete gridSaveResult[i].content;

    for (var i = 0; i < window.currentContent.widgets.length; i++) {
        for (var j = 0; j < gridSaveResult.length; j++) {
            if (window.currentContent.widgets[i].index == gridSaveResult[j].id) {
                window.currentContent.widgets[i].props = gridSaveResult[j];
            }
        }
    }
    window.currentDashboard.displayName = $('#dashboardDisplayName').val();
    window.currentDashboard.recOrder = parseInt($('#dashboardRecorder').val());
    window.currentDashboard.content = JSON.stringify(window.currentContent);
    if ($('#dashboardCopy').is(':checked'))
        window.currentDashboard.recID = utils.emptyGuid;
    var result = await service.call('fDashboard/save', 'POST', window.currentDashboard);
    if (result) {
        window.currentDashboard = result;
        if (parent.initDashboardMenu) {
            parent.initDashboardMenu();
        }
        utils.showToast('success', lngHelper.get('generic.saved'));
        $('#modalSaveDashboard').modal('hide');
    }
}
function btnAddWidgetClick() {
    var newIndex = 1;
    for (var i = 0; i < window.currentContent.widgets.length; i++) {
        if (window.currentContent.widgets[i].index >= newIndex)
            newIndex = window.currentContent.widgets[i].index + 1;
    }
    widget = {
        index: newIndex,
        type: FDashboardWidgetType.Chart,
        chart: {
            series: [],
            legendPosition: ChartLegendPosition.Top,
            legendAlign: ChartLegendAlign.Center
        }
    }
    var elem = $(addWidgetDomElement(widget.index));
    window.currentContent.widgets.push(widget);
    elem.find('.card-header button').css('display', 'inline-block');

    $('#chartSerieObject').val('0');
    $('#chartSerieLabelColumn').val(utils.emptyGuid);
    $('#chartSerieValueAggregateFunction').val('0');
    $('#chartSerieValueColumn').val(utils.emptyGuid);
    $('#chartSerieDisplayName').val('');
    $('#chartSerieChartType').val('0');
    $('#chartSerieYAxis').val('left');
    $('#chartSerieColorPalette').val(1);
    $('#chartSerieOrder').val(window.fEnums.FDashboardChartSerieOrder.LabelAsc);

    widgetEditClick(null, elem.find('.card-header button').first());
}
async function widgetEditClick(e, elem) {
    var sender;
    if (elem)
        sender = elem;
    else
        sender = utils.getEventSender(e);
    var widgetIndex = parseInt(sender.closest('.grid-stack-item').attr('widget-index'));
    var widget = null;
    for (var i = 0; i < window.currentContent.widgets.length; i++) {
        if (window.currentContent.widgets[i].index == widgetIndex) {
            widget = window.currentContent.widgets[i];
            break;
        }
    }
    if (widget != null) {
        $('#displayName').val(widget.displayName);
        $('#widgetType').val(widget.type);
        widgetTypeChange();
        $('#widgetContentInternalUrl').val(widget.url);
        $('#widgetContentExternalUrl').val(widget.url);
        $('#widgetContentStaticContent').val(widget.staticContent);
        $('#modalEditWidget').data('widget', widget);
        fillChartDataSerieTable();
        if (widget.chart) {
            $('#chartLegendPosition').val(widget.chart.legendPosition);
            $('#chartLegendAlign').val(widget.chart.legendAlign);
        }
        await fillWidgetContentTypeOptions();
        if (widget.type == FDashboardWidgetType.ObjectTable) {
            if (widget.fProps && !$.isEmptyObject(widget.fProps)) {
                $('#widgetContentObject').val(widget.fProps.targetObject);
                widgetContentObjectChange();
                $('#widgetContentTable').val(widget.fProps.targetTable);
            } else {
                $('#widgetContentObject').val('0');
                widgetContentObjectChange();
                $('#widgetContentTable').val('');
            }
        }
        if (widget.type == FDashboardWidgetType.Map) {
            $('#widgetMapBaseLayers').html('');
            $('#widgetMapDataLayers').html('');
            $('#widgetMapCenterLatitude').val(0);
            $('#widgetMapCenterLongitude').val(0);
            $('#widgetMapZoom').val(10);
            if (widget.fProps && !$.isEmptyObject(widget.fProps)) {
                if (widget.fProps.baseLayers) {
                    for (var i = 0; i < widget.fProps.baseLayers.length; i++) {
                        var allOptions = $('#baseMaps').data('allOptions');
                        for (var j = 0; j < allOptions.length; j++) {
                            if (allOptions[j].value == widget.fProps.baseLayers[i]) {
                                addLayerItem($('#widgetMapBaseLayers'), widget.fProps.baseLayers[i], allOptions[j].text);
                                break;
                            }
                        }
                    }
                }
                if (widget.fProps.dataLayers) {
                    var allDataMaps = $('#dataMaps').data('allOptions');
                    for (var i = 0; i < widget.fProps.dataLayers.length; i++) {
                        for (var j = 0; j < allDataMaps.length; j++) {
                            if (allDataMaps[j].value == widget.fProps.dataLayers[i].layerName) {
                                addLayerItem($('#widgetMapDataLayers'), allDataMaps[j].value, allDataMaps[j].text, widget.fProps.dataLayers[i].query);
                                break;
                            }
                        }
                    }
                }
                if (widget.fProps.center) {
                    $('#widgetMapCenterLatitude').val(widget.fProps.center[0]);
                    $('#widgetMapCenterLongitude').val(widget.fProps.center[1]);
                    $('#widgetMapZoom').val(widget.fProps.zoom);
                }
            }
            updateMapSelect($('#baseMaps'));
            updateMapSelect($('#dataMaps'));
        }
        $('#modalEditWidget').modal('show');
    }
}
function widgetDeleteClick(e) {
    var sender = utils.getEventSender(e);
    var el = sender.closest('.grid-stack-item');
    window.grid.removeWidget(el[0]);
    var widgetIndex = parseInt(el.attr('widget-index'));
    for (var i = 0; window.currentContent.widgets.length; i++) {
        if (window.currentContent.widgets[i].index == widgetIndex) {
            window.currentContent.widgets.splice(i, 1);
            break;
        }
    }
}
function btnSaveWidgetClick() {
    var widget = $('#modalEditWidget').data('widget');

    if (widget == null) {
        var newIndex = 1;
        for (var i = 0; i < window.currentContent.widgets.length; i++) {
            if (window.currentContent.widgets[i].index >= newIndex)
                newIndex = window.currentContent.widgets[i].index + 1;
        }
        widget = {
            index: newIndex
        }
        var elem = $(addWidgetDomElement(widget.index));
        window.currentContent.widgets.push(widget);
    } else {
    }
    widget.displayName = $('#displayName').val();
    widget.type = parseInt($('#widgetType').val());
    widget.url = '';
    widget.staticContent = '';
    switch (widget.type) {
        case FDashboardWidgetType.ObjectTable:
            var parts = $('#widgetContentTable').val().split('|');
            var value = parts[parts.length - 1];
            var url = 'tableDataList.html?tableID=' + value;
            widget.fProps = {
                targetObject: $('#widgetContentObject').val(),
                targetTable: $('#widgetContentTable').val()
            }
            $('#widgetContentInternalUrl').val(url);
            widget.url = $('#widgetContentInternalUrl').val();
            break;
        case FDashboardWidgetType.ExternalPage:
            widget.url = $('#widgetContentExternalUrl').val();
            break;
        case FDashboardWidgetType.StaticContent:
            widget.staticContent = $('#widgetContentStaticContent').val();
            break;
        case FDashboardWidgetType.Map:
            var baseLayers = [];
            $('#widgetMapBaseLayers .list-item').each((index, item) => {
                baseLayers.push(parseInt($(item).attr('data-id')));
            })
            var dataLayers = [];
            $('#widgetMapDataLayers .list-item').each((index, item) => {
                dataLayers.push({
                    layerName: $(item).attr('data-id'),
                    query : $(item).data('query')
                });
            })
            widget.fProps = {
                baseLayers: baseLayers,
                dataLayers: dataLayers,
                center: [$('#widgetMapCenterLatitude').val() ? parseFloat($('#widgetMapCenterLatitude').val()) : 0, $('#widgetMapCenterLongitude').val() ? parseFloat($('#widgetMapCenterLongitude').val()) : 0],
                zoom: parseInt($('#widgetMapZoom').val())
            }
            break;
    }
    if (widget.chart) {
        widget.chart.legendPosition = parseInt($('#chartLegendPosition').val());
        widget.chart.legendAlign = parseInt($('#chartLegendAlign').val());
    }
    var elem = updateWidgetDomElement(widget);
    elem.find('.card-header button').css('display', 'inline-block');
    $('#modalEditWidget').modal('hide');
}
function updateWidgetDomElement(widget) {
    var elem = $('.grid-stack .grid-stack-item[widget-index=' + widget.index + ']');
    elem.find('.card-header .widget-title').html(widget.displayName);
    switch (widget.type) {
        case FDashboardWidgetType.ObjectTable:
        case FDashboardWidgetType.ExternalPage:
            var html = '<iframe src="' + widget.url + '"></iframe>';
            elem.find('.card-body').html(html);
            break;
        case FDashboardWidgetType.StaticContent:
            elem.find('.card-body').html(widget.staticContent);
            break;
        case FDashboardWidgetType.Chart:
            drawChartWidget(widget);
            break;
        case FDashboardWidgetType.Map:
            showMap(widget);
            break;
    }
    return elem;
}
function addWidgetDomElement(index, options) {
    var widgetContent = '<div class="card">\
                                  <div class="card-header d-flex justify-content-between align-items-center"><span class="widget-title"></span><span><button type="button" class="btn btn-sm" onclick="widgetEditClick()"><i class="fas fa-pencil-alt"></i></button><button type="button" class="btn btn-sm" onclick="widgetDeleteClick()"><i class="fas fa-times"></i></button></span></div>\
                                  <div class="card-body"></div>\
                            </div>';
    options = Object.assign({
        content: widgetContent,
        w: 4,
        h: 4,
        id: index
    }, options);
    var panel = window.grid.addWidget(options);
    $(panel).attr('widget-index', index.toString());
    return panel;
}
function btnAuthorizationClick() {
    utils.openPage('authorization.html?category=dashboard&authObjectRecID=' + window.currentDashboard.recID);
}
function chartSerieColorPaletteItemClick(value) {
    $('#chartSerieColorPalette').val(value);
    chartDataSerieParamChanged();
    $('#chartSerieColorPaletteSelect').modal('hide');
}
function initChartColorPalettes() {
    window.chartColorPalettes = [];
    window.chartColorPalettes.push(['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c', '#98df8a', '#d62728', '#ff9896', '#9467bd', '#c5b0d5', '#8c564b', '#c49c94', '#e377c2', '#f7b6d2', '#7f7f7f', '#c7c7c7', '#bcbd22', '#dbdb8d', '#17becf', '#9edae5']);
    window.chartColorPalettes.push(['#aec7e8', '#ffbb78', '#98df8a', '#ff9896', '#c5b0d5', '#c49c94', '#f7b6d2', '#c7c7c7', '#dbdb8d', '#9edae5']);
    window.chartColorPalettes.push(['#b10318', '#dba13a', '#309343', '#d82526', '#ffc156', '#69b764', '#f26c64', '#ffdd71', '#9fcd99']);
    window.chartColorPalettes.push(['#7b66d2', '#a699e8', '#dc5fbd', '#ffc0da', '#5f5a41', '#b4b19b', '#995688', '#d898ba', '#ab6ad5', '#d098ee', '#8b7c6e', '#dbd4c5']);
    window.chartColorPalettes.push(['#32a251', '#acd98d', '#ff7f0f', '#ffb977', '#3cb7cc', '#98d9e4', '#b85a0d', '#ffd94a', '#39737c', '#86b4a9', '#82853b', '#ccc94d']);
    window.chartColorPalettes.push(['#2c69b0', '#b5c8e2', '#f02720', '#ffb6b0', '#ac613c', '#e9c39b', '#6ba3d6', '#b5dffd', '#ac8763', '#ddc9b4', '#bd0a36', '#f4737a']);
    window.chartColorPalettes.push(['#1f83b4', '#12a2a8', '#2ca030', '#78a641', '#bcbd22', '#ffbf50', '#ffaa0e', '#ff7f0e', '#d63a3a', '#c7519c', '#ba43b4', '#8a60b0', '#6f63bb']);
    window.chartColorPalettes.push(['#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5', '#d9d9d9', '#bc80bd', '#ccebc5', '#ffed6f']);
    window.chartColorPalettes.push(['#9999ff', '#993366', '#ffffcc', '#ccffff', '#660066', '#ff8080', '#0066cc', '#ccccff', '#000080', '#ff00ff', '#ffff00', '#0000ff', '#800080', '#800000', '#008080', '#0000ff']);
    window.chartColorPalettes.push(['#a50026', '#d73027', '#f46d43', '#fdae61', '#fee090', '#ffffbf', '#e0f3f8', '#abd9e9', '#74add1', '#4575b4', '#313695']);
    window.chartColorPalettes.push(['#a6cee3', '#1f78b4', '#b2df8a', '#33a02c', '#fb9a99', '#e31a1c', '#fdbf6f', '#ff7f00', '#cab2d6', '#6a3d9a', '#ffff99', '#b15928']);
    window.chartColorPalettes.push(['#b9ddf1', '#afd6ed', '#a5cfe9', '#9bc7e4', '#92c0df', '#89b8da', '#80b0d5', '#79aacf', '#72a3c9', '#6a9bc3', '#6394be', '#5b8cb8', '#5485b2', '#4e7fac', '#4878a6', '#437a9f', '#3d6a98', '#376491', '#305d8a', '#2a5783']);
    window.chartColorPalettes.push(['#ffc685', '#fcbe75', '#f9b665', '#f7ae54', '#f5a645', '#f59c3c', '#f49234', '#f2882d', '#f07e27', '#ee7422', '#e96b20', '#e36420', '#db5e20', '#d25921', '#ca5422', '#c14f22', '#b84b23', '#af4623', '#a64122', '#9e3d22']);
    window.chartColorPalettes.push(['#b3e0a6', '#a5db96', '#98d687', '#8ed07f', '#85ca77', '#7dc370', '#75bc69', '#6eb663', '#67af5c', '#61a956', '#59a253', '#519c51', '#49964f', '#428f4d', '#398949', '#308344', '#2b7c40', '#27763d', '#256f3d', '#24693d']);
    window.chartColorPalettes.push(['#ffbeb2', '#feb4a6', '#fdab9b', '#fca290', '#fb9984', '#fa8f79', '#f9856e', '#f77b66', '#f5715d', '#f36754', '#f05c4d', '#ec5049', '#e74545', '#e13b42', '#da323f', '#d3293d', '#ca223c', '#c11a3b', '#b8163a', '#ae123a']);
    window.chartColorPalettes.push(['#eec9e5', '#eac1df', '#e6b9d9', '#e0b2d2', '#daabcb', '#d5a4c4', '#cf9dbe', '#ca96b8', '#c48fb2', '#be89ac', '#b882a6', '#b27ba1', '#aa759d', '#a27099', '#9a6a96', '#926591', '#8c5f86', '#865986', '#81537f', '#7c4d79']);
    window.chartColorPalettes.push(['#eedbbd', '#ecd2ad', '#ebc994', '#eac085', '#e8b777', '#e5ae6c', '#e2a562', '#de9d5a', '#d99455', '#d38c54', '#ce8451', '#c9784d', '#c47247', '#c16941', '#bd6036', '#b85636', '#b34d34', '#ad4433', '#a63d32', '#9f3632']);
    window.chartColorPalettes.push(['#dcd4d0', '#d4ccc8', '#cdc4c0', '#c5bdb9', '#beb6b2', '#b7afab', '#b0a7a4', '#a9a09d', '#a29996', '#9b938f', '#948c88', '#8d8481', '#867e7b', '#807774', '#79706e', '#736967', '#6c6260', '#665c51', '#5f5654', '#59504e']);
    window.chartColorPalettes.push(['#bce4d8', '#aedcd5', '#a1d5d2', '#95cecf', '#89c8cc', '#7ec1ca', '#72bac6', '#66b2c2', '#59acbe', '#4ba5ba', '#419eb6', '#3b96b2', '#358ead', '#3586a7', '#347ea1', '#32779b', '#316f96', '#2f6790', '#2d608a', '#2c5985']);
    window.chartColorPalettes.push(['#f4d166', '#f6c760', '#f8bc58', '#f8b252', '#f7a84a', '#f69e41', '#f49538', '#f38b2f', '#f28026', '#f0751e', '#eb6c1c', '#e4641e', '#de5d1f', '#d75521', '#cf4f22', '#c64a22', '#bc4623', '#b24223', '#a83e24', '#9e3a26']);
    window.chartColorPalettes.push(['#f5cac7', '#fbb3ab', '#fd9c8f', '#fe8b7a', '#fd7864', '#f46b55', '#ea5e45', '#e04e35', '#d43e25', '#c92b14', '#bd1100']);
    $('#chartSerieColorPalette').attr('max', window.chartColorPalettes.length);
    $('#chartSerieColorPalette').val(1);
    var html = '';
    for (var i = 0; i < window.chartColorPalettes.length; i++) {
        html += '<li onclick="chartSerieColorPaletteItemClick(' + (i + 1).toString() + ')">';
        var palette = window.chartColorPalettes[i];
        for (var j = 0; j < Math.min(palette.length, 10); j++) {
            html += '<div style="background-color:' + palette[j] + '"></div>';
        }
        html += '</li>';
    }
    $('#chartSerieColorPaletteSelect .modal-body ul').html(html);
}
async function init() {
    initWidgetMapProps();

    var recID = utils.getUrlParam('recID');
    if (recID) {
        window.currentDashboard = await service.call('fDashboard/get', 'GET', { recID: recID });
        window.currentContent = JSON.parse(window.currentDashboard.content);
        var sessionInfo = await service.call('fSession/getSessionInfo', 'GET', {});
        if (sessionInfo.isSystemAdmin) {
            $('#btnEditModeOpenContainer').show();
        } else {
            var authorization = await service.call('fAuthorization/getListByUser', 'GET', { authObject: 40, authObjectRecID: recID });
            if (authorization && authorization.length > 0) {
                for (var i = 0; i < authorization.length; i++) {
                    if (authorization[i].authStatus) {
                        $('#btnEditModeOpenContainer').show();
                        break;
                    }
                }
            }
        }
    } else {
        $('#btnEditModeOpenContainer').show();
        window.currentDashboard = {
            recID: utils.emptyGuid,
            displayName: lngHelper.get('dashboard.newDashboard'),
            recOrder: 1
        }
        window.currentContent = {
            widgets: []
        };
    }
    $('#dashboardDisplayNameTitle').html(window.currentDashboard.displayName);
    var options = [];
    for (const [key, value] of Object.entries(FDashboardWidgetType)) {
        options.push({
            value: value,
            text: lngHelper.get('dashboard.widgetType_' + key)
        })
    }
    utils.fillSelectOptions($('#widgetType'), options, true);
    widgetTypeChange();
    window.grid = GridStack.init({
        margin: 4,
        disableResize: true,
        disableDrag: true
    });
    window.grid.on('resizestart', function (event, el) {
        $('.grid-stack .card-body').hide();
    });
    window.grid.on('resizestop', function (event, el) {
        $('.grid-stack .card-body').show();
        var widgetIndex = parseInt($(el).attr('widget-index'));
        var widget = null;
        for (var i = 0; i < window.currentContent.widgets.length; i++) {
            if (window.currentContent.widgets[i].index == widgetIndex) {
                widget = window.currentContent.widgets[i];
                break;
            }
        }
        if (widget && widget.type == FDashboardWidgetType.Chart)
            updateWidgetDomElement(widget);
        if (widget && widget.type == FDashboardWidgetType.Map) {
            $(el).find('.map-container').data('fMapHelper').map.invalidateSize();
        }
    });
    if (window.currentContent.widgets) {
        for (var i = 0; i < window.currentContent.widgets.length; i++) {
            var widget = window.currentContent.widgets[i];
            addWidgetDomElement(widget.index, widget.props);
            updateWidgetDomElement(widget);
        }
    }
    $('#dashboardDisplayNameTitle').html(window.currentDashboard.displayName);
    if (!recID)
        btnEditModeOpenClick();
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.QueryResultAggregateFunction)) {
        options.push({
            value: value,
            text: lngHelper.get('dashboard.chartSerieValueAggregateFunction_' + key)
        })
    }
    utils.fillSelectOptions($('#chartSerieValueAggregateFunction'), options, true);

    var options = [];
    for (const [key, value] of Object.entries(ChartType)) {
        options.push({
            value: value,
            text: lngHelper.get('dashboard.chartSerieChartType_' + key)
        })
    }
    utils.fillSelectOptions($('#chartSerieChartType'), options, true);

    var options = [];
    for (const [key, value] of Object.entries(ChartLegendPosition)) {
        options.push({
            value: value,
            text: lngHelper.get('dashboard.chartLegendPosition_' + key)
        })
    }
    utils.fillSelectOptions($('#chartLegendPosition'), options, true);

    var options = [];
    for (const [key, value] of Object.entries(ChartLegendAlign)) {
        options.push({
            value: value,
            text: lngHelper.get('dashboard.chartLegendAlign_' + key)
        })
    }
    utils.fillSelectOptions($('#chartLegendAlign'), options, true);


    window.chartDataSerieDT = new dataTableHelper('chartDataSerieTable', {
        data: [],
        dom: '<"top">rt<"bottom"<p>><"clear">',
        columns: [
            { title: "ID", visible: false },
            { title: lngHelper.get('dashboard.chartSerieObject') },
            { title: lngHelper.get('dashboard.chartSerieLabelColumn') },
            { title: lngHelper.get('dashboard.chartSerieValueColumn') }
        ]
    }, {
        buttons: [{
            text: lngHelper.get('generic.edit'),
            clickFunction: 'btnEditChartDataSerieClick'
        }, {
            text: lngHelper.get('generic.delete'),
            clickFunction: 'btnDeleteChartDataSerieClick'
        }],
        dblClickFunction: 'btnEditChartDataSerieClick'
    });

    //window.chartDataSerieDataTable = $('#chartDataSerieTable').DataTable({
    //    data: [],
    //    dom: '<"top">rt<"bottom"<p>><"clear">',
    //    columns: [
    //        { title: "ID", visible: false },
    //        { title: lngHelper.get('dashboard.chartSerieObject') },
    //        { title: lngHelper.get('dashboard.chartSerieLabelColumn') },
    //        { title: lngHelper.get('dashboard.chartSerieValueColumn') }
    //    ],
    //    'language': {
    //        'url': '../lib/datatables/' + lngHelper.currentLanguage.dataTablesName
    //    }
    //});
    //dataTableHelper.setClickEvents(window.chartDataSerieDataTable);
    $('#chartDataSerie select').on('change', () => {
        chartDataSerieParamChanged();
    })
    $('#chartDataSerie input').on('change', () => {
        chartDataSerieParamChanged();
    })
    $('#chartDataSerie').on('hidden.bs.modal', () => {
        var widget = $('#modalEditWidget').data('widget');
        if (widget.type == FDashboardWidgetType.Chart) {
            drawChartWidget(widget);
        }
    })
    $("#chartDataSerie").draggable({
        handle: ".modal-header"
    });
    $("#modalEditWidget").draggable({
        handle: ".modal-header"
    });
    initChartColorPalettes();

    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FDashboardChartSerieOrder)) {
        options.push({
            value: value,
            text: lngHelper.get('dashboard.chartSerieOrder_' + key)
        })
    }
    utils.fillSelectOptions($('#chartSerieOrder'), options, true);
}
async function initWidgetMapProps() {
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FMapBaseLayers)) {
        options.push({
            value: value,
            text: lngHelper.get('formDesign.mapBaseLayer_' + key)
        })
    }
    $('#baseMaps').data('allOptions', options);
    utils.fillSelectOptions($('#baseMaps'), options, true);

    var result = await service.call('fMap/getMapLayers', 'GET', {});
    window.currentMapDataLayers = result;
    options = [];
    for (var i = 0; i < result.length; i++) {
        options.push({
            value: result[i].fObjectProperty.fObjectID + '-' + result[i].fObjectProperty.recID,
            text: result[i].layerName
        })
    }
    utils.fillSelectOptions($('#dataMaps'), options, true);
    $('#dataMaps').data('allOptions', options);

    $(".edit-widget-map .sortable-list").sortable();
    $(".edit-widget-map .sortable-list").disableSelection();
}
function updateMapSelect(selectElem) {
    var layers = selectElem.data('allOptions');
    var listElem = selectElem.parents('.form-group').find('.sortable-list');

    var options = [];
    for (var i = 0; i < layers.length; i++) {
        if (selectElem.attr('id') == 'dataMaps')
            options.push(layers[i]);
        else {
            var elem = listElem.find('[data-id=' + layers[i].value + ']');
            if (!elem || elem.length == 0) {
                options.push(layers[i]);
            }
        }
    }
    utils.fillSelectOptions(selectElem, options, true);
}
function addLayerItem(elem, value, text, query) {
    var html = '<div class="list-item" data-id="' + value + '" data-item-sortable-id="0" draggable="true" role="option" aria-grabbed="false">\
                    <div class="main-content">' + text + '</div>\
                    <div><button class="btn" onclick="btnDeleteMapLayerClick(event)"><i class="fas fa-times"></i></button></div>\
                </div>';
    if (elem.attr('id') == 'widgetMapDataLayers') {
        html = '<div class="list-item" data-id="' + value + '" data-item-sortable-id="0" draggable="true" role="option" aria-grabbed="false">\
                    <div class="main-content">' + text + '</div>\
                    <div class="dropdown">\
                      <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                        <i class="fas fa-ellipsis-v"></i></button>\
                      </button>\
                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">\
                        <a class="dropdown-item" href="#" onclick="btnMapDataLayerQueryClick(event)">' + lngHelper.get('generic.query') + '</a>\
                        <a class="dropdown-item" href="#" onclick="btnDeleteMapLayerClick(event)">' + lngHelper.get('generic.delete') + '</a>\
                      </div>\
                    </div>\
                </div>';
    }
    var item = $(html);
    elem.append(item);
    if (query)
        item.data('query', query);
}
function btnAddMapLayerClick(e) {
    var sender = utils.getEventSender(e);
    var selectElem = sender.parents('.form-group').find('select');
    var selectedOption = selectElem.find('option:selected');
    var listElem = selectElem.parents('.form-group').find('.sortable-list');
    if (!selectedOption || selectedOption.length == 0)
        return false;
    addLayerItem(listElem, selectedOption.attr('value'), selectedOption.html());
    updateMapSelect(selectElem);
}
function btnDeleteMapLayerClick(e) {
    var sender = utils.getEventSender(e);
    var selectElem = sender.parents('.form-group').find('select');
    sender.parents('.list-item').remove();
    updateMapSelect(selectElem);
}
function btnMapDataLayerQueryClick(e) {
    var sender = utils.getEventSender(e);
    var item = sender.parents('.list-item');
    var layerName = item.find('.main-content').text();
    var fObjectID = '';
    for (var i = 0; i < window.currentMapDataLayers.length; i++) {
        if (window.currentMapDataLayers[i].layerName == layerName) {
            fObjectID = window.currentMapDataLayers[i].fObject.recID;
            break;
        }
    }
    if (fObjectID) {
        queryHelper.getDataQuery(fObjectID, null, item.data('query'), null, function (query) {
            item.data('query', query);
            //chartDataSerieParamChanged(); 
        });
    }
}
function showMap(widget){
    var elem = $('.grid-stack .grid-stack-item[widget-index=' + widget.index + '] .card-body');
    elem.html('<div class="map-container"></div>');

    var allMapDataLayers = $('#dataMaps').data('allOptions');
    var dataLayers = [];
    if (widget.fProps && widget.fProps.dataLayers) {
        for (var i = 0; i < widget.fProps.dataLayers.length; i++) {
            for (var j = 0; j < allMapDataLayers.length; j++) {
                if (allMapDataLayers[j].value == widget.fProps.dataLayers[i].layerName) {
                    dataLayers.push({
                        layerName: allMapDataLayers[j].value,
                        displayName: allMapDataLayers[j].text,
                        query: widget.fProps.dataLayers[i].query
                    });
                }
            }
        }
    }
    var mapOptions = {};
    if (widget.fProps && widget.fProps.center)
        mapOptions.center = widget.fProps.center;
    if (widget.fProps.zoom)
        mapOptions.zoom = widget.fProps.zoom;
    var mapHelper = new fMapHelper(elem.find('.map-container'), widget.fProps.baseLayers, dataLayers, mapOptions);
    elem.data('fMapHelper', mapHelper);
}
function btnGetFromMapClick(e) {
    var sender = utils.getEventSender(e);
    var inputElem = sender.parents('.form-group').find('input');
    var inputElemID = sender.parents('.form-group').find('input').attr('id');
    var widget = $('#modalEditWidget').data('widget');
    var mapContainer = $('.grid-stack .grid-stack-item[widget-index=' + widget.index + '] .card-body').find('.map-container');
    var mapHelper = mapContainer.data('fMapHelper');
    if (mapHelper && mapHelper.map) {
        switch (inputElem.attr('id')) {
            case 'widgetMapCenterLatitude':
                inputElem.val(mapHelper.map.getCenter().lat);
                break;
            case 'widgetMapCenterLongitude':
                inputElem.val(mapHelper.map.getCenter().lng);
                break;
            case 'widgetMapZoom':
                inputElem.val(mapHelper.map.getZoom());
                break;
        }
    }
}
function mapPropChanged() {
    var widget = $('#modalEditWidget').data('widget');
    var tmpWidget = JSON.parse(JSON.stringify(widget));

    var baseLayers = [];
    $('#widgetMapBaseLayers .list-item').each((index, item) => {
        baseLayers.push(parseInt($(item).attr('data-id')));
    })
    var dataLayers = [];
    $('#widgetMapDataLayers .list-item').each((index, item) => {
        dataLayers.push({
            layerName: $(item).attr('data-id'),
            query: $(item).data('query')
        });
    })
    tmpWidget.fProps = {
        baseLayers: baseLayers,
        dataLayers: dataLayers,
        center: [$('#widgetMapCenterLatitude').val() ? parseFloat($('#widgetMapCenterLatitude').val()) : 0, $('#widgetMapCenterLongitude').val() ? parseFloat($('#widgetMapCenterLongitude').val()): 0],
        zoom: $('#widgetMapZoom').val() ? parseInt($('#widgetMapZoom').val()) : 0
    }
    showMap(tmpWidget);
}
$(document).on('initcompleted', function () {
    init();
})
