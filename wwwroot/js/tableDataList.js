﻿function btnImportDataClick(e) {
    utils.preventClickTwice(e);
    utils.openPage('importData.html?objectID=' + window.currentTable.fObjectID.toString(), function () {
        window.dt.reload();
    });
}
function childTitleChanged() {
    if (window.addEditPopup && window.addEditPopup.find('iframe') && window.addEditPopup.find('iframe').length > 0) {
        window.addEditPopup.find('.modal-title').html(window.addEditPopup.find('iframe')[0].contentDocument.title);
    }
}
async function btnSaveClick(e) {
    utils.preventClickTwice(e);
    var btnSave = $('#btnSave');
    if (!btnSave || btnSave.length == 0 || btnSave.css('display') == 'none')
        return;
    if (window.btnSaveClick_OnStart) {
        var result = await window.btnSaveClickStart.apply(null, arguments);
        if (result == false)
            return;
    }
    var bSaved = false;
    var rows = window.dt.dtObject.rows();
    for (var i = 0; i < rows.count(); i++) {
        var row = window.dt.dtObject.rows(i);
        var rowNode = row.nodes()[0];
        var cellNodes = $(rowNode).find('td');

        var formData = {};
        for (var j = 0; j < cellNodes.length; j++) {
            var children = $(cellNodes[j]).find('.form-control');
            if (children.length > 0) {
                var columns = window.dt.dtObject.settings()[0].aoColumns;
                var counter = 0;
                var columnIndex = 0;
                for (var k = 0; k < columns.length; k++) {
                    if (!columns[k].bVisible)
                        continue;
                    if (counter == j) {
                        columnIndex = k;
                        break;
                    }
                    counter++;
                }
                var elem = $(children[0]);
                var property = window.dt.dtObject.settings()[0].aoColumns[columnIndex].fObjectProperty;
                var propertyID = property.recID;
                var initValue = elem.attr('initvalue');
                switch (property.editorType) {
                    case window.fEnums.FObjectPropertyEditorType.Checkbox:
                        var value = elem.is(':checked') ? '1' : '0';
                        if (value != initValue)
                            formData[propertyID] = value;
                        break;
                    default:
                        if (elem.val() != initValue) {
                            formData[propertyID] = elem.val();
                        }
                        break;
                }
            }
        }
        var recID = row.data()[0][0];
        if (i == 0) {
            [].unshift.call(arguments, formData);
            [].unshift.call(arguments, recID);
        } else {
            arguments[0] = recID;
            arguments[1] = formData;
        }
        if (window.btnSaveClick_OnRecordSaveStart) {
            var result = await window.btnSaveClick_OnRecordSaveStart.apply(null, arguments);
            if (result == false)
                return;
        }
        if (!$.isEmptyObject(formData)) {
            var url = 'fObjectData/save?recID=' + recID + '&fObjectID=' + window.currentTable.fObjectID;
            await service.call(url, 'POST', formData);
            bSaved = true;
            if (window.btnSaveClick_OnRecordSaveEnd) {
                var result = await window.btnSaveClick_OnRecordSaveEnd.apply(null, arguments);
                if (result == false)
                    return;
            }
        }
    }
    if (bSaved) {
        window.dt.reload();
        utils.showToast('success', lngHelper.get('tableDataList.saved'));
    } else {
        utils.showToast('info', lngHelper.get('tableDataList.nothingChanged'));
    }
    if (window.btnSaveClick_OnEnd) {
        var result = await window.btnSaveClick_OnEnd.apply(null, arguments);
        if (result == false)
            return;
    }
}
async function btnDeleteClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    [].unshift.call(arguments, recID);
    if (window.btnDeleteClick_OnStart) {
        var result = await window.btnDeleteClick_OnStart.apply(null, arguments);
        if (result == false)
            return;
    }

    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            if (window.btnDeleteClick_OnConfirmed)
                window.btnDeleteClick_OnConfirmed.apply(null, arguments);
            service.call('fObjectData/delete', 'GET', { recID: recID, fObjectID: window.currentTable.fObjectID }).then(function (result) {
                if (window.btnDeleteClick_OnEnd)
                    window.btnDeleteClick_OnEnd.apply(null, arguments);
                window.dt.reload();
            }, function () { })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
async function btnUndeleteClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    [].unshift.call(arguments, recID);
    if (window.btnUndeleteClick_OnStart) {
        var result = await window.btnUndeleteClick_OnStart.apply(null, arguments);
        if (result == false)
            return;
    }
    utils.alertConfirm(lngHelper.get('trash.confirmUndelete'), async () => {
        if (window.btnUndeleteClick_OnConfirmed)
            window.btnUndeleteClick_OnConfirmed.apply(null, arguments);
        service.call('fObjectData/undelete', 'GET', { recID: recID, fObjectID: window.currentTable.fObjectID }).then(function (result) {
            if (window.btnUndeleteClick_OnEnd)
                window.btnUndeleteClick_OnEnd.apply(null, arguments);
            window.dt.reload();
        }, function () { })
    })
}
async function btnDeletePermanentlyClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    [].unshift.call(arguments, recID);
    if (window.btnDeletePermanentlyClick_OnStart) {
        var result = await window.btnDeletePermanentlyClick_OnStart.apply(null, arguments);
        if (result == false)
            return;
    }
    utils.alertDanger(lngHelper.get('trash.deletePermanentlyWarning'), () => {
        utils.alertConfirm(lngHelper.get('trash.confirmDeletePermanently'), async () => {
            if (window.btnDeletePermanentlyClick_OnConfirmed)
                window.btnDeletePermanentlyClick_OnConfirmed.apply(null, arguments);
            var result = await service.call('fObjectData/delete', 'GET', { recID: recID, fObjectID: window.currentTable.fObjectID, hard: true });
            if (window.btnDeletePermanentlyClick_OnEnd)
                window.btnDeletePermanentlyClick_OnEnd.apply(null, arguments);
            window.dt.reload();
        })
    });
}
function getDefaultForm() {
    return new Promise(function (resolve, reject) {
        if (window.defaultForm)
            resolve(window.defaultForm);
        else {
            service.call('fObject/getDefaultForm', 'GET', { fObjectID: window.currentTable.fObjectID }).then(function (result) {
                if (!result) {
                    utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('tableDataList.couldNotFindDefaultForm'));
                    reject();
                } else {
                    window.defaultForm = result;
                    resolve(result);
                }
            })
        }
    })
}
async function btnAddClick(e) {
    utils.preventClickTwice(e);
    if (window.btnAddClick_OnStart) {
        var result = await window.btnAddClick_OnStart.apply(null, arguments);
        if (result == false)
            return;
    }
    getDefaultForm().then(function (result) {
        var formID = result.recID;
        var url = 'formDataEdit.html?formID=' + formID + '&closeOnSave=true';
        if (window.relations)
            url += '&relations=' + window.relations;
        window.addEditPopup = utils.openPage(url, () => {
            if (window.btnAddClick_OnPopupClosed)
                window.btnAddClick_OnPopupClosed.apply(null, arguments);
            if (window.addEditPopup.find('iframe')[0].contentWindow.windowResult == 'Updated')
                window.dt.reload();
        });
    })
    if (window.btnAddClick_OnEnd)
        window.btnAddClick_OnEnd.apply(null, arguments);
}
async function btnEditClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    var formID = window.currentTable.recID;

    [].unshift.call(arguments, recID);
    if (window.btnEditClick_OnStart) {
        var result = await window.btnEditClick_OnStart.apply(null, arguments);
        if (result == false)
            return;
    }

    getDefaultForm().then(function (result) {
        var formID = result.recID;
        var recID = selectedRow.data()[0];
        var url = 'formDataEdit.html?formID=' + formID.toString() + '&recID=' + recID.toString() + '&closeOnSave=true';
        if (window.relations)
            url += '&relations=' + window.relations;
        window.addEditPopup = utils.openPage(url, () => {
            if (window.btnEditClick_OnPopupClosed)
                window.btnEditClick_OnPopupClosed.apply(null, arguments);
            if (window.addEditPopup.find('iframe')[0].contentWindow.windowResult == 'Updated')
                window.dt.reload(false);
        });
    })
    if (window.btnEditClick_OnEnd)
        window.btnEditClick_OnEnd.apply(null, arguments);
}
function btnRefreshClick(e) {
    utils.preventClickTwice(e);
    window.dt.reload();
}
function btnQueryClick(e) {
    utils.preventClickTwice(e);
    var propertyFilterArray = [];
    for (var i = 0; i < window.currentTableColumnList.length; i++) {
        var tableColumn = window.currentTableColumnList[i];
        var str = tableColumn.fObjectPropertyID;
        if (tableColumn.objectRelation && tableColumn.objectRelation != utils.emptyGuid)
            str = tableColumn.objectRelation + '_' + str;
        propertyFilterArray.push(str);
    }
    queryHelper.getDataQuery(window.currentTable.fObjectID, propertyFilterArray, window.currentQuery, null, function (query) {
        window.currentQuery = query;
        window.dt.reload();
    });
}
function filterChange(e, columnIndex) {
    if (!e)
        e = window.event;
    var sender = e.srcElement || e.target;

    if (window.dt.dtObject.column(columnIndex).search() !== sender.value) {
        window.dt.dtObject
            .column(columnIndex)
            .search(sender.value);

        if (window.filterTimeout) {
            clearTimeout(window.filterTimeout);
        }
        window.filterTimeout = setTimeout(function () {
            window.dt.dtObject.draw();
        }, 500);
    }
}
async function getBProcesses() {
    var retValue = [];

    if (window.currentTable.props) {
        var props;
        try {
            props = JSON.parse(window.currentTable.props)
        } catch (err) { }
        if (props && props.bProcesses) {
            for (var i = 0; i < props.bProcesses.length; i++) {
                var bProcess = await service.call('fBProcess/get', 'GET', { recID: props.bProcesses[i] });
                if (bProcess) {
                    retValue.push({
                        text: bProcess.displayName,
                        value: bProcess.recID,
                        bProcessUrlParameters: props.bProcessUrlParameters
                    })
                }
            }
        }
    }
    return retValue;
}
async function initTable() {
    if (!window.tableID) {
        window.currentObjectID = utils.getUrlParam('objectID');
        if (window.currentObjectID) {
            var tables = await service.call('fTable/getList', 'GET', { fObjectID: window.currentObjectID });
            if (tables && tables.length && tables.length > 0) {
                window.currentTable = tables[0];
                window.tableID = window.currentTable.recID;
            }
        }
    } else
        window.currentTable = await service.call('fTable/get', 'GET', { recID: window.tableID });

    utils.changeTitleFromInside(window.currentTable.displayName);

    var bProcesses = await getBProcesses();
    if (!window.currentTable)
        return;
    if (window.currentTable.deleteTime) {
        utils.alertWarning(lngHelper.get('tableDataList.tableIsDeleted'));
        return;
    }
    if (window.currentTable.css) {
        var style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = window.currentTable.css;
        document.getElementsByTagName('head')[0].appendChild(style);
    }
    if (window.currentTable.script) {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.innerHTML = window.currentTable.script;
        document.getElementsByTagName('body')[0].appendChild(script);
    }

    var btnNewVisible = true;
    var btnEditVisible = true;
    var btnDeleteVisible = true;
    var btnExportVisible = true;
    var btnImportVisible = true;
    var btnUndeleteVisible = false;
    var btnDeletePermanentlyVisible = false;
    var btnAddRelationWithExistingItem = true;
    if (window.currentTable.props) {
        var props = JSON.parse(window.currentTable.props);
        if (!props.operation_New)
            btnNewVisible = false;
        if (!props.operation_Edit)
            btnEditVisible = false;
        if (!props.operation_Delete)
            btnDeleteVisible = false;
        if (!props.operation_Export)
            btnExportVisible = false;
        if (!props.operation_Import)
            btnImportVisible = false;
        if (!props.operation_AddRelationWithExistingItem)
            btnAddRelationWithExistingItem = false;
        if (props.formID) {
            window.defaultForm = {
                recID: props.formID
            }
        }
    }
    if (btnNewVisible || btnEditVisible || btnDeleteVisible) {
        var sessionInfo = await service.call('fSession/getSessionInfo', 'GET', {});
        if (!sessionInfo.isSystemAdmin) {
            var result = await service.call('fAuthorization/getListByObjectTypeArray', 'GET', {
                authObjects: window.fEnums.AuthObjectType.ObjectDataCreate + ',' + window.fEnums.AuthObjectType.ObjectDataDelete,
                authObjectRecID: window.currentTable.fObjectID
            });
            var bCreateAuth = false;
            var bDeleteAuth = false;
            for (var i = 0; i < result.length; i++) {
                if (result[i].authObject == window.fEnums.AuthObjectType.ObjectDataCreate && result[i].authStatus == window.fEnums.AuthStatusType.Allowed)
                    bCreateAuth = true;
                if (result[i].authObject == window.fEnums.AuthObjectType.ObjectDataDelete && result[i].authStatus == window.fEnums.AuthStatusType.Allowed)
                    bDeleteAuth = true;
            }
            if (!bCreateAuth)
                btnNewVisible = false;
            if (!bDeleteAuth)
                btnDeleteVisible = false;
        }
    }
    if (window.fSoftDeleteStatus == window.fEnums.FSoftDeleteStatus.OnlyDeleted) {
        btnDeletePermanentlyVisible = true;
        btnUndeleteVisible = true;
        btnNewVisible = false;
        btnDeleteVisible = false;
    }
    if (btnNewVisible)
        $('#btnAddContainer').show();
    if (btnExportVisible)
        $('#btnExport').show();
    if (btnImportVisible)
        $('#btnImport').show();

    document.title = window.currentTable.displayName;
    window.currentTableColumnList = await service.call('fTableColumn/getList', 'GET', { fTableID: window.tableID })
    if (!window.currentTableColumnList)
        return;

    var columns = [];
    for (var i = 0; i < window.currentTableColumnList.length; i++) {
        var tableColumn = window.currentTableColumnList[i];
        var str = tableColumn.fObjectPropertyID;
        if (tableColumn.objectRelation && tableColumn.objectRelation != utils.emptyGuid)
            str = tableColumn.objectRelation + '_' + str;
        columns.push(str)
    }

    window.currentPropertyDetails = await service.call('fObject/getPropertyDetails?fObjectID=' + window.currentTable.fObjectID, 'POST', columns);

    window.tableColumnArray = [];
    if (window.currentTableColumnList.length > 0) {
        tableColumnArray.push({
            title: 'recid',
            name: 'recid',
            visible: false
        })
    }
    var bEdit = false;
    for (var i = 0; i < window.currentTableColumnList.length; i++) {
        var currentColumn = window.currentTableColumnList[i];
        if (currentColumn.props)
            currentColumn.props = JSON.parse(currentColumn.props);
        var currentColumnFullID = currentColumn.fObjectPropertyID;
        if (currentColumn.objectRelation && currentColumn.objectRelation != utils.emptyGuid)
            currentColumnFullID = currentColumn.objectRelation + '_' + currentColumnFullID;
        var bFound = false;
        for (var j = 0; j < window.currentPropertyDetails.length; j++) {
            var propertyDetails = window.currentPropertyDetails[j];
            if (currentColumnFullID == propertyDetails.propertyFullID) {
                var property = propertyDetails.property;
                tableColumn = {
                    title: currentColumn.displayName != '' ? currentColumn.displayName : property.displayName,
                    name: currentColumnFullID,
                    fObjectProperty: property,
                    fTableColumn: currentColumn
                }
                if (currentColumn.props && currentColumn.props.hidden) 
                    tableColumn.visible = false;
                if (currentColumn.editMode == '1') {
                    bEdit = true;
                    switch (property.editorType) {
                        case window.fEnums.FObjectPropertyEditorType.InputNumber:
                            tableColumn.render = function (data, type, row) {
                                return '<input type="number" initvalue="' + utils.htmlEscape(data) + '" value="' + utils.htmlEscape(data) + '" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" min="-2147483647" max="2147483647">';
                            }
                            break;
                        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
                            tableColumn.x = 'y';
                            tableColumn.render = function (data, type, row, meta) {
                                var step = '0.01';
                                if (window.tableColumnArray[meta.col].fObjectProperty.decimalScale)
                                    step = (1 / Math.pow(10, window.tableColumnArray[meta.col].fObjectProperty.decimalScale)).toString();
                                return '<input type="number" initvalue="' + utils.htmlEscape(data) + '" value="' + utils.htmlEscape(data) + '" class="form-control" step="' + step + '" min="-100000000" max="100000000">';
                            }
                            break;
                        case window.fEnums.FObjectPropertyEditorType.Checkbox:
                            tableColumn.render = function (data, type, row) {
                                return '<span class="switch switch-icon"><label><input type="checkbox" initvalue="' + utils.htmlEscape(data) + '" value="' + utils.htmlEscape(data) + '" class="form-control"' + (data == '1' ? ' checked' : '') + '><span></span></label></span>';
                            }
                            break;
                        case window.fEnums.FObjectPropertyEditorType.DateTime:
                            tableColumn.render = function (data, type, row) {
                                if (!data) return data;
                                return '<input type="datetime-local" initvalue="' + moment(data).format('YYYY-MM-DD[T]HH:mm') + '" value="' + moment(data).format('YYYY-MM-DD[T]HH:mm') + '" class="form-control">';
                            }
                            break;
                        case window.fEnums.FObjectPropertyEditorType.Date:
                            tableColumn.render = function (data, type, row) {
                                if (!data) return data;
                                return '<input type="date" initvalue="' + utils.htmlEscape(data) + '" value="' + moment(data).format('YYYY-MM-DD') + '" class="form-control">';
                            }
                            break;
                        case window.fEnums.FObjectPropertyEditorType.Time:
                            tableColumn.render = function (data, type, row) {
                                return '<input type="time" initvalue="' + utils.htmlEscape(data) + '" value="' + data + '" class="form-control">';
                            }
                            break;
                        case window.fEnums.FObjectPropertyEditorType.RadioButton:
                        case window.fEnums.FObjectPropertyEditorType.Select:
                            if (!window.editSelectHtml)
                                window.editSelectHtml = {};
                            if (!window.editSelectHtml[property.recID]) {
                                var elem = $('<select class="form-control"></select>');
                                var options = [];
                                var result = await service.call('fOptionListItem/getList', 'GET', { fOptionListID: property.fOptionListID });
                                for (var k = 0; k < result.length; k++) {
                                    options.push({
                                        text: result[k].displayName,
                                        value: result[k].recID
                                    });
                                }
                                utils.fillSelectOptions(elem, options, false, utils.emptyGuid);
                                window.editSelectHtml[property.recID] = elem;
                            }
                            tableColumn.render = function (data, type, row, meta) {
                                var propertyID = window.tableColumnArray[meta.col].fObjectProperty.recID;
                                var elem = window.editSelectHtml[propertyID].clone();
                                elem.attr('initvalue', data);
                                elem.find('option').each((index, item) => {
                                    item = $(item);
                                    if (item.html() == data) {
                                        item.attr('selected', true);
                                        return false;
                                    }
                                })
                                elem.val(data);
                                return elem.prop('outerHTML');
                            }
                            break;
                        default:
                            tableColumn.render = function (data, type, row) {
                                return '<input type="text" initvalue="' + utils.htmlEscape(data) + '" value="' + utils.htmlEscape(data) + '" class="form-control">';
                            }
                            break;
                    }
                } else {
                    switch (property.editorType) {
                        case window.fEnums.FObjectPropertyEditorType.DateTime:
                            tableColumn.render = lngHelper.dataTableDateTimeColumnRender('dateTime');
                            tableColumn.className = 'dt-body-right';
                            break;
                        case window.fEnums.FObjectPropertyEditorType.Date:
                            tableColumn.render = lngHelper.dataTableDateTimeColumnRender('date');
                            tableColumn.className = 'dt-body-right';
                            break;
                        case window.fEnums.FObjectPropertyEditorType.Time:
                            tableColumn.render = lngHelper.dataTableDateTimeColumnRender('time');
                            tableColumn.className = 'dt-body-right';
                            break;
                        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
                            tableColumn.render = lngHelper.dataTableNumberRender(property.decimalScale);
                            tableColumn.className = 'dt-body-right';
                            break;
                        case window.fEnums.FObjectPropertyEditorType.InputNumber:
                            tableColumn.render = lngHelper.dataTableNumberRender(0);
                            tableColumn.className = 'dt-body-right';
                            break;
                    }
                }
                tableColumnArray.push(tableColumn);
                break;
            }
        }
    }
    if (bEdit) {
        $('#btnSave').show();
    }
    var dataTableUrl = '/fObjectData/getForDataTable?fTableID=' + window.currentTable.recID;
    if (window.fSoftDeleteStatus)
        dataTableUrl += '&fSoftDeleteStatus=' + window.fSoftDeleteStatus;
    if ((window.relations && window.relations.length > 0)) {
        var relationsParam = '';
        try {
            var obj = JSON.parse(window.relations);
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    relationsParam += '&relations[' + prop + ']=' + obj[prop];
                }
            }
            dataTableUrl += relationsParam;
        }
        catch { }
    }
    var lengthMenu = [10, 25, 50, 100];
    var pageLength = 10;
    if (utils.getUrlParam('pageLength')) {
        pageLength = parseInt(utils.getUrlParam('pageLength'));
        if (lengthMenu.indexOf(pageLength) < 0)
            lengthMenu.push(pageLength);
    }

    var fSelectMenuItems = [];
    var removeRelationItemVisible = false;
    if (window.relations) {
        window.objectRelations = await service.call('fObjectRelation/getList', 'GET', { fObjectID: window.currentTable.fObjectID });
        var relationObj = JSON.parse(window.relations);
        for (var i = 0; i < window.objectRelations.length; i++) {
            var relation = window.objectRelations[i];
            if (relationObj[relation.recID] && relationObj[relation.recID] != utils.emptyGuid && (relation.relationType == window.fEnums.FObjectRelationType.ManyToMany || (relation.relationType == window.fEnums.FObjectRelationType.OneToMany && relation.childFObjectID == window.currentTable.fObjectID))) {
                if (btnAddRelationWithExistingItem) {
                    $('#addRelationWithExistingItem').data('relation', relation);
                    $('#addRelationWithExistingItem').show();
                }
                if (relation.relationType == window.fEnums.FObjectRelationType.ManyToMany)
                    removeRelationItemVisible = true;
            }
        }
    }
    var buttonArray = [];
    if (btnUndeleteVisible) {
        buttonArray.push({
            text: lngHelper.get('trash.undelete'),
            clickFunction: 'btnUndeleteClick'
        });
        fSelectMenuItems.push({
            displayName: lngHelper.get('trash.undelete'),
            onclick: function () {
                var rows = window.dt.dtObject.rows({ selected: true });
                utils.alertConfirm(lngHelper.get('trash.confirmUndeleteMultiple').replace(/%/, rows.count()), async () => {
                    if (window.btnUndeleteClick_OnConfirmed)
                        window.btnUndeleteClick_OnConfirmed.apply(null, arguments);
                    var promiseArray = [];
                    for (var i = 0; i < rows.count(); i++) {
                        promiseArray.push(service.call('fObjectData/undelete', 'GET', { recID: rows.data()[i][0], fObjectID: window.currentTable.fObjectID }));
                    }
                    Promise.all(promiseArray).then((values) => {
                        if (window.btnUndeleteClick_OnEnd)
                            window.btnUndeleteClick_OnEnd.apply(null, arguments);
                        window.dt.reload();
                    });
                });
            }
        })
    }
    if (btnDeletePermanentlyVisible) {
        buttonArray.push({
            text: lngHelper.get('trash.deletePermanently'),
            clickFunction: 'btnDeletePermanentlyClick'
        });
        fSelectMenuItems.push({
            displayName: lngHelper.get('trash.deletePermanently'),
            onclick: function () {
                var rows = window.dt.dtObject.rows({ selected: true });
                utils.alertDanger(lngHelper.get('trash.deletePermanentlyWarning'), () => {
                    utils.alertConfirm(lngHelper.get('trash.confirmDeletePermanentlyMultiple').replace(/%/, rows.count()), async () => {
                        if (window.btnDeletePermanentlyClick_OnConfirmed)
                            window.btnDeletePermanentlyClick_OnConfirmed.apply(null, arguments);
                        var promiseArray = [];
                        for (var i = 0; i < rows.count(); i++) {
                            promiseArray.push(service.call('fObjectData/delete', 'GET', { recID: rows.data()[i][0], fObjectID: window.currentTable.fObjectID, hard: true }));
                        }
                        Promise.all(promiseArray).then((values) => {
                            if (window.btnDeletePermanentlyClick_OnEnd)
                                window.btnDeletePermanentlyClick_OnEnd.apply(null, arguments);
                            window.dt.reload(false);
                        });
                    });
                });
            }
        })
    }
    if (btnEditVisible) {
        buttonArray.push({
            text: lngHelper.get('generic.edit'),
            clickFunction: 'btnEditClick'
        });
    }
    if (btnDeleteVisible) {
        buttonArray.push({
            text: lngHelper.get('generic.delete'),
            clickFunction: 'btnDeleteClick'
        });
        fSelectMenuItems.push({
            displayName: lngHelper.get('generic.delete'),
            onclick: function () {
                var rows = window.dt.dtObject.rows({ selected: true });
                utils.alertConfirm(lngHelper.get('generic.confirmDeleteMultiple').replace(/%/, rows.count()), () => {
                    var promiseArray = [];
                    for (var i = 0; i < rows.count(); i++) {
                        promiseArray.push(service.call('fObjectData/delete', 'GET', { recID: rows.data()[i][0], fObjectID: window.currentTable.fObjectID }));
                    }
                    Promise.all(promiseArray).then((values) => {
                        window.dt.reload(false);
                    });
                })
            }
        })
    }
    if (removeRelationItemVisible) {
        buttonArray.unshift({
            text: lngHelper.get('tableDataList.removeRelationItem'),
            clickFunction: 'removeRelationItem'
        });
        fSelectMenuItems.push({
            displayName: lngHelper.get('tableDataList.removeRelationItem'),
            onclick: function () {
                var rows = window.dt.dtObject.rows({ selected: true });
                utils.alertConfirm(lngHelper.get('tableDataList.confirmRemoveRelationItemMultiple').replace(/%/, rows.count()), async () => {
                    var relationObj = JSON.parse(window.relations);
                    for (var i = 0; i < window.objectRelations.length; i++) {
                        if (relationObj[window.objectRelations[i].recID] && window.objectRelations[i].relationType == window.fEnums.FObjectRelationType.ManyToMany) {
                            var relationObj = JSON.parse(window.relations);
                            var fObjectRelation = window.objectRelations[i];
                            var promiseArray = [];
                            for (var i = 0; i < rows.count(); i++) {
                                var parentRecID = utils.emptyGuid;
                                var childRecID = utils.emptyGuid;
                                if (fObjectRelation.childFObjectID == window.currentTable.fObjectID) {
                                    parentRecID = relationObj[fObjectRelation.recID];
                                    childRecID = rows.data()[i][0];
                                }
                                if (fObjectRelation.parentFObjectID == window.currentTable.fObjectID) {
                                    childRecID = relationObj[fObjectRelation.recID];
                                    parentRecID = rows.data()[i][0];
                                }
                                promiseArray.push(service.call('fObjectData/deleteNNRelation?parentRecID=' + parentRecID + '&childRecID=' + childRecID, 'POST', fObjectRelation));
                            }
                            Promise.all(promiseArray).then((values) => {
                                window.dt.reload(false);
                            });
                        }
                    }
                })
            }
        })
    }
    if (window.op == 'select' && !window.bProcessRunElementID) {
        buttonArray.unshift({
            text: lngHelper.get('tableDataList.selectForParent'),
            clickFunction: 'btnSelectForCallerClick'
        })
        $('#btnDeselect').show();
        fSelectMenuItems.unshift({
            displayName: lngHelper.get('tableDataList.selectForParent'),
            onclick: async function () {
                var rows = window.dt.dtObject.rows({ selected: true });
                var recIDs = [];
                for (var i = 0; i < rows.count(); i++)
                    recIDs.push(rows.data()[i][0]);

                if (window.bProcessRunElementID) {
                    var outInfo = await service.call('fBProcessRunElement/getOutInfo', 'GET', { recID: window.bProcessRunElementID });
                    if (outInfo.bProcessConnectors.length > 0) {
                        var connectorID = outInfo.bProcessConnectors[0].recID;
                        var runResult = await service.call('fBProcessRunElement/run', 'GET', { recID: window.bProcessRunElementID, connectorID: connectorID, selectedRecordID: recIDs[0] });
                        bProcessHelper.handleBProcessRunResult(runResult);
                    }
                } else {
                    if (window.selectForCallerCallBackFunction)
                        window.selectForCallerCallBackFunction(recIDs, utils.getUrlParamList());
                }

            }
        })
    }
    if (buttonArray.length > 0 && bProcesses && bProcesses.length > 0) {
        buttonArray.push({
            isSeperator: true
        })
    }
    var bProcessUrlParams = '';

    for (var i = 0; i < bProcesses.length; i++) {
        buttonArray.push({
            text: bProcesses[i].text,
            clickFunction: 'btnBProcessClick',
            attributes: {
                'process-id': bProcesses[i].value,
                'url-parameters': JSON.stringify(bProcesses[i].bProcessUrlParameters)
            }
        });
    }
    if (window.fSoftDeleteStatus == window.fEnums.FSoftDeleteStatus.OnlyDeleted) {
        tableColumnArray.push({
            title: lngHelper.get('trash.column_deleteTime'),
            name: 'deletetime',
            render: lngHelper.dataTableDateTimeColumnRender('dateTime')
        })
    }
    var order = [];
    if (window.defaultOrder && Object.prototype.toString.call(window.defaultOrder) === '[object Array]') {
        for (var i = 0; i < window.defaultOrder.length; i++) {
            for (var j = 0; j < window.currentTableColumnList.length; j++) {
                if (window.defaultOrder[i].name == window.currentTableColumnList[j].displayName) {
                    var direction = 'asc';
                    if (window.defaultOrder[i].direction)
                        direction = window.defaultOrder[i].direction;
                    order.push([j + 2, direction]);
                    break;
                }
            }
        }
    }
    if (order.length == 0)
        order = [[3, 'asc']];

    var fSelectMenuElem = null;
    if (fSelectMenuItems.length) {
        fSelectMenuElem = $('#selectionMenu');
        $('#selectionMenu').closest('.dropdown').show();
    }

    var tFoot = $('#dataTable tfoot');
    var footRowCount = 0;
    for (var i = 0; i < tableColumnArray.length; i++) {
        var props = tableColumnArray[i].fTableColumn ? tableColumnArray[i].fTableColumn.props : null;
        if (props && props.aggregate && props.aggregate.length) {
            for (var j = 0; j < props.aggregate.length; j++) {
                var aggregate = props.aggregate[j];
                var row = tFoot.find('tr:nth-child(' + (j + 1).toString() + ')');
                if (!row || !row.length) {
                    row = $('<tr><td></td><td></td></tr>');
                    tFoot.append(row);
                    for (var k = 0; k < tableColumnArray.length; k++) {
                        var cell = $('<td></td>');
                        if (tableColumnArray[k].className)
                            cell.addClass(tableColumnArray[k].className);
                        row.append(cell);
                    }
                }
                var cell = row.find('td:nth-child(' + (i + 3).toString() + ')');
                cell.attr('data-column-index', (i + 2).toString());
                cell.data('tableColumn', tableColumnArray[i]);
                cell.data('aggregate', aggregate);
                var aggregateTypeName = '';
                for (const [key, value] of Object.entries(window.fEnums.QueryResultAggregateFunction)) {
                    if (value == aggregate.type) {
                        aggregateTypeName = lngHelper.get('tableEditColumns.aggregate_' + key);
                        break;
                    }
                }
                if (aggregate.scope == window.fEnums.FTableColumnAggregateFunctionScope.All)
                    aggregateTypeName = aggregateTypeName.toUpperCase();
                cell.html('<div class="aggregate-type">' + aggregateTypeName + '</div><div class="aggregate-value"></div>');
            }
        }
    }

    window.dt = new dataTableHelper('dataTable', {
        ajax: {
            url: dataTableUrl,
            type: 'POST',
            data: function (d) {
                if (window.addFilters)
                    d.addFilters = window.addFilters;
                if (window.currentQuery)
                    d.query = window.currentQuery;
            }
        },
        serverSide: true,
        processing: true,
        orderCellsTop: true,
        bFilter: true,
        pageLength: pageLength,
        lengthMenu: lengthMenu,
        dom: '<"top">rt<"bottom"<pi>l><"clear">',
        columns: tableColumnArray,
        fnFooterCallback: function (nRow, aaData, iStart, iEnd, aiDisplay) {
            var api = this.api();
            var cells = $('#dataTable tfoot tr td[data-column-index]');
            $.each(cells, function (index, item) {
                item = $(item);
                var columnIndex = parseInt(item.attr('data-column-index'));
                if (item.data('aggregate').scope == window.fEnums.FTableColumnAggregateFunctionScope.Page)
                    item.find('.aggregate-value').html(getAggregateValue(item.data('aggregate'), item.data('tableColumn'), api.column(columnIndex).data()));
                else {
                    var targetElem = item;
                    targetElem.find('.aggregate-value').html('');
                    queryAggregateValue([{
                        fObjectPropertyRecID: item.data('tableColumn').name,
                        aggregateFunction: item.data('aggregate').type
                    }], targetElem);
                }
            });
        },
        order: order,
        fixedHeader: true
    }, {
        buttons: buttonArray,
        dblClickFunction: window.op == 'select' ? 'btnSelectForCallerClick' : (btnEditVisible ? 'btnEditClick' : ''),
        fSelect: true,
        fSelectMenuElem: fSelectMenuElem,
        fSelectMenuItems: fSelectMenuItems
    });

    $('#dataTable').on('draw.dt', function () {
        var headers = window.dt.dtObject.columns().header();
        var filterPlaceHolder = lngHelper.get('tableDataList.filter');
        for (var i = 0; i < headers.length; i++) {
            var elem = $(headers[i]);
            if (!elem.hasClass('action-column') && !elem.hasClass('select-checkbox') && !elem.find('input').length)
                elem.append($('<input type="text" placeholder="' + filterPlaceHolder + '" onkeyup="filterChange(event, ' + i.toString() + ')" onclick="event.stopPropagation()" onchange="filterChange(event, ' + i.toString() + ')" style="width:100%"/>'));
        }
    });
    initReportMenu();
}
//TODO : Her bir değer için ayrı sorgu yapıyoruz. Tek sorgu ile çözülebilir. Şu anda böyle yapmamın nedeni aynı kolon için iki aggregate sorguya gönderildiğinde hata veriyor. Düzeltmek lazım.    
function queryAggregateValue(resultColumns, targetElem) {
    var dataQuery = {
        resultColumns: resultColumns,
        columnFilters: []
    };
    if (window.addFilters && window.addFilters.columnFilters)
        dataQuery.columnFilters = dataQuery.columnFilters.concat(window.addFilters.columnFilters);
    if (window.currentQuery && window.currentQuery.columnFilters)
        dataQuery.columnFilters = dataQuery.columnFilters.concat(window.currentQuery.columnFilters);

    var params = [];
    var columns = window.dt.dtObject.init().columns;
    for (var i = 1; i < columns.length; i++) {
        if (window.dt.dtObject.column(i).search()) {
            var valueOrj = window.dt.dtObject.column(i).search();
            var value = '%' + valueOrj + '%';
            var operator = 'LIKE';
            var bSkip = false;
            switch (columns[i].fObjectProperty.editorType) {
                case window.fEnums.FObjectPropertyEditorType.Checkbox:
                case window.fEnums.FObjectPropertyEditorType.InputNumber:
                    value = parseInt(valueOrj);
                    if (isNaN(value))
                        bSkip = true;
                    value = value.toString()
                    operator = '=';
                    break;
                case window.fEnums.FObjectPropertyEditorType.InputDecimal:
                    value = parseFloat(valueOrj);
                    if (isNaN(value))
                        bSkip = true;
                    value = value.toString()
                    operator = '=';
                    break;
                case window.fEnums.FObjectPropertyEditorType.DateTime:
                    try {
                        value = moment(valueOrj, 'L LT').format('YYYYMMDD HH:mm');
                        operator = '=';
                    } catch (err) {
                        bSkip = true;
                    }
                    break;
                case window.fEnums.FObjectPropertyEditorType.Date:
                    try {
                        value = moment(valueOrj, 'L').format('YYYYMMDD');
                        operator = '=';
                    } catch (err){
                        bSkip = true;
                    }
                    break;
                case window.fEnums.FObjectPropertyEditorType.Time:
                    try {
                        value = moment(valueOrj, 'LT').format('HH:mm');
                        operator = '=';
                    } catch (err) {
                        bSkip = true;
                    }
                    break;
            }
            if (!bSkip) {
                dataQuery.columnFilters.push({
                    name: columns[i].name,
                    operator: operator,
                    value: value
                })
            }
        }
    }
    var url = 'fObjectData/query?fObjectID=' + window.currentTable.fObjectID;
    for (var i = 0; i < params.length; i++)
        url += '&' + params[i].name + '=' + params[i].value;

    service.call(url, 'POST', dataQuery, false, true).then(function (result) {
        if (result && result.length) {
            var tableColumn = targetElem.data('tableColumn');
            var value = result[0][tableColumn.fObjectProperty.dbColumnName];
            if (typeof (value) != 'object') {
                switch (tableColumn.fObjectProperty.editorType) {
                    case window.fEnums.FObjectPropertyEditorType.InputNumber:
                        value = lngHelper.formatNumber(value, 0);
                        break;
                    case window.fEnums.FObjectPropertyEditorType.InputDecimal:
                        if (value != null)
                            value = value.toFixed(tableColumn.fObjectProperty.decimalScale);
                        value = lngHelper.formatNumber(value, tableColumn.fObjectProperty.decimalScale);
                        break;
                    case window.fEnums.FObjectPropertyEditorType.DateTime:
                        if (value != null)
                            value = moment(value).format('L LT');
                        break;
                    case window.fEnums.FObjectPropertyEditorType.Date:
                        if (value != null)
                            value = moment(value).format('L');
                        break;
                    case window.fEnums.FObjectPropertyEditorType.Time:
                        if (value != null)
                            value = moment(value).format('LT');
                        break;
                }
                targetElem.find('.aggregate-value').text(value);
            }
        }
    });
}
function getAggregateValueInner(aggregate, fObjectProperty, dataArray) {
    var minValue = maxValue = sum = null;

    for (var i = 0; i < dataArray.length; i++) {
        var data = dataArray[i];
        if (data == null)
            continue;
        if (minValue == null) {
            minValue = data;
            maxValue = data;
            sum = data;
        } else {
            if (minValue > data)
                minValue = data;
            if (maxValue < data)
                maxValue = data;
            sum += data;
        }
    }
    switch (aggregate.type) {
        case window.fEnums.QueryResultAggregateFunction.Count:
            return dataArray.length;
        case window.fEnums.QueryResultAggregateFunction.Sum:
            return sum;
        case window.fEnums.QueryResultAggregateFunction.Maximum:
            return maxValue;
        case window.fEnums.QueryResultAggregateFunction.Minimum:
            return minValue;
        case window.fEnums.QueryResultAggregateFunction.Average:
            return sum/dataArray.length;
    }
    return 0;
}
function getAggregateValue(aggregate, tableColumn, dataArray) {
    switch (tableColumn.fObjectProperty.editorType) {
        case window.fEnums.FObjectPropertyEditorType.InputNumber:
            for (var i = 0; i < dataArray.length; i++) {
                dataArray[i] = parseInt(dataArray[i]);
                if (isNaN(dataArray[i]))
                    dataArray[i] = 0;
            }
            break;
        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
            for (var i = 0; i < dataArray.length; i++) {
                dataArray[i] = parseFloat(dataArray[i]);
                if (isNaN(dataArray[i]))
                    dataArray[i] = 0;
            }
            break;
        case window.fEnums.FObjectPropertyEditorType.Date:
        case window.fEnums.FObjectPropertyEditorType.DateTime:
            for (var i = 0; i < dataArray.length; i++) {
                if (dataArray[i])
                    dataArray[i] = moment(dataArray[i]);
                else
                    dataArray[i] = null;
            }
            break;
        case window.fEnums.FObjectPropertyEditorType.Time:
            for (var i = 0; i < dataArray.length; i++) {
                if (dataArray[i])
                    dataArray[i] = window.moment.utc(dataArray[i], 'HH:mm');
                else
                    dataArray[i] = null;
            }
            break;
    }
    var value = getAggregateValueInner(aggregate, tableColumn.fObjectProperty, dataArray);
    switch (tableColumn.fObjectProperty.editorType) {
        case window.fEnums.FObjectPropertyEditorType.InputNumber:
            value = lngHelper.formatNumber(value, 0);
            break;
        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
            if (value != null)
                value = value.toFixed(tableColumn.fObjectProperty.decimalScale);
            value = lngHelper.formatNumber(value, tableColumn.fObjectProperty.decimalScale);
            break;
        case window.fEnums.FObjectPropertyEditorType.DateTime:
            if (value != null)
                value = moment(value).format('L LT');
            break;
        case window.fEnums.FObjectPropertyEditorType.Date:
            if (value != null)
                value = moment(value).format('L');
            break;
        case window.fEnums.FObjectPropertyEditorType.Time:
            if (value != null)
                value = moment(value).format('LT');
            break;
    }
    if (value == null)
        value = '-';
    return value;
}
function btnBProcessClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var sender = utils.getEventSender(e);
    var url = './bProcessRun.html?bProcessID=' + sender.attr('process-id');
    var urlParameters = sender.attr('url-parameters');
    if (urlParameters) {
        var urlParametersObject;
        try {
            urlParametersObject = JSON.parse(urlParameters);
        } catch (err) { }
        if (urlParametersObject) {
            for (const [key, value] of Object.entries(urlParametersObject)) {
                var paramValue = '';
                switch (key) {
                    case 'recID':
                        paramValue = selectedRow.data()[0];
                        break;
                    case 'tableID':
                        paramValue = window.currentTable.recID;
                        break;
                    case 'objectID':
                        paramValue = window.currentTable.fObjectID;
                        break;
                }
                if (paramValue)
                    url += '&' + value + '=' + paramValue;
            }
        }
    }
    utils.openPage(url);
}
async function initReportMenu() {
    var result = await service.call('fReport/getList', 'GET', { fObjectID: window.currentTable.fObjectID });
    var str = '';
    for (var i = 0; i < result.length; i++) {
        if (result[i].showOnTable)
            str += '<a class="dropdown-item" href="#" report-type="' + result[i].reportType + '" report-id="' + result[i].recID + '" onclick="btnReportClick(event)">' + result[i].displayName + '</a>';
    }
    if (str == '')
        $('#reportMenu').parents('.dropdown').hide();
    else {
        $('#reportMenu').html(str);
        $('#reportMenu').parents('.dropdown').show();
    }
}
function setSelectForCallerCallBackFunction(f) {
    window.selectForCallerCallBackFunction = f;
}
async function btnSelectForCallerClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var selectedRecordID = selectedRow.data()[0];
    if (window.bProcessRunElementID) {
        var outInfo = await service.call('fBProcessRunElement/getOutInfo', 'GET', { recID: window.bProcessRunElementID });
        if (outInfo.bProcessConnectors.length > 0) {
            var connectorID = outInfo.bProcessConnectors[0].recID;
            var runResult = await service.call('fBProcessRunElement/run', 'GET', { recID: window.bProcessRunElementID, connectorID: connectorID, selectedRecordID: selectedRecordID });
            bProcessHelper.handleBProcessRunResult(runResult);
        }
    } else {
        if (window.selectForCallerCallBackFunction)
            window.selectForCallerCallBackFunction(selectedRecordID, utils.getUrlParamList());
    }
}
function btnDeselectForCallerClick(e) {
    utils.preventClickTwice(e);
    if (window.selectForCallerCallBackFunction)
        window.selectForCallerCallBackFunction(utils.emptyGuid, utils.getUrlParamList());
}
function recordSaved() {
    if (window.addEditPopup) {
        window.addEditPopup.modal('hide');
    }
    window.dt.reload();
}
function addRelationWithExistingItemClick(e) {
    utils.preventClickTwice(e);

    var relation = $('#addRelationWithExistingItem').data('relation');
    if (relation.relationType == window.fEnums.FObjectRelationType.ManyToMany) {
        var relatedObjectID = relation.parentFObjectID;
        if (window.currentTable.fObjectID == relation.childFObjectID)
            relatedObjectID = relation.childFObjectID;

        window.selectNNRelationItem_ObjectRelation = relation;
        var addFilters = null;
        if (window.nnRelationAddFilters)
            addFilters = 'addFilters=' + window.nnRelationAddFilters;
        utils.selectObjectItemFromTable(relatedObjectID, window.currentTable.recID, selectNNRelationItemSelected, addFilters);
    } else {
        utils.selectObjectItemFromTable(relation.childFObjectID, window.currentTable.recID, select1NRelationItemSelected, 'relations={"' + relation.recID + '":"' + utils.emptyGuid + '"}');
    }
}
async function select1NRelationItemSelected(recIDArray, params) {
    if (!Array.isArray(recIDArray))
        recIDArray = [recIDArray];
    var relation = $('#addRelationWithExistingItem').data('relation');
    var relationObj = JSON.parse(window.relations);
    var serviceUrl = 'fObjectData/save?fObjectID=' + relation.childFObjectID + '&relations[' + relation.recID + ']=' + relationObj[relation.recID];
    var promiseArray = [];
    for (var i = 0; i < recIDArray.length; i++) {
        promiseArray.push(service.call(serviceUrl + '&recID=' + recIDArray[i], 'POST', {}));
    }
    Promise.all(promiseArray).then((values) => {
        window.dt.reload();
    })
}
async function selectNNRelationItemSelected(recID, params) {
    var recIDArray = recID;
    if (!Array.isArray(recIDArray))
        recIDArray = [recIDArray];
    var fObjectRelation = window.selectNNRelationItem_ObjectRelation;
    for (var i = 0; i < recIDArray.length; i++) {
        recID = recIDArray[i];
        var parentRecID = utils.emptyGuid;
        var childRecID = utils.emptyGuid;
        var relationObj = JSON.parse(window.relations);
        if (fObjectRelation.childFObjectID == window.currentTable.fObjectID) {
            parentRecID = relationObj[fObjectRelation.recID];
            childRecID = recID;
        }
        if (fObjectRelation.parentFObjectID == window.currentTable.fObjectID) {
            childRecID = relationObj[fObjectRelation.recID];
            parentRecID = recID;
        }
        var promiseArray = [];
        if (parentRecID != utils.emptyGuid && childRecID != utils.emptyGuid) {
            promiseArray.push(service.call('fObjectData/saveNNRelation?parentRecID=' + parentRecID + '&childRecID=' + childRecID, 'POST', fObjectRelation));
        }
        Promise.all(promiseArray).then((values) => {
            for (var i = 0; i < values.length; i++) {
                if (!values[i])
                    utils.showToast('error', lngHelper.get('generic.errorOccurred'));
                else {
                    if (values[i].result != '1')
                        utils.showToast('warning', lngHelper.get('tableDataList.saveNNRelation_' + values[i].message));
                }
            }
            window.dt.reload();
        });
    }
}
function removeRelationItem(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var selectedID = selectedRow.data()[0];
    utils.confirmDelete(async () => {
        var relationObj = JSON.parse(window.relations);
        for (var i = 0; i < window.objectRelations.length; i++) {
            if (relationObj[window.objectRelations[i].recID] && window.objectRelations[i].relationType == window.fEnums.FObjectRelationType.ManyToMany) {
                var parentRecID = 0;
                var childRecID = 0;
                var relationObj = JSON.parse(window.relations);
                var fObjectRelation = window.objectRelations[i];
                if (fObjectRelation.childFObjectID == window.currentTable.fObjectID) {
                    parentRecID = relationObj[fObjectRelation.recID];
                    childRecID = selectedID;
                }
                if (fObjectRelation.parentFObjectID == window.currentTable.fObjectID) {
                    childRecID = relationObj[fObjectRelation.recID];
                    parentRecID = selectedID;
                }
                await service.call('fObjectData/deleteNNRelation?parentRecID=' + parentRecID + '&childRecID=' + childRecID, 'POST', fObjectRelation);
                window.dt.reload();
            }
        }
    })
}

function download(url) {
    var form = $('<iframe style="width:0px;height:0px;border:none;position:absolute;left:-10px;top:-10px"></iframe>').attr('src', url);
    form.appendTo('body');
};

async function btnExportClick(e) {
    utils.preventClickTwice(e);
    var url = '/fObjectData/export?fTableID=' + window.currentTable.recID;
    if ((window.relations && window.relations.length > 0)) {
        var relationsParam = '';
        try {
            var obj = JSON.parse(window.relations);
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    relationsParam += '&relations[' + prop + ']=' + obj[prop];
                }
            }
            url += relationsParam;
        }
        catch { }
    }
    var params = [];
    var columns = window.dt.dtObject.init().columns;
    for (var i = 1; i < columns.length; i++) {
        params.push({
            name: 'model.columns[' + (i - 1).toString() + '].name',
            value: columns[i].name
        });
        params.push({
            name: 'model.columns[' + (i - 1).toString() + '].search.value',
            value: window.dt.dtObject.column(i).search()
        });
    }
    var orders = window.dt.dtObject.order();
    for (var i = 0; i < orders.length; i++) {
        params.push({
            name: 'model.order[' + i.toString() + '].column',
            value: orders[i][0] - 1
        });
        params.push({
            name: 'model.order[' + i.toString() + '].dir',
            value: orders[i][1]
        });
    }
    params.push({ name: 'model.start', value: 0 });
    params.push({ name: 'model.length', value: 10000 });

    if (window.currentQuery && window.currentQuery.columnFilters) {
        for (var i = 0; i < window.currentQuery.columnFilters.length; i++) {
            var columnFilter = window.currentQuery.columnFilters[i];
            params.push({
                name: 'model.query.columnFilters[' + i.toString() + '].name',
                value: columnFilter.name
            });
            params.push({
                name: 'model.query.columnFilters[' + i.toString() + '].operator',
                value: columnFilter.operator
            });
            params.push({
                name: 'model.query.columnFilters[' + i.toString() + '].value',
                value: columnFilter.value
            });
        }
    }
    for (var i = 0; i < params.length; i++)
        url += '&' + params[i].name + '=' + params[i].value;
    download(url);
}
async function btnReportClick(e) {
    var sender = utils.getEventSender(e);
    var reportID = sender.attr('report-id');
    var reportType = parseInt(sender.attr('report-type'));
    var url = '/fReport/getReportByQuery?fReportID=' + reportID + '&languageName=' + window.lngHelper.currentLanguage.name;
    if ((window.relations && window.relations.length > 0)) {
        var relationsParam = '';
        try {
            var obj = JSON.parse(window.relations);
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    relationsParam += '&relations[' + prop + ']=' + obj[prop];
                }
            }
            url += relationsParam;
        }
        catch { }
    }
    var params = [];
    var columns = window.dt.dtObject.init().columns;
    for (var i = 1; i < columns.length; i++) {
        params.push({
            name: 'model.columns[' + (i-1).toString() + '].name',
            value: columns[i].name
        });
        params.push({
            name: 'model.columns[' + (i-1).toString() + '].search.value',
            value: window.dt.dtObject.column(i).search()
        });
    }
    var orders = window.dt.dtObject.order();
    for (var i = 0; i < orders.length; i++) {
        params.push({
            name: 'model.order[' + i.toString() + '].column',
            value: orders[i][0] - 1
        });
        params.push({
            name: 'model.order[' + i.toString() + '].dir',
            value: orders[i][1]
        });
    }
    params.push({ name: 'model.start', value: 0 });
    params.push({ name: 'model.length', value: 10000 });

    if (window.currentQuery && window.currentQuery.columnFilters) {
        for (var i = 0; i < window.currentQuery.columnFilters.length; i++) {
            var columnFilter = window.currentQuery.columnFilters[i];
            params.push({
                name: 'model.query.columnFilters[' + i.toString() + '].name',
                value: columnFilter.name
            });
            params.push({
                name: 'model.query.columnFilters[' + i.toString() + '].operator',
                value: columnFilter.operator
            });
            params.push({
                name: 'model.query.columnFilters[' + i.toString() + '].value',
                value: columnFilter.value
            });
        }
    }
    for (var i = 0; i < params.length; i++)
        url += '&' + params[i].name + '=' + params[i].value;
    params = [];
    download(url);
}
$(document).on('initcompleted', function () {
    window.tableID = utils.getUrlParam('tableID');
    window.relations = utils.getUrlParam('relations');
    if (window.relations) {
        try {
            JSON.parse(window.relations);
        }
        catch (err){
            window.relations = decodeURIComponent(window.relations);
        }
    }
    window.op = utils.getUrlParam('op');
    window.addFilters = utils.getUrlParam('addFilters');
    window.fSoftDeleteStatus = utils.getUrlParam('fSoftDeleteStatus');
    window.nnRelationAddFilters = utils.getUrlParam('nnRelationAddFilters'); //IF-179
    window.bProcessRunElementID = utils.getUrlParam('bProcessRunElementID');
    window.defaultOrder = utils.getUrlParam('defaultOrder');
    if (window.defaultOrder) {
        try { window.defaultOrder = JSON.parse(window.defaultOrder); }
        catch (err){ }
    }
    var formID = utils.getUrlParam('formID');
    if (formID) {
        window.defaultForm = {
            recID: formID
        }
    }
    if (window.addFilters) {
        try {
            window.addFilters = decodeURIComponent(window.addFilters);
        } catch {
        }
        try {
            window.addFilters = JSON.parse(window.addFilters);
        } catch {
            window.addFilters = null;
        }
    }
    if (utils.getUrlParam('disabled') && utils.getUrlParam('disabled') == 'true') {
        $('.root-content').hide();
        //utils.openPopup('info', lngHelper.get('generic.info'), lngHelper.get('tableDataList.thisPartWillBeOpenedAfterSave'));
        utils.showToast('info', lngHelper.get('tableDataList.thisPartWillBeOpenedAfterSave'), { 'positionClass': 'toast-center-center' });
    } else {
        initTable();
    }
})