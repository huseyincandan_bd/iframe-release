﻿function btnAddClick() {
    window.editRecID = utils.emptyGuid;
    window.editRecOrder = 1;
    for (var i = 0; i < window.dt.dtObject.data().length; i++) {
        if (parseInt(window.dt.dtObject.data()[i][5]) >= window.editRecOrder)
            window.editRecOrder = parseInt(window.dt.dtObject.data()[i][5]) + 1;
    }
    $('#name').val('0');
    $('#displayName').val('');
    $('#editMode').val(0);
    $('#isHidden').prop('checked', false);
    $('#relation').val(utils.emptyGuid);
    relationChange();
    $('#aggregateFunctionListContainer').html('');
    $('#modalEdit').modal('show');
}
function btnSaveClick() {
    var props = {
        hidden: $('#isHidden').is(':checked'),
        aggregate: []
    };
    $.each($('#aggregateFunctionListContainer .list-item'), function(index, item) {
        props.aggregate.push({
            type: parseInt($(item).attr('faggregate-type')),
            scope: parseInt($(item).attr('faggregate-scope')),
        })
    });
    var fTableColumn = {
        RecID: window.editRecID,
        FTableID: window.fTableID,
        FObjectPropertyID: $('#name').val(),
        RecOrder: window.editRecOrder,
        DisplayName: $('#displayName').val(),
        EditMode: parseInt($('#editMode').val()),
        ObjectRelation: $('#relation').val(),
        Props: JSON.stringify(props)
    }
    if (fTableColumn.FObjectPropertyID == 0) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('tableEditColumns.selectColumn'));
        return;
    }
    if (fTableColumn.DisplayName == "")
        fTableColumn.DisplayName = $('#name option:selected').html();
    service.call('fTableColumn/save', 'POST', fTableColumn).then(function (result) {
        $('#modalEdit').modal('hide');
        window.dt.reload();
    })
}
async function btnEditClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.editRecID = selectedRow.data()[0];
    window.editRecOrder = parseInt(selectedRow.data()[5]);
    $('#relation').val(selectedRow.data()[7]);
    relationChange(selectedRow.data()[6]);
    $('#name').val(selectedRow.data()[6]);
    $('#displayName').val(selectedRow.data()[2]);
    $('#editMode').val(selectedRow.data()[4]);
    var result = await service.call('fTableColumn/get', 'GET', { recID: selectedRow.data()[0] });
    $('#isHidden').prop('checked', false);
    $('#aggregateFunctionListContainer').html('');
    if (result && result.props) {
        var props = JSON.parse(result.props);
        $('#isHidden').prop('checked', props.hidden);
        if (props.aggregate) {
            for (var i = 0; i < props.aggregate.length; i++) {
                var type = props.aggregate[i].type;
                var scope = props.aggregate[i].scope;
                var item = $('<div class="list-item" faggregate-type="' + type + '" faggregate-scope="' + scope + '"><div class="main-content">' + aggregateToString(type, scope) + '</div><div></div><button class="btn" onclick="btnDeleteAggregateFunctionClick(event)"><i class="fas fa-times"></i></button></div>');
                $('#aggregateFunctionListContainer').append(item);
            }
        }
    }

    $('#modalEdit').modal('show');
}
function aggregateToString(type, scope) {
    var str = '';
    for (const [key, value] of Object.entries(window.fEnums.QueryResultAggregateFunction)) {
        if (value == type) {
            str = lngHelper.get('tableEditColumns.aggregate_' + key);
            break;
        }
    }
    for (const [key, value] of Object.entries(window.fEnums.FTableColumnAggregateFunctionScope)) {
        if (value == scope) {
            str += ' (' + lngHelper.get('tableEditColumns.aggregateScope_' + key) + ')';
            break;
        }
    }
    return str;
}
function btnAddAggregateFunctionClick() {
    var type = parseInt($('#aggregateType').val());
    var scope = parseInt($('#aggregateScope').val());
    var item = $('<div class="list-item" faggregate-type="' + type + '" faggregate-scope="' + scope + '"><div class="main-content">' + aggregateToString(type, scope) + '</div><div></div><button class="btn" onclick="btnDeleteAggregateFunctionClick(event)"><i class="fas fa-times"></i></button></div>');
    $('#aggregateFunctionListContainer').append(item);
}
function btnDeleteAggregateFunctionClick(e){
    var sender = utils.getEventSender(e);
    sender.closest('.list-item').remove();
}

function btnDeleteClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            service.call('fTableColumn/delete', 'GET', { recID: selectedRow.data()[0] }).then(function (result) {
                window.dt.reload();
            }, function () { })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function btnMoveClick(e, moveDirection) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    service.call('fTableColumn/move', 'GET', { recID: selectedRow.data()[0], direction: moveDirection }).then(function () {
        window.dt.reload();
    })
}
function btnMoveUpClick(e) {
    btnMoveClick(e, 'up');
}
function btnMoveDownClick(e) {
    btnMoveClick(e, 'down');
}

function relationChange(currentPropertyID) {
    var relationID = $('#relation').val();
    var allOptions = $('#name').data('allOptions');
    var data = window.dt.dtObject.data();

    var options = [];
    for (var i = 0; i < allOptions.length; i++) {
        var opt = allOptions[i];
        if (opt.attributes.relationid == relationID) {
            var bFound = false;
            if (opt.value != currentPropertyID) {
                for (var j = 0; j < data.length; j++) {
                    if (data[j][7] == opt.attributes.relationid && data[j][6] == opt.value) {
                        bFound = true;
                        break;
                    }
                }
            }
            if (!bFound)
                options.push(opt);
        }
    }
    utils.fillSelectOptions($('#name'), options);
    if (relationID == utils.emptyGuid) {
        $('#editMode').prop('disabled', false);
    } else {
        $('#editMode').val('0');
        $('#editMode').prop('disabled',true);
    }
}
async function fillColumnOptions() {
    var fTable = await service.call('fTable/get', 'GET', { recID: window.fTableID });

    utils.changeTitleFromInside(fTable.displayName + ' | ' + lngHelper.get('tableEditColumns.tableEditColumns'));

    var propertyDetails = await service.call('fObject/getPropertyDetails?fObjectID=' + fTable.fObjectID, 'POST', []);

    var relations = [];
    var relOptions = [];
    var options = [];
    for (var i = 0; i < propertyDetails.length; i++) {
        var propertyDetail = propertyDetails[i];
        var property = propertyDetail.property;

        var relationID = utils.emptyGuid;
        var index = propertyDetail.propertyFullID.lastIndexOf('_');
        if (index > 0) {
            relationID = propertyDetail.propertyFullID.substring(0, index);
        }
        if (relations.indexOf(relationID) < 0) {
            relations.push(relationID);
            relOptions.push({
                text: propertyDetail.relationFullName,
                value: relationID
            })
        }
        options.push({
            text: property.displayName,
            value: property.recID,
            attributes: {
                relationid: relationID
            }
        });
    }

    utils.fillSelectOptions($('#relation'), relOptions, true);
    $('#name').data('allOptions', options);
    relationChange();
}
//function aggregateFunctionChange() {
//    var aggregateFunctionValue = 0;
//    if ($('#aggregateFunction').val())
//        aggregateFunctionValue = parseInt($('#aggregateFunction').val());
//    if (aggregateFunctionValue == window.fEnums.QueryResultAggregateFunction.ColumnValue)
//        $('#aggregateScope').closest('.form-group').hide();
//    else
//        $('#aggregateScope').closest('.form-group').show();
//}

function init() {
    window.dt = new dataTableHelper('dataTable', {
        ajax: '/fTableColumn/getListDataTable?fTableID=' + window.fTableID,
        columns: [
            { title: "ID", visible: false },
            { title: lngHelper.get('tableEditColumns.name') },
            { title: lngHelper.get('tableEditColumns.displayName') },
            {
                title: lngHelper.get('tableEditColumns.editMode'),
                render: function (data, type, row) {
                    return lngHelper.get('tableEditColumns.' + data);
                }
            },
            { title: "EditModeID", visible: false },
            { title: "RecOrder", visible: false },
            { title: "FObjectPropertyID", visible: false },
            { title: "ObjectRelation", visible: false }
        ],
        initComplete: function (settings, json) {
            $('#dataTable tbody').sortable({
                stop: async function (event, ui) {
                    var recID = window.dt.dtObject.row(ui.item[0]).data()[0];
                    var recOrder = ui.item.index() + 1 + window.dt.dtObject.page.info().start;
                    var result = await service.call('fTableColumn/moveTo', 'GET', { recID: recID, recOrder: recOrder });
                    window.dt.reload(false);
                }
            });
        },
        order: [[6, 'asc']]
    }, {
        buttons: [{
            text: lngHelper.get('generic.edit'),
            clickFunction: 'btnEditClick'
        }, {
            text: lngHelper.get('generic.delete'),
            clickFunction: 'btnDeleteClick'
        }, {
            text: lngHelper.get('tableEditColumns.moveUp'),
            clickFunction: 'btnMoveUpClick'
        }, {
            text: lngHelper.get('tableEditColumns.moveDown'),
            clickFunction: 'btnMoveDownClick'
        }],
        dblClickFunction: 'btnEditClick'
    });
}
$(document).on('initcompleted', function () {
    window.fTableID = utils.getUrlParam("fTableID");
    fillColumnOptions();
    init();
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.QueryResultAggregateFunction)) {
        if (value > 2) {
            options.push({
                value: value,
                text: lngHelper.get('tableEditColumns.aggregate_' + key)
            })
        }
    }
    utils.fillSelectOptions($('#aggregateType'), options, true);
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.FTableColumnAggregateFunctionScope)) {
        options.push({
            value: value,
            text: lngHelper.get('tableEditColumns.aggregateScope_' + key)
        })
    }
    utils.fillSelectOptions($('#aggregateScope'), options, true);

    $('#aggregateFunctionListContainer').sortable();
    $('#aggregateFunctionListContainer').disableSelection();
})