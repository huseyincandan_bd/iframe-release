﻿async function btnSaveGenericClick() {
    if (!$('#sessionTimeOutMinutes').val())
        $('#sessionTimeOutMinutes').val(20);
    var fAppSetting = {
        settingType: window.fEnums.FAppSettingType.Generic,
        settingValue: JSON.stringify({
            appName: $('#appName').val(),
            sessionTimeOutMinutes: $('#sessionTimeOutMinutes').val(),
            mainImageText: $('#mainImageText').val(),
            documentTitle: $('#documentTitle').val()
        })
    };
    if ($('#loginImage').data('changed')) {
        var settingValue = JSON.parse(fAppSetting.settingValue);
        settingValue.loginImage = $('#loginImage').css('background-image');
        fAppSetting.settingValue = JSON.stringify(settingValue);
    }
    if ($('#mainImage').data('changed')) {
        var settingValue = JSON.parse(fAppSetting.settingValue);
        settingValue.mainImage = $('#mainImage').css('background-image');
        fAppSetting.settingValue = JSON.stringify(settingValue);
    }
    if ($('#favIcon').data('changed')) {
        var settingValue = JSON.parse(fAppSetting.settingValue);
        settingValue.favIcon = $('#favIcon').css('background-image');
        fAppSetting.settingValue = JSON.stringify(settingValue);
    }
    var result = await service.call('fAppSetting/save', 'POST', fAppSetting);
    if (result) {
        utils.alertInfo(lngHelper.get('generic.saved'));
        $('#loginImage').data('changed', false);
        $('#mainImage').data('changed', false);
    }
}
async function btnSaveMailClick() {
    var fAppSetting = {
        settingType: window.fEnums.FAppSettingType.Mail,
        settingValue: JSON.stringify({
            senderAddress: $('#mailSenderAddress').val(),
            senderDisplayName: $('#mailSenderDisplayName').val(),
            password: $('#mailPassword').val(),
            host: $('#mailHost').val(),
            port: parseInt($('#mailPort').val()),
            securityOptions: parseInt($('#mailSecurityOptions').val())
        })
    }
    var result = await service.call('fAppSetting/save', 'POST', fAppSetting);
    if (result)
        utils.alertInfo(lngHelper.get('generic.saved'));
}
function btnTestMailClick() {
    $('#modalMailTest').modal('show');
}
async function btnTestMailSendClick() {
    var toAddress = $('#testToMailAddress').val();
    if (!toAddress) {
        utils.alertWarning(lngHelper.get('appSetting.testToMailAddressIsRequired'));
        return;
    }
    var sMTPSettings = {
        senderAddress: $('#mailSenderAddress').val(),
        senderDisplayName: $('#mailSenderDisplayName').val(),
        password: $('#mailPassword').val(),
        host: $('#mailHost').val(),
        port: parseInt($('#mailPort').val()),
        securityOptions: parseInt($('#mailSecurityOptions').val())
    }
    var result = await service.call('fAppSetting/testMail?toAddress=' + toAddress, 'POST', sMTPSettings);
    if (result) {
        utils.alertInfo(lngHelper.get('appSetting.testMailSuccess'));
    }
}
async function btnSaveLogClick() {
    var fAppSetting = {
        settingType: window.fEnums.FAppSettingType.Log,
        settingValue: JSON.stringify({
            dBConnectionString: $('#logConnectionString').val(),
            dBName: $('#logDBName').val(),
            dBTableName: $('#logDBTableName').val()
        })
    }
    var result = await service.call('fAppSetting/save', 'POST', fAppSetting);
    if (result)
        utils.alertInfo(lngHelper.get('generic.saved'));
}
function displayLDAPProperties() {
    var rowIndex = parseInt($('#modalLDAP').data('rowIndex'));
    var lDAPProperties = {
        "host": "",
        "port": 389,
        "domainName": "",
        "userName": "",
        "password": "",
        "dCArray": [],
        "secureSocketLayer": false,
        "maxSerchResultCount": 100,
        "autoCreateUser": false,
        "userAttributes": {
            "commonName": "cn",
            "userName": "sAMAccountName",
            "name": "givenName",
            "surname": "sn",
            "title": "title",
            "department": "department",
            "manager": "manager",
            "company": "company",
            "email": "mail",
            "phoneNumber": "telephoneNumber"
        },
        "groupAttributes": {
            "commonName": "cn"
        }
    }
    if (rowIndex != -1) {
        lDAPProperties = window.currentLDAPSetting[rowIndex];
    }
    $('#lDAPHost').val(lDAPProperties.host);
    $('#lDAPPort').val(lDAPProperties.port);
    $('#lDAPDomainName').val(lDAPProperties.domainName);
    $('#lDAPUserName').val(lDAPProperties.userName);
    $('#lDAPPassword').val(lDAPProperties.password);
    $('#lDAPDCArray').val(lDAPProperties.dCArray.join(','));
    $('#lDAPSecureSocketLayer').prop('checked', lDAPProperties.secureSocketLayer);
    $('#lDAPMaxSerchResultCount').val(lDAPProperties.maxSerchResultCount);
    $('#lDAPlDAPAutoCreateUser').prop('checked', lDAPProperties.autoCreateUser);
    $('#lDAPUserAttributesCommonName').val(lDAPProperties.userAttributes.commonName);
    $('#lDAPUserAttributesUserName').val(lDAPProperties.userAttributes.userName);
    $('#lDAPUserAttributesName').val(lDAPProperties.userAttributes.name);
    $('#lDAPUserAttributesSurname').val(lDAPProperties.userAttributes.surname);
    $('#lDAPUserAttributesTitle').val(lDAPProperties.userAttributes.title);
    $('#lDAPUserAttributesDepartment').val(lDAPProperties.userAttributes.department);
    $('#lDAPUserAttributesManager').val(lDAPProperties.userAttributes.manager);
    $('#lDAPUserAttributesCompany').val(lDAPProperties.userAttributes.company);
    $('#lDAPUserAttributesEmail').val(lDAPProperties.userAttributes.email);
    $('#lDAPUserAttributesPhoneNumber').val(lDAPProperties.userAttributes.phoneNumber);
    $('#lDAPGroupAttributesCommonName').val(lDAPProperties.groupAttributes.commonName);
}
function btnAddLDAPClick() {
    $('#modalLDAP').data('rowIndex', -1);
    displayLDAPProperties();
    $('#modalLDAP').modal('show');
}
function getLDAPPropertiesFromForm() {
    var lDAPProperties = {
        "host": $('#lDAPHost').val(),
        "port": $('#lDAPPort').val() == '' ? 0 : parseInt($('#lDAPPort').val()),
        "domainName": $('#lDAPDomainName').val(),
        "userName": $('#lDAPUserName').val(),
        "password": $('#lDAPPassword').val(),
        "dCArray": $('#lDAPDCArray').val().split(','),
        "secureSocketLayer": $('#lDAPSecureSocketLayer').prop('checked'),
        "maxSerchResultCount": $('#lDAPMaxSerchResultCount').val() == '' ? 100 : parseInt($('#lDAPMaxSerchResultCount').val()),
        "autoCreateUser": $('#lDAPlDAPAutoCreateUser').prop('checked'),
        "userAttributes": {
            "commonName": $('#lDAPUserAttributesCommonName').val(),
            "userName": $('#lDAPUserAttributesUserName').val(),
            "name": $('#lDAPUserAttributesName').val(),
            "surname": $('#lDAPUserAttributesSurname').val(),
            "title": $('#lDAPUserAttributesTitle').val(),
            "department": $('#lDAPUserAttributesDepartment').val(),
            "manager": $('#lDAPUserAttributesManager').val(),
            "company": $('#lDAPUserAttributesCompany').val(),
            "email": $('#lDAPUserAttributesEmail').val(),
            "phoneNumber": $('#lDAPUserAttributesPhoneNumber').val()
        },
        "groupAttributes": {
            "commonName": $('#lDAPGroupAttributesCommonName').val()
        }
    }
    for (var i = 0; i < lDAPProperties.dCArray.length; i++)
        lDAPProperties.dCArray[i] = lDAPProperties.dCArray[i].trim()
    return lDAPProperties;
}
function btnSaveLDAPClick() {
    var rowIndex = parseInt($('#modalLDAP').data('rowIndex'));

    var lDAPProperties = getLDAPPropertiesFromForm();
    if (rowIndex == -1)
        window.currentLDAPSetting.push(lDAPProperties);
    else
        window.currentLDAPSetting[rowIndex] = lDAPProperties;
    saveLDAPSetting();
    fillLDAPTable();
    $('#modalLDAP').modal('hide');
}
async function btnTestLDAPConnectionClick() {
    var lDAPProperties = getLDAPPropertiesFromForm();
    var url = 'ldap/checkConnection';
    var result = await service.call(url, 'POST', lDAPProperties);
    if (result)
        utils.openPopup('primary', lngHelper.get('generic.info'), lngHelper.get('appSetting.lDAPTestConnectionSuccess'));
    else
        utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('appSetting.lDAPTestConnectionUnSuccess'));
}
function btnEditLDAPClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    $('#modalLDAP').data('rowIndex', rowIndex);
    displayLDAPProperties();
    $('#modalLDAP').modal('show');
}
function btnDeleteLDAPClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    window.currentLDAPSetting.splice(rowIndex, 1);
    saveLDAPSetting();
    fillLDAPTable();
}
async function saveLDAPSetting() {
    var fAppSetting = {
        settingType: window.fEnums.FAppSettingType.LDAP,
        settingValue: JSON.stringify(window.currentLDAPSetting)
    }
    var result = await service.call('fAppSetting/save', 'POST', fAppSetting);
}
//async function btnSaveLDAPAllClick() {
//    var fAppSetting = {
//        settingType: window.fEnums.FAppSettingType.LDAP,
//        settingValue: JSON.stringify(window.currentLDAPSetting)
//    }
//    var result = await service.call('fAppSetting/save', 'POST', fAppSetting);
//    if (result)
//        utils.alertInfo(lngHelper.get('generic.saved'));
//}

function fillLDAPTable() {
    var dataSet = [];
    for (var i = 0; i < window.currentLDAPSetting.length; i++) {
        var lDAPSetting = window.currentLDAPSetting[i];
        dataSet.push([i.toString(), lDAPSetting.host, lDAPSetting.port.toString(), lDAPSetting.domainName]);
    }
    if (!window.lDAPDT) {
        window.lDAPDT = new dataTableHelper('lDAPDataTable', {
            data: dataSet,
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging: false,
            columns: [
                { title: "index", visible: false },
                { title: lngHelper.get('appSetting.lDAPHost') },
                { title: lngHelper.get('appSetting.lDAPPort') },
                { title: lngHelper.get('appSetting.lDAPDomainName') }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditLDAPClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteLDAPClick'
            }],
            dblClickFunction: 'btnEditLDAPClick'
        });
    } else
        window.lDAPDT.updateData(dataSet);
}
function imageChange(e) {
    var sender = utils.getEventSender(e);
    sender.parents('.form-group').find('.image-input-wrapper').data('changed', true);
    const files = sender[0].files;
    if (files.length > 0) {
        const reader = new FileReader();
        reader.readAsDataURL(files[0]);
        reader.onload = () => {
            sender.parents('.form-group').find('.image-input-wrapper').css('background-image', 'url("' + reader.result + '")');
        }
        reader.onerror = error => { };
    }
}
async function init() {
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.MailSecureSocketOptions)) {
        options.push({
            value: value,
            text: key
        })
    }
    utils.fillSelectOptions($('#mailSecurityOptions'), options, true);
    $('#mailSecurityOptions').val('0');
    $('#sessionTimeOutMinutes').val(20);

    window.currentLDAPSetting = [];
    var settings = await service.call('fAppSetting/getList', 'GET');
    for (var i = 0; i < settings.length; i++) {
        var setting = settings[i];
        var value = JSON.parse(setting.settingValue);
        switch (setting.settingType) {
            case window.fEnums.FAppSettingType.Mail:
                $('#mailSenderAddress').val(value.senderAddress);
                $('#mailSenderDisplayName').val(value.senderDisplayName);
                $('#mailPassword').val(value.password);
                $('#mailHost').val(value.host);
                $('#mailPort').val(value.port);
                $('#mailSecurityOptions').val(value.securityOptions);
                break;
            case window.fEnums.FAppSettingType.Generic:
                $('#appName').val(value.appName);
                $('#sessionTimeOutMinutes').val(value.sessionTimeOutMinutes);
                if (!value.mainImageText)
                    value.mainImageText = 'i-frame';
                $('#mainImageText').val(value.mainImageText);
                if (!value.documentTitle)
                    value.documentTitle = 'i-frame';
                $('#documentTitle').val(value.documentTitle);
                break;
            case window.fEnums.FAppSettingType.Log:
                $('#logConnectionString').val(value.dBConnectionString);
                $('#logDBName').val(value.dBName);
                $('#logDBTableName').val(value.dBTableName);
                break;
            case window.fEnums.FAppSettingType.LDAP:
                window.currentLDAPSetting = value;
                break;
        }
    }
    fillLDAPTable();
}

$(document).on('initcompleted', function () {
    init();
})