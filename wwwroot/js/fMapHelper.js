﻿class fMapHelper {
    constructor(mapElem, baseLayers, dataLayers, options) {
        options = Object.assign({
            crs: L.CRS.EPSG3857,
            center: [0, 0],
            zoom: 1,
            zoomControl: false,
            layers: [],
            contextmenu: true,
            //contextmenuWidth: 200,   
            contextmenuItems: this.getContentMenuItems(options.allowEdit)
        }, options)
        this.map = L.map(mapElem[0], options);
        mapElem.data('fMapHelper', this);

        L.control.zoom({
            zoomInTitle: lngHelper.get('map.zoomInTitle'),
            zoomOutTitle: lngHelper.get('map.zoomOutTitle')
        }).addTo(this.map);

        if (!baseLayers)
            baseLayers = [];

        var baseLayerControls = {};
        for (var i = 0; i < baseLayers.length; i++) {
            var layer = this.getBaseLayerObject(baseLayers[i]);

            for (const [key, value] of Object.entries(window.fEnums.FMapBaseLayers)) {
                if (value == baseLayers[i]) {
                    baseLayerControls[lngHelper.get('formDesign.mapBaseLayer_' + key)] = layer;
                }
            }
            if (i == 0)
                this.map.addLayer(layer);
        }

        var overlays = {};
        this.dataLayers = dataLayers;
        if (dataLayers) {
            //for (var i = 0; i < dataLayers.length; i++) {
            //    var url = '/fMap/wms';
            //    if (dataLayers[i].query)
            //        url += '?query=' + encodeURIComponent(JSON.stringify(dataLayers[i].query));
            //    var iframeLayer = L.tileLayer.wms(url, {
            //        layers: dataLayers[i].layerName,
            //        crs: L.CRS.EPSG3857,
            //        version: '1.3.0',
            //        opacity: 1,
            //        format: 'image/png',
            //        tileSize: 512,
            //        transparent: true
            //    }).addTo(this.map);
            //    overlays[dataLayers[i].layerName] = iframeLayer;
            //}
            for (var i = 0; i < dataLayers.length; i++) {
                var layerGroup = L.layerGroup([]).addTo(this.map);
                this.fillDataLayerGroup(dataLayers[i], layerGroup);
                overlays[dataLayers[i].displayName] = layerGroup;
            }
        }
        if ($.isEmptyObject(overlays))
            overlays = null;

        L.control.layers(baseLayerControls, overlays).addTo(this.map);

        this.map.fMapHelper = this;
        L.control.mousePosition().addTo(this.map);
        if (!options || !options.noClickFunction)
            this.addClickFunction();
    };
    async fillDataLayerGroup(dataLayer, layerGroup) {
        layerGroup.clearLayers();
        var query = dataLayer.query;
        if (!query)
            query = {};
        var result = await service.call('fMap/getGeometriesWKT?layerName=' + dataLayer.layerName, 'POST', query );
        if (result) {
            var layerProps = null;
            if (result.mapLayer && result.mapLayer.fObjectProperty && result.mapLayer.fObjectProperty.props) {
                layerProps = JSON.parse(result.mapLayer.fObjectProperty.props);
            }
            for (var i = 0; i < result.data.length; i++) {
                var row = result.data[i];
                var wkt = row[1];

                //if (wkt.startsWith("GEOMETRYCOLLECTION"))
                //    continue;

                if (wkt) {
                    try {
                        var layer = omnivore.wkt.parse(wkt);
                        var offset = [0, 0];
                        if (layerProps) {
                            var geometryType = 'Polygon';
                            for (var l in layer._layers) {
                                geometryType = layer._layers[l].feature.geometry.type;
                            }
                            switch (geometryType) {
                                case 'Point':
                                    offset = [0, 20];
                                    var icon = L.icon({
                                        iconUrl: '/img/map-icon/' + layerProps.GeometryIcon,
                                        iconSize: [layerProps.GeometryIconWidth, layerProps.GeometryIconHeight]
                                    })
                                    for (var l in layer._layers) {
                                        layer._layers[l].setIcon(icon);
                                    }
                                    break;
                                case 'LineString':
                                    layer.setStyle({
                                        color: layerProps.GeometryLineColor,
                                        weight: layerProps.GeometryLineWidth
                                    });
                                    break;
                                default:
                                    layer.setStyle({
                                        color: layerProps.GeometryOutlineColor,
                                        weight: layerProps.GeometryOutlineWidth,
                                        fillColor: layerProps.GeometryFillColor,
                                        fillOpacity: 1
                                    });
                                    break;
                            }
                        }
                        layerGroup.addLayer(layer);
                        if (layerProps.GeometryLabelPropertyID != utils.emptyGuid)
                            layer.bindTooltip(row[2], { permanent: true, direction: 'center', offset: offset });
                    } catch { }
                }
            }
        }
    }
    getMbToken() {
        return 'pk.eyJ1IjoiaHVzZXlpbmNhbmRhbiIsImEiOiJja3dhaHNlYjYwajkxMnNxbHEyOXR4Z2o5In0.vDCcKZwfcffSBsdJ7tCGgw';
    }
    getBaseLayerObject(baselayer) {
        switch (baselayer) {
            case window.fEnums.FMapBaseLayers.BingRoad:
                return L.bingLayer('AqOOPOrYeNmVRx6MOMIuiQRa7nJvb98-4krqI7J4ysT9nyWciCL4Wh7ztKEY5dsR', {
                    imagerySet: 'Road',
                    culture: (!lngHelper || !lngHelper.currentLanguage || lngHelper.currentLanguage.name == 'en' ? 'en-US' : lngHelper.currentLanguage.name)
                });
            case window.fEnums.FMapBaseLayers.BingAerialWithLabels:
                return L.bingLayer('AqOOPOrYeNmVRx6MOMIuiQRa7nJvb98-4krqI7J4ysT9nyWciCL4Wh7ztKEY5dsR', {
                    imagerySet: 'AerialWithLabels',
                    culture: (!lngHelper || !lngHelper.currentLanguage || lngHelper.currentLanguage.name == 'en' ? 'en-US' : lngHelper.currentLanguage.name)
                });
            case window.fEnums.FMapBaseLayers.BingAerialWithLabelsOnDemand:
                return L.bingLayer('AqOOPOrYeNmVRx6MOMIuiQRa7nJvb98-4krqI7J4ysT9nyWciCL4Wh7ztKEY5dsR', {
                    imagerySet: 'AerialWithLabelsOnDemand',
                    culture: (!lngHelper || !lngHelper.currentLanguage || lngHelper.currentLanguage.name == 'en' ? 'en-US' : lngHelper.currentLanguage.name)
                });
            case window.fEnums.FMapBaseLayers.BingCanvasDark:
                return L.bingLayer('AqOOPOrYeNmVRx6MOMIuiQRa7nJvb98-4krqI7J4ysT9nyWciCL4Wh7ztKEY5dsR', {
                    imagerySet: 'CanvasDark',
                    culture: (!lngHelper || !lngHelper.currentLanguage || lngHelper.currentLanguage.name == 'en' ? 'en-US' : lngHelper.currentLanguage.name)
                });
            case window.fEnums.FMapBaseLayers.OpenStreetMap:
                return L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                });
            default:
                return null;
        }
    }
    getContentMenuItems(allowEdit) {
        var items= [{
            text: lngHelper.get('map.contextGeoCode'),
            callback: this.geoCode
        }, '-', {
            text: lngHelper.get('map.directionSetStartPoint'),
            callback: this.directionSetStartPoint
        }, {
            text: lngHelper.get('map.directionSetEndPoint'),
            callback: this.directionSetEndPoint
        }];
        if (allowEdit) {
            items.push({
                text: lngHelper.get('map.editGeometryCoordinates'),
                callback: this.editGeometryCoordinates
            });
        }
        return items;
    }
    showDirection() {
        var self = this;
        if (self.directionStartPointLayer && self.directionEndPointLayer) {
            var coordinates = self.directionStartPointLayer._latlng.lng.toString() + ',' + self.directionStartPointLayer._latlng.lat.toString() + ';' + self.directionEndPointLayer._latlng.lng.toString() + ',' + self.directionEndPointLayer._latlng.lat.toString();
            var url = 'https://api.mapbox.com/directions/v5/mapbox/driving/' + coordinates + '?geometries=geojson&overview=full&access_token=' + self.getMbToken();
            $.ajax({
                'method': 'GET',
                'url': url,
                'contentType': 'application/json',
                'dataType': 'json',
                'success': function (result, status, xhr) {
                    if (!result || !result.routes || result.routes.length == 0) {
                        utils.alertDanger(lngHelper.get('map.directionNotFound'));
                        return;
                    }
                    if (self.directionLayers) {
                        for (var i = 0; i < self.directionLayers.length; i++) {
                            self.map.removeLayer(self.directionLayers[i]);
                        }
                    }
                    self.directionLayers = [];
                    for (var i = 0; i < result.routes.length; i++) {
                        self.directionLayers.push(L.geoJSON(result.routes[i].geometry).addTo(self.map));
                    }
                },
                'error': function (xhr, status, error) {
                    debugger;
                }
            });
        }
    }
    directionSetStartPoint(e) {
        var fMapHelper = this.fMapHelper;
        if (fMapHelper.directionStartPointLayer)
            fMapHelper.map.removeLayer(fMapHelper.directionStartPointLayer);
        fMapHelper.directionStartPointLayer = L.marker(e.latlng).addTo(fMapHelper.map);
        fMapHelper.showDirection();
    }
    directionSetEndPoint(e) {
        var fMapHelper = this.fMapHelper;
        if (fMapHelper.directionEndPointLayer)
            fMapHelper.map.removeLayer(fMapHelper.directionEndPointLayer);
        fMapHelper.directionEndPointLayer = L.marker(e.latlng).addTo(fMapHelper.map);
        fMapHelper.showDirection();
    }
    geoCode(e) {
        if (window.mapGeoCodePopup) {
            window.mapGeoCodePopup.modal('show');
            return;
        }
        var self = this.fMapHelper;
        var buttons = [{
                text: lngHelper.get('generic.cancel'),
                class: 'btn-secondary'
            },{
                text: lngHelper.get('generic.ok'),
                class: 'btn-primary',
                onclick: function () {
                    var searchText = window.mapGeoCodePopup.find('.geoCodeSearchText').val();
                    var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + encodeURIComponent(searchText) + '.json?access_token=' + self.getMbToken();

                    $.ajax({
                        'method': 'GET',
                        'url': url,
                        'contentType': 'application/json',
                        'dataType': 'json',
                        'success': function (result, status, xhr) {
                            if (!result || !result.features || result.features.length == 0) {
                                utils.alertDanger(lngHelper.get('map.geoCodeNotFound'));
                                return;
                            }
                            if (result.features.length > 1) {
                                var ul = $('<ul class="list-group"></ul>');
                                for (var i = 0; i < result.features.length; i++) {
                                    var li = $('<li class="list-group-item">' + utils.htmlEscape(result.features[i].place_name) + '</li>');
                                    li.data('feature', result.features[i]);
                                    li.data('fMapHelper', self);
                                    ul.append(li);
                                    li.on('click', function () {
                                        var fMapHelper = $(this).data('fMapHelper');
                                        var feature = $(this).data('feature');
                                        var center = [feature.center[1], feature.center[0]];
                                        if (fMapHelper.geoCodeMarker)
                                            fMapHelper.map.removeLayer(fMapHelper.geoCodeMarker);
                                        fMapHelper.geoCodeMarker = L.marker(center).addTo(fMapHelper.map);
                                        fMapHelper.map.setView(center, 12);
                                    })
                                }
                                window.mapGeoCodePopup.find('.geoCodeSearchResult').html('');
                                window.mapGeoCodePopup.find('.geoCodeSearchResult').append(ul);
                            }
                        },
                        'error': function (xhr, status, error) {
                            debugger;
                        }
                    });
                }
            }];
        var html = '<div><input type="text" class="geoCodeSearchText form-control"/></div><div class="geoCodeSearchResult"></div>';
        window.mapGeoCodePopup = utils.openPopup('info', lngHelper.get('map.contextGeoCode'), html, buttons, { draggable: true });
    }
    addDrawLayer(allowEdit) {
        var drawnItems = new L.FeatureGroup();
        this.drawingFeatureGroup = drawnItems;
        this.map.addLayer(drawnItems);
        if (allowEdit) {
            L.drawLocal = Object.assign(L.drawLocal, {
                draw: {
                    toolbar: {
                        actions: {
                            title: lngHelper.get('map.drawLocal_drawToolbarActionsTitle'),
                            text: lngHelper.get('map.drawLocal_drawToolbarActionsText')
                        },
                        finish: {
                            title: lngHelper.get('map.drawLocal_drawToolbarFinishTitle'),
                            text: lngHelper.get('map.drawLocal_drawToolbarFinishText')
                        },
                        undo: {
                            title: lngHelper.get('map.drawLocal_drawToolbarUndoTitle'),
                            text: lngHelper.get('map.drawLocal_drawToolbarUndoText')
                        },
                        buttons: {
                            polyline: lngHelper.get('map.drawLocal_drawToolbarButtonsPolyLine'),
                            polygon: lngHelper.get('map.drawLocal_drawToolbarButtonsPolygon'),
                            rectangle: lngHelper.get('map.drawLocal_drawToolbarButtonsRectangle'),
                            circle: lngHelper.get('map.drawLocal_drawToolbarButtonsCircle'),
                            marker: lngHelper.get('map.drawLocal_drawToolbarButtonsMarker'),
                            circlemarker: lngHelper.get('map.drawLocal_drawToolbarButtonsCircleMarker')
                        }
                    },
                    handlers: {
                        circle: {
                            tooltip: {
                                start: lngHelper.get('map.drawLocal_drawHandlersCircleTooltipStart')
                            },
                            radius: lngHelper.get('map.drawLocal_drawHandlersCircleRadius')
                        },
                        circlemarker: {
                            tooltip: {
                                start: lngHelper.get('map.drawLocal_drawHandlersCircleMarkerTooltipStart')
                            }
                        },
                        marker: {
                            tooltip: {
                                start: lngHelper.get('map.drawLocal_drawHandlersMarkerTooltipStart')
                            }
                        },
                        polygon: {
                            tooltip: {
                                start: lngHelper.get('map.drawLocal_drawHandlersPolygonTooltipStart'),
                                cont: lngHelper.get('map.drawLocal_drawHandlersPolygonTooltipCont'),
                                end: lngHelper.get('map.drawLocal_drawHandlersPolygonTooltipEnd')
                            }
                        },
                        polyline: {
                            error: lngHelper.get('map.drawLocal_drawHandlersPolylineError'),
                            tooltip: {
                                start: lngHelper.get('map.drawLocal_drawHandlersPolylineTooltipStart'),
                                cont: lngHelper.get('map.drawLocal_drawHandlersPolylineTooltipCont'),
                                end: lngHelper.get('map.drawLocal_drawHandlersPolylineTooltipEnd')
                            }
                        },
                        rectangle: {
                            tooltip: {
                                start: lngHelper.get('map.drawLocal_drawHandlersRectangleTooltipStart')
                            }
                        },
                        simpleshape: {
                            tooltip: {
                                end: lngHelper.get('map.drawLocal_drawHandlersSimpleShapeTooltipEnd')
                            }
                        }
                    }
                },
                edit: {
                    toolbar: {
                        actions: {
                            save: {
                                title: lngHelper.get('map.drawLocal_editToolbarActionsSaveTitle'),
                                text: lngHelper.get('map.drawLocal_editToolbarActionsSaveText')
                            },
                            cancel: {
                                title: lngHelper.get('map.drawLocal_editToolbarActionsCancelTitle'),
                                text: lngHelper.get('map.drawLocal_editToolbarActionsCancelText')
                            },
                            clearAll: {
                                title: lngHelper.get('map.drawLocal_editToolbarActionsClearAllTitle'),
                                text: lngHelper.get('map.drawLocal_editToolbarActionsClearAllText')
                            }
                        },
                        buttons: {
                            edit: lngHelper.get('map.drawLocal_editToolbarButtonsEdit'),
                            editDisabled: lngHelper.get('map.drawLocal_editToolbarButtonsEditDisabled'),
                            remove: lngHelper.get('map.drawLocal_editToolbarButtonsRemove'),
                            removeDisabled: lngHelper.get('map.drawLocal_editToolbarButtonsRemoveDisabled')
                        }
                    },
                    handlers: {
                        edit: {
                            tooltip: {
                                text: lngHelper.get('map.drawLocal_editHandlersEditTooltipText'),
                                subtext: lngHelper.get('map.drawLocal_editHandlersEditTooltipSubtext')
                            }
                        },
                        remove: {
                            tooltip: {
                                text: lngHelper.get('map.drawLocal_editHandlersRemoveTooltipText')
                            }
                        }
                    }
                }
            });

            var drawControl = new L.Control.Draw({
                edit: {
                    featureGroup: drawnItems
                },
                draw: {
                    polygon: {
                        allowIntersection: false,
                        showArea: true,
                    },
                    circle: false,
                    circlemarker: false
                }
            });
            this.map.addControl(drawControl);
        }

        this.map.on(L.Draw.Event.CREATED, function (event) {
            var layer = event.layer;
            drawnItems.addLayer(layer);
        });
    }
    queryClickedCoordinates(layer, latlng, query) {
        return new Promise(async (resolve, reject) => {
            var dataQuery = {};
            if (query)
                dataQuery = JSON.parse(JSON.stringify(query));
            if (!dataQuery.resultColumns)
                dataQuery.resultColumns = [];
            dataQuery.resultColumns.push({
                fObjectPropertyRecID: utils.emptyGuid,
                aggregateFunction: window.fEnums.QueryResultAggregateFunction.CountDistinct
            });
            if (!dataQuery.columnFilters)
                dataQuery.columnFilters = [];
            dataQuery.columnFilters.push({
                name: layer.fObjectProperty.recID,
                operator: 'INTERSECTS',
                value: (latlng.lng - 1e-6).toString() + ',' + (latlng.lng + 1e-6).toString() + ',' + (latlng.lat - 1e-6).toString() + ',' + (latlng.lat + 1e-6).toString()
            });
            var result = await service.call('fObjectData/query?fObjectID=' + layer.fObject.recID.toString(), 'POST', dataQuery, false, true);
            if (result && result.length > 0) {
                dataQuery.resultColumns = [];
                resolve({
                    layer: layer,
                    query: dataQuery,
                    count: parseInt(result[0].recID)
                });
            }
            else {
                resolve({
                    layer: layer,
                    query: dataQuery,
                    count: 0
                });
            }
        })
    }
    async addClickFunction() {
        this.allDataLayers = await service.call('fmap/getMapLayers');

        this.map.on('click', (e) => {
            if (this.mapDrawing)
                return;
            this.mapClickTimeout = setTimeout(() => {
                if (this.mapStopClick) {
                    this.mapStopClick = false;
                    return;
                }
                this.mapStopClick = false;
                let promises = [];
                for (var i = 0; i < this.dataLayers.length; i++) {
                    for (var j = 0; j < this.allDataLayers.length; j++) {
                        if (this.dataLayers[i].layerName == this.allDataLayers[j].layerName) {
                            promises.push(this.queryClickedCoordinates(this.allDataLayers[j], e.latlng, this.dataLayers[i].query));
                            break;
                        }
                    }
                }
                Promise.all(promises).then(async (results) => {
                    var totalResults = 0;
                    for (var i = results.length - 1; i >= 0; i--) {
                        totalResults += results[i].count;
                        if (results[i].count == 0) {
                            results.splice(i, 1);
                        }
                    }
                    if (totalResults == 0)
                        return;
                    if (totalResults == 1) {
                        var dataQuery = results[0].query;
                        dataQuery.resultColumns.push({
                            fObjectPropertyRecID: utils.emptyGuid,
                            aggregateFunction: window.fEnums.QueryResultAggregateFunction.ColumnValue
                        });
                        var result = await service.call('fObjectData/query?fObjectID=' + results[0].layer.fObject.recID, 'POST', dataQuery, false, true);
                        if (result && result.length > 0 && result[0].recID) {
                            var forms = await service.call('fForm/getList', 'GET', { fObjectID: results[0].layer.fObject.recID });
                            if (forms && forms.length > 0)
                                utils.openPage('/html/formDataEdit.html?formID=' + forms[0].recID + '&recID=' + result[0].recID);
                        }
                    } else {
                        var pages = [];
                        for (var i = 0; i < results.length; i++) {
                            pages.push({
                                url: '/html/tableDataList.html?objectID=' + results[i].layer.fObject.recID + '&addFilters=' + encodeURIComponent(JSON.stringify(results[i].query.columnFilters)),
                                title: results[i].layer.fObject.displayName + '-' + results[i].layer.fObjectProperty.displayName
                            })
                        }
                        if (pages.length == 1)
                            utils.openPage(pages[0].url);
                        else
                            utils.openPage('/html/multiPageContainer.html?title=' + encodeURIComponent(lngHelper.get('dashboard.mapClickPointFeatureInfoTitle')) + '&pages=' + encodeURIComponent(JSON.stringify(pages)));
                    }
                })
            }, 200);
        })
        this.map.on('dblclick', (e) => {
            this.mapStopClick = true;
            clearTimeout(this.mapClickTimeout);
        })
        this.map.on('draw:drawstart', () => {
            this.mapDrawing = true;
        })
        this.map.on('draw:drawstop', () => {
            this.mapDrawing = false;
        })
    }
    getDrawnGeoJSON() {
        return this.drawingFeatureGroup.toGeoJSON();
    }
    drawGeoJSON(data) {
        this.drawingFeatureGroup.addLayer(L.geoJSON(data));
        this.map.fitBounds(this.drawingFeatureGroup.getBounds());
    }
    drawWKT(wkt) {
        var layer = omnivore.wkt.parse(wkt);
        var layers = layer.getLayers();
        for (var i = 0; i < layers.length; i++) {
            this.drawingFeatureGroup.addLayer(layers[i]);
        }
        this.map.fitBounds(this.drawingFeatureGroup.getBounds());
    }
    getLayerWKT(layer) {
        var lng, lat, coords = [];
        if (layer instanceof L.Polygon || layer instanceof L.Polyline) {
            var latlngs = layer.getLatLngs();
            for (var i = 0; i < latlngs.length; i++) {
                var latlngs1 = latlngs[i];
                if (latlngs1.length) {
                    for (var j = 0; j < latlngs1.length; j++) {
                        coords.push(latlngs1[j].lng + " " + latlngs1[j].lat);
                        if (j === 0) {
                            lng = latlngs1[j].lng;
                            lat = latlngs1[j].lat;
                        }
                    }
                }
                else {
                    coords.push(latlngs[i].lng + " " + latlngs[i].lat);
                    if (i === 0) {
                        lng = latlngs[i].lng;
                        lat = latlngs[i].lat;
                    }
                }
            };
            if (layer instanceof L.Polygon) {
                return "POLYGON((" + coords.join(",") + "," + lng + " " + lat + "))";
            } else if (layer instanceof L.Polyline) {
                return "LINESTRING(" + coords.join(",") + ")";
            }
        } else if (layer instanceof L.Marker) {
            return "POINT(" + layer.getLatLng().lng + " " + layer.getLatLng().lat + ")";
        }
    }
    getWKT() {
        var layers = this.drawingFeatureGroup.getLayers();
        if (!layers || layers.length < 1)
            return '';
        if (layers[0].getLayers)
            layers = layers[0].getLayers();
        if (layers.length == 1)
            return this.getLayerWKT(layers[0]);
        else {
            var parts = [];
            for (var i = 0; i < layers.length; i++) {
                parts.push(this.getLayerWKT(layers[i]));
            }
            return 'GEOMETRYCOLLECTION (' + parts.join(',') + ')';
        }
    }
    editGeometryCoordinates() {
        window.addProjectionsToProj4();
        var fMapHelper = this.fMapHelper;
        var html = '<div class="container"><div class="header-container"><button class="btn btn-primary" onclick="editGeometryCoordinatesAddGeometryClick(event)">' + lngHelper.get('generic.add') + '</button></div><table><tbody></tbody></table></div>';
        var buttons = [{
            text: lngHelper.get('generic.ok'),
            class: 'btn-primary',
            onclick: function () {
                var geometries = fMapHelper.editGeometryCoordinatesPopup.data('geometries');
                fMapHelper.drawingFeatureGroup.clearLayers();
                for (var i = 0; i < geometries.length; i++) {
                    var layer = null;
                    var geometry = geometries[i];
                    var geomType = fMapHelper.getGeomType(geometry);
                    switch (geomType) {
                        case 'Point':
                            layer = L.marker(geometry[0]);
                            break;
                        case 'Linestring':
                            layer = L.polyline(geometry);
                            break;
                        case 'Polygon':
                            layer = L.polygon(geometry);
                            break;
                    }
                    if (layer)
                        fMapHelper.drawingFeatureGroup.addLayer(layer);
                }
                fMapHelper.editGeometryCoordinatesPopup.modal('hide');
            }
        }];
        fMapHelper.editGeometryCoordinatesPopup = utils.openPopup('', lngHelper.get('map.editGeometryCoordinates'), html, buttons, { customClass: 'editGeometryCoordinatesPopup' });
        fMapHelper.editGeometryCoordinatesPopup.data('fMapHelper', fMapHelper);

        var layers = fMapHelper.drawingFeatureGroup.getLayers();
        if (!layers || layers.length < 1)
            return '';
        if (layers[0].getLayers)
            layers = layers[0].getLayers();
        var geometries = [];
        for (var i = 0; i < layers.length; i++) {
            var layer = layers[i];
            var geometry = [];
            var latLngs = [];

            if (layer instanceof L.Marker)
                latLngs = [layer.getLatLng()];
            if (layer instanceof L.Polyline)
                latLngs = layer.getLatLngs();
            if (layer instanceof L.Polygon)
                latLngs = layer.getLatLngs()[0];

            for (var j = 0; j < latLngs.length; j++) {
                geometry.push([latLngs[j].lat, latLngs[j].lng]);
            }
            if (layer instanceof L.Polygon)
                geometry.push([latLngs[0].lat, latLngs[0].lng]);
            geometries.push(geometry);
        }
        fMapHelper.editGeometryCoordinatesPopup.data('geometries', geometries);
        fMapHelper.fillGeometryCoordinatesTable();
    }
    getGeomType(geometry) {
        var geomType = '';
        if (geometry.length == 1)
            geomType = 'Point';
        else {
            var firstCoordinate = geometry[0];
            var lastCoordinate = geometry[geometry.length - 1];
            if (firstCoordinate[0] == lastCoordinate[0] && firstCoordinate[1] == lastCoordinate[1])
                geomType = 'Polygon';
            else
                geomType = 'Linestring';
        }
        return geomType;
    }
    fillGeometryCoordinatesTable() {
        var self = this;
        var tbody = this.editGeometryCoordinatesPopup.find('tbody');
        tbody.html('');
        var geometries = this.editGeometryCoordinatesPopup.data('geometries');
        for (var i = 0; i < geometries.length; i++) {
            var geometry = geometries[i];
            var geomType = this.getGeomType(geometry)
            var length = '';
            if (geomType == 'Polygon')
                length = ' (' + (geometry.length - 1).toString() + ')';
            if (geomType == 'Linestring')
                length = ' (' + geometry.length.toString() + ')';
            geomType = lngHelper.get('map.geomType_' + geomType);
            var row = $('<tr ondblclick="btnGeometryCoordinatesEditClick(event)"><td>' + geomType + length + '</td><td><button class="btn" onclick="btnGeometryCoordinatesDeleteClick(event)"><i class="fas fa-times"></i></button></td></tr>');
            row.data('rowIndex', i);
            tbody.append(row);
        }
    }
    editGeometryCoordinatesClick(sender) {
        if (sender && sender.prop('tagName') == 'TD')
            sender = sender.parents('tr');
        var html = '<div class="container"><div class="header-container"><div><button class="btn btn-primary" onclick="editGeometryCoordinatesAddCoordinateClick(event)">' + lngHelper.get('generic.add') + '</button></div><div><select class="form-control" onchange="editGeometryCoordinatesProjectionChange()"></select></div></div><table><tbody><tr><th></th><th>Latitude</th><th>Longitude</th><th></th></tr></tbody></table></div>';
        var buttons = [{
            text: lngHelper.get('generic.ok'),
            class: 'btn-primary',
                onclick: function (e) {
                    var sender = utils.getEventSender(e);
                    var fMapHelper = sender.parents('.editGeometryCoordinatesInnerPopup').data('fMapHelper');
                    var geometries = fMapHelper.editGeometryCoordinatesPopup.data('geometries');
                    var rowIndex = fMapHelper.editGeometryCoordinatesInnerPopup.data('rowIndex');
                    var coordinates = [];
                    var projection = fMapHelper.editGeometryCoordinatesInnerPopup.find('select').val();
                    var transform = null;
                    if (projection != 'EPSG:4326') {
                        transform = proj4(proj4.defs[projection], proj4.defs['EPSG:4326']);
                    }
                    var rows = fMapHelper.editGeometryCoordinatesInnerPopup.find('tr');
                    for (var i = 1; i < rows.length; i++) {
                        var row = $(rows[i]);
                        var coordinate = [parseFloat(row.find('td:nth-child(2) input').val()), parseFloat(row.find('td:nth-child(3) input').val())];
                        if (!isNaN(coordinate[0]) && !isNaN(coordinate[1])) {
                            if (transform) {
                                var beforeCoordinate = [coordinate[1], coordinate[0]];
                                var afterCoordinate = transform.forward(beforeCoordinate);
                                coordinate = [afterCoordinate[1], afterCoordinate[0]];
                            }
                            coordinates.push(coordinate);
                        }
                    }
                    if (rowIndex == -1)
                        geometries.push(coordinates);
                    else
                        geometries[rowIndex] = coordinates;
                    fMapHelper.editGeometryCoordinatesPopup.data('geometries', geometries);
                    fMapHelper.fillGeometryCoordinatesTable();
                    fMapHelper.editGeometryCoordinatesInnerPopup.modal('hide');
            }
        }];
        var coordinates = [];
        var rowIndex = -1;
        if (sender) {
            rowIndex = sender.data('rowIndex');
            var geometries = this.editGeometryCoordinatesPopup.data('geometries');
            coordinates = geometries[rowIndex];
        }
        this.editGeometryCoordinatesInnerPopup = utils.openPopup('', lngHelper.get('map.editGeometryCoordinatesInner'), html, buttons, { customClass: 'editGeometryCoordinatesInnerPopup' });
        this.editGeometryCoordinatesInnerPopup.data('fMapHelper', this);
        this.editGeometryCoordinatesInnerPopup.data('rowIndex', rowIndex);

        for (var i = 0; i < coordinates.length; i++)
            this.editGeometryCoordinatesAddRowToInnerTable(coordinates[i]);

        this.editGeometryCoordinatesInnerPopup.find('tbody').sortable({ handle: '.sortable-handle' });

        var options = [];
        for (const [key, value] of Object.entries(proj4.defs)) {
            options.push({
                text: key,
                value: key
            })
        }
        utils.fillSelectOptions(this.editGeometryCoordinatesInnerPopup.find('select'), options, true);
    }
    editGeometryCoordinatesProjectionChange(sender) {
        var geometries = this.editGeometryCoordinatesInnerPopup.data('geometries');
        var rows = this.editGeometryCoordinatesInnerPopup.find('tr');
        var firstProjectionName = sender.data('lastSelected');
        if (!firstProjectionName)
            firstProjectionName = 'EPSG:4326';
        var secondProjectionName = sender.val();
        var firstProjection = proj4.defs[firstProjectionName];
        var secondProjection = proj4.defs[secondProjectionName];
        var transform = proj4(firstProjection, secondProjection);
        for (var i = 1; i < rows.length; i++) {
            var row = $(rows[i]);
            var coordinate = [parseFloat(row.find('td:nth-child(3) input').val()), parseFloat(row.find('td:nth-child(2) input').val())];
            if (!isNaN(coordinate[0]) && !isNaN(coordinate[1])) {
                var newCoordinate = transform.forward(coordinate);
                row.find('td:nth-child(3) input').val(newCoordinate[0]);
                row.find('td:nth-child(2) input').val(newCoordinate[1]);
            }
        }
        sender.data('lastSelected', secondProjectionName);
    }
    editGeometryCoordinatesAddRowToInnerTable(coordinate) {
        var tbody = this.editGeometryCoordinatesInnerPopup.find('tbody');
        var value1 = '';
        var value2 = ''
        if (coordinate && coordinate.length > 1) {
            value1 = 'value="' + coordinate[0] + '"';
            value2 = 'value="' + coordinate[1] + '"';
        }
        var row = $('<tr><td><i class="fas fa-ellipsis-v sortable-handle"></i></td><td><input type="number" class="form-control" ' + value1 + '></td><td><input type="number" class="form-control" ' + value2 + '></td><td><button class="btn" onclick="editGeometryCoordinatesDeleteCoordinateClick(event)"><i class="fas fa-times"></i></button></td></tr>');
        tbody.append(row);
    }
    editGeometryCoordinatesAddCoordinateClick(sender) {
        this.editGeometryCoordinatesAddRowToInnerTable();
    }
    editGeometryCoordinatesDeleteCoordinateClick(sender) {
        sender.parents('tr').remove();
    }
    deleteGeometryCoordinatesClick(sender) {
        var rowIndex = sender.parents('tr').data('rowIndex');
        var geometries = sender.parents('.editGeometryCoordinatesPopup').data('geometries');
        var fMapHelper = sender.parents('.editGeometryCoordinatesPopup').data('fMapHelper');
        geometries.splice(rowIndex, 1);
        sender.parents('.editGeometryCoordinatesPopup').data('geometries', geometries);
        fMapHelper.fillGeometryCoordinatesTable();
    }
}
window.btnGeometryCoordinatesEditClick = function (e) {
    var sender = utils.getEventSender(e);
    var fMapHelper = sender.parents('.editGeometryCoordinatesPopup').data('fMapHelper');
    fMapHelper.editGeometryCoordinatesClick(sender);
}
window.btnGeometryCoordinatesDeleteClick = function (e) {
    var sender = utils.getEventSender(e);
    var fMapHelper = sender.parents('.editGeometryCoordinatesPopup').data('fMapHelper');
    fMapHelper.deleteGeometryCoordinatesClick(sender);
}
window.editGeometryCoordinatesAddCoordinateClick = function (e) {
    var sender = utils.getEventSender(e);
    var fMapHelper = sender.parents('.editGeometryCoordinatesInnerPopup').data('fMapHelper');
    fMapHelper.editGeometryCoordinatesAddCoordinateClick(sender);
}
window.editGeometryCoordinatesDeleteCoordinateClick = function (e) {
    var sender = utils.getEventSender(e);
    var fMapHelper = sender.parents('.editGeometryCoordinatesInnerPopup').data('fMapHelper');
    fMapHelper.editGeometryCoordinatesDeleteCoordinateClick(sender);
}
window.editGeometryCoordinatesAddGeometryClick = function (e) {
    var sender = utils.getEventSender(e);
    var fMapHelper = sender.parents('.editGeometryCoordinatesPopup').data('fMapHelper');
    fMapHelper.editGeometryCoordinatesClick(null);
}
window.editGeometryCoordinatesProjectionChange = function (e) {
    var sender = utils.getEventSender(e);
    var fMapHelper = sender.parents('.editGeometryCoordinatesInnerPopup').data('fMapHelper');
    fMapHelper.editGeometryCoordinatesProjectionChange(sender);
}
window.addProjectionsToProj4 = function(){
    if (window.proj4 && !window.proj4ProjectionsAdded) {
        window.proj4.defs('EPSG:2319', 'PROJCS["ED50 / TM27",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",27],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2319"]]');
        window.proj4.defs('EPSG:2320', 'PROJCS["ED50 / TM30",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",30],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2320"]]');
        window.proj4.defs('EPSG:2321', 'PROJCS["ED50 / TM33",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",33],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2321"]]');
        window.proj4.defs('EPSG:2322', 'PROJCS["ED50 / TM36",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",36],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2322"]]');
        window.proj4.defs('EPSG:2323', 'PROJCS["ED50 / TM39",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",39],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2323"]]');
        window.proj4.defs('EPSG:2324', 'PROJCS["ED50 / TM42",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",42],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2324"]]');
        window.proj4.defs('EPSG:2325', 'PROJCS["ED50 / TM45",GEOGCS["ED50",DATUM["European_Datum_1950",SPHEROID["International 1924",6378388,297,AUTHORITY["EPSG","7022"]],TOWGS84[-87,-98,-121,0,0,0,0],AUTHORITY["EPSG","6230"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4230"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",45],PARAMETER["scale_factor",1],PARAMETER["false_easting",500000],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2325"]]');
        window.proj4.defs('SR-ORG:7931', '+proj=tmerc +lat_0=0 +lon_0=27 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4.defs('SR-ORG:7932', '+proj=tmerc +lat_0=0 +lon_0=30 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4.defs('SR-ORG:7933', '+proj=tmerc +lat_0=0 +lon_0=33 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4.defs('SR-ORG:7934', '+proj=tmerc +lat_0=0 +lon_0=36 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4.defs('SR-ORG:7935', '+proj=tmerc +lat_0=0 +lon_0=39 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4.defs('SR-ORG:7936', '+proj=tmerc +lat_0=0 +lon_0=42 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4.defs('SR-ORG:7937', '+proj=tmerc +lat_0=0 +lon_0=45 +k=1 +x_0=500000 +y_0=0 +ellps=GRS80 +units=m +no_defs');
        window.proj4ProjectionsAdded = true;
    }
}
