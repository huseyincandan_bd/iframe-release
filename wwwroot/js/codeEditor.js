﻿var langModelObj = {
    defaultToken: '',
    tokenPostfix: '.cs',

    brackets: [
        { open: '{', close: '}', token: 'delimiter.curly' },
        { open: '[', close: ']', token: 'delimiter.square' },
        { open: '(', close: ')', token: 'delimiter.parenthesis' },
        { open: '<', close: '>', token: 'delimiter.angle' }
    ],

    keywords: [
        'extern', 'alias', 'using', 'bool', 'decimal', 'sbyte', 'byte', 'short',
        'ushort', 'int', 'uint', 'long', 'ulong', 'char', 'float', 'double',
        'object', 'dynamic', 'string', 'assembly', 'is', 'as', 'ref',
        'out', 'this', 'base', 'new', 'typeof', 'void', 'checked', 'unchecked',
        'default', 'delegate', 'var', 'const', 'if', 'else', 'switch', 'case',
        'while', 'do', 'for', 'foreach', 'in', 'break', 'continue', 'goto',
        'return', 'throw', 'try', 'catch', 'finally', 'lock', 'yield', 'from',
        'let', 'where', 'join', 'on', 'equals', 'into', 'orderby', 'ascending',
        'descending', 'select', 'group', 'by', 'namespace', 'partial', 'class',
        'field', 'event', 'method', 'param', 'property', 'public', 'protected',
        'internal', 'private', 'abstract', 'sealed', 'static', 'struct', 'readonly',
        'volatile', 'virtual', 'override', 'params', 'get', 'set', 'add', 'remove',
        'operator', 'true', 'false', 'implicit', 'explicit', 'interface', 'enum',
        'null', 'async', 'await', 'fixed', 'sizeof', 'stackalloc', 'unsafe', 'nameof',
        'when'
    ],

    namespaceFollows: [
        'namespace', 'using',
    ],

    parenFollows: [
        'if', 'for', 'while', 'switch', 'foreach', 'using', 'catch', 'when'
    ],

    operators: [
        '=', '??', '||', '&&', '|', '^', '&', '==', '!=', '<=', '>=', '<<',
        '+', '-', '*', '/', '%', '!', '~', '++', '--', '+=',
        '-=', '*=', '/=', '%=', '&=', '|=', '^=', '<<=', '>>=', '>>', '=>'
    ],

    symbols: /[=><!~?:&|+\-*\/\^%]+/,

    // escape sequences
    escapes: /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,

    // The main tokenizer for our languages 
    tokenizer: {
        root: [

            // identifiers and keywords 
            [/\@?[a-zA-Z_]\w*/, {
                cases: {
                    '@namespaceFollows': { token: 'keyword.$0', next: '@namespace' },
                    '@keywords': { token: 'keyword.$0', next: '@qualified' },
                    '@default': { token: 'identifier', next: '@qualified' }
                }
            }],

            // whitespace
            { include: '@whitespace' },

            // delimiters and operators
            [/}/, {
                cases: {
                    '$S2==interpolatedstring': { token: 'string.quote', next: '@pop' },
                    '$S2==litinterpstring': { token: 'string.quote', next: '@pop' },
                    '@default': '@brackets'
                }
            }],
            [/[{}()\[\]]/, '@brackets'],
            [/[<>](?!@symbols)/, '@brackets'],
            [/@symbols/, {
                cases: {
                    '@operators': 'delimiter',
                    '@default': ''
                }
            }],


            // numbers
            [/[0-9_]*\.[0-9_]+([eE][\-+]?\d+)?[fFdD]?/, 'number.float'],
            [/0[xX][0-9a-fA-F_]+/, 'number.hex'],
            [/0[bB][01_]+/, 'number.hex'], // binary: use same theme style as hex
            [/[0-9_]+/, 'number'],

            // delimiter: after number because of .\d floats
            [/[;,.]/, 'delimiter'],

            // strings
            [/"([^"\\]|\\.)*$/, 'string.invalid'],  // non-teminated string
            [/"/, { token: 'string.quote', next: '@string' }],
            [/\$\@"/, { token: 'string.quote', next: '@litinterpstring' }],
            [/\@"/, { token: 'string.quote', next: '@litstring' }],
            [/\$"/, { token: 'string.quote', next: '@interpolatedstring' }],

            // characters
            [/'[^\\']'/, 'string'],
            [/(')(@escapes)(')/, ['string', 'string.escape', 'string']],
            [/'/, 'string.invalid']
        ],

        qualified: [
            [/[a-zA-Z_][\w]*/, {
                cases: {
                    '@keywords': { token: 'keyword.$0' },
                    '@default': 'identifier'
                }
            }],
            [/\./, 'delimiter'],
            ['', '', '@pop'],
        ],

        namespace: [
            { include: '@whitespace' },
            [/[A-Z]\w*/, 'namespace'],
            [/[\.=]/, 'delimiter'],
            ['', '', '@pop'],
        ],

        comment: [
            [/[^\/*]+/, 'comment'],
            // [/\/\*/,    'comment', '@push' ],    // no nested comments :-( 
            ['\\*/', 'comment', '@pop'],
            [/[\/*]/, 'comment']
        ],

        string: [
            [/[^\\"]+/, 'string'],
            [/@escapes/, 'string.escape'],
            [/\\./, 'string.escape.invalid'],
            [/"/, { token: 'string.quote', next: '@pop' }]
        ],

        litstring: [
            [/[^"]+/, 'string'],
            [/""/, 'string.escape'],
            [/"/, { token: 'string.quote', next: '@pop' }]
        ],

        litinterpstring: [
            [/[^"{]+/, 'string'],
            [/""/, 'string.escape'],
            [/{{/, 'string.escape'],
            [/}}/, 'string.escape'],
            [/{/, { token: 'string.quote', next: 'root.litinterpstring' }],
            [/"/, { token: 'string.quote', next: '@pop' }]
        ],

        interpolatedstring: [
            [/[^\\"{]+/, 'string'],
            [/@escapes/, 'string.escape'],
            [/\\./, 'string.escape.invalid'],
            [/{{/, 'string.escape'],
            [/}}/, 'string.escape'],
            [/{/, { token: 'string.quote', next: 'root.interpolatedstring' }],
            [/"/, { token: 'string.quote', next: '@pop' }]
        ],

        whitespace: [
            [/^[ \t\v\f]*#((r)|(load))(?=\s)/, 'directive.csx'],
            [/^[ \t\v\f]*#\w.*$/, 'namespace.cpp'],
            [/[ \t\v\f\r\n]+/, ''],
            [/\/\*/, 'comment', '@comment'],
            [/\/\/.*$/, 'comment'],
        ],
    },
};

require.config({ paths: { 'vs': '../../lib/monaco-editor/min/vs' } });

require(["vs/editor/editor.main"], function () {
    monaco.languages.registerCompletionItemProvider('csharp', getcsharpCompletionProvider(monaco));

    monaco.languages.setLanguageConfiguration('csharp', {
        indentationRules: {
            decreaseIndentPattern: /^((?!.*?\/\*).*\*\/)?\s*[\}\]\)].*$/,
            increaseIndentPattern: /^((?!\/\/).)*(\{[^}"'`]*|\([^)"'`]*|\[[^\]"'`]*)$/
        }
    });

    window.editorObj = monaco.editor.create(document.getElementById('monaco'), {
        value: '',
        language: 'csharp',
        suggestOnTriggerCharacters: true,
        //theme: 'vs-dark',
        minimap: {
            enabled: false
        },
        formatOnType: true,
        formatOnPaste: true
    });
    window.editorObj.updateOptions({
        "autoIndent": true,
        "formatOnPaste": true,
        "formatOnType": true
    });

    window.editorObj.addAction({
        id: "myToggleLineComment",
        label: "Toggle Line Comment",
        keybindings: [monaco.KeyMod.CtrlCmd | monaco.KeyCode.US_SLASH],
        contextMenuGroupId: "9_cutcopypaste",
        run: (editor) => {
            editor.trigger(window.editorObj.getModel().getValue(), 'editor.action.commentLine');
        }
    });

    //initCode();

    var langModel = monaco.editor.createModel("return " + JSON.stringify(langModelObj), 'javascript');

    monaco.editor.setModelLanguage(window.editorObj.getModel(), window.codeLanguage);

    monaco.languages.registerDocumentFormattingEditProvider('csharp', {
        provideDocumentFormattingEdits: async function (model, options, token) {
            $('#codeformat').text('code Formatting.');
            return new Promise(async function (resolve, reject) {
                var obj = { SourceCode: model.getValue(), Nuget: $('#nugetPacks').val(), lineNumberOffsetFromTemplate: 0 };
                var result = await service.call('FCodeEditor/formatCode', 'POST', obj, false, false, { dataType: false, onError: function (xhr, status, error) { } });
                var obj = [
                    {
                        range: {
                            startLineNumber: 1,
                            startColumn: 1,
                            endLineNumber: window.editorObj.getModel().getLineCount(),
                            endColumn: 100
                        },
                        text: result
                    }
                ];
                resolve(obj);
            })
            //return new Promise((resolve, reject) => {
            //    $.ajax({
            //        url: '/MonacoEditor/formatCode',
            //        data: JSON.stringify(obj),
            //        type: 'POST',
            //        traditional: true,
            //        contentType: 'application/json',
            //        success: function (data) {
            //            var obj = [
            //                {
            //                    range: {
            //                        startLineNumber: 1,
            //                        startColumn: 1,
            //                        endLineNumber: ed.getModel().getLineCount(),
            //                        endColumn: 100
            //                    },
            //                    text: data
            //                }
            //            ];
            //            $('#codeformat').text('code Formatted.');
            //            debugger;
            //            resolve(obj);
            //        },
            //        error: function (error) {
            //            console.log(error)
            //        },
            //    })
            //})
        }
    });
});

function getcsharpCompletionProvider(monaco) {
    return {
        triggerCharacters: ['.', '(', ' ', ',', ')'],
        provideCompletionItems: function (model, position) {

            var textUntilPosition = model.getValueInRange({ startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column });
            var cursor = textUntilPosition.length;

            var obj = { SourceCode: model.getValue(), Nuget: '', lineNumberOffsetFromTemplate: cursor };

            return new Promise((resolve, reject) => {

                if (window.csharpCompletionTimeout)
                    window.clearTimeout(window.csharpCompletionTimeout);
                window.csharpCompletionTimeout = setTimeout(() => {
                    $.ajax({
                        url: '/FCodeEditor/resolve',
                        data: JSON.stringify(obj),
                        type: 'POST',
                        traditional: true,
                        contentType: 'application/json',
                        success: function (data) {
                            var availableResolvers = [];
                            if (data && data.items) {
                                for (var i = 0; i < data.items.length; i++) {
                                    if (data.items[i].properties.symbolName) {
                                        var ob = {
                                            label: data.items[i].properties.symbolName,
                                            insertText: data.items[i].properties.symbolName,
                                            kind: data.items[i].properties.symbolKind,// monaco.languages.CompletionItemKind.Property,  
                                            detail: data.items[i].tags[0],
                                            documentation: data.items[i].tags[1]
                                        };
                                        availableResolvers.push(ob);
                                    } else {
                                        var obj = {
                                            label: data.items[i].displayText,
                                            insertText: data.items[i].displayText,
                                            kind: monaco.languages.CompletionItemKind.Property,
                                            detail: data.items[i].displayText,

                                        };
                                        availableResolvers.push(obj);
                                    }
                                }
                                availableResolvers = {
                                    suggestions: availableResolvers
                                };
                                resolve(availableResolvers);
                            } else {
                                $('#buildstatus').text(data);
                            }
                        },
                        error: function (error) {
                            console.log(error)
                        },
                    })

                }, 500)
            })
        }
    };
}

//function getcsharpCompletionProvider_Orjinal(monaco) {
//    return {
//        triggerCharacters: ['.', ';', '(', ' ', ',', ')'],
//        provideCompletionItems: function (model, position) {

//            var textUntilPosition = model.getValueInRange({ startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column });
//            var cursor = textUntilPosition.length;

//            var obj = { SourceCode: model.getValue(), Nuget: '', lineNumberOffsetFromTemplate: cursor };

//            return new Promise((resolve, reject) => {
//                $.ajax({
//                    url: '/FCodeEditor/resolve',
//                    data: JSON.stringify(obj),
//                    type: 'POST',
//                    traditional: true,
//                    contentType: 'application/json',
//                    success: function (data) {
//                        var availableResolvers = [];
//                        if (data && data.items) {
//                            for (var i = 0; i < data.items.length; i++) {
//                                if (data.items[i].properties.symbolName) {
//                                    var ob = {
//                                        label: data.items[i].properties.symbolName,
//                                        insertText: data.items[i].properties.symbolName,
//                                        kind: data.items[i].properties.symbolKind,// monaco.languages.CompletionItemKind.Property,
//                                        detail: data.items[i].tags[0],
//                                        documentation: data.items[i].tags[1]
//                                    };
//                                    availableResolvers.push(ob);
//                                } else {
//                                    var obj = {
//                                        label: data.items[i].displayText,
//                                        insertText: data.items[i].displayText,
//                                        kind: monaco.languages.CompletionItemKind.Property,
//                                        detail: data.items[i].displayText,

//                                    };
//                                    availableResolvers.push(obj);
//                                }
//                            }
//                            availableResolvers = {
//                                suggestions: availableResolvers
//                            };
//                            resolve(availableResolvers);
//                        } else {
//                            $('#buildstatus').text(data);
//                        }
//                    },
//                    error: function (error) {
//                        console.log(error)
//                    },
//                })
//            })
//        }
//    };
//}

async function btnCompileClick(obj) {
    var obj = getModelValue();
    btnSaveClick();
    var result = await service.call('fCodeEditor/compile', 'POST', obj, false, false, { dataType: false, onError: function (xhr, status, error) { } });
    if (result.length == 0)
        utils.showToast('success', lngHelper.get('generic.compiledSuccessfully'));
    else {
        var errorExists = false;
        var ul = $(document.createElement('UL'));
        for (var i = 0; i < result.length; i++) {
            var line = result[i];
            var li = $(document.createElement('LI'));
            li.html(line);
            ul.append(li);
            if (line.indexOf('hidden CS8019') < 0 && line.indexOf(': warning') < 0)
                errorExists = true;
        }
        utils.openPopup((errorExists ? 'danger' : 'warning'), (errorExists ? lngHelper.get('generic.error') : lngHelper.get('generic.warning')), ul.html(), null, { size: 'lg' });
    }
}
function getModelValue() {
    return { SourceCode: window.editorObj.getModel().getValue(), Nuget: '', lineNumberOffsetFromTemplate: 0, DynamicAssemblyID: window.dynamicAssemblyID, IsScript:window.isScript };
}
function getLastLine() {
    return window.editorObj.getModel().getLineCount();
}
function getLastLineContent() {
    return window.editorObj.getModel().getLineContent(window.editorObj.getModel().getLineCount());
}
function insertTextIntoLine(insertText, lineNum) {
    var line = window.editorObj.getPosition();
    var range = new monaco.Range(lineNum, lineNum, lineNum, lineNum);
    var id = { major: 1, minor: 1 };
    var text = insertText;
    var op = { identifier: id, range: range, text: text, forceMoveMarkers: true };
    window.editorObj.executeEdits("source", [op]);
}
function waitForEditor() {
    return new Promise((resolve, reject) => {
        if (window.editorObj)
            resolve('');
        var editorWaitInterval = setInterval(function () {
            if (window.editorObj) {
                clearInterval(editorWaitInterval);
                resolve('');
            }
        }, 100);
    })
}
function format() {
    window.editorObj.trigger(window.editorObj.getModel().getValue(), 'editor.action.formatDocument');
}
function getCode() {
    return window.editorObj.getModel().getValue();
}
async function setCode(code) {
    if (!code)
        code = '';
    await waitForEditor();
    window.editorObj.getModel().setValue(code);
}
function btnSaveClick(close) {
    if (!window.fOpenerWindow || !window.fOpenerWindow.codeEditorSaveClick) {
        utils.alertDanger(lngHelper.get('generic.error'), lngHelper.get('codeEditor.couldNotFindOpener'));
        return;
    }
    window.fOpenerWindow.codeEditorSaveClick(getCode());
    if (close) {
        utils.closePageFromInside();
    }
}
async function initCode() {
    await setCode(window.fOpenerWindow.currentCodeEditorContent);
    if (utils.getUrlParam('formatOnLoad')) {
        setTimeout(() => {
            format();
        }, 500);
    }
}
$(document).on('initcompleted', function () {
    window.dynamicAssemblyID = utils.getUrlParam('dynamicAssemblyID');
    if (!window.dynamicAssemblyID)
        window.dynamicAssemblyID == utils.emptyGuid;
    window.isScript = utils.getUrlParam('isScript');
    if (window.isScript == undefined)
        window.isScript = true;
    if (window.isScript == '1' || window.isScript == 'true')
        window.isScript = true;
    else
        window.isScript = false;
    window.codeLanguage = utils.getUrlParam('language');
    if (!window.codeLanguage)
        window.codeLanguage = 'csharp';
    if (window.codeLanguage == 'csharp')
        $('#btnCompile').show();
    initCode();
})
