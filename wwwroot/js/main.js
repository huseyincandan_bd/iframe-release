﻿function preventParentPopupOnThisPage() {
	return true;
}
async function logout() {
	var result = await service.call('fSession/logout', 'GET', {});
	location.href = 'login.html';
}
function openUserProfile() {
	$('#mainFrame').attr('src', 'userInfo.html?userID=' + window.currentSession.user.recID);
}
function getMenuHtml(item, level) {
	switch (level) {
		case 0:
			var html = '';
			for (var i = 0; i < item.length; i++)
				html += getMenuHtml(item[i], 1);
			return html;
		case 1:
			var html = '<li class="menu-section"><h4 class="menu-text">' + item.displayName.toLocaleUpperCase(lngHelper.currentLanguage.name) + '</h4><i class="menu-icon ki ki-bold-more-hor icon-md"></i></li>';
			for (var i = 0; i < item.children.length; i++)
				html += getMenuHtml(item.children[i], 2);
			html += '</ul></div></li>';
			return html;
		case 2:
			var html = '<li class="menu-item menu-item-submenu" aria-haspopup="true" data-menu-toggle="hover">\
									<a href="javascript:;" class="menu-link menu-toggle">\
										<i class="menu-item-icon ' + item.iconName + '"></i>\
										<span class="menu-text">' + item.displayName + '</span>\
										<i class="menu-arrow"></i>\
									</a>\
									<div class="menu-submenu">\
										<i class="menu-arrow"></i>\
										<ul class="menu-subnav">\
											<li class="menu-item menu-item-parent" aria-haspopup="true">\
												<span class="menu-link">\
													<span class="menu-text">' + item.displayName + '</span>\
												</span>\
											</li>'
			for (var i = 0; i < item.children.length; i++)
				html += getMenuHtml(item.children[i], 3);
			html += '</ul></div></li>';
			return html;
		case 3:
			var targetUrl = item.targetUrl;
			if (item.props) {
				var props = JSON.parse(item.props);
				if (props.targetParam) {
					if (targetUrl.indexOf('?') > 0)
						targetUrl += '&';
					else
						targetUrl += '?';
					targetUrl += props.targetParam;
				}
			}
			return '<li class="menu-item" aria-haspopup="true">\
												<a class="menu-link" targetUrl="' + encodeURI(targetUrl) + '" targetPage="' + item.targetPage + '" onclick="menuClick(event)">\
													<i class="menu-bullet menu-bullet-dot">\
														<span></span>\
													</i>\
													<span class="menu-text">' + item.displayName + '</span>\
												</a>\
											</li>';
		default:
			return '';
	}
}
function menuClick(e) {
	if (!e)
		e = window.event;
	var sender = e.srcElement || e.target;
	while (sender && sender.nodeName.toLowerCase() != "a")
		sender = sender.parentNode;
	var targetUrl = $(sender).attr('targetUrl');
	if (!targetUrl)
		return;
	var targetPage = $(sender).attr('targetPage');
	switch (targetPage) {
		case '_blank':
			window.open(targetUrl);
			break;
		default:
			//$('#pageTitle').text($(sender).text());
			$('#mainFrame').attr('src', targetUrl);
			break;
	}
}
function getAuthorizationStatus(authDescription) {
	if (window.currentSession.isSystemAdmin)
		return 1;
	for (var i = 0; i < window.authorizationObjectTypes.length; i++) {
		if (window.authorizationObjectTypes[i].description == authDescription) {
			var authObjectID = window.authorizationObjectTypes[i].id;
			for (var j = 0; j < window.authorizationList.length; j++) {
				if (window.authorizationList[j].authObject == authObjectID) {
					return window.authorizationList[j].authStatus;
					break;
				}
			}
			break;
		}
	}
	return 0;
}
async function initDashboardMenu(bOpen) {
	var dashboards = await service.call('fDashboard/getSimpleList', 'GET', {});
	var str = '';
	for (var i = 0; i < dashboards.length; i++) {
		str += '<a href="#" class="navi-item" targetUrl="dashboard.html?recID=' + dashboards[i].recID + '" targetPage="_inside" onclick="menuClick(event)">\
								<div class="navi-link rounded">\
									<div class="symbol symbol-40 mr-3">\
										<div class="symbol-label">\
											<i class="far fa-chart-bar" style="color:#3b9bfe"></i>\
										</div>\
									</div>\
									<div class="navi-text">\
										<div class="font-weight-bold font-size-lg">' + dashboards[i].displayName + '</div>\
									</div>\
								</div>\
							</a>';
	}
	if (str != '')
		str += '<hr/>';
	str += '<a href="#" class="navi-item" targetUrl="dashboard.html" targetPage="_inside" onclick="menuClick(event)">\
								<div class="navi-link rounded">\
									<div class="symbol symbol-40 mr-3">\
										<div class="symbol-label">\
											<i class="fas fa-plus" style="color:#3b9bfe"></i>\
										</div>\
									</div>\
									<div class="navi-text">\
										<div class="font-weight-bold font-size-lg">' + lngHelper.get('main.createDashboard') + '</div>\
									</div>\
								</div>\
							</a>';
	$('#dashboardContainer').html(str);
	if (bOpen && dashboards && dashboards.length > 0) {
		$('#mainFrame').attr('src', './dashboard.html?recID=' + dashboards[0].recID);
	}
}
async function initAdminMenu() {
	window.authorizationList = await service.call('fAuthorization/getListByUser', 'GET', { authObject: 0 });
	window.authorizationObjectTypes = await service.call('fAuthorization/getAuthorizationObjectTypes', 'GET', { category: 'system' });

	var admin = {
		object: getAuthorizationStatus('object_create'),
		list: getAuthorizationStatus('optionList_edit'),
		businessProcess: getAuthorizationStatus('businessprocess_edit'),
		menu: getAuthorizationStatus('menu_create'),
		company: getAuthorizationStatus('company_create') || getAuthorizationStatus('company_edit') || getAuthorizationStatus('company_delete'),
		repository: getAuthorizationStatus('repository_edit'),
		userTitle: getAuthorizationStatus('userTitle_edit'),
		user: getAuthorizationStatus('user_create') || getAuthorizationStatus('user_edit') || getAuthorizationStatus('user_delete'),
		userGroup: getAuthorizationStatus('usergroup_create') || getAuthorizationStatus('usergroup_edit') || getAuthorizationStatus('usergroup_delete'),
		authorization: getAuthorizationStatus('authorization_edit'),
		log: getAuthorizationStatus('log_view'),
		trash: getAuthorizationStatus('trash'),
		appSettings: getAuthorizationStatus('appSettings_edit'),
		apiToken: window.getAuthorizationStatus('apiToken_edit'),
		externalDatabase: getAuthorizationStatus('externalDatabase_edit'),
		dynamicAssembly: getAuthorizationStatus('dynamicassembly_edit'),
		scheduledTask: getAuthorizationStatus('scheduledtask_edit'),
		module: getAuthorizationStatus('module_edit'),
		apiDocumentation: getAuthorizationStatus('apiDocumentation_view')
	}
	//admin.generic = admin.object || admin.list || admin.businessProcess || admin.menu || admin.externalDatabase || admin.scheduledTask || admin.dynamicAssembly || admin.module;
	//admin.definitions = admin.company || admin.repository;
	//admin.system = admin.log || admin.trash || admin.appSettings;

	//admin.all = admin.generic || admin.definitions || admin.system || admin.apiToken;
	//if (admin.all)
	//	admin.system = true;

	if (!admin.object) $('#adminObject').hide();
	if (!admin.list) $('#adminList').hide();
	if (!admin.businessProcess) $('#adminBusinessProcess').hide();
	if (!admin.menu) $('#adminMenu').hide();
	if (!admin.company) $('#adminCompany').hide();
	if (!admin.repository) $('#adminRepository').hide();
	if (!admin.userTitle) $('#adminUserTitle').hide();
	if (!admin.user) $('#adminUser').hide();
	if (!admin.userGroup) $('#adminUserGroup').hide();
	if (!admin.authorization) $('#adminAuthorization').hide();
	if (!admin.log) $('#adminLog').hide();
	if (!admin.trash) $('#adminTrash').hide();
	if (!admin.appSettings) $('#adminAppSettings').hide();
	if (!admin.apiToken) $('#adminApiToken').hide();
	if (!admin.externalDatabase) $('#adminExternalDatabase').hide();
	if (!admin.dynamicAssembly) $('#adminDynamicAssembly').hide();
	if (!admin.scheduledTask) $('#adminScheduledTask').hide();
	if (!admin.module) $('#adminModule').hide();
	if (!admin.apiDocumentation) $('#apiDocumentation').hide();

	$('#kt_quick_panel .nav-link[href*=adminGeneric]').closest('.nav-item').hide();
	$('#adminGeneric .navi-item').each(function (index) {
		if ($(this).css('display') != 'none') {
			$('#kt_quick_panel .nav-link[href*=adminGeneric]').closest('.nav-item').show();
			return false;
		}
	})
	$('#kt_quick_panel .nav-link[href*=adminDefinitions]').closest('.nav-item').hide();
	$('#adminDefinitions .navi-item').each(function(index) {
		if ($(this).css('display') != 'none') {
			$('#kt_quick_panel .nav-link[href*=adminDefinitions]').closest('.nav-item').show();
			return false;
        }
	})
	$('#kt_quick_panel .nav-link[href*=adminSystem]').closest('.nav-item').hide();
	$('#adminSystem .navi-item').each(function (index) {
		if ($(this).css('display') != 'none') {
			$('#kt_quick_panel .nav-link[href*=adminSystem]').closest('.nav-item').show();
			return false;
		}
	})
	$('#kt_quick_panel .nav-link.active').removeClass('active');
	$('#kt_quick_panel .tab-pane.active').removeClass('active');

	$('#kt_quick_panel .nav-link').each(function () {
		if ($(this).parent().css('display') != 'none') {
			$(this).addClass('active');
			$($(this).attr('href')).addClass('active').addClass('show');
			return false;
        }
	});

	$('#kt_quick_panel .nav-item').each(function () {
		if ($(this).css('display') != 'none') {
			$('#kt_quick_panel').show();
			$('#topRightButtonAdmin').show();
        }
	})
}
function languageClick(lng) {
	lngHelper.setCurrentLanguage(lng);
	$('#selectedLanguageIcon').attr('src', '../img/flag/' + lngHelper.currentLanguage.icon);
	location.reload();
}
function initLanguage() {
	str = '';
	for (var i = 0; i < lngHelper.supportedLanguages.length; i++) {
		var lng = lngHelper.supportedLanguages[i];
		str += '<li class="navi-item">\
								<a href="#" class="navi-link" onclick="languageClick(\'' + lng.name + '\')" lng="' + lng.name + '">\
									<span class="symbol symbol-20 mr-3">\
										<img src="../img/flag/' + lng.icon + '" alt="" />\
									</span>\
									<span class="navi-text">' + lng.displayName + '</span>\
								</a>\
							</li>';
	}
	$('#languageList').html(str);
	lngHelper.loadCurrentLanguage();
	$('#selectedLanguageIcon').attr('src', '../img/flag/' + lngHelper.currentLanguage.icon);
}
async function getAssignedBProcessRunElementCount() {
	var result = await service.call('fUser/getAssignedBProcessInfo', 'GET', {}, true, true, {
		onError: function (xhr, status, error) {
			if (xhr.status == 401 || xhr.status == 403) {
				clearInterval(window.processCountInterval);
			}
		}
	});
	var total = 0;
	var str = '';
	for (var i = 0; i < result.length; i++) {
		total += result[i].runCount;
		str += '<a href="#" class="navi-item" targetUrl="bProcessAssignedToUser.html?bProcessRecID=' + result[i].bProcessRecID + '" targetPage="_inside" onclick="menuClick(event)">\
					<div class="navi-link rounded">\
						<div class="symbol symbol-40 mr-3">\
							<div class="symbol-label">\
								<i class="far fa-chart-bar" style="color:#3b9bfe"></i>\
							</div>\
						</div>\
						<div class="navi-text">\
							<div class="font-weight-bold font-size-lg">' + utils.htmlEscape(result[i].bProcessDisplayName) + '</div>\
						</div>\
						<div class="navi-text">\
							<div class="font-weight-bold font-size-lg" style="text-align:right">' + result[i].runCount.toString() + '</div>\
						</div>\
					</div>\
				</a>';
	}
	$('#assignedBProcessRunElementCount').html(total ? total.toString() : '');
	$('#currentAssignedBProcessContainer').html(str);
}
async function getAllAssignedBProcesses() {
	var html = '';
	var result = await service.call('fBProcess/getList');
	if (result) {
		for (var i = 0; i < result.length; i++) {
			html += '<a href="#" class="navi-item" targetUrl="bProcessAssignedToUser.html?bProcessRecID=' + result[i].recID + '&getAllItems=1" targetPage="_inside" onclick="menuClick(event)">\
					<div class="navi-link rounded">\
						<div class="symbol symbol-40 mr-3">\
							<div class="symbol-label">\
								<i class="far fa-chart-bar" style="color:#3b9bfe"></i>\
							</div>\
						</div>\
						<div class="navi-text">\
							<div class="font-weight-bold font-size-lg">' + utils.htmlEscape(result[i].displayName) + '</div>\
						</div>\
					</div>\
				</a>';
        }
    }
	$('#allAssignedBProcessContainer').html(html);
}
async function initMainMenu() {
	window.authorizationList = await service.call('fAuthorization/getListByUser', 'GET', { authObject: 0 });
	window.authorizationObjectTypes = await service.call('fAuthorization/getAuthorizationObjectTypes', 'GET', { category: 'system' });
	window.menuList = await service.call('fMenu/getList', 'GET', {});
	window.menuTree = menuHelper.menuListToTree(window.menuList);
	var openMenus = [];
	$('.menu-item-open .menu-toggle .menu-text').each((index, item) => {
		openMenus.push($(item).html());
	})
	var html = getMenuHtml(window.menuTree, 0);
	$('#kt_aside_menu .menu-nav').html(html);
	$('.menu-item .menu-toggle .menu-text').each((index, item) => {
		if (openMenus.indexOf($(item).html()) >= 0) {
			$(item).parents('.menu-toggle').parents('.menu-item').addClass('menu-item-open');
		}
	})
}
async function getAppInfo() {
	var appInfo = await service.call('generic/getAppInfo', 'GET', {});
	$('#divAppVersion').html('v.' + appInfo.appVersion);
}
async function init() {
	window.currentSession = await service.call('fSession/getSessionInfo', 'GET', {});
	if (!window.currentSession) {
		location.href = 'login.html';
		return;
	}
	var result = await service.call('fappsetting/getPublicAppSettings', 'GET', {});	if (result) {
		if (result.mainImageText)			$('.main-image-text').html(result.mainImageText);
		if (result.documentTitle)			document.title = result.documentTitle;
	}	var user = window.currentSession.user;
	$('#userAvatarMenu').attr('src', ' /fUser/getAvatar?recID=' + user.recID);
	$('#userAvatarImage').css('background-image', 'url("/fUser/getAvatar?recID=' + user.recID + '")');
	$('#userNameSurname').html(user.name + ' ' + user.surname);
	$('#userEmailAddress').html(user.eMailAddress);
	$('#userTitle').html(user.userName);
	await initMainMenu();
	initAdminMenu();
	var openPage = utils.getUrlParam('openPage');
	initDashboardMenu(!openPage);
	if (openPage) {
		$('#mainFrame').attr('src', openPage);
    }
	initLanguage();
	getAssignedBProcessRunElementCount();
	window.processCountInterval = setInterval(getAssignedBProcessRunElementCount, 10000);
	getAllAssignedBProcesses();
	getAppInfo();
}
$(document).on('initcompleted', function () {
	init();
})

$('#notificationRoot .dropdown-menu .nav-link').on('click', (e) => {
	var sender = utils.getEventSender(e);
	$('#notificationRoot .dropdown-menu .nav-link').removeClass('active');
	//sender.addClass('active');
	sender.tab('show');
	e.stopPropagation();
	//$('#notificationRoot .dropdown-menu').scrollTop(0);
	//$('#notificationRoot .dropdown-menu').toggleClass('show');
});