﻿function getRelationIDFromElemID(str) {
    return str.substring(0, str.lastIndexOf('_'));
}
function getPropertyIDFromElemID(str) {
    return str.substring(str.lastIndexOf('_') + 1);
}
function getParentRelationIDFromRelationID(str) {
    return str.substring(0, str.lastIndexOf('_'));
}
function getPropertyIDFromFModel(str) {
    return str.match(/\{\{([^}]+)\}\}/)[1];
}