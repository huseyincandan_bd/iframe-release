﻿"use strict";

function getSelectedElement(noAlert) {
    var selectedElem = $('#reportContainer').parent().find('.felement.selected');
    if (!selectedElem || selectedElem.length == 0) {
        if (!noAlert)
            utils.showToast('error',lngHelper.get('reportDesign.selectedElementRequired'));
        return null;
    }
    return selectedElem;
}
function setSelectedElement(elem) {
    $('.felement.selected').removeClass('selected');
    if (elem)
        elem.addClass('selected');
}
function elementClick(e,forceSelect) {
    var sender = utils.getEventSender(e);
    if (!sender.hasClass('felement'))
        sender = sender.closest('.felement');
    var selected = sender.hasClass('selected');
    $('.felement.selected').removeClass('selected');
    var selectedElem = null;
    if (selected && !forceSelect)
        sender.removeClass('selected');
    else {
        sender.addClass('selected');
        selectedElem = sender;
    }
    updateBars(selectedElem);
    e.stopPropagation();
}
function elementDblClick(e) {
    elementClick(e, true);
    btnElemPropsClick();
    e.stopPropagation();
}
function updateToolbar(selectedElem) {
    if (!selectedElem)
        selectedElem = getSelectedElement(true);
    $('#toolbar button').prop('disabled', true);
    $('#btnSave').prop('disabled', false);
    if (!selectedElem || selectedElem.length == 0) {
        $('#btnClearElem').prop('disabled', false);
        $('#btnElemProps').prop('disabled', false);
        return;
    }
    if (!selectedElem.hasClass('no-content') && !selectedElem.hasClass('no-add')) {
        $('#btnAddElement').prop('disabled', false);
        $('#btnAddRecordList').prop('disabled', false);
    }
    if (!selectedElem.hasClass('no-content') && !selectedElem.hasClass('no-clear'))
        $('#btnClearElem').prop('disabled', false);
    if (!selectedElem.hasClass('no-remove'))
        $('#btnRemoveElem').prop('disabled', false);
    if (!selectedElem.hasClass('no-move'))
        $('#btnGroupMove .btn').prop('disabled', false);
    if (!selectedElem.hasClass('no-prop'))
        $('#btnElemProps').prop('disabled', false);

    var ftag = selectedElem.attr('ftag');
    if (!selectedElem.hasClass('no-content') && !selectedElem.hasClass('no-add')) {
        $.each(selectedElem.parents('[loop-id]'), function (index, item) {
            if ($(item).attr('loop-object-id') != utils.emptyGuid) {
                $('#btnAddProperty').prop('disabled', false);
                return false;
            }
        })
    }
}
function updateBars(selectedElem) {
    updateToolbar(selectedElem);
    updateInfoBar();
}
function createLoopID() {
    var index = 1;
    while (true) {
        var id = index.toString();
        if (!$('[loop-id=' + id + ']').length)
            return id;
        index++;
    }
}
function updateLoopContainer(elem) {
    var loopRelationID = elem.attr('loop-relation-id');
    var loopObjectID = elem.attr('loop-object-id');
    var loopQueryType = parseInt(elem.attr('loop-query-type'));
    var loopID = elem.attr('loop-id');
    if (!loopObjectID || loopObjectID == utils.emptyGuid) {
        elem.find('.fcode-container').first().children('.fcode-description').html('');
        elem.find('.fcode-container').last().children('.fcode-description').html('');
    } else {
        if (!loopID) {
            loopID = createLoopID();
            elem.attr('loop-id', loopID);
        }
        if (!elem.attr('loop-query-type'))
            elem.attr('loop-query-type', window.reportEnums.ElemLoopQueryType.None);
        var objectName = '';
        var allObjects = $('#elemLoopObject').data('allOptions');
        for (var i = 0; i < allObjects.length; i++) {
            if (allObjects[i].value == loopObjectID) {
                objectName = allObjects[i].text;
                break;
            }
        }
        if (objectName)
            objectName = ' (' + objectName + ')';
        elem.find('.fcode-container').first().children('.fcode-description').html('┏ ' + lngHelper.get('reportDesign.recordListStart') + ' - ' + loopID + objectName);
        elem.find('.fcode-container').last().children('.fcode-description').html('┗ ' + lngHelper.get('reportDesign.recordListEnd') + ' - ' + loopID + objectName);
        var query = 'new FObjectDataQuery()';
        if (loopQueryType == window.reportEnums.ElemLoopQueryType.FromTable)
            query = '__CurrentDataQuery';
        var relation = 'new Dictionary&lt;Guid, string&gt;()';
        var code = '';
        if (loopQueryType == window.reportEnums.ElemLoopQueryType.ChildRecords) {
            code += '\nDictionary&lt;Guid, string&gt;\n__fRelationVariable' + loopID + '=new Dictionary&lt;Guid, string&gt;();\n__fRelationVariable' + loopID + '[Guid.Parse("' + elem.attr('loop-relation-id') + '")]=__fLoopVariable' + elem.attr('loop-relation-target-loop-id') + '.ObjectData.RecID;\n';
            relation = '__fRelationVariable' + loopID;
        }
        code += '\nint __fIndexVariable' + loopID + '=0;\nforeach (FObjectHelper __fLoopVariable' + loopID + ' in new FObjectHelper(Guid.Parse("' + loopObjectID + '"), __CurrentDbCommand).GetByQuery(' + query + ',' + relation + ', __CurrentUser, __CurrentDbCommand, new List&lt;string&gt;())) {\n__fIndexVariable' + loopID + '++;\n';
        code = '\n<% ' + code + ' %>\n';
        elem.find('.fcode-container').first().find('.fcode').html(code);
    }
}
function btnAddElementClick(e, type) {
    var selectedElem = getSelectedElement();
    if (!selectedElem)
        return;
    switch (type) {
        case 'columns':
            var html = '<div class="form-group row">\
                            <label class="col-sm-4 col-form-label">' + lngHelper.get('reportDesign.columnCount') + '</label>\
                            <div class="col-sm-8">\
                                <select class="form-control">\
                                </select>\
                            </div>\
                        </div>';
            var popup = utils.openPopup('info', lngHelper.get('generic.info'), html, [{
                text: lngHelper.get('generic.cancel'),
                class: 'btn-secondary'
            }, {
                text: lngHelper.get('generic.ok'),
                class: 'btn-primary',
                    onclick: function () {
                        var columnCount = parseInt(popup.find('select').val());
                        var columnSize = 12 / columnCount;
                        var html = '<div class="row felement" ftag="columnContainer">';
                        for (var i = 0; i < columnCount; i++) {
                            html += '<div class="col felement" ftag="column"></div>';
                        }
                        html += '</div>';
                        selectedElem.append($(html));
                        popup.modal('hide');
                }
            }
            ]);
            var options = [{ text: '2', value: '2' }, { text: '3', value: '3' }, { text: '4', value: '4' }, { text: '6', value: '6' }];
            utils.fillSelectOptions(popup.find('select'), options, true);
            break;
        case 'table':
            $('#modalTableElemProps').modal('show');
            break;
        case 'fpage-break-after':
            selectedElem.append($('<div>', { 'class': 'felement fpage-break-after no-content', 'ftag':'fpage-break-after' }));
            break;
        case 'recordList':
            var html = '\
            <div class="felement no-clear" ftag="recordList" loop-object-id="' + window.currentReport.fObjectID + '" loop-query-type="' + window.reportEnums.ElemLoopQueryType.FromTable + '">\
                <span class="fcode-container"><span class="fcode-description"></span><span class="fcode"></span></span>\
                <div class="felement no-remove no-move" ftag="record"></div>\
                <span class="fcode-container"><span class="fcode-description"></span><span class="fcode">\n<%\n}\n%></span></span>\
            </div>';
            var elem = $(html.trim());
            selectedElem.append(elem);
            updateLoopContainer(elem);
            break;
        case 'rowNumber':
            var loopID = '';
            $.each(selectedElem.parents('[loop-id]'), function (index, item) {
                if ($(item).attr('loop-object-id') && $(item).attr('loop-object-id') != utils.emptyGuid) {
                    loopID = $(item).attr('loop-id');
                    return false;
                }
            })
            if (!loopID) {
                utils.alertError(lngHelper.get('reportDesign.parentLoopIsRequired'));
                return;
            }
            var code = '<%\n__CurrentReportContent.Append(__fIndexVariable' + loopID + '.ToString());\n%>';
            var html = '\
            <span class="felement no-clear no-prop" ftag="rowNumber">\
                <span class="fcode-container"><span class="fcode-description">' + lngHelper.get('reportDesign.elemType_rowNumber') + '</span><span class="fcode">' + code + '</span></span>\
            </span>';
            selectedElem.append($(html.trim()));
            break;
        case 'codeBlock':
            var html = '<span class="fcode-container"><span class="fcode-description">' + lngHelper.get('reportDesign.elemType_codeBlock') + '</span><span class="fcode"></span></span>';
            if (selectedElem.attr('fTag') == 'tBody') {
                html = '<tr class="code-row felement no-clear" ftag="codeBlock"><td colspan="1000">' + html + '</td></tr>';
                selectedElem.find('tr.felement').last().after($(html.trim()));
            }
            else {
                html = '<span class="felement no-clear" ftag="codeBlock">' + html + '</span>';
                selectedElem.append($(html.trim()));
            }
            break;
        default:
            var fTag = type.replace(/</, '').replace(/>/, '');
            var noContent = (fTag == 'hr' ? 'no-content' : '');
            selectedElem.append($(type, { 'class': 'felement ' + noContent, 'ftag':fTag }));
            break;
    }
}
async function btnElemPropsClick() {
    var selectedElem = getSelectedElement(true);
    $('#codeBlockPropContainer').hide();
    if (selectedElem) {
        var fTag = selectedElem.attr('ftag');
        if (fTag == 'property') {
            initPropertyModal();
            $('#propElemLoop').val(selectedElem.attr('property-loop-id'));
            await propElemLoopChange();
            var propertyFullID = selectedElem.attr('property-full-id');
            var relationID = ''
            if (propertyFullID.indexOf('_') >= 0)
                relationID = propertyFullID.substring(0, propertyFullID.indexOf('_'));
            $('#propElemRelation').val(relationID);
            propElemRelationChange();
            $('#propElemProperty').val(propertyFullID);
            $('#modalPropElemProps').modal('show');
            return;
        } 
        var title = lngHelper.get('reportDesign.elemPropsTitle');
        if (fTag)
            title += ' | ' + lngHelper.get('reportDesign.elemType_' + fTag);
        $('#modalElemPropsTitle').html(title);
        $('#pageCss').closest('.form-group').hide();
        $('#pageJs').closest('.form-group').hide();
        $('#elemStyle').val(selectedElem.attr('style'));
        $('#elemHtml').val(selectedElem.html());
        $('#recordListPropContainer').hide();
        if (fTag == 'tBody' || fTag == 'recordList') {
            $('#recordListPropContainer').show();
            var loopObjectID = selectedElem.attr('loop-object-id');
            if (!loopObjectID)
                loopObjectID = utils.emptyGuid;
            if (!selectedElem.attr('loop-query-type'))
                selectedElem.attr('loop-query-type', window.reportEnums.ElemLoopQueryType.None);
            $('#elemLoopQueryType').val(selectedElem.attr('loop-query-type'));
            await elemLoopQueryTypeChange();
            $('#elemLoopObject').val(loopObjectID);
            $('#elemLoopRelation').val(selectedElem.attr('loop-relation-id'));
        }
        if (fTag == 'img') {
            $('#imageSrc').closest('.form-group').show();
            $('#imageSrc').val(selectedElem.attr('src'));
        }
        else
            $('#imageSrc').closest('.form-group').hide();
        if (fTag == 'codeBlock') {
            $('#codeBlockPropContainer').show();
            $('#codeBlockDescription').val(selectedElem.find('.fcode-description').html());
            var code = selectedElem.find('.fcode').text().trim();
            if (code.startsWith('<%'))
                code = code.substring(2);
            if (code.endsWith('%>'))
                code = code.substring(0, code.length - 2);
            //code = code.replace(/&lt;/g, '<').replace(/&gt;/g, '>'); 

            $('#codeBlockCode').val(code);
        }
    } else {
        $('#modalElemPropsTitle').html(lngHelper.get('reportDesign.reportPropsTitle'));
        selectedElem = $('#rootContainer');
        $('#pageCss').closest('.form-group').show();
        $('#pageJs').closest('.form-group').show();
        $('#pageCss').val($('#rootContainer').attr('page-css'));
        $('#pageJs').val($('#rootContainer').attr('page-js'));
    }
    $('#modalElemProps').data('selectedElem', selectedElem);
    $('#modalElemProps').modal('show');
}
function btnSaveElemPropsClick(hidePopup = true) {
    var selectedElem = $('#modalElemProps').data('selectedElem');
    selectedElem.attr('style', $('#elemStyle').val());
    selectedElem.html($('#elemHtml').val());
    if ($('#pageCss').is(':visible')) {
        $('#rootContainer').attr('page-css',$('#pageCss').val());
        $('#rootContainer').attr('page-js', $('#pageJs').val());
    }
    if ($('#imageSrc').is(':visible'))
        selectedElem.attr('src', $('#imageSrc').val());
    if ($('#recordListPropContainer').is(':visible'))
        selectedElem.attr('loop-query-type', $('#elemLoopQueryType').val());
    if ($('#elemLoopObject').is(':visible')) {
        selectedElem.attr('loop-object-id', $('#elemLoopObject').val());
        updateLoopContainer(selectedElem);
    }
    if ($('#elemLoopRelation').is(':visible')){
        var relOption = $('#elemLoopRelation option:selected');
        selectedElem.attr('loop-relation-id', relOption.val());
        selectedElem.attr('loop-object-id', relOption.attr('childobjectid'));
        selectedElem.attr('loop-relation-target-loop-id', relOption.attr('loopID'));
        updateLoopContainer(selectedElem);
    }
    if ($('#codeBlockPropContainer').is(':visible')) {
        selectedElem.find('.fcode-description').html($('#codeBlockDescription').val());
        selectedElem.find('.fcode').text('<%' + $('#codeBlockCode').val() + '%>');
    }
    if (hidePopup)
        $('#modalElemProps').modal('hide');
}
async function elemLoopQueryTypeChange() {
    var loopQueryType = parseInt($('#elemLoopQueryType').val());
    var options = $('#elemLoopObject').data('allOptions');
    $('#elemLoopRelation').closest('.form-group').hide();
    $('#elemLoopObject').closest('.form-group').hide();
    switch (loopQueryType) {
        case window.reportEnums.ElemLoopQueryType.ChildRecords:
            var selectedElem = getSelectedElement();
            var parentLoopObjectIDArrray = [];
            $.each(selectedElem.parents('[loop-id]'), function (index, item) {
                var loopObjectID = $(item).attr('loop-object-id');
                if (loopObjectID && loopObjectID != utils.emptyGuid)
                    parentLoopObjectIDArrray.push({
                        loopID: $(item).attr('loop-id'),
                        loopObjectID: loopObjectID
                    });
            })
            if (parentLoopObjectIDArrray.length == 0) {
                utils.alertError(lngHelper.get('reportDesign.parentLoopIsRequired'));
                $('#elemLoopQueryType').val(window.reportEnums.ElemLoopQueryType.None);
                await elemLoopQueryTypeChange();
                return;
            }
            var options = [];
            for (var i = 0; i < parentLoopObjectIDArrray.length; i++) {
                var result = await service.call('fObjectRelation/getListWithDetail', 'GET', { fObjectID: parentLoopObjectIDArrray[i].loopObjectID });
                for (var j = 0; j < result.length; j++) {
                    if (result[j].relation.relationType == window.fEnums.FObjectRelationType.OneToMany && result[j].parentObject.recID == parentLoopObjectIDArrray[i].loopObjectID) {
                        var relationStr = result[j].parentObject.displayName + ' |';
                        if (result[j].relation.displayName)
                            relationStr += '(' + result[j].relation.displayName + ')';
                        relationStr += '< ' + result[j].childObject.displayName;

                        options.push({
                            text: relationStr,
                            value: result[j].relation.recID,
                            attributes: {
                                childObjectID: result[j].childObject.recID,
                                loopID: parentLoopObjectIDArrray[i].loopID
                            }
                        });
                    }
                }
                utils.fillSelectOptions($('#elemLoopRelation'), options, true);
            }
            $('#elemLoopRelation').closest('.form-group').show();
            break;
        case window.reportEnums.ElemLoopQueryType.FromTable:
            $('#elemLoopObject').closest('.form-group').show();
            break;
        case window.reportEnums.ElemLoopQueryType.None:
            break;
        case window.reportEnums.ElemLoopQueryType.Custom:
            utils.alertDanger("Bu özellik henüz desteklenmiyor.");
            $('#elemLoopQueryType').val(window.reportEnums.ElemLoopQueryType.None);
            await elemLoopQueryTypeChange();
            break;            
    }
    utils.fillSelectOptions($('#elemLoopObject'), options, false, utils.emptyGuid);
}
function btnSaveTableElemPropsClick() {
    var selectedElem = getSelectedElement();
    var html = '<table class="felement no-clear" ftag="table" style="width:calc(100% - 8px)"><thead>';
    for (var i = 0; i < parseInt($('#tableHeaderRowCount').val()); i++) {
        html += '<tr class="felement" ftag="tableHeaderRow">';
        for (var j = 0; j < parseInt($('#tableColumnCount').val()); j++)
            html += '<th class="felement" ftag="tableHeaderCell"></th>';
        html += '</tr>';
    }
    html += '</thead><tbody class="felement no-remove no-move" ftag="tBody" loop-object-id="' + window.currentReport.fObjectID + '" loop-query-type="' + window.reportEnums.ElemLoopQueryType.FromTable + '"><tr class="code-row"><td colspan="1000"><span class="fcode-container"><span class="fcode-description"></span><span class="fcode"></span></span></td></tr>';
    for (var i = 0; i < parseInt($('#tableRowCount').val()); i++) {
        html += '<tr class="felement" ftag="tableRow">';
        for (var j = 0; j < parseInt($('#tableColumnCount').val()); j++) {
            html += '<td class="felement" ftag="tableCell"></td>';
        }
        html += '</tr>';
    }
    html += '<tr class="code-row"><td colspan="1000"><span class="fcode-container"><span class="fcode-description"></span><span class="fcode"><%}%></span></span></td></tr></tbody><tfoot>';
    for (var i = 0; i < parseInt($('#tableFooterRowCount').val()); i++) {
        html += '<tr class="felement" ftag="tableFooterRow">';
        for (var j = 0; j < parseInt($('#tableColumnCount').val()); j++)
            html += '<th class="felement" ftag="tableFooterCell"></th>';
        html += '</tr>';
    }
    html += '</tfoot></table>';
    if (selectedElem.attr('tag') == 'table') {
        //TODO: Burası değişecek.
        selectedElem.replaceWith(html);
        updateLoopContainer(selectedElem);
    }
    else {
        var newElem = $(html);
        selectedElem.append(newElem);
        updateLoopContainer(newElem.find('[loop-object-id]'));
    }
    $('#modalTableElemProps').modal('hide');
}
function btnRemoveElemClick() {
    var selectedElem = getSelectedElement();
    if (selectedElem == $('#reportContainer'))
        utils.alertError('Sayfayı silemezsiniz.');
    else
        selectedElem.remove();
}
function btnClearElemClick() {
    var selectedElem = getSelectedElement(true);
    if (selectedElem)
        selectedElem.html('');
    else {
        $('#rootContainer').html('\
            <div class="fheader felement no-remove" ftag="header"></div>\
            <table id="containerTable" style="table-layout:fixed;width:100%">\
                <thead id="containerTableTHead">\
                    <tr>\
                        <td>\
                            <div class="header-space"></div>\
                        </td>\
                    </tr>\
                </thead>\
                <tbody>\
                    <tr>\
                        <td>\
                            <div class="main-content felement no-remove" ftag="content"></div>\
                        </td>\
                    </tr>\
                </tbody>\
                <tfoot id="containerTableTFoot">\
                    <tr>\
                        <td>\
                            <div class="footer-space"></div>\
                        </td>\
                    </tr>\
                </tfoot>\
            </table>\
            <div class="ffooter felement no-remove" ftag="footer"></div>');
    }
}

function initPropertyModal() {
    var selectedElem = getSelectedElement();
    var options = [];
    if (selectedElem.attr["loop-id"]) {
        options.push({
            text: selectedElem.attr('loop-id'),
            value: selectedElem.attr('loop-id')
        });
    }
    $.each(selectedElem.parents('[loop-id]'), function (index, item) {
        options.push({
            text: $(item).attr('loop-id'),
            value: $(item).attr('loop-id')
        });
    });
    utils.fillSelectOptions($('#propElemLoop'), options, true);
}
async function btnAddPropertyClick() {
    initPropertyModal();
    $('#propElemLoop').val($('#propElemLoop option').first().val());
    await propElemLoopChange();
    $('#modalPropElemProps').modal('show');
}
function btnSavePropElemPropsClick() {
    var selectedElem = getSelectedElement();
    var propertyFullID = $('#propElemProperty').val();
    var propertyDisplayName = $('#propElemProperty').find('option:selected').text();
    var loopID = selectedElem.closest('[loop-id]:visible').attr('loop-id');
    var html = '<span class="felement no-clear" ftag="property" property-loop-id="' + loopID + '" property-full-id="' + propertyFullID + '"><span class="fcode-container"><span class="fcode-description">' + propertyDisplayName + '</span><span class="fcode"><%__CurrentReportContent.Append(__fLoopVariable' + loopID + '.ObjectData.Properties["' + propertyFullID + '"].ToStringWithNullCheck());%></span></span></span>';
    if (selectedElem.attr('ftag') == 'property')
        selectedElem.replaceWith(html);
    else
        selectedElem.append(html);
    $('#modalPropElemProps').modal('hide');
}
async function propElemLoopChange(){
    var loopID = $('#propElemLoop').val();
    var loopElem = $('[loop-id=' + loopID + ']');
    var objectID = loopElem.attr('loop-object-id');
    var propertyDetails = await service.call('fObject/getPropertyDetails?fObjectID=' + objectID, 'POST', []);
    var relationOptions = [];
    var propertyOptions = [];
    for (var i = 0; i < propertyDetails.length; i++) {
        var propertyDetail = propertyDetails[i];
        var relationID = ''
        var charIndex = propertyDetail.propertyFullID.lastIndexOf('_');
        if (charIndex >= 0)
            relationID = propertyDetail.propertyFullID.substring(0, charIndex);
        var bFound = false;
        for (var j = 0; j < relationOptions.length; j++) {
            if (relationOptions[j].value == relationID) {
                bFound = true;
                break;
            }
        }
        if (!bFound) {
            relationOptions.push({
                text: propertyDetail.relationFullName,
                value: relationID
            })
        }
        propertyOptions.push({
            text: propertyDetail.property.displayName,
            value: propertyDetail.propertyFullID
        });
    }
    utils.fillSelectOptions($('#propElemRelation'), relationOptions, true);
    $('#propElemRelation').val(relationOptions[0].value);
    $('#propElemProperty').data('allOptions', propertyOptions);
    propElemRelationChange();
}
function propElemRelationChange() {
    var relationID = $('#propElemRelation').val();
    var allOptions = $('#propElemProperty').data('allOptions');
    var options = [];
    for (var i = 0; i < allOptions.length; i++) {
        if ((relationID == '' && allOptions[i].value.indexOf('_') < 0) || (relationID != '' && allOptions[i].value.startsWith(relationID + '_')))
            options.push(allOptions[i]);
    }
    utils.fillSelectOptions($('#propElemProperty'), options, true);
}
function btnMoveClick(direction) {
    var selectedElem = getSelectedElement();
    if (selectedElem == null)
        return;
    switch (direction) {
        case 'up':
            var previousSibling = selectedElem.prev();
            if (previousSibling.hasClass('felement'))
                previousSibling.before(selectedElem);
            break;
        case 'down':
            var nextSibling = selectedElem.next();
            if (nextSibling.hasClass('felement'))
                nextSibling.after(selectedElem);
            break;
    }
}
function infobarLinkClick(e) {
    var sender = utils.getEventSender(e);
    if (!sender || sender.length == 0)
        return;
    setSelectedElement(sender.data('elem'));
    updateInfoBar();
}
function updateInfoBarInner(elem) {
    var parentElem = elem.parent().closest('.felement');
    if (!parentElem || parentElem.length == 0)
        return;
    if (parentElem.attr('ftag')) {
        var domElem = $('<span class="infobar-link" onclick="infobarLinkClick(event)">' + lngHelper.get('reportDesign.elemType_' + parentElem.attr('ftag')) + '</span>');
        domElem.data('elem', parentElem);
        $('#infobar').prepend($('<span> &gt; </span>'));
        $('#infobar').prepend(domElem);
    }
    updateInfoBarInner(parentElem);
}
function updateInfoBar() {
    $('#infobar').html('');
    var selectedElem = getSelectedElement(true);
    if (!selectedElem)
        return;
    $('#infobar').html(lngHelper.get('reportDesign.elemType_' + selectedElem.attr('ftag')));
    updateInfoBarInner(selectedElem);
}
function updateReportContent() {
    var headerHeight = ($('#reportContainer .fheader').height() + 50) + 'px';
    var footerHeight = ($('#reportContainer .ffooter').height() + 50) + 'px';
    $('#reportContainer .fheader').css('height', headerHeight);
    $('#reportContainer .header-space').css('height', headerHeight);
    $('#reportContainer .ffooter').css('height', footerHeight);
    $('#reportContainer .footer-space').css('height', footerHeight);
    $('.felement.selected').removeClass('selected');
}
function btnEditElemHtmlInEditorClick() {
    window.currentCodeEditorContent = $('#elemHtml').val();
    window.currentCodeEditorTarget = 'elemHtml';
    utils.openPage('./codeEditor.html?language=html&formatOnLoad=true');
}
function btnEditPageCssInEditorClick() {
    window.currentCodeEditorContent = $('#pageCss').val();
    window.currentCodeEditorTarget = 'pageCss';
    utils.openPage('./codeEditor.html?language=css&formatOnLoad=true');
}
function btnEditPageJsInEditorClick() {
    window.currentCodeEditorContent = $('#pageJs').val();
    window.currentCodeEditorTarget = 'pageJs';
    utils.openPage('./codeEditor.html?language=js&formatOnLoad=true');
}
function btnEditCodeBlockCodeInEditorClick() {
    window.currentCodeEditorContent = $('#codeBlockCode').val();
    window.currentCodeEditorTarget = 'codeBlockCode';
    utils.openPage('./codeEditor.html?language=csharp&formatOnLoad=true');
}
function codeEditorSaveClick(code) {
    switch (window.currentCodeEditorTarget) {
        case 'elemHtml':
            $('#elemHtml').val(code);
            break;
        case 'pageCss':
            $('#pageCss').val(code);
            break;
        case 'pageJs':
            $('#pageJs').val(code);
            break;
        case 'codeBlockCode':
            $('#codeBlockCode').val(code);
            break;
    }
    btnSaveElemPropsClick(false);
}
async function btnSaveClick() {
    updateReportContent();
    var result = await service.call('fReport/setHtmlTemplateString?recID=' + window.currentReportID, 'POST', $('#reportContainer').html(), false, false, { processData: false });
    if (result)
        utils.showToast('success', lngHelper.get('generic.saved'));
    if (!window.fOpenerWindow || !window.fOpenerWindow.codeEditorSaveClick) {
        //utils.alertDanger(lngHelper.get('codeEditor.couldNotFindOpener'));
        return;
    }
    window.fOpenerWindow.htmlEditorSaveClick($('#reportContainer').html());
}
async function init() {
    btnClearElemClick();
    $('#reportContainer').on('click', function (e) {
        elementClick(e);
    });
    $('#reportContainer').on('dblclick', function (e) {
        elementDblClick(e);
    });
    window.currentReportID = utils.getUrlParam('reportID');
    window.currentReport = await service.call('fReport/get', 'GET', { recID: window.currentReportID });
    if (!window.currentReport)
        return;
    document.title = window.currentReport.displayName + ' | ' + lngHelper.get('reportDesign.pageTitle');
    var result = await service.call('fReport/getHtmlTemplateString', 'GET', { recID: window.currentReportID });
    if (result && result.result)
        $('#reportContainer').html(result.result);
    var result = await service.call('fObject/getList');
    var options = [];
    for (var i = 0; i < result.length; i++) {
        options.push({
            text: result[i].displayName,
            value: result[i].recID
        })
    }
    utils.fillSelectOptions($('#elemLoopObject'), options, false, utils.emptyGuid);
    $('#elemLoopObject').data('allOptions', options);
    options = [];
    for (const [key, value] of Object.entries(window.reportEnums.ElemLoopQueryType)) {
        options.push({
            value: value,
            text: lngHelper.get('reportDesign.elemLoopQueryType_' + key)
        })
    }
    utils.fillSelectOptions($('#elemLoopQueryType'), options, true);
    updateBars();
}
$(document).on('initcompleted', function () {
    init();
})
window.reportEnums = {
    ElemLoopQueryType: { None: 0, FromTable: 1, ChildRecords: 2, Custom: 3 }
}
