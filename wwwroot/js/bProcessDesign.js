﻿const FBProcessElementType = {
    UserAction: 0,
    Notification: 1,
    CreateRecord: 2,
    ChangeState: 3,
    Start: 4,
    End: 5,
    Gateway: 6,
    UpdateRecord: 7,
    DeleteRecord: 8,
    SetParameter: 9,
    ExecuteFunction: 10
};
const FBProcessEventType = {
    UserDefined: 0,
    Timeout: 1
};
const GateWayOperator = {
    "==": 1,
    ">": 2,
    "<": 3,
    ">=": 4,
    "<=": 5,
    "!=": 6
};
class historyHelper  {
    constructor(container, backButton, forwardButton) {
        this.container = container ? container : $('#businessProcessContainer');
        this.contentArray = [];
        this.index = -1;
        this.backButton = backButton ? backButton : $('#btnBack');
        this.forwardButton = forwardButton ? forwardButton : $('#btnForward');
        this.updateButtons();
    }
    updateButtons() {
        if (this.index == 0)
            this.backButton.prop('disabled', true);
        else
            this.backButton.prop('disabled', false);
        if (this.index >= this.contentArray.length - 1)
            this.forwardButton.prop('disabled', true);
        else
            this.forwardButton.prop('disabled', false);
    }
    add() {
        if (this.index != -1 && this.index < this.contentArray.length - 1) {
            this.contentArray.splice(this.index + 1);
            this.index = this.contentArray.length - 1;
        }
        this.contentArray.push(this.container.html());
        this.index++;
        this.updateButtons();
    }
    getContent() {
        return this.contentArray[this.index];
    }
    showContent() {
        this.container.html(this.contentArray[this.index]);
    }
    back() {
        this.index--;
        if (this.index < 0)
            this.index = 0;
        this.updateButtons();
        this.showContent();
    }
    forward() {
        this.index++;
        if (this.index >= this.contentArray.length)
            this.index = this.contentArray.length - 1;
        this.updateButtons();
        this.showContent();
    }
}
function onActionConnectorDragStart(e) {
    var target = $(e.target);
    //target = target.parents('.item-container');    
    e.dataTransfer.setData("sourceElemNumber", target.attr('elemnumber'));
    e.dataTransfer.setData("startX", e.offsetX);
    e.dataTransfer.setData("startY", e.offsetY);
    e.dataTransfer.setData("a", "b");
    $('.action-conpoint').addClass('dragging');
}
function onActionConnectorDragOver(e) {
}     
function onActionConnectorDrop(e) {
    var targetElem = utils.getEventSender(e);
    //targetElem = targetElem.parents('.item-container');

    e.stopPropagation();
    e.preventDefault();
    var elemNumber = e.dataTransfer.getData("sourceElemNumber");
    if (!elemNumber)
        return;
    var startX = parseInt(e.dataTransfer.getData("startX"));
    var startY = parseInt(e.dataTransfer.getData("startY"));
    var sourceElem = $('[elemNumber=' + elemNumber + ']');

    $('.action-conpoint').removeClass('dragging');
    if (sourceElem.attr('elemnumber') == targetElem.attr('elemnumber'))
        return;
    if (sourceElem.parents('.item-container').attr('elemnumber') == targetElem.parents('.item-container').attr('elemnumber')) {
        $('.conline[sourceElemNumber=' + sourceElem.attr('elemnumber') + ']').attr('sourceelemnumber', targetElem.attr('elemnumber'));
        $('.conline[targetElemNumber=' + sourceElem.attr('elemnumber') + ']').attr('targetelemnumber', targetElem.attr('elemnumber'));
        updateActionLines(sourceElem.parents('.item-container'));
    } else
        createLine(sourceElem, targetElem);
    this.processHistory.add();
}
function fillActionChageStateList() {
    var str = '<option value="0">-' + lngHelper.get('generic.select') + '-</option>';
    for (var i = 0; i < window.currentBProcessPackage.states.length; i++) {
        str += '<option value="' + window.currentBProcessPackage.states[i].stateNumber + '">' + window.currentBProcessPackage.states[i].displayName + '</option>';
    }
    $('#actionChageState').html(str);
}
function fillactionAssigneeActionProcessUserList(currentElem) {
    var options = [];

    $('#businessProcessContainer .event-start').each((index, item) => {
        options.push({
            value: '5|' + $(item).attr('elemnumber'),
            text: 'User (' + $(item).find('.item-inner').text() + ')'
        })
    })
    $('#businessProcessContainer .action-user').each((index, item) => {
        options.push({
            value: '5|' + $(item).attr('elemnumber'),
            text: 'User (' + $(item).find('.item-inner').text() + ')'
        })
    })
    utils.fillSelectOptions($('#actionAssigneeActionProcessUser'), options, true);
    $('#actionAssignee').selectpicker('refresh');
}
async function onActionDblClick(e) {
    var sender = utils.getEventSender(e);
    if (!sender.hasClass('item-container'))
        sender = sender.parents('.item-container');
    $('#actionText').val(sender.find('.item-inner').html());
    $('#modalEditAction').data('editItem', sender);
    if (sender.hasClass('action-user')) {
        fillactionAssigneeActionProcessUserList(sender);
        if (sender.attr('assignee'))
            $('#actionAssignee').selectpicker('val', JSON.parse(sender.attr('assignee')));
        else
            $('#actionAssignee').selectpicker('val', []);

        var props = {};
        if (sender.attr('props'))
            props = JSON.parse(sender.attr('props'));
        $('#modalEditAction').data('props', props);
        if (!props.userActionType)
            props.userActionType = window.fEnums.FBProcessUserActionType.OpenRecord;
        $('#actionUserActionType').val(props.userActionType.toString());
        actionUserActionTypeChange();
        fillParameterList($('#actionUserActionSelectRecordIDParameter'));
        fillParameterList($('#actionUserActionOpenRecordParameter'));
        switch (props.userActionType) {
            case window.fEnums.FBProcessUserActionType.OpenRecord:
                if (sender.attr('props'))
                    $('#modalEditAction').data('props', JSON.parse(sender.attr('props')));
                else
                    $('#modalEditAction').data('props', null);
                actionUserActionTypeChange();
                fillTblActionUserActionOpenRecords();
                break;
            case window.fEnums.FBProcessUserActionType.SelectRecord:
                $('#actionUserActionSelectRecordObject').val(props.fObjectID);
                actionUserActionSelectRecordObjectChange();
                $('#actionUserActionSelectRecordTable').val(props.fTableID);
                $('#actionUserActionSelectRecordIDParameter').val(props.parameterNumber.toString());
                break;

        }
        //$('#actionForm optgroup').prop('disabled', true);
        //$('.action-create-record').each((index, item) => {
        //    item = $(item);
        //    if (item.attr('createrecordobjectid')) {
        //        $('#actionForm optgroup[objectid=' + item.attr('createrecordobjectid') + ']').prop('disabled',false);
        //    }
        //})
        //$('#actionForm').selectpicker('refresh');
        //if (sender.attr('forms'))
        //    $('#actionForm').selectpicker('val', JSON.parse(sender.attr('forms')));
        //else
        //    $('#actionForm').selectpicker('val', []);
        //var forms = [];
        //if (sender.attr('forms'))
        //    $('#actionForm').val(sender.attr('forms'));
        //else
        //    $('#actionForm').selectpicker('val', '');
        $('#actionUserActionContainer').show();
    } else {
        $('#actionUserActionContainer').hide();
    }

    if (sender.hasClass('action-create-record') || sender.hasClass('action-update-record') || sender.hasClass('action-delete-record')) {
        if (sender.attr('createrecordobjectid'))
            $('#actionCreateRecordObject').val(sender.attr('createrecordobjectid'));
        else
            $('#actionCreateRecordObject').val('0');
        $('#actionCreateRecordObject').parents('.form-group').show();
    } else
        $('#actionCreateRecordObject').parents('.form-group').hide();

    if (sender.hasClass('action-update-record')) {
        $('#actionCreateRecordObject').attr('onchange', 'actionCreateRecordObjectChange()');
        initActionUpdateRecordPropertyModal();
        var props = sender.attr('props');
        if (props) {
            props = JSON.parse(props);
            $('#actionCreateRecordObject').val(props.objectID);
            actionCreateRecordObjectChange();
            $('#actionUpdateRecordIDParameterNumber').val(props.recordIDParameterNumber);
            var tableData = [];
            var objectProperties = await service.call('fObjectProperty/getList', 'GET', { fObjectID: props.objectID });
            for (var i = 0; i < props.properties.length; i++) {
                var row = [];
                row.push(i + 1);
                row.push(props.properties[i].propertyID);
                var objectPropertyName = '';
                for (var j = 0; j < objectProperties.length; j++) {
                    if (objectProperties[j].recID == props.properties[i].propertyID) {
                        objectPropertyName = objectProperties[j].displayName;
                        break;
                    }
                }
                row.push(objectPropertyName);
                row.push(props.properties[i].parameterNumber);
                var paramName = ''
                for (var j = 0; j < window.currentBProcessPackage.parameters.length; j++) {
                    if (window.currentBProcessPackage.parameters[j].parameterNumber == props.properties[i].parameterNumber) {
                        paramName = window.currentBProcessPackage.parameters[j].displayName;
                        break;
                    }
                }
                row.push(paramName);
                tableData.push(row);
            }
            $('#actionUpdateRecordPropertyValues').data('tableData', tableData);
            fillActionUpdateRecordPropertyValueTable();
        } else {
            $('#actionUpdateRecordPropertyValues').data('tableData', []);
            fillActionUpdateRecordPropertyValueTable();
        }
        $('#actionUpdateRecordObjectContainer').show();
    } else {
        $('#actionCreateRecordObject').attr('onchange', '');
        $('#actionUpdateRecordObjectContainer').hide();
    }
    if (sender.hasClass('action-delete-record')) {
        $('#actionUpdateRecordObjectContainer').show();
        $('#actionUpdateRecordPropertyValues').parents('.form-group').hide();
        fillParameterList($('#actionUpdateRecordIDParameterNumber'));
        var props = sender.attr('props');
        if (props) {
            props = JSON.parse(props);
            $('#actionCreateRecordObject').val(props.objectID);
            $('#actionUpdateRecordIDParameterNumber').val(props.recordIDParameterNumber);
        }
    }
    if (sender.hasClass('gateway-regular')) {
        $('#actionGatewayContainer').data('sender', sender);
        initActionGatewayModal();
        $('#actionGatewayContainer').show();
    } else
        $('#actionGatewayContainer').hide();

    if (sender.hasClass('action-change-state')) {
        fillActionChageStateList();
        $('#actionChageState').val(sender.attr('changestateid'));
        $('#actionChageState').parents('.form-group').show();
    }
    else
        $('#actionChageState').parents('.form-group').hide();

    if (sender.hasClass('action-set-parameter')) {
        var props = sender.attr('props');
        if (props) {
            props = JSON.parse(props);
        } else {
            if (!props)
                props = {};
            props.params = [];
        }
        $('#actionSetParameterContainer').show();
        $('#modalEditAction').data('props', props);
        fillTblActionSetParameterParams();
    }
    else
        $('#actionSetParameterContainer').hide();

    if (sender.hasClass('action-notification')) {
        var props = sender.attr('props');
        if (!props) {
            props = {
                notificationType: '0',
                targetRoles: [],
                targetAddress: '',
                subject: '',
                message: ''
            }
        } else
            props = JSON.parse(props);
        $('#actionNotificationNotificationType').val(props.notificationType);
        fillActionNotificationNotificationType();
        fillActionNotificationTargetRoles();
        $('#actionNotificationTargetRoles').selectpicker('val', props.targetRoles);
        $('#actionNotificationTargetAddress').val(props.targetAddress);
        $('#actionNotificationSubject').val(props.subject);
        $('#actionNotificationMessage').val(props.message);
        $('#actionNotificationContainer').show();
    } else
        $('#actionNotificationContainer').hide();

    if (sender.hasClass('action-execute-function')) {
        //$('#actionExecuteFunctionContainer').show();
        //$('#actionExecuteFunctionCode').val(sender.attr('props'));

        window.currentCodeEditorContent = sender.attr('props');
        window.currentEditFormExecutablePage = utils.openPage('./codeEditor.html?language=csharp', null);
        window.currentCodeEditorTarget = sender;
        return;
    }
    //else {
    //    $('#actionExecuteFunctionContainer').hide();
    //}

    if (sender.hasClass('action-execute-function'))
        $('#modalEditAction .modal-dialog').addClass('modal-xxl');
    else
        $('#modalEditAction .modal-dialog').removeClass('modal-xxl');
    actionNotificationNotificationTypeChange();
    $('#modalEditAction').modal('show');
}
function codeEditorSaveClick(code) {
    window.currentCodeEditorTarget.attr('props', code);        
}
function fillActionNotificationTargetRoles() {
    var options = [];
    var parameters = JSON.parse(JSON.stringify(window.currentBProcessPackage.parameters));
    parameters.sort((a, b) => { if (a.displayName > b.displayName) return 1; if (a.displayName < b.displayName) return -1; return 0 });
    for (var i = 0; i < parameters.length; i++) {
        options.push({
            value: window.fEnums.FBProcessRoleType.ProcessParameter.toString() + '|' + parameters[i].parameterNumber,
            text: parameters[i].displayName
        })
    }
    utils.fillSelectOptions($('#actionNotificationTargetRoles optgroup[id=' + window.fEnums.FBProcessRoleType.ProcessParameter.toString() + ']'), options, true);
    options = [];
    var elements = JSON.parse(JSON.stringify(window.currentBProcessPackage.elements));
    elements.sort((a, b) => { if (a.displayName > b.displayName) return 1; if (a.displayName < b.displayName) return -1; return 0 });
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        if (element.elementType == FBProcessElementType.Start || element.elementType == FBProcessElementType.UserAction) {
            options.push({
                value: window.fEnums.FBProcessRoleType.RunElementUser.toString() + '|' + element.elementNumber,
                text: element.displayName
            })
        }
    }
    utils.fillSelectOptions($('#actionNotificationTargetRoles optgroup[id=' + window.fEnums.FBProcessRoleType.RunElementUser.toString() + ']'), options, true);
    $('#actionNotificationTargetRoles').selectpicker('refresh');
}
function btnEditActionSaveClick() {
    var editItem = $('#modalEditAction').data('editItem');
    editItem.find('.item-inner').html($('#actionText').val());
    if (editItem.hasClass('action-user')) {
        editItem.attr('assignee', JSON.stringify($('#actionAssignee').selectpicker('val')));
        var props = {
            userActionType: parseInt($('#actionUserActionType').val())
        };
        switch (props.userActionType) {
            case window.fEnums.FBProcessUserActionType.OpenRecord:
                if ($('#modalEditAction').data('props'))
                    Object.assign(props, $('#modalEditAction').data('props'));
                break;
            case window.fEnums.FBProcessUserActionType.SelectRecord:
                props.fObjectID = $('#actionUserActionSelectRecordObject').val();
                props.fTableID = $('#actionUserActionSelectRecordTable').val();
                props.parameterNumber = parseInt($('#actionUserActionSelectRecordIDParameter').val());
                break;
        }
        editItem.attr('props', JSON.stringify(props));
    }
    //if (editItem.hasClass('action-user'))
    //    editItem.attr('forms', JSON.stringify($('#actionForm').selectpicker('val')));
    if (editItem.hasClass('action-create-record')) 
        editItem.attr('createrecordobjectid', $('#actionCreateRecordObject').val());
    if (editItem.hasClass('action-change-state'))
        editItem.attr('changestateid', $('#actionChageState').val());
    if (editItem.hasClass('action-notification')) {
        editItem.attr('props', JSON.stringify({
            notificationType: $('#actionNotificationNotificationType').val(),
            targetRoles: $('#actionNotificationTargetRoles').selectpicker('val'),
            targetAddress: $('#actionNotificationTargetAddress').val(),
            subject: $('#actionNotificationSubject').val(),
            message: $('#actionNotificationMessage').val()
        }))
    }
    if (editItem.hasClass('action-update-record')) {
        if ($('#actionCreateRecordObject').val() == '0') {
            utils.alertWarning('Object is required.')
            return;
        }
        if ($('#actionUpdateRecordIDParameterNumber').val() == '0') {
            utils.alertWarning('Record ID is required')
            return;
        }
        var updateRecordInfo = {
            objectID: $('#actionCreateRecordObject').val(),
            recordIDParameterNumber: $('#actionUpdateRecordIDParameterNumber').val(),
            properties: []
        }
        var tableData = $('#actionUpdateRecordPropertyValues').data('tableData');
        if (tableData) {
            for (var i = 0; i < tableData.length; i++) {
                updateRecordInfo.properties.push({
                    propertyID: tableData[i][1],
                    parameterNumber : tableData[i][3]
                })
            }
        }
        editItem.attr('props', JSON.stringify(updateRecordInfo));
    }
    if (editItem.hasClass('action-delete-record')) {
        if ($('#actionCreateRecordObject').val() == '0') {
            utils.alertWarning('Object is required.')
            return;
        }
        if ($('#actionUpdateRecordIDParameterNumber').val() == '0') {
            utils.alertWarning('Record ID is required')
            return;
        }
        var deleteRecordInfo = {
            objectID: $('#actionCreateRecordObject').val(),
            recordIDParameterNumber: $('#actionUpdateRecordIDParameterNumber').val()
        }
        editItem.attr('props', JSON.stringify(deleteRecordInfo));
    }
    if (editItem.hasClass('gateway-regular')) {
        var props = [];
        var tableData = $('#actionGatewayConditions').data('tableData');
        if (tableData) {
            for (var i = 0; i < tableData.length; i++) {
                props.push({
                    recID: tableData[i][0],
                    displayName: tableData[i][1],
                    complex: tableData[i][2],
                    expression:tableData[i][3]
                })
            }
        }
        editItem.attr('props', JSON.stringify(props));
    }
    if (editItem.hasClass('action-set-parameter')) {
        var props = $('#modalEditAction').data('props');
        //var props = {
        //    parameterNumber: $('#actionSetParameterParameter').val(),
        //    valueStr: $('#actionSetParameterValue').val()
        //}
        editItem.attr('props', JSON.stringify(props));
    }
    //if (editItem.hasClass('action-execute-function')) {
        //editItem.attr('props', window.actionExecuteFunctionCodeEditor.getValue());  
        //editItem.attr('props', $('#actionExecuteFunctionCode').val());        
    //} 
    this.processHistory.add();
    updateActionLines(editItem);
    $('#modalEditAction').modal('hide');
}
function fillActionNotificationNotificationType() {
    if ($('#actionNotificationNotificationType option').length == 0) {
        var options = [];
        for (const [key, value] of Object.entries(window.fEnums.FBProcessActionNotificationType)) {
            options.push({
                value: value,
                text: lngHelper.get('bProcessDesign.notificationType_' + key)
            })
        }
        utils.fillSelectOptions($('#actionNotificationNotificationType'), options, true);
    }
    actionNotificationNotificationTypeChange();
}
function actionNotificationNotificationTypeChange() {
    var notificationType = parseInt($('#actionNotificationNotificationType').val());
    if (notificationType == window.fEnums.FBProcessActionNotificationType.InApp) {
        $('#actionNotificationTargetAddress').parents('.form-group').hide();
    } else {
        $('#actionNotificationTargetAddress').parents('.form-group').show();
    }
}
function btnEditActionDeleteClick() {
    var editItem = $('#modalEditAction').data('editItem');
    var elemNumber = editItem.attr('elemnumber');
    editItem.remove();
    $('.conline').each((index, item) => {
        item = $(item);
        if (item.attr('sourceelemrootnumber') == elemNumber || item.attr('targetelemrootnumber') == elemNumber) {
            item.remove();
        }
    })
    this.processHistory.add();
    $('#modalEditAction').modal('hide');
}
function fillConnectorEventList(sender) {
    var options = [];
    
    var sourceElem = $('[elemNumber=' + sender.attr('sourceelemrootnumber') + ']');
    if (sourceElem) {
        if (sourceElem.hasClass('gateway-regular')) {
            var props = sourceElem.attr('props');
            if (props) {
                props = JSON.parse(props);
                for (var i = 0; i < props.length; i++) {
                    options.push({
                        value: props[i].recID,
                        text: props[i].displayName
                    });
                }
                options.push({
                    value: -1,
                    text: 'Otherwise'
                });
                utils.fillSelectOptions($('#connectorEvent'), options);
            }
            return true;
        }
        if (sourceElem.hasClass('action-user')) {
            for (var i = 0; i < window.currentBProcessPackage.events.length; i++) {
                options.push({
                    value: window.currentBProcessPackage.events[i].eventNumber,
                    text: window.currentBProcessPackage.events[i].displayName
                });
            }
            utils.fillSelectOptions($('#connectorEvent'), options);
            return true;
        }
    }
    return false;
}
function onConnectorDblClick(e) {
    var sender = utils.getEventSender(e);
    if (fillConnectorEventList(sender)) {
        $('#modalEditConnector_NoOptionAvailable').parent().children().show();
        $('#modalEditConnector_NoOptionAvailable').hide();
    } else {
        $('#modalEditConnector_NoOptionAvailable').parent().children().hide();
        $('#modalEditConnector_NoOptionAvailable').show();
    }
    $('#connectorEvent').val(sender.attr('event') ? sender.attr('event') : '0');
    $('#modalEditAction').data('editItem', sender);
    $('#modalEditConnector').modal('show');
}
function btnEditConnectorSaveClick() {
    var editItem = $('#modalEditAction').data('editItem');
    editItem.attr('event', $('#connectorEvent').val());
    this.processHistory.add();
    $('#modalEditConnector').modal('hide');
}
function btnEditConnectorDeleteClick() {
    var editItem = $('#modalEditAction').data('editItem');
    editItem.remove();
    this.processHistory.add();
    $('#modalEditConnector').modal('hide');
}
function onDragStart(e) {
    var target = $(e.target);
    if (!target.hasClass('item-container') && target.parents('.main-left').length == 0) {
        return;
    }
    if (target.parents('.main-left').length == 0) {
        e.dataTransfer.setData('sourcePanel', 'right');
        e.dataTransfer.setData('elemnumber', target.attr('elemnumber'));
    } else {
        e.dataTransfer.setData('id', target.attr('id'));
        e.dataTransfer.setData('sourcePanel', 'left');
    }
    e.dataTransfer.setData("startX", e.offsetX);
    e.dataTransfer.setData("startY", e.offsetY);
    e.stopPropagation();
}
function onDragOver(e) {
    e.preventDefault();
}
function onDragLeave(e) {
    e.preventDefault();
}
function onDrop(e) {
    var targetElem = utils.getEventSender(e);

    e.stopPropagation();
    e.preventDefault();
    var startX = parseInt(e.dataTransfer.getData("startX"));
    var startY = parseInt(e.dataTransfer.getData("startY"));

    var containerOffset = $('#businessProcessContainer').offset();

    var fromLeftPanel = e.dataTransfer.getData('sourcePanel') =='left';

    if (fromLeftPanel) {
        var sourceElem = $('#' + e.dataTransfer.getData('id'));
        var newElem = null;
        switch (sourceElem.attr('id')) {
            case 'action-user':
                newElem = $('<div class="item-container action-user"><div class="item-inner">' + lngHelper.get('bProcessDesign.userAction') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon far fa-user"></i></div>');
                break;
            case 'action-notification':
                newElem = $('<div class="item-container action-notification"><div class="item-inner">' + lngHelper.get('bProcessDesign.notification') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon far fa-envelope"></i></div>');
                break;
            case 'action-create-record':
                newElem = $('<div class="item-container action-create-record"><div class="item-inner">' + lngHelper.get('bProcessDesign.createRecord') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon far fa-file"></i></div>');
                break;
            case 'action-update-record':
                newElem = $('<div class="item-container action-update-record"><div class="item-inner">' + lngHelper.get('bProcessDesign.updateRecord') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon far fa-file-alt"></i></div>');
                break;
            case 'action-delete-record':
                newElem = $('<div class="item-container action-delete-record"><div class="item-inner">' + lngHelper.get('bProcessDesign.deleteRecord') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon far fa-trash-alt"></i></div>');
                break;
            case 'action-change-state':
                newElem = $('<div class="item-container action-change-state"><div class="item-inner">' + lngHelper.get('bProcessDesign.changeState') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon far fa-edit"></i></div>');
                break;
            case 'action-set-parameter':
                newElem = $('<div class="item-container action-set-parameter"><div class="item-inner">' + lngHelper.get('bProcessDesign.setParameter') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon fas fa-sliders-h"></i></div>');
                break;
            case 'action-execute-function':
                newElem = $('<div class="item-container action-execute-function"><div class="item-inner">' + lngHelper.get('bProcessDesign.execFunction') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div><i class="action-icon fas fa-code"></i></div>');
                break;
            case 'event-start':
                newElem = $('<div class="item-container event-start"><div class="item-inner event-start-inner">' + lngHelper.get('bProcessDesign.startAction') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div></div>');
                break;
            case 'event-end':
                newElem = $('<div class="item-container event-end"><div class="item-inner event-end-inner">' + lngHelper.get('bProcessDesign.endAction') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div></div>');
                break;
            case 'gateway-regular':
                newElem = $('<div class="item-container gateway-regular"><div class="item-inner gateway-regular-inner">' + lngHelper.get('bProcessDesign.gateway') + '</div><div class="action-conpoint action-conpoint-left"></div><div class="action-conpoint action-conpoint-right"></div><div class="action-conpoint action-conpoint-top"></div><div class="action-conpoint action-conpoint-bottom"></div></div>');
                break;
        }
        var elemNumber = newElemNumber();
        var connectors = newElem.find('.action-conpoint');
        connectors.attr('ondragstart', 'onActionConnectorDragStart(event)')
            //.attr('ondragover', 'onActionConnectorDragOver(event)')
            .attr('ondragleave', 'onDragLeave(event)')
            .attr('ondrop', 'onActionConnectorDrop(event)')
            .attr('draggable', 'true');
        newElem.find('.action-conpoint-left').attr('elemnumber', elemNumber + '-left');
        newElem.find('.action-conpoint-right').attr('elemnumber', elemNumber + '-right');
        newElem.find('.action-conpoint-top').attr('elemnumber', elemNumber + '-top');
        newElem.find('.action-conpoint-bottom').attr('elemnumber', elemNumber + '-bottom');
        if (newElem) {
            newElem.css('top', (e.clientY - startY - containerOffset.top).toString() + 'px');
            newElem.css('left', (e.clientX - startY - containerOffset.left).toString() + 'px');
            newElem.attr('elemnumber', elemNumber);
            newElem.attr('draggable', 'true');
            newElem.attr('ondragstart', 'onDragStart(event)');
            newElem.attr('ondblclick', 'onActionDblClick(event)');
            $('#businessProcessContainer').append(newElem);
        }
    } else {
        var sourceElem = $('[elemNumber=' + e.dataTransfer.getData('elemnumber') + ']');
        if (sourceElem.hasClass('selected')) {
            var position = sourceElem.position();
            var distance = {
                x: e.clientX - startX - containerOffset.left - position.left,
                y: e.clientY - startY - containerOffset.top - position.top
            }
            $('.main-center .item-container.selected').each((index, item) => {
                item = $(item);
                var position = item.position();
                item.css('top', (position.top + distance.y).toString() + 'px');
                item.css('left', (position.left + distance.x).toString() + 'px');
                updateActionLines(item);
            })
        } else {
            sourceElem.css('top', (e.clientY - startY - containerOffset.top).toString() + 'px');
            sourceElem.css('left', (e.clientX - startX - containerOffset.left).toString() + 'px');
            updateActionLines(sourceElem);
        }
    }
    this.processHistory.add();
}
function newElemNumber() {
    var number = 1;
    $('.item-container').each((index, item) => {
        var tmp = $(item).attr('elemnumber');
        if (tmp)
            tmp = parseInt(tmp);
        if (tmp >= number)
            number = tmp + 1;
    })
    return number;
}
function setLineCoords(elem1, elem2, line) {
    var isDragging = elem1.hasClass('dragging');
    if (!isDragging) {
        elem1.addClass('dragging');
        elem2.addClass('dragging');
    }                
    var offset1 = elem1.offset();
    var offset2 = elem2.offset();
    var containerOffset = $('#businessProcessContainer').offset();
    var x1 = offset1.left - containerOffset.left + elem1.width() / 2;
    var y1 = offset1.top - containerOffset.top + elem1.height() / 2;
    var x2 = offset2.left - containerOffset.left + elem2.width() / 2;
    var y2 = offset2.top - containerOffset.top + elem2.height() / 2; 
    line.attr('x1', x1.toString());
    line.attr('y1', y1.toString());
    line.attr('x2', x2.toString());
    line.attr('y2', y2.toString());
    if (!isDragging) {
        elem1.removeClass('dragging');
        elem2.removeClass('dragging');
    }
}
function createLine(elem1, elem2) {
    var svg = $('svg');
    var line = $(document.createElementNS('http://www.w3.org/2000/svg', 'line'));
    setLineCoords(elem1, elem2, line);
    line.attr('connectornumber', newLineNumber());
    line.addClass('conline');
    line.attr('sourceelemnumber',elem1.attr('elemnumber'));
    line.attr('targetelemnumber', elem2.attr('elemnumber'));
    line.attr('sourceelemrootnumber', elem1.parents('.item-container').attr('elemnumber'));
    line.attr('targetelemrootnumber', elem2.parents('.item-container').attr('elemnumber'));
    line.attr('ondblclick', 'onConnectorDblClick(event)');
    line.attr('marker-end', 'url(#arrowhead)');
    svg.append(line);
}
function newLineNumber() {
    var number = 1;
    $('.conline').each((index, item) => {
        var tmp = $(item).attr('connectornumber');
        if (tmp)
            tmp = parseInt(tmp);
        if (tmp >= number)
            number = tmp + 1;
    })
    return number;
}
function updateActionLines(actionElem) {
    $('.conline[sourceelemrootnumber=' + actionElem.attr('elemnumber') + ']').each((index, item) => {
        var line = $(item);
        setLineCoords($('[elemNumber=' + line.attr('sourceelemnumber') + ']'), $('[elemNumber=' + line.attr('targetelemnumber') + ']'), line);
    })
    $('.conline[targetelemrootnumber=' + actionElem.attr('elemnumber') + ']').each((index, item) => {
        var line = $(item);
        setLineCoords($('[elemNumber=' + line.attr('sourceelemnumber') + ']'), $('[elemNumber=' + line.attr('targetelemnumber') + ']'), line);
    })
}
async function initActionModal() {
    var users = await service.call('fUser/getList', 'GET', {});
    var options = [];
    for (var i = 0; i < users.length; i++) {
        options.push({
            value: '0|' + users[i].recID,
            text: users[i].userName
        })
    }
    utils.fillSelectOptions($('#actionAssigneeUsers'), options, true);
    //utils.fillSelectOptions($('#actionNotificationTargetUsers'), options, true); 
    var groups = await service.call('fUserGroup/getList', 'GET', {});
    options = [];
    for (var i = 0; i < groups.length; i++) {
        options.push({
            value: '1|' + groups[i].recID,
            text: groups[i].displayName
        })
    }
    utils.fillSelectOptions($('#actionAssigneeGroups'), options, true);
    //utils.fillSelectOptions($('#actionNotificationTargetGroups'), options, true);

    var options = [];
    for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
        options.push({
            value: '2|' + window.currentBProcessPackage.parameters[i].parameterNumber,
            text: window.currentBProcessPackage.parameters[i].displayName
        })
    }
    utils.fillSelectOptions($('#actionAssigneeParameter'), options, true);

    $('#actionAssignee').selectpicker();

    //TODO : Yukarıdaki bölüm de bu formata dönüştürülmeli
    var optGroups = [];
    var options = [];
    for (var i = 0; i < users.length; i++) {
        options.push({
            value: window.fEnums.FBProcessRoleType.User.toString() + '|' + users[i].recID,
            text: users[i].userName
        })
    }
    optGroups.push({
        label: lngHelper.get('bProcessDesign.roleTypeUser'),
        id: window.fEnums.FBProcessRoleType.User.toString(),
        options: options
    })
    options = [];
    for (var i = 0; i < groups.length; i++) {
        options.push({
            value: window.fEnums.FBProcessRoleType.UserGroup.toString() + '|' + groups[i].recID,
            text: groups[i].displayName
        })
    }
    optGroups.push({
        label: lngHelper.get('bProcessDesign.roleTypeUserGroup'),
        id: window.fEnums.FBProcessRoleType.UserGroup.toString(),
        options: options
    })
    options = [];
    for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
        options.push({
            value: window.fEnums.FBProcessRoleType.ProcessParameter.toString() + '|' + window.currentBProcessPackage.parameters[i].parameterNumber,
            text: window.currentBProcessPackage.parameters[i].displayName
        })
    }
    optGroups.push({
        label: lngHelper.get('bProcessDesign.roleTypeProcessParameter'),
        id: window.fEnums.FBProcessRoleType.ProcessParameter.toString(),
        options: options
    })
    options = [];
    for (var i = 0; i < window.currentBProcessPackage.elements.length; i++) {
        var element = window.currentBProcessPackage.elements[i];
        if (element.elementType == FBProcessElementType.Start || element.elementType == FBProcessElementType.UserAction) {
            options.push({
                value: window.fEnums.FBProcessRoleType.RunElementUser.toString() + '|' + element.elementNumber,
                text: element.displayName
            })
        }
    }
    optGroups.push({
        label: lngHelper.get('bProcessDesign.roleTypeRunElementUser'),
        id: window.fEnums.FBProcessRoleType.RunElementUser.toString(),
        options: options
    })    
    utils.fillSelectOptionGroups($('#actionNotificationTargetRoles'), optGroups, true);

    $('#actionNotificationTargetRoles').selectpicker();

    var objects = await service.call('fObject/getList', 'GET', {});
    options = [];
    for (var i = 0; i < objects.length; i++) {
        options.push({
            value: objects[i].recID,
            text: objects[i].displayName
        })
    }
    utils.fillSelectOptions($('#actionCreateRecordObject'), options);
    utils.fillSelectOptions($('#actionUserActionOpenRecordObject'), options);
    utils.fillSelectOptions($('#actionUserActionSelectRecordObject'), options);    

    options = [];
    var forms = await service.call('fForm/getList', 'GET', {});
    for (var i = 0; i < forms.length; i++) {
        options.push({
            text: forms[i].displayName,
            value: forms[i].recID,
            attributes: {
                fobjectid: forms[i].fObjectID
            }
        })
    }
    $('#actionUserActionOpenRecordForm').data('allOptions', options);
    //$('#actionForm').selectpicker();

    var tables = await service.call('fTable/getList', 'GET', {});
    options = [];
    for (var i = 0; i < tables.length; i++) {
        options.push({
            text: tables[i].displayName,
            value: tables[i].recID,
            attributes: {
                fobjectid: tables[i].fObjectID
            }
        })
    }
    $('#actionUserActionSelectRecordTable').data('allOptions', options);
}
function btnAddProcessEventClick() {
    $('#modalEditProcessEvent').data('recID', utils.emptyGuid);
    $('#processEventDisplayName').val('');
    $('#processEventAskComment').prop('checked', false);
    $('#processEventType').val('0');
    $('#processEventTimeoutDuration').val('');
    processEventTypeChange();
    $('#modalEditProcessEvent').modal('show');
}
function btnEditProcessEventClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    $('#modalEditProcessEvent').data('recID', selectedRow.data()[0]);
    $('#processEventDisplayName').val(selectedRow.data()[1]);
    $('#processEventType').val(selectedRow.data()[3]);
    var timeoutDuration = '';
    var props = selectedRow.data()[4];
    if (props) {
        props = JSON.parse(props);
        timeoutDuration = props.timeoutDuration;
    }
    $('#processEventTimeoutDuration').val(timeoutDuration);
    processEventTypeChange();
    $('#processEventAskComment').prop('checked', false);
    if (props && props.askComment)
        $('#processEventAskComment').prop('checked', true);
    $('#modalEditProcessEvent').modal('show');
}
function processEventTypeChange() {
    if (parseInt($('#processEventType').val()) == FBProcessEventType.Timeout) {
        $('#processEventTimeoutDuration').parents('.form-group').show();
        $('#processEventAskComment').parents('.form-group').hide();
    }
    else {
        $('#processEventTimeoutDuration').parents('.form-group').hide();
        $('#processEventAskComment').parents('.form-group').show();
    }
}
function btnDeleteProcessEventClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    utils.confirmDelete(() => {
        for (var i = 0; i < window.currentBProcessPackage.events.length; i++) {
            if (window.currentBProcessPackage.events[i].eventNumber == row.data()[0]) {
                window.currentBProcessPackage.events.splice(i, 1);
                break;
            }
        }
        fillProcessEventTable();
    })
}
function fillProcessEventTable() {
    if (!window.processEventDT) {
        window.processEventDT = new dataTableHelper('processEventDataTable', {
            data: getProcessEventsDataSet(),
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging: false,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('bProcessDesign.name') },
                { title: lngHelper.get('bProcessDesign.eventType'), visible:false },
                { title: 'eventType', visible: false },
                { title: 'props', visible: false }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditProcessEventClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteProcessEventClick'
            }],
            dblClickFunction: 'btnEditProcessEventClick'
        });
    } else
        window.processEventDT.dtObject.clear().rows.add(getProcessEventsDataSet()).draw();
}
function getProcessEventsDataSet() {
    var dataSet = [];
    for (var i = 0; i < window.currentBProcessPackage.events.length; i++) {
        dataSet.push([
            window.currentBProcessPackage.events[i].eventNumber.toString(),
            window.currentBProcessPackage.events[i].displayName,
            window.currentBProcessPackage.events[i].eventType ? lngHelper.get('bProcessDesign.eventTypeTimeout') : lngHelper.get('bProcessDesign.eventTypeUserDefined'),
            window.currentBProcessPackage.events[i].eventType ? window.currentBProcessPackage.events[i].eventType : 0,
            window.currentBProcessPackage.events[i].props ? window.currentBProcessPackage.events[i].props : '']);
    }
    return dataSet;
}
function btnEditProcessEventSaveClick() {
    if (!$('#processEventDisplayName').val()) {
        utils.alertWarning(lngHelper.get('bProcessDesign.eventNameIsRequired'));
        return;
    }
    var event = null;
    var editRecID = $('#modalEditProcessEvent').data('recID');
    var bIsNew = editRecID == utils.emptyGuid;
    if (bIsNew) {
        var newNumber = 0;
        for (var i = 0; i < window.currentBProcessPackage.events.length; i++) {
            if (newNumber < window.currentBProcessPackage.events[i].eventNumber)
                newNumber = window.currentBProcessPackage.events[i].eventNumber;
        }
        newNumber++;
        event = {
            recID: utils.emptyGuid,
            eventNumber: newNumber
        }
    } else {
        for (var i = 0; i < window.currentBProcessPackage.events.length; i++) {
            if (window.currentBProcessPackage.events[i].eventNumber == editRecID) {
                event = window.currentBProcessPackage.events[i];
                break;
            }
        }
    }
    if (event) {
        var displayName = $('#processEventDisplayName').val();
        for (var i = 0; i < window.currentBProcessPackage.events.length; i++) {
            var tmpEvent = window.currentBProcessPackage.events[i];
            if (tmpEvent.eventNumber != event.eventNumber && tmpEvent.displayName == displayName) {
                utils.alertWarning(lngHelper.get('bProcessDesign.displayNameExists'));
                return;
            }
        }
        if (bIsNew)
            window.currentBProcessPackage.events.push(event);
        event.displayName = displayName;
        event.eventType = parseInt($('#processEventType').val());
        event.props = JSON.stringify({
            timeoutDuration: parseInt($('#processEventTimeoutDuration').val()),
            askComment: $('#processEventAskComment').is(':checked')
        })
    }
    fillProcessEventTable();
    $('#modalEditProcessEvent').modal('hide');
}
function btnAddProcessStateClick() {
    $('#modalEditProcessState').data('recID', utils.emptyGuid);
    $('#processStateDisplayName').val('');
    $('#modalEditProcessState').modal('show');
}
function btnEditProcessStateClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    $('#modalEditProcessState').data('recID', row.data()[0]);
    $('#processStateDisplayName').val(row.data()[1]);
    $('#modalEditProcessState').modal('show');
}
function btnDeleteProcessStateClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    utils.confirmDelete(() => {
        for (var i = 0; i < window.currentBProcessPackage.states.length; i++) {
            if (window.currentBProcessPackage.states[i].stateNumber == row.data()[0]) {
                window.currentBProcessPackage.states.splice(i, 1);
                break;
            }
        }
        fillProcessStateTable();
    })
}
function fillProcessStateTable() {
    if (!window.processStateDT) {
        window.processStateDT = new dataTableHelper('processStateDataTable', {
            data: getProcessStatesDataSet(),
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging: false,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('bProcessDesign.name') }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditProcessStateClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteProcessStateClick'
            }],
            dblClickFunction: 'btnEditProcessStateClick'
        });
    } else
        window.processStateDT.dtObject.clear().rows.add(getProcessStatesDataSet()).draw();
}
function getProcessStatesDataSet() {
    var dataSet = [];
    for (var i = 0; i < window.currentBProcessPackage.states.length; i++) {
        dataSet.push([window.currentBProcessPackage.states[i].stateNumber.toString(), window.currentBProcessPackage.states[i].displayName]);
    }
    return dataSet;
}
function btnEditProcessStateSaveClick() {
    if (!$('#processStateDisplayName').val()) {
        utils.alertWarning('State name is required.');
        return;
    }
    var state = null;
    var editRecID = $('#modalEditProcessState').data('recID');
    var bIsNew = editRecID == utils.emptyGuid;
    if (bIsNew) {
        var newNumber = 0;
        for (var i = 0; i < window.currentBProcessPackage.states.length; i++) {
            if (newNumber < window.currentBProcessPackage.states[i].stateNumber)
                newNumber = window.currentBProcessPackage.states[i].stateNumber;
        }
        newNumber++;
        state = {
            recID: utils.emptyGuid,
            stateNumber: newNumber,
        }

    } else {
        for (var i = 0; i < window.currentBProcessPackage.states.length; i++) {
            if (window.currentBProcessPackage.states[i].stateNumber == editRecID) {
                state = window.currentBProcessPackage.states[i];
                break;
            }
        }
    }
    if (state) {
        var displayName = $('#processStateDisplayName').val();
        for (var i = 0; i < window.currentBProcessPackage.states.length; i++) {
            var tmpState = window.currentBProcessPackage.states[i];
            if (tmpState.stateNumber != event.stateNumber && tmpState.displayName == displayName) {
                utils.alertWarning(lngHelper.get('bProcessDesign.displayNameExists'));
                return;
            }
        }
        state.displayName = displayName;
        if (bIsNew)
            window.currentBProcessPackage.states.push(state);
    }
    fillProcessStateTable();
    $('#modalEditProcessState').modal('hide');
}

// #region ACTION USERACTION
function actionUserActionTypeChange() {
    var userActionType = parseInt($('#actionUserActionType').val());
    switch (userActionType) {
        case window.fEnums.FBProcessUserActionType.OpenRecord:
            $('#actionUserActionOpenRecords').parents('.form-group').show();
            $('#actionUserActionSelectRecordObject').parents('.form-group').hide();
            $('#actionUserActionSelectRecordTable').parents('.form-group').hide();
            $('#actionUserActionSelectRecordIDParameter').parents('.form-group').hide();
            break;
        case window.fEnums.FBProcessUserActionType.SelectRecord:
            $('#actionUserActionOpenRecords').parents('.form-group').hide();
            $('#actionUserActionSelectRecordObject').parents('.form-group').show();
            $('#actionUserActionSelectRecordTable').parents('.form-group').show();
            $('#actionUserActionSelectRecordIDParameter').parents('.form-group').show();
            break;
    }
}
function actionUserActionObjectChange() {
    var objectID = $('#actionUserActionObject').val();
    var forms = $('#actionForm').data('allOptions');
    var options = [];
    for (var i = 0; i < forms.length; i++) {
        if (forms[i].attributes.fobjectid == objectID) {
            options.push(forms[i]);
        }
    }
    utils.fillSelectOptions($('#actionForm'), options);
    $('#actionForm').selectpicker('refresh');
    var tables = $('#actionUserActionTable').data('allOptions');
    options = [];
    for (var i = 0; i < tables.length; i++) {
        if (tables[i].attributes.fobjectid == objectID) {
            options.push(tables[i]);
        }
    }
    utils.fillSelectOptions($('#actionUserActionTable'), options);
}

function fillActionUserActionOpenRecordCreateElement() {
    var options = [];
    $('.action-create-record').each((index, item) => {
        if ($(item).attr('elemnumber')) {
            options.push({
                text: $(item).find('.item-inner').html(),
                value: $(item).attr('elemnumber')
            })
        }
    });
    utils.fillSelectOptions($('#actionUserActionOpenRecordCreateElement'), options);
}
function fillTblActionUserActionOpenRecords() {
    if (!window.tblActionUserActionOpenRecordsDT) {
        window.tblActionUserActionOpenRecordsDT = new dataTableHelper('tblActionUserActionOpenRecords', {
            data: [],
            dom: '<"top">rt<"bottom"<p>><"clear">',
            columns: [
                { title: lngHelper.get('bProcessDesign.actionUserActionOpenRecordType') },
                { title: lngHelper.get('bProcessDesign.actionUserActionOpenRecordText') }
            ],
            order: [[1, "asc"]]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditActionUserActionOpenRecordClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteActionUserActionOpenRecordClick'
            }],
            dblClickFunction: 'btnEditActionUserActionOpenRecordClick'
        });
    }
    var props = $('#modalEditAction').data('props');
    if (!props)
        props = {};
    if (!props.openRecords)
        props.openRecords = [];

    var dataSet = [];
    for (var i = 0; i < props.openRecords.length; i++) {
        var r = props.openRecords[i];
        var type = '';
        for (const [key, value] of Object.entries(window.fEnums.FBProcessUserActionOpenRecordType)) {
            if (r.type == value) {
                type = lngHelper.get('bProcessDesign.actionUserActionOpenRecordType_' + key);
            }
        }
        dataSet.push([
            type,
            r.displayName
        ]);
    }
    window.tblActionUserActionOpenRecordsDT.updateData(dataSet);
}
function actionUserActionOpenRecordSave() {
    var props = $('#modalEditAction').data('props');
    if (!props)
        props = {};
    if (!props.openRecords)
        props.openRecords = [];
    var recIndex = $('#modalActionUserActionOpenRecordEdit').data('recIndex');
    var item = null;
    if (recIndex < 0) {
        item = {};
        props.openRecords.push(item);
    }
    else
        item = props.openRecords[recIndex];
    if (item != null) {
        item.type = parseInt($('#actionUserActionOpenRecordType').val());
        switch (item.type) {
            case window.fEnums.FBProcessUserActionOpenRecordType.CreatedRecord:
                item.createElementNumber = parseInt($('#actionUserActionOpenRecordCreateElement').val());
                item.fFormID = $('#actionUserActionOpenRecordForm').val();
                item.fObjectID = $('#actionUserActionOpenRecordForm option:selected').attr('fobjectid');
                item.displayName = $('#actionUserActionOpenRecordCreateElement option:selected').text();
                break;
            case window.fEnums.FBProcessUserActionOpenRecordType.AnyRecord:
                item.fObjectID = $('#actionUserActionOpenRecordObject').val();
                item.fFormID = $('#actionUserActionOpenRecordForm').val();
                item.dataRecIDParamNumber = parseInt($('#actionUserActionOpenRecordParameter').val());
                item.displayName = $('#actionUserActionOpenRecordObject option:selected').text();
                break;
        }
    }
    $('#modalEditAction').data('props', props);
    $('#modalActionUserActionOpenRecordEdit').modal('hide');
    fillTblActionUserActionOpenRecords();
}
function actionUserActionOpenRecordTypeChange() {
    var type = parseInt($('#actionUserActionOpenRecordType').val());
    switch (type) {
        case window.fEnums.FBProcessUserActionOpenRecordType.CreatedRecord:
            $('#actionUserActionOpenRecordCreateElement').parents('.form-group').show();
            $('#actionUserActionOpenRecordObject').parents('.form-group').hide();
            $('#actionUserActionOpenRecordForm').parents('.form-group').show();
            $('#actionUserActionOpenRecordParameter').parents('.form-group').hide();
            break;
        case window.fEnums.FBProcessUserActionOpenRecordType.AnyRecord:
            $('#actionUserActionOpenRecordCreateElement').parents('.form-group').hide();
            $('#actionUserActionOpenRecordObject').parents('.form-group').show();
            $('#actionUserActionOpenRecordForm').parents('.form-group').show();
            $('#actionUserActionOpenRecordParameter').parents('.form-group').show();
            break;
    }
}
function btnAddActionUserActionOpenRecordClick() {
    $('#actionUserActionOpenRecordType').val('0');
    actionUserActionOpenRecordTypeChange();
    fillActionUserActionOpenRecordCreateElement();
    $('#modalActionUserActionOpenRecordEdit').data('recIndex', -1);
    $('#modalActionUserActionOpenRecordEdit').modal('show');
}
function btnEditActionUserActionOpenRecordClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    $('#modalActionUserActionOpenRecordEdit').data('recIndex', rowIndex);
    var props = $('#modalEditAction').data('props');
    var item = props.openRecords[rowIndex];
    $('#actionUserActionOpenRecordType').val(item.type.toString());
    actionUserActionOpenRecordTypeChange();
    fillActionUserActionOpenRecordCreateElement();
    switch (item.type) {
        case window.fEnums.FBProcessUserActionOpenRecordType.CreatedRecord:
            $('#actionUserActionOpenRecordCreateElement').val(item.createElementNumber.toString());
            actionUserActionOpenRecordCreateElementChange();
            $('#actionUserActionOpenRecordForm').val(item.fFormID);
            break;
        case window.fEnums.FBProcessUserActionOpenRecordType.AnyRecord:
            $('#actionUserActionOpenRecordObject').val(item.fObjectID);
            actionUserActionOpenRecordObjectChange();
            $('#actionUserActionOpenRecordForm').val(item.fFormID);
            $('#actionUserActionOpenRecordParameter').val(item.dataRecIDParamNumber);
            break;
    }
    $('#modalActionUserActionOpenRecordEdit').modal('show');
}
function btnDeleteActionUserActionOpenRecordClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    var props = $('#modalEditAction').data('props');
    if (props.openRecords && props.openRecords.length > rowIndex)
        props.openRecords.splice(rowIndex, 1);
    $('#modalEditAction').data('props', props);
    fillTblActionUserActionOpenRecords();
}
function actionUserActionOpenRecordObjectChange() {
    var fObjectID = $('#actionUserActionOpenRecordObject').val();
    var allOptions = $('#actionUserActionOpenRecordForm').data('allOptions');
    var options = [];
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].attributes.fobjectid == fObjectID) {
            options.push(allOptions[i]);
        }
    }
    utils.fillSelectOptions($('#actionUserActionOpenRecordForm'), options);
}
function actionUserActionSelectRecordObjectChange() {
    var fObjectID = $('#actionUserActionSelectRecordObject').val();
    var allOptions = $('#actionUserActionSelectRecordTable').data('allOptions');
    var options = [];
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].attributes.fobjectid == fObjectID) {
            options.push(allOptions[i]);
        }
    }
    utils.fillSelectOptions($('#actionUserActionSelectRecordTable'), options);
}
function actionUserActionOpenRecordCreateElementChange() {
    var allOptions = $('#actionUserActionOpenRecordForm').data('allOptions');
    var elemNumber = $('#actionUserActionOpenRecordCreateElement').val();

    var fObjectID = $('[elemnumber=' + elemNumber + ']').attr('createrecordobjectid');
    var options = [];
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].attributes.fobjectid == fObjectID) {
            options.push(allOptions[i]);
        }
    }
    utils.fillSelectOptions($('#actionUserActionOpenRecordForm'), options);
}

// #endregion

// #region PROCESSPARAMETER START    
function btnAddProcessParameterClick() {
    $('#modalEditProcessParameter').data('recID', utils.emptyGuid);
    $('#processParameterDisplayName').val('');
    //$('#processParameterProps').val('');
    //$('#processParameterType').val(0);
    //processParameterTypeChange(); 
    $('#modalEditProcessParameter').modal('show');
}
function btnEditProcessParameterClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    $('#modalEditProcessParameter').data('recID', selectedRow.data()[0]);
    $('#processParameterDisplayName').val(selectedRow.data()[1]);
    //$('#processParameterType').val(selectedRow.data()[2]);
    //$('#processParameterProps').val(selectedRow.data()[3]);
    //processParameterTypeChange();
    //if (parseInt($('#processParameterType').val()) == window.fEnums.FBProcessParameterType.CreatedRecordID)
    //    processParameterTargetElementChange();
    $('#modalEditProcessParameter').modal('show');
}
function btnDeleteProcessParameterClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var selectedID = selectedRow.data()[0];
    utils.confirmDelete(() => {
        for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
            if (window.currentBProcessPackage.parameters[i].parameterNumber == selectedID) {
                window.currentBProcessPackage.parameters.splice(i, 1);
                break;
            }
        }
        fillProcessParameterTable();
    })
}
function fillProcessParameterTable() {
    if (!window.processParameterDT) {
        window.processParameterDT = new dataTableHelper('processParameterDataTable', {
            data: getProcessParametersDataSet(),
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging: false,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('bProcessDesign.name') }
                //{ title: "Type", visible: false },
                //{ title: "Props", visible: false }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditProcessParameterClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteProcessParameterClick'
            }],
            dblClickFunction: 'btnEditProcessParameterClick'
        });
    } else
        window.processParameterDT.dtObject.clear().rows.add(getProcessParametersDataSet()).draw();
}
function getProcessParametersDataSet() {
    var dataSet = [];
    for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
        dataSet.push([
            window.currentBProcessPackage.parameters[i].parameterNumber.toString(),
            window.currentBProcessPackage.parameters[i].displayName,
            window.currentBProcessPackage.parameters[i].parameterType,
            window.currentBProcessPackage.parameters[i].props
        ]);
    }
    return dataSet;
}
function btnEditProcessParameterSaveClick() {
    if (!$('#processParameterDisplayName').val()) {
        utils.alertWarning(lngHelper.get('bProcessDesign.parameterIsRequired'));
        return;
    }
    var currentProcessParameter = null;
    var editRecID = $('#modalEditProcessParameter').data('recID');
    var bIsNew = editRecID == utils.emptyGuid;

    if (bIsNew) {
        var newNumber = 0;
        for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
            if (newNumber < window.currentBProcessPackage.parameters[i].parameterNumber)
                newNumber = window.currentBProcessPackage.parameters[i].parameterNumber;
        }
        newNumber++;
        currentProcessParameter = {
            recID: utils.emptyGuid,
            parameterNumber: newNumber
        }        
    } else {
        for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
            if (window.currentBProcessPackage.parameters[i].parameterNumber == editRecID) {
                currentProcessParameter = window.currentBProcessPackage.parameters[i];
                break;
            }
        }
    }
    var displayName = $('#processParameterDisplayName').val();
    for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
        var tmpParameter = window.currentBProcessPackage.parameters[i];
        if (tmpParameter.parameterNumber != currentProcessParameter.parameterNumber && tmpParameter.displayName == displayName) {
            utils.alertWarning(lngHelper.get('bProcessDesign.displayNameExists'));
            return;
        }
    }
    currentProcessParameter.displayName = displayName;
    if (bIsNew)
        window.currentBProcessPackage.parameters.push(currentProcessParameter);
    fillProcessParameterTable();
    $('#modalEditProcessParameter').modal('hide');
}

// #endregion

// #region ACTIONUPDATERECORDPROPERTYVALUES START
async function initActionUpdateRecordPropertyModal() {
    fillParameterList($('#actionUpdateRecordIDParameterNumber'));
    if (!window.actionUpdateRecordPropertyValueDT) {
        window.actionUpdateRecordPropertyValueDT = new dataTableHelper('actionUpdateRecordPropertyValues', {
            data: [],
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging:false,
            //scrollX: true,   
            columns: [
                { title: "rowID", visible: false },
                { title: 'propertyRecID', visible: false },
                { title: lngHelper.get('bProcessDesign.propertyDisplayName') },
                { title: 'parameterNumber', visible: false },
                { title: lngHelper.get('bProcessDesign.parameterDisplayName') }
            ],
            order: [[3, "asc"]]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditActionUpdateRecordPropertyValueClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteActionUpdateRecordPropertyValueClick'
            }],
            dblClickFunction: 'btnEditActionUpdateRecordPropertyValueClick'
        });
    }
}
async function actionCreateRecordObjectChange() {
    $('#actionUpdateRecordProperty').html('');
    if ($('#actionCreateRecordObject').val() != "0") {
        var properties = await service.call('fObjectProperty/getList', 'GET', { fObjectID: $('#actionCreateRecordObject').val() });
        var options = [];
        for (var i = 0; i < properties.length; i++) {
            options.push({
                value: properties[i].recID,
                text: properties[i].displayName
            });
        }
        utils.fillSelectOptions($('#actionUpdateRecordProperty'), options);
    }
    $('#actionUpdateRecordPropertyValues').data('tableData', []);
    fillActionUpdateRecordPropertyValueTable();
}
function btnAddActionUpdateRecordPropertyValueClick() {
    fillParameterList($('#actionUpdateRecordParameter'));
    $('#actionUpdateRecordProperty').val('0');
    $('#modalEditActionUpdateRecordPropertyValue').data('rowID', 0);
    $('#modalEditActionUpdateRecordPropertyValue').modal('show');
}
function btnEditActionUpdateRecordPropertyValueClick(e) {
    var row = dataTableHelper.getRowFromElem(e);

    if (row) {
        $('#actionUpdateRecordProperty').val(row.data()[1]);
        fillParameterList($('#actionUpdateRecordParameter'));
        $('#actionUpdateRecordParameter').val(row.data()[3]);
        $('#modalEditActionUpdateRecordPropertyValue').data('rowID', row.data()[0]);
        $('#modalEditActionUpdateRecordPropertyValue').modal('show');
    }
}
function btnDeleteActionUpdateRecordPropertyValueClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    if (row) {
        utils.confirmDelete(() => {
            var tableData = $('#actionUpdateRecordPropertyValues').data('tableData');
            if (!tableData)
                tableData = [];
            var rowID = row.data()[0];
            for (var i = 0; i < tableData.length; i++) {
                if (tableData[i][0] == rowID) {
                    tableData.splice(i, 1);
                    break;
                }
            }
            $('#actionUpdateRecordPropertyValues').data('tableData', tableData);
            fillActionUpdateRecordPropertyValueTable();
        })
    }
}
function fillActionUpdateRecordPropertyValueTable() {
    window.actionUpdateRecordPropertyValueDT.dtObject.clear();
    var tableData = $('#actionUpdateRecordPropertyValues').data('tableData');
    if (!tableData)
        tableData = [];
    window.actionUpdateRecordPropertyValueDT.dtObject.clear();
    window.actionUpdateRecordPropertyValueDT.dtObject.rows.add(tableData);
    window.actionUpdateRecordPropertyValueDT.dtObject.draw();
}
function btnEditActionUpdateRecordPropertyValueSaveClick() {
    if ($('#actionUpdateRecordProperty').val() == '0') {
        utils.alertWarning(lngHelper.get('bProcessDesign.selectProperty'));
        return;
    }
    if ($('#actionUpdateRecordParameter').val() == '0') {
        utils.alertWarning(lngHelper.get('bProcessDesign.selectParameter'));
        return;
    }
    var tableData = $('#actionUpdateRecordPropertyValues').data('tableData');
    if (!tableData)
        tableData = [];
    var rowID = $('#modalEditActionUpdateRecordPropertyValue').data('rowID');

    var rowData = [
        rowID,
        $('#actionUpdateRecordProperty').val(),
        $('#actionUpdateRecordProperty option:selected').text(),
        parseInt($('#actionUpdateRecordParameter').val()),
        $('#actionUpdateRecordParameter option:selected').text()
    ]
    if (rowID == 0) {
        rowID = 1;
        for (var i = 0; i < tableData.length; i++) {
            if (tableData[i][0] >= rowID)
                rowID = tableData[i][0] + 1;
        }
        rowData[0] = rowID;
        tableData.push(rowData);
    } else {
        for (var i = 0; i < tableData.length; i++) {
            if (tableData[i][0] == rowID) {
                tableData[i] = rowData;
                break;
            }
        }
    }
    $('#actionUpdateRecordPropertyValues').data('tableData', tableData);
    fillActionUpdateRecordPropertyValueTable();
    $('#modalEditActionUpdateRecordPropertyValue').modal('hide');
}
// #endregion

// #region GATEWAY START

function initActionGatewayModal() {
    fillParameterList($('#gatewayConditionParameter'));

    var options = [];
    for (const [key, value] of Object.entries(GateWayOperator)) {
        options.push({
            value: key,
            text:key
        })
    }
    utils.fillSelectOptions($('#gatewayConditionOperator'), options);

    if (!window.actionGatewayConditionsDT) {
        window.actionGatewayConditionsDT = new dataTableHelper('actionGatewayConditions', {
            data: [],
            dom: '<"top">rt<"bottom"<>><"clear">',
            //scrollX: true, 
            columns: [
                { title: "rowID", visible: false },
                { title: lngHelper.get('bProcessDesign.displayName') },
                { title: 'type', visible: false },
                { title: 'expression', visible: true }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditGatewayConditionClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteGatewayConditionClick'
            }],
            dblClickFunction: 'btnEditGatewayConditionClick'
        });
    }
    var sender = $('#actionGatewayContainer').data('sender');
    var tableData = [];
    var props = sender.attr('props');
    if (props) {
        props = JSON.parse(props);
        for (var i = 0; i < props.length; i++) {
            tableData.push([
                props[i].recID,
                props[i].displayName,
                props[i].complex,
                props[i].expression
            ]);
        }
    }
    $('#actionGatewayConditions').data('tableData', tableData);
    fillActionGatewayConditionsTable();
}
function btnAddGatewayConditionClick() {
    $('#modalEditGatewayCondition').data('rowID', 0);
    $('#gatewayConditionDisplayName').val('');
    $('#gatewayConditionParameter').val('0');
    $('#gatewayConditionOperator').val('0');
    $('#gatewayConditionValue').val('');
    $('#gatewayConditionExpression').val('');
    gatewayConditionComplexChange();
    $('#modalEditGatewayCondition').modal('show');
}
function btnEditGatewayConditionClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    if (row) {
        $('#modalEditGatewayCondition').data('rowID', row.data()[0]);
        $('#gatewayConditionDisplayName').val(row.data()[1]);
        var complex = row.data()[2] == 1;
        document.getElementById('gatewayConditionComplex').checked = (complex == 1);
        var eq = row.data()[3];
        if (complex != 1) {
            var match = eq.match(/([\s\S]*)(==|>|<|>=|<=)([\s\S]*)/i);
            var parameter = match[1].trim().replace(/{/g, '').replace(/}/g, '');
            $('#gatewayConditionParameter option').each((index, item) => {
                if ($(item).text() == parameter) {
                    $('#gatewayConditionParameter').val($(item).val());
                }
            })
            $('#gatewayConditionOperator').val(match[2].trim());
            $('#gatewayConditionValue').val(match[3].trim());
        }
        gatewayConditionComplexChange();
        $('#gatewayConditionExpression').val(eq);
        $('#modalEditGatewayCondition').modal('show');
    }
}
function btnDeleteGatewayConditionClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    if (row) {
        utils.confirmDelete(() => {
            var tableData = $('#actionGatewayConditions').data('tableData');
            if (!tableData)
                tableData = [];
            var rowID = row.data()[0];
            for (var i = 0; i < tableData.length; i++) {
                if (tableData[i][0] == rowID) {
                    tableData.splice(i, 1);
                    break;
                }
            }
            $('#actionGatewayConditions').data('tableData', tableData);
            fillActionGatewayConditionsTable();
        })
    }
}
function gatewayConditionComplexChange() {
    if (document.getElementById('gatewayConditionComplex').checked) {
        $('#gatewayConditionExpression').val('');
        $('#gatewayConditionExpression').parents('.form-group').show();
        $('#gatewayConditionParameter').parents('.form-group').hide();
        $('#gatewayConditionOperator').parents('.form-group').hide();
        $('#gatewayConditionValue').parents('.form-group').hide();
    } else {
        $('#gatewayConditionExpression').parents('.form-group').hide();
        $('#gatewayConditionParameter').parents('.form-group').show();
        $('#gatewayConditionOperator').parents('.form-group').show();
        $('#gatewayConditionValue').parents('.form-group').show();
    }
}
function modalEditGatewayConditionSaveClick() {
    var tableData = $('#actionGatewayConditions').data('tableData');
    if (!tableData)
        tableData = [];

    var complex = document.getElementById('gatewayConditionComplex').checked ? 1 : 0;
    if (complex != 1)
        $('#gatewayConditionExpression').val('{{' + $('#gatewayConditionParameter option:selected').text() + '}} ' + $('#gatewayConditionOperator').val() + ' ' + $('#gatewayConditionValue').val());
    var rowID = $('#modalEditGatewayCondition').data('rowID');
    var rowData = [
        rowID,
        $('#gatewayConditionDisplayName').val(),
        complex,
        $('#gatewayConditionExpression').val()
    ]
    if (rowData[1] == '')
        rowData[1] = rowData[3];
    if (rowID == 0) {
        rowID = 1;
        for (var i = 0; i < tableData.length; i++) {
            if (tableData[i][0] >= rowID)
                rowID = tableData[i][0] + 1;
        }
        rowData[0] = rowID;
        tableData.push(rowData);
    } else {
        for (var i = 0; i < tableData.length; i++) {
            if (tableData[i][0] == rowID) {
                tableData[i] = rowData;
                break;
            }
        }
    }
    $('#actionGatewayConditions').data('tableData', tableData);
    fillActionGatewayConditionsTable();
    $('#modalEditGatewayCondition').modal('hide');
}
function fillActionGatewayConditionsTable() {
    window.actionGatewayConditionsDT.dtObject.clear();
    var tableData = $('#actionGatewayConditions').data('tableData');
    if (!tableData)
        tableData = [];
    window.actionGatewayConditionsDT.dtObject.clear();
    window.actionGatewayConditionsDT.dtObject.rows.add(tableData);
    window.actionGatewayConditionsDT.dtObject.draw();
}
// #endregion

// #region ACTIONSETPARAMETER START

window.processSetParameterValueTypeArray = [
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.Generic, type: window.fEnums.FBProcessSetParameterValueType.Static, dataType:'text' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.Generic, type: window.fEnums.FBProcessSetParameterValueType.UrlParameter, dataType:'text' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.StarterUserInfo, type: window.fEnums.FBProcessSetParameterValueType.StarterUserID, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.StarterUserInfo, type: window.fEnums.FBProcessSetParameterValueType.StarterUserName, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.StarterUserInfo, type: window.fEnums.FBProcessSetParameterValueType.StarterUserSurname, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.StarterUserInfo, type: window.fEnums.FBProcessSetParameterValueType.StarterUserUserName, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.ProcessInfo, type: window.fEnums.FBProcessSetParameterValueType.ProcessStartTime, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.ProcessInfo, type: window.fEnums.FBProcessSetParameterValueType.ProcessRunID, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.ProcessInfo, type: window.fEnums.FBProcessSetParameterValueType.ActionTime, dataType:'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.RecordInfo, type: window.fEnums.FBProcessSetParameterValueType.CreateRecordID, dataType:'object' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.RecordInfo, type: window.fEnums.FBProcessSetParameterValueType.CreateRecordPropertyValue, dataType:'object' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.StarterUserInfo, type: window.fEnums.FBProcessSetParameterValueType.StarterUserEmailAddress, dataType: 'system' },
    { group: window.fEnums.FBProcessSetParameterValueTypeGroup.ProcessInfo, type: window.fEnums.FBProcessSetParameterValueType.ProcessRunStateDisplayName, dataType: 'system' }
];

function initActionSetParameterEditForm() {
    var options = [];
    for (var i = 0; i < window.processSetParameterValueTypeArray.length; i++) {
        var item = window.processSetParameterValueTypeArray[i];
        for (const [key, value] of Object.entries(window.fEnums.FBProcessSetParameterValueType)) {
            if (item.type == value) {
                options.push({
                    text: lngHelper.get('bProcessDesign.setParameterParamType_' + key),
                    value: item.type
                })
            }
        }
    }
    utils.fillSelectOptions($('#actionSetParameterValueType'), options, false, '-');
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.FBProcessSetParameterValueTypeGroup)) {
        options.push({
            text: lngHelper.get('bProcessDesign.setParameterParamTypeGroup_' + key),
            value: value
        })
    }
    utils.fillSelectOptions($('#actionSetParameterValueTypeGroup'), options, false, '-');
}
function btnAddActionSetParameterParamClick() {
    initActionSetParameterEditForm();
    fillParameterList($('#actionSetParameterParam'));
    $('#actionSetParameterValueTypeGroup').val('-');
    actionSetParameterValueTypeGroupChange();
    fillActionSetParameterValueAction();
    $('#modalActionSetParameterParam').data('recIndex', -1);
    $('#actionSetParameterValueText').val('');
    $('#modalActionSetParameterParam').modal('show');
}
async function btnEditActionSetParameterParamClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    $('#modalActionSetParameterParam').data('recIndex', rowIndex);
    var props = $('#modalEditAction').data('props');
    var param = props.params[rowIndex];
    initActionSetParameterEditForm();
    fillParameterList($('#actionSetParameterParam'));
    $('#actionSetParameterValueTypeGroup').val(param.valueTypeGroup);
    actionSetParameterValueTypeGroupChange();
    fillActionSetParameterValueAction();
    $('#actionSetParameterParam').val(param.parameterNumber);
    $('#actionSetParameterValueType').val(param.valueType);
    if (param.valueAction) {
        $('#actionSetParameterValueAction').val(param.valueAction);
    } else 
        $('#actionSetParameterValueAction').val('0');
    actionSetParameterValueTypeChange();
    await actionSetParameterValueActionChange();
    if (param.valueText)
        $('#actionSetParameterValueText').val(param.valueText);
    else
        $('#actionSetParameterValueText').val('');
    if (param.valueObjectProperty)
        $('#actionSetParameterValueObjectProperty').val(param.valueObjectProperty);
    $('#modalActionSetParameterParam').modal('show');
}
function btnDeleteActionSetParameterParamClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    var props = $('#modalEditAction').data('props');
    if (props.params && props.params.length > rowIndex)
        props.params.splice(rowIndex, 1);
    $('#modalEditAction').data('props', props);
    fillTblActionSetParameterParams();
}
function actionSetParameterValueTypeGroupChange() {
    var paramTypeGroup = $('#actionSetParameterValueTypeGroup').val();
    $('#actionSetParameterValueType option').each((index, item) => {
        var type = $(item).val();
        if (type == '-')
            $(item).show();
        else {
            for (var i = 0; i < window.processSetParameterValueTypeArray.length; i++) {
                if (window.processSetParameterValueTypeArray[i].type == type) {
                    if (window.processSetParameterValueTypeArray[i].group == paramTypeGroup)
                        $(item).show();
                    else
                        $(item).hide();
                    break;
                }
            }
        }
    })
    $('#actionSetParameterValueType').val('-');
    actionSetParameterValueTypeChange();
}
function actionSetParameterValueTypeChange() {
    var type = $('#actionSetParameterValueType').val();
    if (type == '-') {
        $('#actionSetParameterValueText').parents('.form-group').hide();
        $('#actionSetParameterValueAction').parents('.form-group').hide();
        $('#actionSetParameterValueObjectProperty').parents('.form-group').hide();
    } else {
        for (var i = 0; i < window.processSetParameterValueTypeArray.length; i++) {
            var typeItem = window.processSetParameterValueTypeArray[i];
            if (typeItem.type == type) {
                if (typeItem.dataType == 'text')
                    $('#actionSetParameterValueText').parents('.form-group').show();
                else
                    $('#actionSetParameterValueText').parents('.form-group').hide();
                if (typeItem.dataType == 'object')
                    $('#actionSetParameterValueAction').parents('.form-group').show();
                else
                    $('#actionSetParameterValueAction').parents('.form-group').hide();
                if (typeItem.type == window.fEnums.FBProcessSetParameterValueType.CreateRecordPropertyValue) {
                    fillActionSetParameterValueObjectProperty();
                    $('#actionSetParameterValueObjectProperty').parents('.form-group').show();
                }
                else
                    $('#actionSetParameterValueObjectProperty').parents('.form-group').hide();
                break;
            }
        }
    }
}
async function actionSetParameterValueActionChange() {
    await fillActionSetParameterValueObjectProperty();
}
function fillActionSetParameterValueAction() {
    var options = [];
    $('.action-create-record').each((index, item) => {
        if ($(item).attr('elemnumber')) {
            options.push({
                text: $(item).find('.item-inner').html(),
                value: $(item).attr('elemnumber')
            })
        }
    });
    utils.fillSelectOptions($('#actionSetParameterValueAction'), options);
}
async function fillActionSetParameterValueObjectProperty() {
    var options = [];
    var actionElemNumber = $('#actionSetParameterValueAction').val();
    var actionElem = $('.action-create-record[elemnumber=' + actionElemNumber + ']');
    if (actionElem) {
        var objectID = actionElem.attr('createrecordobjectid');
        var properties = await service.call('fObjectProperty/getList', 'GET', { fObjectID: objectID });
        for (var i = 0; i < properties.length; i++) {
            options.push({
                text: properties[i].displayName,
                value: properties[i].recID
            })
        }
    }
    utils.fillSelectOptions($('#actionSetParameterValueObjectProperty'), options);
}
function actionSetParameterParamSaveClick() {
    var param = {};
    param.parameterNumber = $('#actionSetParameterParam').val();
    if (param.parameterNumber == '0') {
        utils.alertWarning(lngHelper.get('bProcessDesign.actionSetParameterParamIsRequired'));
        return;
    }
    param.valueTypeGroup = $('#actionSetParameterValueTypeGroup').val();
    if (param.valueTypeGroup == '-') {
        utils.alertWarning(lngHelper.get('bProcessDesign.actionSetParameterValueTypeGroupIsRequired'));
        return;
    }
    param.valueType = $('#actionSetParameterValueType').val();
    if (param.valueType == '-') {
        utils.alertWarning(lngHelper.get('bProcessDesign.actionSetParameterValueTypeIsRequired'));
        return;
    }
    if ($('#actionSetParameterValueAction').is(':visible')) {
        param.valueAction = $('#actionSetParameterValueAction').val();
        if (param.valueAction == '0') {
            utils.alertWarning(lngHelper.get('bProcessDesign.actionSetParameterValueActionIsRequired'));
            return;
        }
    } else
        delete param.valueAction;
    if ($('#actionSetParameterValueObjectProperty').is(':visible')) {
        param.valueObjectProperty = $('#actionSetParameterValueObjectProperty').val();
        if (param.valueObjectProperty == '0') {
            utils.alertWarning(lngHelper.get('bProcessDesign.actionSetParameterValueObjectPropertyIsRequired'));
            return;
        }
    } else
        delete param.valueObjectProperty;
    if ($('#actionSetParameterValueText').is(':visible')) {
        param.valueText = $('#actionSetParameterValueText').val();
        if (param.valueObjectProperty == '') {
            utils.alertWarning(lngHelper.get('bProcessDesign.actionSetParameterValueTextIsRequired'));
            return;
        }
    } else
        delete param.valueText;
    var recIndex = $('#modalActionSetParameterParam').data('recIndex');
    var props = $('#modalEditAction').data('props');
    if (!props.params)
        props.params = [];
    if (recIndex == -1)
        props.params.push(param);
    else
        props.params[recIndex] = param;
    $('#modalEditAction').data('props', props);
    $('#modalActionSetParameterParam').modal('hide');
    fillTblActionSetParameterParams();
}
function fillTblActionSetParameterParams() {
    if (!window.tblActionSetParameterParamDT) {
        window.tblActionSetParameterParamDT = new dataTableHelper('tblActionSetParameterParams', {
            data: [],
            dom: '<"top">rt<"bottom"<p>><"clear">',
            columns: [
                { title: lngHelper.get('bProcessDesign.actionSetParameterParam') },
                { title: lngHelper.get('bProcessDesign.actionSetParameterValueType') }
            ],
            order: [[1, "asc"]]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditActionSetParameterParamClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteActionSetParameterParamClick'
            }],
            dblClickFunction: 'btnEditActionSetParameterParamClick'
        });
    }
    var props = $('#modalEditAction').data('props');
    if (!props)
        props = {};
    if (!props.params)
        props.params = [];
    var dataSet = [];
    for (var i = 0; i < props.params.length; i++) {
        var p = props.params[i];
        var paramName = '';
        var valueType = '';
        for (var j = 0; j < window.currentBProcessPackage.parameters.length; j++) {
            if (window.currentBProcessPackage.parameters[j].parameterNumber == p.parameterNumber) {
                paramName = window.currentBProcessPackage.parameters[j].displayName;
                break;
            }
        }
        for (const [key, value] of Object.entries(window.fEnums.FBProcessSetParameterValueType)) {
            if (p.valueType == value) {
                valueType = lngHelper.get('bProcessDesign.setParameterParamType_' + key);
            }
        }
        dataSet.push([paramName, valueType]);
    }
    window.tblActionSetParameterParamDT.updateData(dataSet);
}
// #endregion

function fillParameterList(elem, noTitleOption) {
    var options = [];
    for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
        options.push({
            value: window.currentBProcessPackage.parameters[i].parameterNumber.toString(),
            text: window.currentBProcessPackage.parameters[i].displayName
        });
    }
    utils.fillSelectOptions(elem, options, noTitleOption);
}

function btnSaveClick(bProcessVersionStatus) {
    $('#bpVersionDescription').val(window.currentBProcessPackage.bProcessVersion.description);
    $('#modalBProcessSave').data('status', bProcessVersionStatus);
    $('#modalBProcessSave').modal('show');
}
function modalBProcessSaveClick() {
    //utils.progress('show');
    $('#modalBProcessSave').modal('hide');
    var bProcessVersionStatus = $('#modalBProcessSave').data('status');
    var bProcessPackage = Object.assign({}, window.currentBProcessPackage);
    if (bProcessVersionStatus)
        bProcessPackage.bProcessVersion.status = bProcessVersionStatus;
    bProcessPackage.bProcessVersion.designContent = $('#businessProcessContainer').html().trim();
    bProcessPackage.bProcessVersion.description = $('#bpVersionDescription').val();
    bProcessPackage.elements = [];
    $('.item-container').each((index, item) => {
        item = $(item);
        var elem = {
            elementNumber: parseInt(item.attr('elemnumber')),
            displayName: item.find('.item-inner').html(),
            props: ''
        }
        if (item.hasClass('action-user')) elem.elementType = FBProcessElementType.UserAction;
        if (item.hasClass('action-notification')) elem.elementType = FBProcessElementType.Notification;
        if (item.hasClass('action-create-record')) elem.elementType = FBProcessElementType.CreateRecord;
        if (item.hasClass('action-change-state')) elem.elementType = FBProcessElementType.ChangeState;
        if (item.hasClass('event-start')) elem.elementType = FBProcessElementType.Start;
        if (item.hasClass('event-end')) elem.elementType = FBProcessElementType.End;
        if (item.hasClass('gateway-regular')) elem.elementType = FBProcessElementType.Gateway;
        if (item.hasClass('action-update-record')) elem.elementType = FBProcessElementType.UpdateRecord;
        if (item.hasClass('action-delete-record')) elem.elementType = FBProcessElementType.DeleteRecord;
        if (item.hasClass('action-set-parameter')) elem.elementType = FBProcessElementType.SetParameter;
        if (item.hasClass('action-execute-function')) elem.elementType = FBProcessElementType.ExecuteFunction;

        elem.props = {}
        switch (elem.elementType) {
            case FBProcessElementType.UserAction:
                if (item.attr('props')) {
                    Object.assign(elem.props, JSON.parse(item.attr('props')));
                }
                if (item.attr('assignee'))
                    elem.props.assignee = item.attr('assignee');
                break;
            case FBProcessElementType.CreateRecord:
                if (item.attr('createrecordobjectid'))
                    elem.props.objectID = item.attr('createrecordobjectid');
                break;
            case FBProcessElementType.ChangeState:
                if (item.attr('changestateid'))
                    elem.props.changeStateID = item.attr('changestateid');
                break;
            case FBProcessElementType.UpdateRecord:
            case FBProcessElementType.DeleteRecord:
            case FBProcessElementType.Gateway:
            case FBProcessElementType.SetParameter:
            case FBProcessElementType.Notification:
                if (item.attr('props'))
                    elem.props = JSON.parse(item.attr('props'));
                else
                    elem.props = {};
                break;
            case FBProcessElementType.ExecuteFunction:
                elem.props = item.attr('props');
                break;
        }
        if (elem.elementType != FBProcessElementType.ExecuteFunction)
            elem.props = JSON.stringify(elem.props);
        bProcessPackage.elements.push(elem);
    })
    bProcessPackage.connectors = [];
    $('.conline').each((index, item) => {
        item = $(item);
        bProcessPackage.connectors.push({
            bProcessVersionID: utils.emptyGuid,
            connectorNumber: parseInt(item.attr('connectornumber')),
            startElementID: utils.emptyGuid,
            endElementID: utils.emptyGuid,
            startElementNumber: parseInt(item.attr('sourceelemrootnumber')),
            endElementNumber: parseInt(item.attr('targetelemrootnumber')),
            eventID: utils.emptyGuid,
            eventNumber: item.attr('event') ? parseInt(item.attr('event')) : 0
        });
    })
    service.call('fBProcessPackage/save', 'POST', bProcessPackage).then(function (result) {
        window.currentBProcessPackage = result;
        utils.showToast('success', bProcessVersionStatus == 1 ? lngHelper.get('bProcessDesign.published') : lngHelper.get('bProcessDesign.saved'));
    })
}
function btnStartClick() {
    utils.openPage('bProcessRun.html?bProcessID=' + window.currentBProcessPackage.bProcessVersion.bProcessID);
}
async function initBProcess() {
    var bProcessRecID = utils.getUrlParam('bProcessRecID');
    if (!bProcessRecID)
        bProcessRecID = utils.emptyGuid;
    var bProcessVersionRecID = utils.getUrlParam('bProcessVersionRecID');
    if (!bProcessVersionRecID)
        bProcessVersionRecID = utils.emptyGuid;

    if (bProcessRecID != utils.emptyGuid) {
        utils.progress('show');
        window.currentBProcessPackage = await service.call('fBProcessPackage/get', 'GET', { bProcessRecID: bProcessRecID, bProcessVersionRecID: bProcessVersionRecID });
        var title = window.currentBProcessPackage.bProcess.displayName;
        if (window.currentBProcessPackage.bProcessVersion.description)
            title += ' (' + window.currentBProcessPackage.bProcessVersion.description + ')';
        utils.changeTitleFromInside(title);
        if (!window.currentBProcessPackage.listItems || window.currentBProcessPackage.listItems.length == 0) {
            window.currentBProcessPackage.listItems = [{
                bProcessVersionID: window.currentBProcessPackage.bProcessVersion.recID,
                itemOrder: 1,
                itemType: window.fEnums.FBProcessAssignListItemType.BProcessDisplayName,
                parameterNumber: 0
            },{
                bProcessVersionID: window.currentBProcessPackage.bProcessVersion.recID,
                itemOrder: 2,
                itemType: window.fEnums.FBProcessAssignListItemType.BProcessRunElementTime,
                parameterNumber: 0
            }, {
                bProcessVersionID: window.currentBProcessPackage.bProcessVersion.recID,
                itemOrder: 3,
                itemType: window.fEnums.FBProcessAssignListItemType.BProcessCurrentStateDisplayName,
                parameterNumber: 0
            }]
        }
        if (window.currentBProcessPackage.bProcessVersion.designContent.trim() != '') {
            $('#businessProcessContainer').html(window.currentBProcessPackage.bProcessVersion.designContent);
            $('#businessProcessContainer .action-execute-function').each((index, item) => {
                var elemNumber = $(item).attr('elemnumber');
                for (var i = 0; i < window.currentBProcessPackage.elements.length; i++) {
                    if (window.currentBProcessPackage.elements[i].elementType == FBProcessElementType.ExecuteFunction && window.currentBProcessPackage.elements[i].elementNumber == elemNumber) {
                        $(item).attr('props', window.currentBProcessPackage.elements[i].props);
                        break;
                    }
                }
            })
            $('#businessProcessContainer .dragging').removeClass('dragging');
            window.processHistory.add();
        }
        utils.progress('hide');
    }
}
function initLang() {
    $('#actionAssigneeUsers').attr('label', lngHelper.get('bProcessDesign.users'));
    $('#actionAssigneeGroups').attr('label', lngHelper.get('bProcessDesign.groups'));
    $('#actionAssigneeActionProcessUser').attr('label', lngHelper.get('bProcessDesign.actionUsers'));
    $('#actionAssigneeParameter').attr('label', lngHelper.get('bProcessDesign.actionAssigneeParameter'));
    $('#actionNotificationTargetUsers').attr('label', lngHelper.get('bProcessDesign.users'));
    $('#actionNotificationTargetGroups').attr('label', lngHelper.get('bProcessDesign.groups'));
}
function fillAssignListItemOptions() {
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FBProcessAssignListItemType)) {
        options.push({
            value: value,
            text: lngHelper.get('bProcessDesign.assignListItemType_' + key)
        })
    }
    utils.fillSelectOptions($('#assignListItemType'), options, false, '-');
    assignListItemTypeChange();
}
function assignListItemTypeChange() {
    var assignListItemType = parseInt($('#assignListItemType').val());
    if (assignListItemType == window.fEnums.FBProcessAssignListItemType.BProcessRunParameterValue)
        $('#assignListItemParameter').parents('.form-group').show();
    else
        $('#assignListItemParameter').parents('.form-group').hide();
}
function initLists() {
    var options = [];
    for (const [key, value] of Object.entries(FBProcessEventType)) {
        options.push({
            value: value,
            text: lngHelper.get('bProcessDesign.eventType' + key)
        })
    }
    utils.fillSelectOptions($('#processEventType'), options, true);
}
function initElemSelect() {
    $('#businessProcessContainer').on('mousedown', (e) => {
        var sender = utils.getEventSender(e);
        if (!sender || !sender.length || sender[0].tagName.toUpperCase() != 'SVG')
            return;
        $('.selection').remove();
        window.selectionElem = $('<div class="selection"></div>');
        var coord = {
            x: e.offsetX,
            y: e.offsetY
        }
        window.selectionElem.data('initCoordinate', coord);
        window.selectionElem.css('top', coord.y + 'px');
        window.selectionElem.css('left', coord.x + 'px');
        window.selectionElem.css('width', '0px');
        window.selectionElem.css('height', '0px');
        $('#businessProcessContainer').append(window.selectionElem);
        $('.main-center .item-container.selected').removeClass('selected');
    });
    $('#businessProcessContainer').on('mousemove', (e) => {
        if (!window.selectionElem)
            return;
        var sender = utils.getEventSender(e);
        if (!sender || !sender.length)
            return;
        var initCoordinate = window.selectionElem.data('initCoordinate');
        var currentCoordinate = {
            x: e.offsetX,
            y: e.offsetY
        }
        if (sender[0].tagName.toUpperCase() != 'SVG') {
            var offset = sender.offset();
            var containerOffset = $('#businessProcessContainer').offset();
            currentCoordinate.x += offset.left - containerOffset.left;
            currentCoordinate.y += offset.top - containerOffset.top;
        }
        var bbox = {
            x1: Math.min(initCoordinate.x, currentCoordinate.x),
            y1: Math.min(initCoordinate.y, currentCoordinate.y),
            x2: Math.max(initCoordinate.x, currentCoordinate.x),
            y2: Math.max(initCoordinate.y, currentCoordinate.y),
            width: Math.abs(initCoordinate.x - currentCoordinate.x),
            height: Math.abs(initCoordinate.y - currentCoordinate.y)
        };
        window.selectionElem.css('top', bbox.y1 + 'px');
        window.selectionElem.css('left', bbox.x1 + 'px');
        window.selectionElem.css('height', bbox.height + 'px');
        window.selectionElem.css('width', bbox.width + 'px');

        $('.main-center .item-container').each((index, item) => {
            item = $(item);
            var position = item.position();
            var elemBBox = {
                x1: position.left,
                x2: position.left + item.width(),
                y1: position.top,
                y2: position.top + item.height()
            }
            elemBBox.xCenter = (elemBBox.x1 + elemBBox.x2) / 2;
            elemBBox.yCenter = (elemBBox.y1 + elemBBox.y2) / 2;
            if (elemBBox.xCenter >= bbox.x1 && elemBBox.xCenter <= bbox.x2 && elemBBox.yCenter >= bbox.y1 && elemBBox.yCenter <= bbox.y2) {
                if (!item.hasClass('selected'))
                    item.addClass('selected');
            } else
                item.removeClass('selected');
        })
    });
    $('#businessProcessContainer').on('mouseup', () => {
        if (!window.selectionElem)
            return;
        window.selectionElem.remove();
        window.selectionElem = null;
    });
}
function btnAssignListItemsClick() {
    $('#modalAssignListItems').data('items', window.currentBProcessPackage.listItems);
    fillAssignListItemsDataTable();
    $('#modalAssignListItems').modal('show');
}
function fillAssignListItemParamers() {
    var options = [];
    for (var i = 0; i < window.currentBProcessPackage.parameters.length; i++) {
        options.push({
            text: window.currentBProcessPackage.parameters[i].displayName,
            value: window.currentBProcessPackage.parameters[i].parameterNumber
        })
    }
    utils.fillSelectOptions($('#assignListItemParameter'), options, false, '-');
}
function fillAssignListItemsDataTable() {
    var listItems = $('#modalAssignListItems').data('items');
    if (!listItems) {
        listItems = [];
    }
    var dataSet = [];
    for (var i = 0; i < listItems.length; i++) {
        var listItem = listItems[i];
        var itemType = listItem.itemType;
        var parameterNumber = listItem.parameterNumber;
        var itemName = '';
        var parameterName = '';
        for (const [key, value] of Object.entries(window.fEnums.FBProcessAssignListItemType)) {
            if (value == itemType) {
                itemName = lngHelper.get('bProcessDesign.assignListItemType_' + key);
                break;
            }
        }
        for (var j = 0; j < window.currentBProcessPackage.parameters.length; j++) {
            if (window.currentBProcessPackage.parameters[j].parameterNumber == parameterNumber) {
                parameterName = window.currentBProcessPackage.parameters[j].displayName
            }
        }
        dataSet.push([itemType, parameterNumber, itemName, parameterName, i]);
    }
    if (!window.assignListItemsDataTableDT) {
        window.assignListItemsDataTableDT = new dataTableHelper('assignListItemsDataTable', {
            data: dataSet,
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging: false,
            columns: [
                { title: 'assignListItemType', visible: false },
                { title: 'assignListItemParameterNumber', visible: false },
                { title: lngHelper.get('bProcessDesign.assignListItemTypeName'), orderable: false },
                { title: lngHelper.get('bProcessDesign.assignListItemParameterName'), orderable: false },
                { title: 'itemOrder', visible:false },
            ],
            order: [[5, "asc"]]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditAssignListItemClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteAssignListItemClick'
            }, {
                text: lngHelper.get('generic.moveUp'),
                clickFunction: 'btnAssignListItemMoveUpClick'
            }, {
                text: lngHelper.get('generic.moveDown'),
                    clickFunction: 'btnAssignListItemMoveDownClick'
            }],
            dblClickFunction: 'btnEditAssignListItemClick'
        });
    } else
        assignListItemsDataTableDT.updateData(dataSet);
}
function btnAssignListItemMoveUpClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    if (rowIndex == 0)
        return;
    var listItems = $('#modalAssignListItems').data('items');
    var tmpItem = JSON.parse(JSON.stringify(listItems[rowIndex - 1]));
    listItems[rowIndex - 1] = JSON.parse(JSON.stringify(listItems[rowIndex]));
    listItems[rowIndex] = tmpItem;
    $('#modalAssignListItems').data('items', listItems);
    fillAssignListItemsDataTable();
}
function btnAssignListItemMoveDownClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    var listItems = $('#modalAssignListItems').data('items');
    if (rowIndex >= listItems.length - 1)
        return;
    var tmpItem = JSON.parse(JSON.stringify(listItems[rowIndex + 1]));
    listItems[rowIndex + 1] = JSON.parse(JSON.stringify(listItems[rowIndex]));
    listItems[rowIndex] = tmpItem;
    $('#modalAssignListItems').data('items', listItems);
    fillAssignListItemsDataTable();
}
function btnEditAssignListItemClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    $('#modalAssignListItemAddEdit').data('rowNumber', rowIndex);
    $('#assignListItemType').val(row.data()[0]);
    assignListItemTypeChange();
    fillAssignListItemParamers();
    if (row.data()[0] == window.fEnums.FBProcessAssignListItemType.BProcessRunParameterValue)
        $('#assignListItemParameter').val(row.data()[1]);
    else
        $('#assignListItemParameter').val('-');    
    $('#modalAssignListItemAddEdit').modal('show');
}
function btnDeleteAssignListItemClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var rowIndex = row.index();
    var listItems = $('#modalAssignListItems').data('items');
    listItems.splice(rowIndex, 1);
    $('#modalAssignListItems').data('items', listItems);
    fillAssignListItemsDataTable();
}
function btnAddAsignListItemClick() {
    $('#assignListItemType').val('-');
    assignListItemTypeChange();
    fillAssignListItemParamers();
    $('#modalAssignListItemAddEdit').data('rowNumber', -1);
    $('#modalAssignListItemAddEdit').modal('show');
}
function assignListItemAddSaveClick() {
    var itemType = $('#assignListItemType').val();
    var parameterNumber = $('#assignListItemParameter').val();

    if (itemType == '-') {
        utils.alertWarning(lngHelper.get('bProcessDesign.assignListItemTypeIsRequired'));
        return;
    }
    itemType = parseInt(itemType);
    if (itemType == window.fEnums.FBProcessAssignListItemType.BProcessRunParameterValue && parameterNumber == '-') {
        utils.alertWarning(lngHelper.get('bProcessDesign.assignListItemParameterIsRequired'));
        return;
    }
    parameterNumber = parseInt(parameterNumber);
    var listItems = $('#modalAssignListItems').data('items'); 
    var listItems = window.currentBProcessPackage.listItems;
    if (!listItems)
        listItems = [];
    var item = {
        itemType: itemType,
        parameterNumber: itemType == window.fEnums.FBProcessAssignListItemType.BProcessRunParameterValue ? parameterNumber : 0
    }
    var rowNumber = $('#modalAssignListItemAddEdit').data('rowNumber');
    if (rowNumber == -1)
        listItems.push(item);
    else
        listItems[rowNumber] = item;
    $('#modalAssignListItems').data('items', listItems);
    fillAssignListItemsDataTable();
    $('#modalAssignListItemAddEdit').modal('hide');
}
function assignListItemsSaveClick() {
    var listItems = $('#modalAssignListItems').data('items');
    for (var i = 0; i < listItems.length; i++)
        listItems[i].itemOrder = i + 1;
    window.currentBProcessPackage.listItems = listItems;
    $('#modalAssignListItems').modal('hide');
}
async function initAll() {
    await initBProcess();
    prepareSideBar();
    initActionModal();
    initLists();
    fillProcessEventTable();
    fillProcessStateTable();
    fillProcessParameterTable();
    initElemSelect();
    fillAssignListItemOptions();
}
$(document).on('initcompleted', () => {
    window.processHistory = new historyHelper();
    initAll();
})