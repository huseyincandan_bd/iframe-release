﻿function btnLogClick(e) {
    utils.preventClickTwice(e);
    var addFilters = [];
    addFilters.push({
        name: 'logtype',
        operator: '=',
        value: window.fEnums.FLogType.ObjectData
    });
    addFilters.push({
        name: 'value1',
        operator: '=',
        value: window.currentForm.fObjectID
    });
    addFilters.push({
        name: 'value2',
        operator: '=',
        value: window.recID
    });
    addFilters.push({
        name: 'optype',
        operator: 'IN',
        value: '(' + window.fEnums.FLogOpType.Create + ',' + window.fEnums.FLogOpType.Update + ',' + window.fEnums.FLogOpType.Undelete + ')'
    });
    var url = 'log.html?addFilters=' + encodeURIComponent(JSON.stringify(addFilters));
    utils.openPage(url);
}

window._onSavedSync = function () {
    return new Promise((resolve, reject) => {
        if (window.onSavedSync && $.isFunction(window.onSavedSync)) {
            window.onSavedSync().then(result => { resolve(result) }, err => { reject(err) });
        } else
            resolve();
    })
}
function findObjectPropertyByID(recID, parentList) {
    if (!parentList) {
        var list = window.currentObjectPropertyList;
        for (var i = 0; i < list.length; i++) {
            if (list[i].recID == recID)
                return list[i];
        }
    } else {
        for (var i = 0; i < window.currentParentObjectPropertyWithRelationList.length; i++) {
            var item = window.currentParentObjectPropertyWithRelationList[i];
            var str = (item.relationID ? item.relationID + '_' + item.objectProperty.recID : item.objectProperty.recID);
            if (str == recID)
                return item.objectProperty;
        }
    }
    return null;
}
function checkRequiredFields() {
    var retValue = true;
    $('[fmodel][required]').each((index, item) => {
        if (!$(item).val() || $(item).val().trim() == '') {
            $(item).addClass('required-error');
            retValue = false;
        } else
            $(item).removeClass('required-error');
    })
    return retValue;
}
function getValueFromFormElement(elemJQ) {
    var key = elemJQ.attr('fmodel').replace(/{{/g, '').replace(/}}/g, '');
    var value = null;
    var objectProperty = findObjectPropertyByID(key);
    if (objectProperty) {
        switch (objectProperty.editorType) {
            case window.fEnums.FObjectPropertyEditorType.Checkbox:
                value = (elemJQ[0].checked ? '1' : '0');
                break;
            case window.fEnums.FObjectPropertyEditorType.RadioButton:
                value = $('[name=' + elemJQ.attr('name') + ']').filter(':checked').attr('value');
                break;
            case window.fEnums.FObjectPropertyEditorType.GeoWKT:
                value = elemJQ.data('fMapHelper').getWKT();
                break;
            default:
                value = elemJQ.val();
                break;
        }
    }
    return value;
}
function getDataFromForm() {
    var formData = {};
    $('[fmodel]').each(function (index, item) {
        var key = $(item).attr('fmodel').replace(/{{/g, '').replace(/}}/g, '');
        var value = getValueFromFormElement($(item));
        if (value != null)
            formData[key] = value;
        //var key = $(item).attr('fmodel').replace(/{{/g, '').replace(/}}/g, '');
        //var value = null;
        //var objectProperty = findObjectPropertyByID(key);
        //if (objectProperty) {
        //    switch (objectProperty.editorType) {
        //        case window.fEnums.FObjectPropertyEditorType.Checkbox:
        //            value = (item.checked ? '1' : '0');
        //            break;
        //        case window.fEnums.FObjectPropertyEditorType.RadioButton:
        //            value = $('[name=' + $(item).attr('name') + ']').filter(':checked').attr('value');
        //            break;
        //        case window.fEnums.FObjectPropertyEditorType.GeoWKT:
        //            value = $(item).data('fMapHelper').getWKT();
        //            break;
        //        default:
        //            value = $(item).val();
        //            break;
        //    }
        //    formData[key] = value;
        //}

    })
    return formData;
}
async function saveChildren() {
    for (var i = 0; i < window.currentObjectRelationList.length; i++) {
        var relationID = window.currentObjectRelationList[i].recID;
        var iframe = $('#r-container' + relationID + ' iframe');
        if (iframe.length > 0) {
            await iframe[0].contentWindow.btnSaveClick();
        }
    }
}
async function save(updateParentMacros = true, updateMacros = true) {
    if (!checkRequiredFields()) {
        var message = '<p>' + lngHelper.get('formDataEdit.requiredFieldError') + '</p>';
        var errorList = [];
        $('.required-error').each((index, item) => {
            var itemJQ = $(item);
            var tabName = '';
            var tab = itemJQ.closest('.tab-pane');
            if (tab && tab.length > 0) {
                tabName = $('#formContent .tabs .nav-link[aria-controls=' + tab.attr('id') + ']').text();
            }
            var label = itemJQ.parents('.fform-element').find('.col-form-label').text();
            var errorListItem = null;
            for (var i = 0; i < errorList.length; i++) {
                if (errorList[i].tabName == tabName) {
                    errorListItem = errorList[i];
                    break;
                }
            }
            if (!errorListItem) {
                errorListItem = {
                    tabName: tabName,
                    labels: []
                }
                errorList.push(errorListItem);
            }
            errorListItem.labels.push(label);
        });
        if (errorList.length > 1)
            message += '<ul>';
        for (var i = 0; i < errorList.length; i++) {
            if (errorList.length > 1)
                message += '<li>' + (errorList[i].tabName ? utils.htmlEscape(errorList[i].tabName) : '-');
            message += '<ul>';
            for (var j = 0; j < errorList[i].labels.length; j++) {
                message += '<li>' + utils.htmlEscape(errorList[i].labels[j]) + '</li>';
            }
            message += '</ul>';
            if (errorList.length > 1)
                message += '</li>';
        }
        if (errorList.length > 1)
            message += '</ul>';
        utils.openPopup('danger', lngHelper.get('generic.error'), message);
        return false;
    }
    await saveChildren();
    var formData = getDataFromForm();
    $.each(formData, (key, value) => {   //IF-272
        var elem = $('[fmodel=\\{\\{' + key + '\\}\\}]');
        if (elem && elem.prop && elem.prop('disabled'))
            delete formData[key];
    })
    var url = 'fObjectData/save?recID=' + window.recID + '&fObjectID=' + window.currentForm.fObjectID + '&updateParentMacros=' + updateParentMacros + '&updateMacros=' + updateMacros;

    if (window.parentObjectValues && !$.isEmptyObject(window.parentObjectValues)) {
        for ([key, value] of Object.entries(window.parentObjectValues)) {
            if (value == 0 || value == utils.emptyGuid) {
                for (var i = 0; i < window.currentObjectRelationList.length; i++) {   //IF-191  
                    if (window.currentObjectRelationList[i].recID == key) {
                        if (!window.currentObjectRelationList[i].allowNullParent) {
                            utils.alertWarning(lngHelper.get('formDataEdit.relatedObjectValueIsEmpty'));
                            return;
                        }
                        break;
                    }
                }
            }
            if (value == 0)
                value = utils.emptyGuid;
            if (key.indexOf('_') < 0)  //if not grandparent 
                url += '&relations[' + key + ']=' + value;
        }
    }
    var result = await service.call(url, 'POST', formData);
    if (result) {
        window.recID = result.recID;
        await window._onSavedSync();
        initAttachments();
    }
    return result;
}
async function btnSaveClick(closeWindow) {
    var args = JSON.parse(JSON.stringify(arguments));
    [].unshift.call(args, closeWindow);
    if (window.btnSaveClick_OnStart) {
        var result = await window.btnSaveClick_OnStart.apply(null, args);
        if (result == false)
            return;
    }
    var closePageCalled = false;
    var result = await save();
    [].unshift.call(args, result);
    if (result) {
        window.windowResult = 'Updated';
        if (window.closeOnSave && closeWindow) {
            //var children = $('.relation-child');
            //if (!children || children.length == 0) {   //IF-66 
                var iframes = window.parent.document.querySelectorAll('iframe');
                for (var i = 0; i < iframes.length; i++) {
                    if (iframes[i].contentWindow == window) {
                        if ($(iframes[i]).parents('.modal').length > 0) {
                            if (window.parent.getUtilsObject().closePage) {
                                if (window.btnSaveClick_OnEnd) {
                                    await window.btnSaveClick_OnEnd.apply(null, args);
                                    closePageCalled = true;
                                }
                                window.parent.getUtilsObject().closePage($(iframes[i]).parents('.modal').attr('id'));
                            }
                            return;
                        }
                        break;
                    }
                }
            //}
        }
        if (window.isNewRecord) {
            location.replace(utils.updateURLParam(window.location.href, 'recID', window.recID) + '&windowResult=Updated');
        }
        else {
            $('.relation-child').each(function (index, item) {
                $(this).attr('src', $(this).attr('alternate-src').replace(/\{\{recID\}\}/, window.recID.toString()));
            })
            initForm();
            utils.showToast('success', lngHelper.get('generic.saved'));
        }

    } else {
        utils.showToast('error', lngHelper.get('generic.saveError'));
        //utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.saveError'));         
    }
    if (window.btnSaveClick_OnEnd && !closePageCalled)
        await window.btnSaveClick_OnEnd.apply(result, args);
}
async function btnDeleteClick() {
    if (window.btnDeleteClick_OnStart) {
        var result = await window.btnDeleteClick_OnStart.apply(null, arguments);
        if (result == false)
            return;
    }
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: async function () {
            if (window.btnDeleteClick_OnConfirmed) {
                var result = await window.btnDeleteClick_OnConfirmed.apply(null, arguments);
                if (result == false)
                    return;
            }
            service.call('fObjectData/delete', 'GET', { recID: window.recID, fObjectID: window.currentForm.fObjectID }).then(function (result) {
                window.windowResult = 'Updated';
                popup.modal('hide');
                var popup2 = utils.openPopup('info', lngHelper.get('generic.info'), lngHelper.get('generic.deleted'));
                popup2.on('hidden.bs.modal', async function (e) {
                    if (window.btnDeleteClick_OnEnd)
                        await window.btnDeleteClick_OnEnd.apply(null, arguments);
                    var iframes = window.parent.document.querySelectorAll('iframe');
                    for (var i = 0; i < iframes.length; i++) {
                        if (iframes[i].contentWindow == window) {
                            if ($(iframes[i]).parents('.modal').length > 0) {
                                if (window.parent.getUtilsObject().closePage) {
                                    window.parent.getUtilsObject().closePage($(iframes[i]).parents('.modal').attr('id'));
                                }
                                return;
                            }
                            break;
                        }
                    }
                    //window.close();
                })
            }, function () { })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function setObjectPropertyElemValue(fModelID, item, value) {
    var objectProperty = findObjectPropertyByID(fModelID);
    if (!objectProperty)
        objectProperty = findObjectPropertyByID(fModelID, true);
    if (objectProperty && value != "undefined" && !($.isEmptyObject(value) && typeof (value) == "object")) {
        switch (objectProperty.editorType) {
            case window.fEnums.FObjectPropertyEditorType.Checkbox:
                item.checked = value != 0;
                break;
            case window.fEnums.FObjectPropertyEditorType.RadioButton:
                if (value)
                    $('[name=' + $(item).attr('name') + ']').filter('[value=' + value + ']').prop('checked', true);
                break;
            case window.fEnums.FObjectPropertyEditorType.Select:
                if (value == utils.emptyGuid)
                    value = '';
                $(item).val(value);
                break;
            case window.fEnums.FObjectPropertyEditorType.Date:
                if (value && Object.keys(value).length > 0) {
                    var theValue = moment(value).format('YYYY-MM-DD');
                    $(item).val(theValue);
                }
                break;
            case window.fEnums.FObjectPropertyEditorType.DateTime:
                if (value && Object.keys(value).length > 0) {
                    try {
                        value = moment(value).format('YYYY-MM-DD[T]HH:mm');
                    } catch (err) {}
                    $(item).val(value);
                }
                break;
            case window.fEnums.FObjectPropertyEditorType.Time:
                if (value && Object.keys(value).length > 0) {
                    var timeValue = value;
                    if (timeValue && timeValue.hours) {
                        var theValue = ('0' + timeValue.hours.toString()).slice(-2) + ':' + ('0' + timeValue.minutes.toString()).slice(-2);
                        $(item).val(theValue);
                    } else
                        $(item).val(value);
                }
                break;
            case window.fEnums.FObjectPropertyEditorType.GeoWKT:
                if (value) {
                    var fMapHelper = $(item).data('fMapHelper');
                    if (fMapHelper) {
                        fMapHelper.drawWKT(value);
                    }
                }
                break;
            default:
                $(item).val(value);
                break;
        }
    }
    if (value != null && $(item).hasClass('required-parent-elem'))
        $(item).addClass('required-parent-elem-selected');
}
async function getFormData() {
    window.currentObjectData = await service.call('fObjectData/getWithParents', 'GET', { recID: window.recID, fObjectID: window.currentForm.fObjectID });
    $('[fmodel]').each(function (index, item) {
        var fModelID = getPropertyIDFromFModel($(item).attr('fmodel'));
        var elemValue = window.currentObjectData[fModelID];
        if (typeof elemValue == 'undefined') {  //TODO: Burası geriye uyumluluk için yazıldı.    
            for (const [key, value] of Object.entries(window.currentObjectData)) {
                if (key.indexOf('_') > 0 && key.split('_')[1] == fModelID) {
                    elemValue = value;
                    break;
                }
            }
        }
        setObjectPropertyElemValue(fModelID, item, elemValue);
    })

    for (const [key, value] of Object.entries(window.parentObjectValues)) {
        if (window.currentObjectData['rel-' + key])
            window.parentObjectValues[key] = window.currentObjectData['rel-' + key];
    }
}
async function getOptionList(dbColumnName, fOptionListID, editorType) {
    var result = await service.call('fOptionListItem/getList', 'GET', { fOptionListID: fOptionListID });
    var targetElem = $('[fmodel=\\{\\{' + dbColumnName + '\\}\\}]');
    switch (editorType) {
        case window.fEnums.FObjectPropertyEditorType.Select:
            var options = [];
            for (var i = 0; i < result.length; i++) {
                options.push({
                    value: result[i].recID,
                    text: result[i].displayName,
                    attributes: {
                        'parent-id': result[i].parentFOptionListItemID
                    }
                })
            }
            utils.fillSelectOptions(targetElem, options, false, '');
            break;
        case window.fEnums.FObjectPropertyEditorType.RadioButton:
            var targetElemContainer = targetElem.parents('.form-check').first();
            var targetElemContent = targetElemContainer.html();
            var str = '';
            for (var j = 0; j < result.length; j++) {
                var newElem = targetElemContainer.clone();
                newElem.find('label').html(result[j].displayName);
                newElem.find('[fmodel]').attr('value', result[j].recID);
                if (j != 0)
                    newElem.find('[fmodel]').removeAttr('fmodel');
                str += newElem.html();
            }
            targetElemContainer.html(str);
            break;
    }
}
function initOptionListRelations() {
    for (var i = 0; i < window.currentObjectPropertyList.length; i++) {
        if (window.currentObjectPropertyList[i].editorType == window.fEnums.FObjectPropertyEditorType.Select && window.currentObjectPropertyList[i].fOptionListID != 0) {
            for (var j = 0; j < window.currentOptionListList.length; j++) {
                if (window.currentObjectPropertyList[i].fOptionListID == window.currentOptionListList[j].recID) {
                    if (window.currentOptionListList[j].parentFOptionListID) {
                        for (var k = 0; k < window.currentObjectPropertyList.length; k++) {
                            if (window.currentObjectPropertyList[k].editorType == window.fEnums.FObjectPropertyEditorType.Select && window.currentObjectPropertyList[k].fOptionListID == window.currentOptionListList[j].parentFOptionListID) {
                                //var parentSelectElem = $('[fmodel=\\{\\{f-' + window.currentObjectPropertyList[k].recID.toString() + '\\}\\}]');
                                //var childSelectElem = $('[fmodel=\\{\\{f-' + window.currentObjectPropertyList[i].recID.toString() + '\\}\\}]'); 
                                var parentSelectElem = $('[fmodel=\\{\\{' + window.currentObjectPropertyList[k].recID.toString() + '\\}\\}]');
                                var childSelectElem = $('[fmodel=\\{\\{' + window.currentObjectPropertyList[i].recID.toString() + '\\}\\}]');
                                childSelectElem.attr('parent-list-id', window.currentObjectPropertyList[k].recID.toString());
                                parentSelectElem.on('change', function () {
                                    //var parentListID = $(this).attr('fmodel').match(/\{\{([^}]+)\}\}/)[1].split('-')[1]; 
                                    //var parentListID = $(this).attr('fmodel').match(/\{\{([^}]+)\}\}/)[1];

                                    var parentListID = getPropertyIDFromFModel($(this).attr('fmodel'));
                                    var childElem = $('[parent-list-id=' + parentListID + ']');
                                    childElem.find('option').hide();
                                    childElem.find('option[value=""]').show();
                                    //var parentItemID = $(this).find(':selected').val();
                                    var parentItemID = $(this).val();
                                    if (parentItemID != '')
                                        childElem.find('[parent-id=' + parentItemID + ']').show();
                                    var selectedOptions = childElem.find('option:selected');
                                    if (selectedOptions && selectedOptions.length > 0 && selectedOptions[0].style.display == 'none') {
                                        var visibleOptions = childElem.find('option').filter((index, item) => {
                                            return item.style.display != 'none';
                                        })
                                        if (visibleOptions && visibleOptions.length > 0)
                                            childElem.val(visibleOptions.first().val());
                                    }
                                })
                                parentSelectElem.trigger('change');
                            }
                        }
                    }
                }
            }
        }
    }
}
function initSelectSource() {
    $('[sourcetableid]').each((index, item) => {
        $(item).parent().addClass('input-group');
        $(item).after('<div class="input-group-append"><button type="button" class="btn btn-outline-secondary" onclick="btnSelectSourceClick(event)">...</button></div>');
    })
}
function btnSelectSourceClick(e) {
    if (!e)
        e = window.event;
    var sender = e.srcElement || e.target;
    var elem = $(sender).parent().siblings().filter('[sourcetableid]');
    var sourceObjectID = elem.attr('sourceobjectid');
    var sourceTableID = elem.attr('sourcetableid');
    var params = elem.attr('sourceselectparameters');
    var addFilterArray = [];
    if (params) {
        params = JSON.parse(params);
        for (var i = 0; i < params.length; i++) {
            var elem = $('[fmodel=\\{\\{' + params[i].sourceObjectPropertyRecID + '\\}\\}]');
            if (elem && elem.length > 0) {
                var value = null;
                var item = elem[0];
                var objectProperty = findObjectPropertyByID(params[i].sourceObjectPropertyRecID);
                if (objectProperty) {
                    switch (objectProperty.editorType) {
                        case window.fEnums.FObjectPropertyEditorType.Checkbox:
                            value = (item.checked ? '1' : '0');
                            break;
                        case window.fEnums.FObjectPropertyEditorType.RadioButton:
                            value = $('[name=' + $(item).attr('name') + ']').filter(':checked').attr('value');
                            break;
                        default:
                            value = $(item).val();
                            break;
                    }
                    if (value) {
                        addFilterArray.push({
                            name: params[i].targetObjectPropertyRecID,
                            operator: '=',
                            value: value
                        })
                    }
                }
            }
        }
    }
    var extParam = null;
    if (addFilterArray.length > 0)
        extParam = 'addFilters=' + encodeURIComponent(JSON.stringify(addFilterArray));
    utils.selectObjectItemFromTable(sourceObjectID, sourceTableID, sourceSelected, extParam);
}
async function sourceSelected(recID, params) {
    var result = null;
    if (recID != utils.emptyGuid)
        result = await service.call('fObjectData/get', 'GET', { recID: recID, fObjectID: params.objectID });
    $('[sourceobjectid=' + params.objectID + ']').each((index, item) => {
        var value = result ? result[$(item).attr('sourcepropertyid')] : null;
        $(item).val(value);
    })
}
async function initChildTables() {
    var result = await service.call('fObjectRelation/getList', 'GET', { fObjectID: window.currentForm.fObjectID });

    if (result && result.length > 0) {
        for (var i = 0; i < result.length; i++) {
            var targetElem = $('[fmodel=\\{\\{' + result[i].recID.toString() + '\\}\\}]');
            if (targetElem && targetElem.length > 0) {
                var tableID = targetElem.attr('table-id');
                if (!tableID) {
                    var tables = await service.call('fTable/getList', 'GET', { fObjectID: result[i].childFObjectID });
                    if (tables && tables.length > 0)
                        tableID = tables[0].recID;
                }
                var src = './tableDataList.html?tableID=' + tableID;
                var formID = targetElem.attr('form-id');
                if (formID)
                    src += '&formID=' + formID;
                var alternateSrc = './tableDataList.html?tableID=' + tableID;
                var relationValue = window.recID;
                if (result[i].parentColumnName != 'recID') {
                    for (var j = 0; j < window.currentObjectPropertyList.length; j++) {
                        if (window.currentObjectPropertyList[j].dbColumnName == result[i].parentColumnName) {
                            relationValue = window.currentObjectData[window.currentObjectPropertyList[j].recID];
                            if (!relationValue)
                                relationValue = '';
                            break;
                        }
                    }
                }
                src += '&relations={"' + result[i].recID.toString() + '":"' + relationValue + '"}';   //TODO: null ise hata vermesine neden oluyor.
                alternateSrc += '&relations={"' + result[i].recID.toString() + '":"{{recID}}"}';

                if (!window.recID || window.recID == utils.emptyGuid)
                    src += '&disabled=true';

                //TODO : Aşağıdaki yöntem iyileştirilmeli. Tablodaki satır sayısını ayarlıyor
                var style = targetElem.parents('.fform-element').attr('style');
                if (style) {
                    var parts = style.split(';');
                    for (var j = 0; j < parts.length; j++) {
                        var parts2 = parts[j].split(':');
                        if (parts2[0] == 'pageLength') {
                            src += '&pageLength=' + parts2[1];
                            alternateSrc += '&pageLength=' + parts2[1];
                        }
                    }
                }
                var html = '<iframe class="relation-child" src=\'' + src + '\' alternate-src=\'' + alternateSrc + '\' width="100%" height="400" frameborder="0" style="width:100%;height:400px;border:1px solid lightgray"></iframe>';
                targetElem.replaceWith(html);
            }
        }
    }

    const event = new Event('initChildTablesCompleted');
    document.dispatchEvent(event);

    //service.call('fObjectRelation/getList', 'GET', { fObjectID: window.currentForm.fObjectID }).then(async function (result) {                
    //}) 
}
function setParentObjectValue(relationID, propertyID, value) {
    var targetElem;
    if (relationID != null)
        targetElem = $('[fmodel=\\{\\{' + relationID + '_' + propertyID + '\\}\\}]');
    //if (!window.currentObjectHasRelationToItself && (targetElem == null || targetElem.length == 0))
    //    targetElem = $('[fmodel=\\{\\{' + propertyID + '\\}\\}]');
    if (targetElem && targetElem.length > 0) {
        setObjectPropertyElemValue(relationID + '_' + propertyID, targetElem[0], value);
        targetElem.addClass('required-parent-elem-selected');
    }
}
async function getAndShowParentObjectValues(recID, fObjectID, relationID) {
    if (relationID) {
        if (!window.currentObjectData || !window.currentObjectData['rel-' + relationID] || window.currentObjectData['rel-' + relationID] != recID) {
            var rootRelationID = relationID;
            var tmpIndex = rootRelationID.indexOf('_');
            if (tmpIndex > 0)
                rootRelationID = rootRelationID.substring(0, tmpIndex);
            $('[fmodel^=\\{\\{' + rootRelationID + ']').each((index, item) => {
                var fModelID = $(item).attr('fmodel').replace(/{/g, '').replace(/}/g, '');
                var tmpRelation = fModelID;
                var tmpIndex = tmpRelation.lastIndexOf('_');
                if (tmpIndex >= 0) {
                    tmpRelation = tmpRelation.substring(0, tmpIndex);
                    if (tmpRelation != relationID && relationID.indexOf(tmpRelation) == 0)
                        setObjectPropertyElemValue(fModelID, item, '');
                }
            })
            if (!window.currentObjectData)
                window.currentObjectData = [];
            window.currentObjectData['rel-' + relationID] = recID;
        }
    }
    if (recID == utils.emptyGuid) {   //IF-313
        var nonDisabledExist = false;
        var itemArray = [];
        $('[fmodel^=\\{\\{' + relationID + ']').each((index, item) => {
            item = $(item);
            var fModelID = item.attr('fmodel').replace(/{/g, '').replace(/}/g, '');
            var remainingPart = fModelID.substring(relationID.length + 1);
            if (remainingPart.indexOf('_') >= 0) {  //daha üst nesne ise
                itemArray.push(item);
                if (!item.attr('disabled')) {
                    nonDisabledExist = true;
                    return false;
                }
            } else {
                setObjectPropertyElemValue(fModelID, item[0], '');
                item.removeClass('required-parent-elem-selected');
                window.currentObjectData['rel-' + relationID] = utils.emptyGuid;
            }
        })
        if (!nonDisabledExist) {
            for (var i = 0; i < itemArray.length; i++) {
                var item = itemArray[i];
                var fModelID = item.attr('fmodel').replace(/{/g, '').replace(/}/g, '');
                setObjectPropertyElemValue(fModelID, item[0], '');
                var parentRelationID = fModelID.substring(0, fModelID.lastIndexOf('_'));
                window.currentObjectData['rel-' + parentRelationID] = recID;
            }
        }
        //var fObjectProperties = await service.call('fObjectProperty/getList', 'GET', { fObjectID: fObjectID });
        //for (var i = 0; i < fObjectProperties.length; i++) {
        //    setParentObjectValue(relationID, fObjectProperties[i].recID, "");
        //}
    } else {
        var objectDataWithParents = await service.call('fObjectData/getWithParents', 'GET', { recID: recID, fObjectID: fObjectID });
        for (const [key, value] of Object.entries(objectDataWithParents)) {
            setParentObjectValue(relationID, key, value);
        }
        $('[fmodel^=\\{\\{' + relationID + ']').each((index, item) => {
            item = $(item);
            var fModelID = item.attr('fmodel').replace(/{/g, '').replace(/}/g, '');
            var remainingPart = fModelID.substring(relationID.length + 1);
            if (remainingPart.indexOf('_') >= 0) {
                var parentRelationID = fModelID.substring(0, fModelID.lastIndexOf('_'));
                var remainingRelation = remainingPart.substring(0, remainingPart.lastIndexOf('_'));
                if (objectDataWithParents['rel-' + remainingRelation])
                    window.currentObjectData['rel-' + parentRelationID] = objectDataWithParents['rel-' + remainingRelation];
            }
        })
    }
}
async function propertyElemDataSelected(recID, params) {
    window.parentObjectValues[params.callerRelationID] = recID;
    await getAndShowParentObjectValues(recID, params.objectID, params.callerRelationID);
}
function parentPropertyElemClick(e) {
    var sender = utils.getEventSender(e);
    utils.preventClickTwice(sender);
    var tableID = null;
    if (sender.attr('table-id'))
        tableID = sender.attr('table-id');

    var relations = {};
    var fModelID = getPropertyIDFromFModel(sender.attr('fmodel'));
    var relationID = getRelationIDFromElemID(fModelID);

    if (window.currentObjectData) {
        for (const [key, value] of Object.entries(window.currentObjectData)) {
            if (key.indexOf('rel-' + relationID) == 0 && value && value != utils.emptyGuid) {
                var tmpRelation = key.substring(('rel-' + relationID).length + 1);
                if (tmpRelation && tmpRelation.indexOf('_') < 0) {
                    var list = $('[fmodel^=\\{\\{' + key.substring(4) + ']'); //IF-309 
                    if (list && list.length > 0) {
                        relations[tmpRelation] = value;
                    }
                }
            }
        }
    }
    var addFilterObj = [];
    var parentSelectParameters = sender.attr('parentSelectParameters');
    if (parentSelectParameters) {
        parentSelectParameters = JSON.parse(parentSelectParameters);
        for (var i = 0; i < parentSelectParameters.length; i++) {
            var parameter = parentSelectParameters[i];
            value = null;
            switch (parameter.parentSourceParameterValueType) {
                case window.fEnums.FFormParentSourceParameterValueType.Static:
                    value = parameter.parentSourceParameterStaticValue;
                    break;
                case window.fEnums.FFormParentSourceParameterValueType.PropertyValue:
                    var sourceElem = $('[fmodel=\\{\\{' + parameter.sourceObjectPropertyRecID + '\\}\\}]');
                    if (sourceElem.length)
                        value = getValueFromFormElement(sourceElem);
                    break;
            }
            if (value != null) {
                addFilterObj.push({
                    name: parameter.targetObjectPropertyRecID,
                    operator: '=',
                    value: value
                })
            }
        }
    }

    if (sender.attr('add-filters')) {  //IF-228  
        var tmpAddFilter = JSON.parse(decodeURIComponent(sender.attr('add-filters')));
        if (tmpAddFilter) {
            for (var i = 0; i < tmpAddFilter.length; i++) {
                addFilterObj.push(tmpAddFilter[i]);
            }
        }
    }
    var extParam = 'callerRelationID=' + relationID;
    if (addFilterObj && addFilterObj.length > 0)
        extParam += '&addFilters=' + encodeURIComponent(JSON.stringify(addFilterObj));
    if (!$.isEmptyObject(relations))
        extParam += '&relations=' + encodeURIComponent(JSON.stringify(relations));

    utils.selectObjectItemFromTable(sender.attr('fobjectid'), tableID, propertyElemDataSelected, extParam);
}
async function initParentPropertyElements() {
    for (var i = 0; i < window.currentParentObjectPropertyWithRelationList.length; i++) {
        var property = window.currentParentObjectPropertyWithRelationList[i].objectProperty;
        var targetElem = $('[fmodel=\\{\\{' + window.currentParentObjectPropertyWithRelationList[i].relationID + '_' + property.recID.toString() + '\\}\\}]');
        for (var j = 0; j < window.currentObjectRelationList.length; j++) {
            if (window.currentObjectRelationList[j].recID == window.currentParentObjectPropertyWithRelationList[i].relationID) {
                if (!window.currentObjectRelationList[j].allowNullParent)
                    targetElem.addClass('required-parent-elem');
                break;
            }
        }

        if (targetElem && targetElem.length > 0) {
            targetElem.attr('fobjectid', property.fObjectID);
            targetElem.attr('onclick', 'parentPropertyElemClick(event)');
            if (property.editorType == window.fEnums.FObjectPropertyEditorType.Select) {
                var items = await service.call('fOptionListItem/getList', 'GET', { fOptionListID: property.fOptionListID });
                var options = [];
                for (var j = 0; j < items.length; j++) {
                    options.push({
                        text: items[j].displayName,
                        value: items[j].recID
                    })
                }
                utils.fillSelectOptions(targetElem, options, false, '');
            }
            if (property.editorType == window.fEnums.FObjectPropertyEditorType.RadioButton) {
                var items = await service.call('fOptionListItem/getList', 'GET', { fOptionListID: property.fOptionListID });
                var str = '';
                for (var j = 0; j < items.length; j++) {
                    str += '<div><input class="form-check-input" type="radio" name="' + window.currentParentObjectPropertyWithRelationList[i].relationID + '_' + property.recID + '" fmodel="{{' + window.currentParentObjectPropertyWithRelationList[i].relationID + '_' + property.recID + '}}" value="' + items[j].recID + '" onclick="parentPropertyElemClick(event)" fobjectid="' + property.fObjectID + '"><label class="form-check-label drag-over">' + items[j].displayName + '</label></div>';
                }
                targetElem.parents('.form-check').html(str);
            }
        }
    }
    window.parentObjectValues = {};
    //Üst kayıt içindeki tablodan kayıt girişi yapılıyorsa, üst kaydın alanları dolduruluyor.  
    for (var i = 0; i < window.currentObjectRelationList.length; i++) {
        if (window.currentObjectRelationList[i].relationType == window.fEnums.FObjectRelationType.OneToMany && window.currentObjectRelationList[i].childFObjectID == window.currentForm.fObjectID) {
            window.parentObjectValues[window.currentObjectRelationList[i].recID] = 0;
            if (window.relations[window.currentObjectRelationList[i].recID]) {
                window.parentObjectValues[window.currentObjectRelationList[i].recID] = window.relations[window.currentObjectRelationList[i].recID];
                getAndShowParentObjectValues(window.parentObjectValues[window.currentObjectRelationList[i].recID], window.currentObjectRelationList[i].parentFObjectID, window.currentObjectRelationList[i].recID);
            }
        }
    }
}
async function initBProcess() {
    $('#btnProcessHistoryContainer').show();
    var outInfo = await service.call('fBProcessRunElement/getOutInfo', 'GET', { recID: window.bProcessRunElementID });

    $('#buttonContainer .btn-bprocess-event').remove();
    var btns = '';
    for (var i = 0; i < outInfo.bProcessEvents.length; i++) {
        for (var j = 0; j < outInfo.bProcessConnectors.length; j++) {
            if (outInfo.bProcessEvents[i].recID == outInfo.bProcessConnectors[j].eventID) {
                var askComment = 0;
                if (outInfo.bProcessEvents[i].props) {
                    if (JSON.parse(outInfo.bProcessEvents[i].props).askComment)
                        askComment = 1;
                }
                btns += '<button onclick="btnProcessEventClick(event)" connectorid="' + outInfo.bProcessConnectors[j].recID + '" askcomment="' + askComment + '" class="btn-bprocess-event btn btn-outline-primary font-weight-bold btn-sm mr-2">' + outInfo.bProcessEvents[i].displayName + '</button>';
            }
        }
    }
    if (btns != '')
        $('#buttonContainer').prepend(btns);

    if (outInfo.bProcessRunElement && outInfo.bProcessRunElement.inEventComment) {
        utils.openPopup('info', lngHelper.get('formDataEdit.inEventComment'), outInfo.bProcessRunElement.inEventComment);
    }
}
function refrehParentTable() {
    utils.callChildIFrameFunction(true, 'reloadProcessList');
}
function btnProcessEventAskComment(sender) {
    return new Promise((resolve, reject) => {
        if (sender.attr('askcomment') == 1) {
            var html = '<textarea class="form-control" rows="3"></textarea>';
            var commentPopup = utils.openPopup('info', lngHelper.get('bProcessDesign.eventComment'), html, [{
                text: lngHelper.get('generic.ok'),
                class: 'btn-primary',
                onclick: function () {
                    var retValue = commentPopup.find('.form-control').val();
                    commentPopup.modal('hide');
                    resolve(retValue);
                }
            }],{
                onCloseFunction: function () {
                    reject('');
                }
            });
        } else
            resolve('');
    })
}
async function btnProcessEventClick(e) {
    var sender = utils.getEventSender(e);
    var connectorid = sender.attr('connectorid');

    var args = JSON.parse(JSON.stringify(arguments));
    [].unshift.call(args, e);
    [].unshift.call(args, connectorid);
    [].unshift.call(args, sender[0]);

    if (window.btnProcessEventClick_OnStart) {
        var result = await window.btnProcessEventClick_OnStart.apply(null, args);
        if (result == false)
            return;
    }

    var result = await save();
    if (result) {
        btnProcessEventAskComment(sender).then(async (askCommentResult) => {
            var runResult = await service.call('fBProcessRunElement/run', 'GET', { recID: window.bProcessRunElementID, connectorID: connectorid, eventComment: askCommentResult});
            refrehParentTable();
            if (window.btnProcessEventClick_OnEnd) {
                var result = await window.btnProcessEventClick_OnEnd.apply(null, args);
                if (result == false)
                    return;
            }
            bProcessHelper.handleBProcessRunResult(runResult);
        }, (result) => {
        })
    }
}
async function btnProcessHistoryClick(e) {
    utils.preventClickTwice(e);
    if (!window.currentBProcessRunElement)
        window.currentBProcessRunElement = await service.call('fBProcessRunElement/get', 'GET', { recID: window.bProcessRunElementID });
    utils.openPage('bProcessRunHistory.html?bProcessRunID=' + window.currentBProcessRunElement.bProcessRunID);
}
function getFileIcon(extension) {
    var className = ''
    switch (extension) {
        case 'csv':
            className = 'fas fa-file-csv';
            break;
        case 'pdf':
            className = 'far fa-file-pdf';
            break;
        case 'doc':
        case 'docx':
            className = 'far fa-file-word';
            break;
        case 'xls':
        case 'xlsx':
            className = 'far fa-file-excel';
            break;
        case 'ppt':
        case 'pptx':
            className = 'far fa-file-powerpoint';
            break;
        case 'jpg':
        case 'jpeg':
        case 'tif':
        case 'tiff':
        case 'bmp':
        case 'png':
            className = 'far fa-file-image';
            break;
        case 'txt':
            className = 'far fa-file-alt';
            break;
        case 'wav':
        case 'mp3':
            className = 'far fa-file-audio';
            break;
        case 'avi':
        case 'mp4':
            className = 'far fa-file-video';
            break;
        case 'zip':
        case 'rar':
            className = 'far fa-file-archive';
            break;
        default:
            className = 'far fa-file';
            break;

    }
    return '<i class="' + className + '"></i>';
}
async function initAttachments() {
    if (!window.recID || window.recID == utils.emptyGuid)
        return;
    var attachmentDataTableElem = $('#attachmentDataTable');
    if (attachmentDataTableElem.length == 0)
        return;

    if (!window.attachmentDT) {
        window.attachmentDT = new dataTableHelper(attachmentDataTableElem.attr('id'), {
            ajax: '/fAttachment/getListDataTable?fObjectID=' + window.currentForm.fObjectID + '&fObjectDataID=' + window.recID,
            dom: '<"top">rt<"bottom"<pi>><"clear">',
            columns: [
                { title: "ID", visible: false },
                {
                    title: lngHelper.get('formDataEdit.attachmentName'),
                    render: function (data, type, row) {
                        return getFileIcon(row[8]) + row[1] + '.' + row[8];
                    }
                },
                { title: lngHelper.get('formDataEdit.attachmentCreateTime'), visible: true, render: lngHelper.dataTableDateTimeColumnRender('dateTime') },
                { title: lngHelper.get('formDataEdit.attachmentCreateUser'), visible: true }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('formDataEdit.downloadAttachment'),
                clickFunction: 'btnDownloadAttachmentClick'
            }, {
                text: lngHelper.get('formDataEdit.deleteAttachment'),
                clickFunction: 'btnDeleteAttachmentClick'
            }, {
                text: lngHelper.get('formDataEdit.versions'),
                clickFunction: 'btnShowVersions'
            }],
            dblClickFunction: 'btnDownloadAttachmentClick'
        });
        attachmentDataTableElem.after('<input type="file" id="uploadAttach" name="filename" multiple><button type="button" class="btn btn-primary" onclick="uploadFiles()">' + lngHelper.get('formDataEdit.uploadFiles') + '</button>');
    }
}
async function uploadFiles() {
    var input = document.getElementById('uploadAttach');
    var files = input.files;
    var formData = new FormData();

    if (window.recID == utils.emptyGuid) {
        var result = await save();
        if (!result)
            return;
        window.attachmentDT.dtObject.ajax.url('/fAttachment/getListDataTable?fObjectID=' + window.currentForm.fObjectID + '&fObjectDataID=' + window.recID);
    }

    for (var i = 0; i != files.length; i++) {
        formData.append("files", files[i]);
    }
    var url = 'fAttachment/uploadFiles?recID=0&fObjectID=' + window.currentForm.fObjectID + '&fObjectDataID=' + window.recID;
    var result = await service.call(url, 'POST', formData, false, false, { contentType: false, processData: false, dataType: false });
    window.attachmentDT.reload();
}
function btnDeleteAttachmentClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var recID = row.data()[0];
    utils.confirmDelete(async () => {
        var result = await service.call('fAttachment/delete', 'GET', { recID: recID });
        window.attachmentDT.reload();
    })
}
function btnDeleteAttachmentClick_Version(e) {
    btnDeleteAttachmentClick(e);
    if (window.attachmentVersionsPopup)
        window.attachmentVersionsPopup.modal('hide');
}
function btnDownloadAttachmentClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    window.open('../fAttachment/download?recID=' + row.data()[0]);
}
async function updateAttachmentVersionTable() {
    var versionData = await service.call('fAttachment/getVersionsDataTable', 'GET', { recID: window.attachmentVersionsPopup.data('recID') });
    window.attachmentVersionDT.dtObject.clear();
    window.attachmentVersionDT.dtObject.rows.add(versionData.data);
    window.attachmentVersionDT.dtObject.draw();
}
async function btnShowVersions(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var recID = row.data()[0];
    if (!window.attachmentVersionsPopup) {
        var cnt = '<div class="attachment-container"><table id="attachmentVersionsTable" class="table table-bordered dataTable" style="width:100%"><tr><th>' + lngHelper.get('formDataEdit.attachmentName') + '</th><th>' + lngHelper.get('formDataEdit.attachmentCreateTime') + '</th><th>' + lngHelper.get('formDataEdit.attachmentCreateUser') + '</th></tr></table></div>';
        window.attachmentVersionsPopup = utils.openPopup('', lngHelper.get('formDataEdit.versions'), cnt, null, { size: 'xxl' });

        window.attachmentVersionDT = new dataTableHelper('attachmentVersionsTable', {
            data: [],
            dom: '<"top">rt<"bottom"<pi>><"clear">',
            columns: [
                { title: "ID", visible: false },
                {
                    title: lngHelper.get('formDataEdit.attachmentName'),
                    render: function (data, type, row) {
                        return getFileIcon(row[8]) + row[1] + '.' + row[8];
                    }
                },
                { title: lngHelper.get('formDataEdit.attachmentCreateTime'), visible: true },
                { title: lngHelper.get('formDataEdit.attachmentCreateUser'), visible: true }
            ],
            order: [[3, "desc"]]
        }, {
            buttons: [{
                text: lngHelper.get('formDataEdit.downloadAttachment'),
                clickFunction: 'btnDownloadAttachmentClick'
            }, {
                text: lngHelper.get('formDataEdit.deleteAttachment'),
                clickFunction: 'btnDeleteAttachmentClick_Version'
            }],
            dblClickFunction: 'btnDownloadAttachmentClick'
        });
    } else
        window.attachmentVersionsPopup.modal('show');
    for (var i = 0; i < window.attachmentDT.dtObject.rows().count(); i++) {
        if (window.attachmentDT.dtObject.cell(i, 0).data() == recID.toString()) {
            window.attachmentVersionsPopup.find('.modal-header .modal-title').html(lngHelper.get('formDataEdit.versions') + ' - <span style="font-size:10pt">' + window.attachmentDT.dtObject.cell(i, 1).data() + '</span>');
            break;
        }
    }
    window.attachmentVersionsPopup.data('recID', recID);
    updateAttachmentVersionTable();
}
async function initReportMenu() {
    if (!window.recID)
        return;
    var result = await service.call('fReport/getList', 'GET', { fObjectID: window.currentForm.fObjectID });
    var str = '';
    for (var i = 0; i < result.length; i++) {
        if (result[i].showOnForm)
            str += '<li class="navi-item"><a class="dropdown-item" href="#" onclick="btnReportClick(\'' + result[i].recID + '\')">' + result[i].displayName + '</a></li>';
    }
    if (str == '')
        $('#reportMenu').parents('.dropdown').hide();
    else {
        $('#reportMenu').html(str);
        $('#reportMenu').parents('.dropdown').show();
    }
}
async function btnReportClick(reportID) {
    var url = '/fReport/getReportByID?fReportID=' + reportID + '&recID=' + window.recID.toString() + '&languageName=' + window.lngHelper.currentLanguage.name;
    window.open(url);
}
function initFormExecutables() {
    $('[f-events]').each((index, item) => {
        item = $(item);
        var events = JSON.parse(item.attr('f-events'));
        var targetElem = null;
        if (item.find('button'))
            targetElem = item.find('button');
        if (targetElem == null)
            targetElem = item;
        for (var i = 0; i < events.length; i++) {
            targetElem.on(events[i].trigger, (e) => {
                var elem = utils.getEventSender(e);
                var events = elem.attr('f-events');
                if (elem.attr('f-events'))
                    events = elem.attr('f-events');
                if (!events)
                    events = elem.parents(['f-events']).attr('f-events');
                if (events) {
                    var events = JSON.parse(events);
                    for (var i = 0; i < events.length; i++) {
                        if (events[i].trigger == e.type) {
                            executeCode(events[i].executableID);
                            break;
                        }
                    }
                }
            })
        }
    })
}
async function initMaps() {
    if (!window.allDataLayers) {
        window.allDataLayers = await service.call('fMap/getMapLayers', 'GET', {});
    }
    $('.map-control').each((index, item) => {
        item = $(item);
        var overlayMaps = [{
            displayName: 'test6-konum',
            layerName: 'test6-konum'
        }]
        var baseLayers = item.attr('base-layers');
        var mapBaseLayers = [];
        if (baseLayers)
            mapBaseLayers = JSON.parse(baseLayers);
        var dataLayers = item.attr('data-layers');
        var mapDataLayers = [];
        if (dataLayers) {
            var tmpLayers = JSON.parse(dataLayers);
            for (var i = 0; i < tmpLayers.length; i++) {
                for (var j = 0; j < window.allDataLayers.length; j++) {
                    if (window.allDataLayers[j].wktLayername == tmpLayers[i]) {
                        mapDataLayers.push({
                            layerName: tmpLayers[i],
                            displayName: window.allDataLayers[j].layerName
                        })
                    }
                }
            }
        }
        var allowEdit = !item.attr('disabled');
        var map = new fMapHelper(item, mapBaseLayers, mapDataLayers, { allowEdit: allowEdit });
        map.addDrawLayer(allowEdit);
    })
}
async function initForm() {
    var result = await service.call('fForm/getAllInfo', 'GET', { recID: window.formID });
    window.currentForm = result.form;
    if (window.currentForm.deleteTime) {
        utils.alertWarning(lngHelper.get('formDataEdit.formIsDeleted'));
        return;
    }
    window.currentObjectPropertyList = result.objectPropertyList;
    window.currentOptionListList = result.optionLists;
    window.currentParentObjectPropertyWithRelationList = result.parentObjectPropertyWithRelationList;
    window.currentObjectRelationList = result.objectRelationList;
    window.currentFormExecutableList = result.formExecutableList;
    //document.title = window.currentForm.displayName;
    utils.changeTitleFromInside(window.currentForm.displayName);

    $('#formContent').html(window.currentForm.formContent);
    $('#formContent').localize();
    $('#formContent [fhidden=true]').closest('.fform-element').hide();

    initMaps();
    initSystemObjectProperties();

    for (var i = 0; i < window.currentObjectPropertyList.length; i++) {
        if (window.currentObjectPropertyList[i].editorType == window.fEnums.FObjectPropertyEditorType.Select || window.currentObjectPropertyList[i].editorType == window.fEnums.FObjectPropertyEditorType.RadioButton) {
            await getOptionList(window.currentObjectPropertyList[i].recID.toString(), window.currentObjectPropertyList[i].fOptionListID, window.currentObjectPropertyList[i].editorType);
        }
    }
    await initParentPropertyElements();

    if (window.recID != utils.emptyGuid) {
        try {
            await getFormData();
        }
        catch (err) { }
    }
    initOptionListRelations();
    if (window.onFormInitCompleted)
        window.onFormInitCompleted();

    initChildTables();
    initSelectSource();
    initAttachments();
    initReportMenu();
    if (window.bProcessRunElementID)
        initBProcess();
    var btnLogVisible = false;
    var btnSaveVisible = true;
    var btnDeleteVisible = true;
    if (window.currentForm.props) {
        var props = JSON.parse(window.currentForm.props);
        if (!props.operation_Save)
            btnSaveVisible = false;
        if (!props.operation_Delete)
            btnDeleteVisible = false;
    }

    //if (btnSaveVisible || btnDeleteVisible) {
    var sessionInfo = await service.call('fSession/getSessionInfo', 'GET', {});
    if (!sessionInfo.isSystemAdmin) {
        var result = await service.call('fAuthorization/getListByObjectTypeArray', 'GET', {
            authObjects: window.fEnums.AuthObjectType.ObjectDataCreate + ',' + window.fEnums.AuthObjectType.ObjectDataDelete + ',' + window.fEnums.AuthObjectType.ObjectDataEdit,
            authObjectRecID: window.currentForm.fObjectID
        });
        var bCreateAuth = false;
        var bDeleteAuth = false;
        var bEditAuth = false;
        for (var i = 0; i < result.length; i++) {
            if (result[i].authObject == window.fEnums.AuthObjectType.ObjectDataCreate && result[i].authStatus == window.fEnums.AuthStatusType.Allowed)
                bCreateAuth = true;
            if (result[i].authObject == window.fEnums.AuthObjectType.ObjectDataDelete && result[i].authStatus == window.fEnums.AuthStatusType.Allowed)
                bDeleteAuth = true;
            if (result[i].authObject == window.fEnums.AuthObjectType.ObjectDataEdit && result[i].authStatus == window.fEnums.AuthStatusType.Allowed)
                bEditAuth = true;
        }
        if (!result || result.length == 0) {
            var result = await service.call('fSession/getSessionInfo', 'GET', {});
            if (result.isSystemAdmin) {
                bCreateAuth = true;
                bDeleteAuth = true;
                bEditAuth = true;
            }
        }
        if ((window.recID == 0 && !bCreateAuth) || (window.recID != 0 && !bEditAuth))
            btnSaveVisible = false;
        if (!bDeleteAuth)
            btnDeleteVisible = false;
    } else
        btnLogVisible = true;
    //}

    if (!btnLogVisible) {
        result = await service.call('fAuthorization/getListByUser', 'GET', { authObject: 0 });
        for (var i = 0; i < result.length; i++) {
            var item = result[i];
            if (item.authObject == window.fEnums.AuthObjectType.LogView && item.authStatus == 1) {
                btnLogVisible = true;
                break;
            }
        }
    }
    if (btnSaveVisible)
        $('#btnSave').show();
    if (btnSaveVisible && window.closeOnSave)
        $('#btnSaveAndClose').show();

    if (btnDeleteVisible)
        $('#btnDelete').show();
    if (btnLogVisible)
        $('#btnLog').show();

    if (window.currentFormExecutableList) {
        for (var i = 0; i < window.currentFormExecutableList.length; i++) {
            if (window.currentFormExecutableList[i].runOnLoad) {
                executeFormExecutableInternal(window.currentFormExecutableList[i]);
            }
        }
    }

    initFormExecutables();
    const event = new Event('initFormCompleted');
    document.dispatchEvent(event);
}
async function executeFormExecutableInternal(formExecutable, args) {
    switch (formExecutable.codeType) {
        case window.fEnums.FormExecutableCodeType.ServerSide:
            if (!args)
                args = {};
            var result = await service.call('fFormExecutable/execute?recID=' + formExecutable.recID + '&fObjectDataRecID=' + window.recID, 'POST', args);
            return result;
            break;
        case window.fEnums.FormExecutableCodeType.ClientSide:
            eval(formExecutable.code);
            break;
    }
    return null;
}
async function executeCode(recIDOrDisplayName, args) {
    var formExecutable = null;
    if (window.currentFormExecutableList) {
        for (var i = 0; i < window.currentFormExecutableList.length; i++) {
            if (window.currentFormExecutableList[i].recID == recIDOrDisplayName || window.currentFormExecutableList[i].displayName == recIDOrDisplayName) {
                formExecutable = window.currentFormExecutableList[i];
                break;
            }
        }
    }
    if (formExecutable) {
        var result = await executeFormExecutableInternal(formExecutable, args);
        return result;
    }
    return null;
}
async function changeForm(formDisplayName) {
    var result = await service.call('fForm/getList', 'GET', { fObjectID: window.currentForm.fObjectID });
    var newFormID = null;
    for (var i = 0; i < result.length; i++) {
        if (result[i].displayName == formDisplayName) {
            newFormID = result[i].recID;
            break;
        }
    }
    if (!newFormID)
        return;

    var formData = getDataFromForm();
    window.formID = newFormID;
    await initForm();

    $('[fmodel]').each(function (index, item) {
        //var fModelID = $(item).attr('fmodel').match(/\{\{([^}]+)\}\}/)[1];
        var fModelID = getPropertyIDFromFModel($(item).attr('fmodel'));
        if (formData.hasOwnProperty(fModelID)) {
            var elemValue = formData[fModelID];
            setObjectPropertyElemValue(fModelID, item, elemValue);
        }
    })
    for (const [key, value] of Object.entries(window.parentObjectValues)) {
        if (formData['rel-' + key])
            window.parentObjectValues[key] = formData['rel-' + key];
    }
}
function initSystemObjectProperties_Inner(fObjectPropertyID, optionsHtml) {
    $('[fmodel$=' + fObjectPropertyID + '\\}\\}]').each((index, item) => {
        item = $(item);
        var newItem = $('<select class="form-control">' + optionsHtml + '</select>');
        newItem.attr('fmodel', item.attr('fmodel'));
        newItem.attr('id', item.attr('id'));
        newItem.attr('name', item.attr('name'));
        newItem.attr('fobjectid', item.attr('fobjectid'));
        item.replaceWith(newItem);
    })
}
function initSystemObjectProperties() {
    var userStatusRecID = 'f392726f-4c82-4169-88cc-0567047af496';
    var companyStatusRecID = '1ce75f97-f50a-4577-afe1-64f6b44e50cc';
    var userGroupSourceType = '632d146a-12e9-4123-915c-42a006d88107';

    initSystemObjectProperties_Inner(userStatusRecID, '<option value=0>' + lngHelper.get('formDataEdit.systemObjectProperty_UserStatus_Active') + '</option><option value=1>' + lngHelper.get('formDataEdit.systemObjectProperty_UserStatus_Passive') + '</option>');
    initSystemObjectProperties_Inner(companyStatusRecID, '<option value=0>' + lngHelper.get('formDataEdit.systemObjectProperty_CompanyStatus_Active') + '</option><option value=1>' + lngHelper.get('formDataEdit.systemObjectProperty_CompanyStatus_Passive') + '</option>');
    initSystemObjectProperties_Inner(userGroupSourceType, '<option value=0>' + lngHelper.get('formDataEdit.systemObjectProperty_UserGroupSourceType_IFrame') + '</option><option value=1>' + lngHelper.get('formDataEdit.systemObjectProperty_UserGroupSourceType_LDAP') + '</option>');
}
$(document).on('initcompleted', function () {
    window.formID = utils.getUrlParam('formID');
    window.recID = utils.getUrlParam('recID');
    if (!window.recID)
        window.recID = utils.emptyGuid;
    window.relations = utils.getUrlParam('relations');
    if (window.relations)
        window.relations = JSON.parse(window.relations);
    else
        window.relations = [];
    window.bProcessRunElementID = utils.getUrlParam('bProcessRunElementID');
    window.isNewRecord = window.recID == utils.emptyGuid;
    window.closeOnSave = utils.getUrlParam('closeOnSave') == "true" || utils.getUrlParam('closeOnSave') == "1";
    window.windowResult = utils.getUrlParam('windowResult');
    initForm();
})