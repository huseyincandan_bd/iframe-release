﻿function menuTargetObjectChange() {
    var fObjectID = $('#menuTargetObject').val();
    var allOptions = $('#menuTargetForm').data('allOptions');
    var options = [];
    if (allOptions) {
        for (var i = 0; i < allOptions.length; i++) {
            if (allOptions[i].attributes.fObjectID == fObjectID) {
                options.push(allOptions[i]);
            }
        }
    }
    utils.fillSelectOptions($('#menuTargetForm'), options);
    allOptions = $('#menuTargetTable').data('allOptions');
    options = [];
    if (allOptions) {
        for (var i = 0; i < allOptions.length; i++) {
            if (allOptions[i].attributes.fObjectID == fObjectID) {
                options.push(allOptions[i]);
            }
        }
    }
    utils.fillSelectOptions($('#menuTargetTable'), options);
}
async function fillTargetProps() {
    var objects = $('#menuTargetObject option');
    if (!objects || objects.length == 0) {
        objects = await service.call('fObject/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < objects.length; i++) {
            options.push({
                value: objects[i].recID,
                text: objects[i].displayName
            });
        }
        utils.fillSelectOptions($('#menuTargetObject'), options);
    }
    var forms = $('#menuTargetForm option');
    if (!forms || forms.length == 0) {
        var forms = await service.call('fForm/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < forms.length; i++) {
            options.push({
                value: forms[i].recID,
                text: forms[i].displayName,
                attributes: {
                    fObjectID: forms[i].fObjectID
                }
            })
        }
        utils.fillSelectOptions($('#menuTargetForm'), []);
        $('#menuTargetForm').data('allOptions', options);
    }
    var tables = $('#menuTargetTable option');
    if (!tables || tables.length == 0) {
        var tables = await service.call('fTable/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < tables.length; i++) {
            options.push({
                value: tables[i].recID,
                text: tables[i].displayName,
                attributes: {
                    fObjectID: tables[i].fObjectID
                }
            })
        }
        utils.fillSelectOptions($('#menuTargetTable'), []);
        $('#menuTargetTable').data('allOptions', options);
    }
    var processes = $('#menuTargetBProcess option');
    if (!processes || processes.length == 0) {
        var processes = await service.call('fBProcess/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < processes.length; i++) {
            options.push({
                value: processes[i].recID,
                text: processes[i].displayName
            })
        }
        utils.fillSelectOptions($('#menuTargetBProcess'), options);
    }
    var dashboards = $('#menuTargetDashboard option');
    if (!dashboards || dashboards.length == 0) {
        var dashboards = await service.call('fDashboard/getSimpleList', 'GET', {});
        var options = [];
        for (var i = 0; i < dashboards.length; i++) {
            options.push({
                value: dashboards[i].recID,
                text: dashboards[i].displayName
            })
        }
        utils.fillSelectOptions($('#menuTargetDashboard'), options);
    }
}
function menuTargetTypeChange() {
    var menuTargetType = $('#menuTargetType').val();
    switch (menuTargetType) {
        case 'url':
            $('#menuTargetUrl').parents('.form-group').show();
            $('#menuTargetObject').parents('.form-group').hide();
            $('#menuTargetForm').parents('.form-group').hide();
            $('#menuTargetTable').parents('.form-group').hide();
            $('#menuTargetBProcess').parents('.form-group').hide();
            $('#menuTargetDashboard').parents('.form-group').hide();
            break;
        case 'objectTable':
            $('#menuTargetUrl').parents('.form-group').hide();
            $('#menuTargetObject').parents('.form-group').show();
            $('#menuTargetForm').parents('.form-group').hide();
            $('#menuTargetTable').parents('.form-group').show();
            $('#menuTargetBProcess').parents('.form-group').hide();
            $('#menuTargetDashboard').parents('.form-group').hide();
            break;
        case 'formCreateRecord':
            $('#menuTargetUrl').parents('.form-group').hide();
            $('#menuTargetObject').parents('.form-group').show();
            $('#menuTargetForm').parents('.form-group').show();
            $('#menuTargetTable').parents('.form-group').hide();
            $('#menuTargetBProcess').parents('.form-group').hide();
            $('#menuTargetDashboard').parents('.form-group').hide();
            break;
        case 'startBusinessProcess':
            $('#menuTargetUrl').parents('.form-group').hide();
            $('#menuTargetObject').parents('.form-group').hide();
            $('#menuTargetForm').parents('.form-group').hide();
            $('#menuTargetTable').parents('.form-group').hide();
            $('#menuTargetBProcess').parents('.form-group').show();
            $('#menuTargetDashboard').parents('.form-group').hide();
            break;
        case 'dashboard':
            $('#menuTargetUrl').parents('.form-group').hide();
            $('#menuTargetObject').parents('.form-group').hide();
            $('#menuTargetForm').parents('.form-group').hide();
            $('#menuTargetTable').parents('.form-group').hide();
            $('#menuTargetBProcess').parents('.form-group').hide();
            $('#menuTargetDashboard').parents('.form-group').show();
            break;
    }
}
function menuUpdated() {
    getMenuList();
    if (window.parent.initMainMenu)
        window.parent.initMainMenu();
}
function menuParentChange() {
    var selectedOption = $('#menuParent option:selected');
    var selectedItem = menuHelper.findMenuInTree(selectedOption.val(), window.menuTree);
    if (!selectedItem || selectedItem.parentID == utils.emptyGuid) {
        $('#menuLeafOptions').hide();
        if (!selectedItem)
            $('#menuIconName').parents('.form-group').hide();
        else
            $('#menuIconName').parents('.form-group').show();
    } else {
        $('#menuLeafOptions').show();
        $('#menuIconName').parents('.form-group').hide();
    }
}
function fillParentSelect() {
    $('#menuParent').html('');
    $('#menuParent').append('<option value="' + utils.emptyGuid + '" ' + (window.currentMenu == null ? 'selected' : '') + '>' + lngHelper.get('menuList.none') + '</option>');
    for (var i = 0; i < window.menuTree.length; i++) {
        if (!window.currentMenu || window.menuTree.recID != window.currentMenu.recID)
            $('#menuParent').append('<option value="' + window.menuTree[i].recID + '" ' + (window.currentMenu && window.currentMenu.parentID == window.menuTree[i].recID ? 'selected' : '') + '>' + window.menuTree[i].displayName + '</option>');
        if (window.menuTree[i].children && window.menuTree[i].children.length > 0) {
            for (var j = 0; j < window.menuTree[i].children.length; j++) {
                if (!window.currentMenu || window.menuTree[i].children[j].recID != window.currentMenu.recID)
                    $('#menuParent').append('<option value="' + window.menuTree[i].children[j].recID + '" ' + (window.currentMenu && window.currentMenu.parentID == window.menuTree[i].children[j].recID ? 'selected' : '') + '>' + window.menuTree[i].children[j].displayName + '</option>');
            }
        }
    }
    menuParentChange();
}
async function btnAddClick() {
    window.currentMenu = null;
    fillParentSelect();
    var result = await fillTargetProps();
    $('#menuTargetPage option').removeAttr('selected');
    $('#menuTargetPage option').first().removeAttr('selected');
    $('#menuDisplayName').val('');
    $('#menuTargetUrl').val('');
    $('#menuIconName').removeClass();
    $('#menuIconName').addClass('fa fa-fw fa-star');
    var selectedMenuList = getSelectedMenuList();
    if (selectedMenuList && selectedMenuList.length > 0) {
        var parentItem = $('#menuParent option[value=' + selectedMenuList[0].recID + ']');
        if (parentItem && parentItem.length > 0) {
            $('#menuParent option:selected').removeAttr('selected');
            parentItem.attr('selected', true);
            menuParentChange();
        }
    }
    $('#menuTargetType').val('objectTable');
    menuTargetTypeChange();
    menuTargetObjectChange();
    $('#menuTargetParam').val('');
    $('#modalEditMenu').modal('show');
}
async function btnEditClick() {
    var selectedMenuList = getSelectedMenuList();
    if (!selectedMenuList || selectedMenuList.length == 0) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('generic.selectRecord'));
        return;
    }
    var selectedMenu = selectedMenuList[0];

    //TODO:Aşağıdaki bölüm geriye uyumluluk için yazıldı. İleride kaldırılabilir.
    //if (selectedMenu && selectedMenu.props && selectedMenu.props.targetForm && selectedMenu.props.targetForm.indexOf('|') > 0) {
    //    var parts = selectedMenu.props.targetForm.split('|');
    //    selectedMenu.props.targetForm = parts[parts.length - 1];
    //}
    //if (selectedMenu && selectedMenu.props && selectedMenu.props.targetTable && selectedMenu.props.targetTable.indexOf('|') > 0) {
    //    var parts = selectedMenu.props.targetTable.split('|');
    //    selectedMenu.props.targetTable = parts[parts.length - 1];
    //}

    window.currentMenu = selectedMenu;
    fillParentSelect();
    await fillTargetProps();
    $('#menuDisplayName').val(selectedMenu.displayName);
    $('#menuTargetUrl').val(selectedMenu.targetUrl);
    $('#menuIconName').removeClass();
    $('#menuIconName').addClass(selectedMenu.iconName);
    $('#menuTargetPage option').attr('selected', false);
    $('#menuTargetPage option').each(function (index, item) {
        if ($(item).val() == window.currentMenu.targetPage) {
            $(item).attr('selected', true);
        }
    })
    if (selectedMenu.props && !$.isEmptyObject(selectedMenu.props)) {
        $('#menuTargetType').val(selectedMenu.props.targetType);
        menuTargetTypeChange();
        $('#menuTargetObject').val(selectedMenu.props.targetObject);
        menuTargetObjectChange();
        $('#menuTargetForm').val(selectedMenu.props.targetForm);
        $('#menuTargetTable').val(selectedMenu.props.targetTable);
        $('#menuTargetBProcess').val(selectedMenu.props.targetBProcess);
        $('#menuTargetDashboard').val(selectedMenu.props.targetDashboard);
        $('#menuTargetParam').val(selectedMenu.props.targetParam);
    } else {
        $('#menuTargetType').val('url');
        $('#menuTargetParam').val('');
        menuTargetTypeChange();
    }
    $('#modalEditMenu').modal('show');
}
function btnMoveClick(moveDirection) {
    var selectedMenuList = getSelectedMenuList();
    if (!selectedMenuList || selectedMenuList.length == 0) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('generic.selectRecord'));
        return;
    }
    var recIDs = [];
    for (var i = 0; i < selectedMenuList.length; i++)
        recIDs.push(selectedMenuList[i].recID);
    service.call('fMenu/moveMultiple?direction=' + moveDirection, 'POST', recIDs).then(function () {
        menuUpdated();
    })
}
function btnSaveMenuClick() {
    if ($('#menuDisplayName').val().trim() == '') {
        utils.alertWarning(lngHelper.get('menuList.displayNameIsRequired'));
        return;
    }

    var parentID = $('#menuParent').val();
    if (window.currentMenu && window.currentMenu.parentID == parentID) {
        recOrder = window.currentMenu.recOrder;
    } else {
        var recOrder = 0;
        for (var i = 0; i < window.menuList.length; i++) {
            if (window.menuList[i].parentID == parentID && window.menuList[i].recOrder >= recOrder)
                recOrder = window.menuList[i].recOrder + 1;
        }
    }
    var props = {};

    if ($('#menuLeafOptions').is(':visible')) {
        props.targetType = $('#menuTargetType').val();
        if ($('#menuTargetObject').is(':visible')) {
            props.targetObject = $('#menuTargetObject').val();
            if (!props.targetObject || props.targetObject == '0') {
                utils.alertWarning(lngHelper.get('menuList.targetObjectIsRequired'));
                return;
            }
        }
        if ($('#menuTargetForm').is(':visible')) {
            props.targetForm = $('#menuTargetForm').val();
            if (!props.targetForm || props.targetForm == '0') {
                utils.alertWarning(lngHelper.get('menuList.targetFormIsRequired'));
                return;
            }
        }
        if ($('#menuTargetTable').is(':visible')) {
            props.targetTable = $('#menuTargetTable').val();
            if (!props.targetTable || props.targetTable == '0') {
                utils.alertWarning(lngHelper.get('menuList.targetTableIsRequired'));
                return;
            }
        }
        if ($('#menuTargetBProcess').is(':visible')) {
            props.targetBProcess = $('#menuTargetBProcess').val();
            if (!props.targetBProcess || props.targetBProcess == '0') {
                utils.alertWarning(lngHelper.get('menuList.targetBProcessIsRequired'));
                return;
            }
        }
        if ($('#menuTargetDashboard').is(':visible')) {
            props.targetDashboard = $('#menuTargetDashboard').val();
            if (!props.targetDashboard || props.targetDashboard == '0') {
                utils.alertWarning(lngHelper.get('menuList.targetDashboardIsRequired'));
                return;
            }
        }
        if (props.targetType == 'objectTable' || props.targetType == 'formCreateRecord' || props.targetType == 'startBusinessProcess' || props.targetType == 'dashboard') {
            var parts;
            var url;
            switch (props.targetType) {
                case 'objectTable':
                    url = './tableDataList.html?tableID=' + $('#menuTargetTable').val();
                    break;
                case 'formCreateRecord':
                    url = './formDataEdit.html?formID=' + $('#menuTargetForm').val();
                    break;
                case 'startBusinessProcess':
                    url = './bProcessRun.html?bProcessID=' + $('#menuTargetBProcess').val();
                    break;
                case 'dashboard':
                    url = './dashboard.html?recID=' + $('#menuTargetDashboard').val();
                    break;
            }
            $('#menuTargetUrl').val(url);
        }
    }
    props.targetParam = $('#menuTargetParam').val();

    var fMenu = {
        RecID: window.currentMenu ? window.currentMenu.recID : utils.emptyGuid,
        ParentID: parentID,
        DisplayName: $('#menuDisplayName').val(),
        TargetUrl: $('#menuTargetUrl').val(),
        TargetPage: $('#menuTargetPage option:selected').val(),
        IconName: $('#menuIconName').attr('class'),
        RecOrder: recOrder,
        Props: JSON.stringify(props)
    }
    $('#modalEditMenu').modal('hide');
    $('#menuTree').html('');

    service.call('fMenu/save', 'POST', fMenu).then(function (result) {
        if (result) {
            menuUpdated();
        }
    })
}
function getSelectedMenuList() {
    var selectedItems = $('#menuTree li.selected');
    var menuList = [];
    if (selectedItems) {
        selectedItems.each((index, item) => {
            menuList.push(menuHelper.findMenuInList($(item).attr('recid'), window.menuList));
        })
    }
    return menuList;
}
function btnDeleteClick() {
    var selectedMenuList = getSelectedMenuList();
    if (!selectedMenuList || selectedMenuList.length == 0) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('generic.selectRecord'));
        return;
    }
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            var recIDs = [];
            for (var i = 0; i < selectedMenuList.length; i++)
                recIDs.push(selectedMenuList[i].recID);
            service.call('fMenu/deleteMultiple', 'POST', recIDs).then(function (result) {
                menuUpdated();
            })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function getMenuHtml(menuArray, depth) {
    var str = '';
    for (var i = 0; i < menuArray.length; i++) {
        str += '<li class="list-group-item" recid="' + menuArray[i].recID + '" onclick="menuItemClick(event)">';
        if (depth == 1) {
            str += '<i class="' + menuArray[i].iconName + '" style="margin-right:10px"></i>';
        }
        str += menuArray[i].displayName;
        if (menuArray[i].children && menuArray[i].children.length > 0) {
            str += '<ul class="list-group">' + getMenuHtml(menuArray[i].children, depth + 1) + '</ul>';
        }
        str += '</li>';
    }
    return str;
}
async function onSortableStop(event, ui) {
    var result = await service.call('fMenu/moveTo', 'GET', { recID: ui.item.attr('recid'), recOrder: ui.item.index() });
    if (window.parent.initMainMenu)
        window.parent.initMainMenu();
}
function getMenuList() {
    service.call('fMenu/getList', 'GET', {}).then(function (result) {
        window.menuList = result;
        for (var i = 0; i < window.menuList.length; i++) {
            if (window.menuList[i].props)
                window.menuList[i].props = JSON.parse(window.menuList[i].props);
            else
                window.menuList[i].props = {};
        }
        window.menuTree = menuHelper.menuListToTree(window.menuList);
        $('#menuTree').html(getMenuHtml(menuTree, 0));

        $('#menuTree').sortable({
            stop: onSortableStop
        });
        $('#menuTree ul').sortable({
            stop: onSortableStop
        });
    })
}
function menuItemClick(e) {
    if (!e)
        e = window.event;
    var sender = e.srcElement || e.target;
    if (e.metaKey || e.ctrlKey) {
        $(sender).toggleClass('selected');
    } else {
        $('#menuTree li').removeClass('selected');
        $(sender).addClass('selected');
    }
    e.stopPropagation();
}
function btnAuthorizationClick() {
    var selectedMenuList = getSelectedMenuList();
    if (!selectedMenuList || selectedMenuList.length == 0) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('generic.selectRecord'));
        return;
    }
    var str = '';
    for (var i = 0; i < selectedMenuList.length; i++) {
        if (str != '')
            str += ',';
        str += selectedMenuList[i].recID;
    }
    utils.openPage('authorization.html?category=menu&authObjectRecIDs=' + str);
}
$(document).on('initcompleted', function () {
    window.menuList = [];
    getMenuList();

    $('.icp-dd').iconpicker({
        //title: 'Dropdown with picker',
        //component:'.btn > i' 
    });

    $(document).keyup(function (e) {
        if (e.which === 27 || e.keyCode === 27) {
            $.ui.ddmanager.current.cancel();
        }
    });
})