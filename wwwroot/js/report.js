﻿$(document).ready(function () {
    var js = $('#rootContainer').attr('page-js');
    var css = $('#rootContainer').attr('page-css');
    if (css)
        $('body').prepend($('<style type="text/css">' + css + '</style>'));
    if (js)
        $('body').prepend($('<script>' + js + '</script>'));
})
