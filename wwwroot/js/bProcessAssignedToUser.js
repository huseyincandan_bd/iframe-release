﻿function btnQueryClick(){
    $('#modalQuery').modal('show');
}
function btnReloadClick() {
    window.dt.reload();
}
function rowDblClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var url = './bProcessRun.html?bProcessRunElementID=' + row.data()[0];
    if (window.getAllItems)
        url = './bProcessRunHistory.html?bProcessRunElementID=' + row.data()[0];
    utils.openPage(url);
}
function btnQueryOKClick() {
    var query = {
        columnFilters: []
    };
    $('#queryContainer .form-group').each((index, item) => {
        var itemJQ = $(item);
        switch (itemJQ.attr('item-type')) {
            case 'BProcessRunElementTime':
                var value = itemJQ.find('#bProcessRunElementTime1').val();
                if (value) {
                    query.columnFilters.push({
                        name: itemJQ.find('#bProcessRunElementTime1').attr('item-id'),
                        operator: '>=',
                        value: value
                    });
                }
                value = itemJQ.find('#bProcessRunElementTime2').val();
                if (value) {
                    query.columnFilters.push({
                        name: itemJQ.find('#bProcessRunElementTime2').attr('item-id'),
                        operator: '<=',
                        value: value
                    });
                }
                break;
            case 'BProcessCurrentStateDisplayName':
                var value = itemJQ.find('.form-control').val();
                if (value && value != utils.emptyGuid) {
                    query.columnFilters.push({
                        name: itemJQ.find('.form-control').attr('item-id'),
                        operator: ' LIKE ',
                        value: '%' + value + '%'
                    });
                }
                break;
            case 'BProcessRunParameterValue':
                var value = itemJQ.find('.form-control').val();
                if (value && value != utils.emptyGuid) {
                    query.columnFilters.push({
                        name: itemJQ.find('.form-control').attr('item-id'),
                        operator: ' LIKE ',
                        value: '%' + value + '%'
                    });
                }
                break;
            default:
                var value = itemJQ.find('.form-control').val();
                if (value && value != utils.emptyGuid) {
                    query.columnFilters.push({
                        name: itemJQ.find('.form-control').attr('item-id'),
                        operator: '=',
                        value: value
                    });
                }
                break;
        }
    })
    window.currentQuery = query;
    btnReloadClick();
    $('#modalQuery').modal('hide');
}
function bProcessChange() {
    var bProcessRecID = $('#bProcess').val();
    var allOptions = $('#bProcessVersion').data('allOptions');
    var options = [];
    if (allOptions) {
        for (var i = 0; i < allOptions.length; i++) {
            if (allOptions[i].attributes['process-id'] == bProcessRecID)
                options.push(allOptions[i]);
        }
    }
    utils.fillSelectOptions($('#bProcessVersion'), options, true);
}
async function initQueryModal(processListItems, processParameters) {
    $('#queryContainer').html('');

    html = '';
    processListItems.forEach((item, index) => {
        switch (item.itemType) {
            case window.fEnums.FBProcessAssignListItemType.BProcessDisplayName:
                html += '<div class="form-group row" item-type="BProcessDisplayName">\
                            <label for="bProcess" class="col-sm-4 col-form-label">' + lngHelper.get('bProcessDesign.assignListItemType_BProcessDisplayName') + '</label>\
                            <div class="col-sm-8">\
                                <select id="bProcess" class="form-control" onchange="bProcessChange()" item-id="' + item.recID + '"></select>\
                            </div>\
                        </div>';
                break;
            case window.fEnums.FBProcessAssignListItemType.BProcessVersionDisplayName:
                html += '<div class="form-group row" item-type="BProcessVersionDisplayName">\
                            <label for="bProcessVersion" class="col-sm-4 col-form-label">' + lngHelper.get('bProcessDesign.assignListItemType_BProcessVersionDisplayName') + '</label>\
                            <div class="col-sm-8">\
                                <select id="bProcessVersion" class="form-control" item-id="' + item.recID + '"></select>\
                            </div>\
                        </div>';
                break;
            case window.fEnums.FBProcessAssignListItemType.BProcessCurrentStateDisplayName:
                html += '<div class="form-group row" item-type="BProcessCurrentStateDisplayName">\
                            <label for="bProcessStateDisplayName" class="col-sm-4 col-form-label">' + lngHelper.get('bProcessDesign.assignListItemType_BProcessCurrentStateDisplayName') + '</label>\
                            <div class="col-sm-8">\
                                <input id="bProcessStateDisplayName" type="text" class="form-control" item-id="' + item.recID + '">\
                            </div>\
                        </div>';
                break;
            case window.fEnums.FBProcessAssignListItemType.BProcessRunElementTime:
                html += '<div class="form-group row" item-type="BProcessRunElementTime">\
                            <label for="bProcessRunElementTime" class="col-sm-4 col-form-label">' + lngHelper.get('bProcessDesign.assignListItemType_BProcessRunElementTime') + '</label>\
                            <div class="col-sm-8" style="display:flex">\
                                <input id="bProcessRunElementTime1" type="datetime-local" class="form-control" style="width:47%" item-id="' + item.recID + '"><span style="line-height:33px;width:6%;text-align:center"> - </span><input id="bProcessRunElementTime2" type="datetime-local" class="form-control" style="width:47%" item-id="' + item.recID  + '">\
                            </div>\
                        </div>';
                break;

            //TODO: İleride kullanıcıya göre filtrelemek istenirse açılacak. 
            //case window.fEnums.FBProcessAssignListItemType.BProcessRunElementAssigneeName:
            //    html += '<div class="form-group row" item-type="BProcessRunElementAssigneeName">\
            //                <label for="bProcessRunElementAssignee" class="col-sm-4 col-form-label">' + lngHelper.get('bProcessDesign.assignListItemType_BProcessRunElementAssigneeName') + '</label>\
            //                <div class="col-sm-8">\
            //                    <select id="bProcessRunElementAssignee" class="form-control" item-id="' + item.recID + '"></select>\
            //                </div>\
            //            </div>';
            //    break;

            case window.fEnums.FBProcessAssignListItemType.BProcessRunParameterValue:
                processParameters.forEach((parameterItem, parameterIndex) => {
                    if (item.parameterNumber == parameterItem.parameterNumber) {
                        html += '<div class="form-group row" item-type="BProcessRunParameterValue">\
                            <label for="bProcessRunParameterValue_' + parameterItem.recID + '" class="col-sm-4 col-form-label">' + lngHelper.get('bProcessDesign.assignListItemType_BProcessRunParameterValue') + '</label>\
                            <div class="col-sm-8">\
                                <input id="bProcessRunParameterValue_' + parameterItem.recID + '" type="text" class="form-control" item-id="' + item.recID + '">\
                            </div>\
                        </div>';
                    }
                });
                break;
        }
    })
    $('#queryContainer').html(html);
    if ($('#bProcess').length > 0) {
        var result = await service.call('fBProcess/getList');
        var options = [];
        for (var i = 0; i < result.length; i++) {
            options.push({
                text: result[i].displayName,
                value: result[i].recID
            });
        }
        utils.fillSelectOptions($('#bProcess'), options, true);
        $('#bProcess').val(window.currentBProcessRecID);
    }
    if ($('#bProcessVersion').length > 0) {
        var result = await service.call('fBProcessVersion/getList');
        var options = [];
        for (var i = 0; i < result.length; i++) {
            options.push({
                text: result[i].description,
                value: result[i].recID,
                attributes: {
                    'process-id': result[i].bProcessID
                }
            });
        }
        $('#bProcessVersion').data('allOptions', options);
    }
    bProcessChange();

    //TODO: İleride kullanıcıya göre filtrelemek istenirse açılacak. 
    //if ($('#bProcessRunElementAssignee').length > 0) {
    //    var result = await service.call('fUser/getList');
    //    var options = [];
    //    for (var i = 0; i < result.length; i++) {
    //        options.push({
    //            text: result[i].userName,
    //            value: result[i].recID
    //        });
    //    }
    //    utils.fillSelectOptions($('#bProcessRunElementAssignee'), options, false, utils.emptyGuid);
    //    //var currentSession = await service.call('fSession/getSessionInfo', 'GET', {});
    //    //$('#bProcessRunElementAssignee').val(currentSession.user.recID); 
    //}
}
async function initTable() {
    var getAllItems = utils.getUrlParam("getAllItems");
    if (!getAllItems)
        getAllItems = false;
    var result = await service.call('fBProcess/getListItems', 'GET', { bProcessRecID: window.currentBProcessRecID });
    var parameters = await service.call('fBProcess/getParameters', 'GET', { bProcessRecID: window.currentBProcessRecID });
    initQueryModal(result, parameters);
    var columns = [{ title: "ID", visible: false }];
    var order = [];
    for (var i = 0; i < result.length; i++) {
        for (const [key, value] of Object.entries(window.fEnums.FBProcessAssignListItemType)) {
            if (value == result[i].itemType) {
                if (result[i].itemType == window.fEnums.FBProcessAssignListItemType.BProcessRunElementTime) {
                    columns.push({ title: lngHelper.get('bProcessDesign.assignListItemType_' + key), render: lngHelper.dataTableDateTimeColumnRender('dateTime') });
                    order = [[i + 2, "desc"]];
                }
                else {
                    if (result[i].itemType == window.fEnums.FBProcessAssignListItemType.BProcessRunParameterValue) {
                        var title = 'Parameter';
                        for (var j = 0; j < parameters.length; j++) {
                            if (result[i].parameterNumber == parameters[j].parameterNumber) {
                                title = parameters[j].displayName;
                                break;
                            }
                        }
                        columns.push({ title: title });
                    }
                    else
                        columns.push({ title: lngHelper.get('bProcessDesign.assignListItemType_' + key) });
                }
                break;
            }
        }
    }
    window.dt = new dataTableHelper('dataTable', {
        ajax: {
            url: '/fBProcess/getAssignedElementRunsDataTable?bProcessRecID=' + window.currentBProcessRecID + '&getAllItems=' + (getAllItems ? 'true' : 'false'),
            type: 'POST',
            data: function (d) {
                if (window.currentQuery)
                    d.query = window.currentQuery;
            }
        },
        columns: columns,
        order: order
    }, {
        buttons: [{
            text: lngHelper.get('bProcessAssignedToUser.details'),
            clickFunction: 'rowDblClick'
        }],
        dblClickFunction: 'rowDblClick'
    });
}
$(document).on('initcompleted', function () {
    window.currentBProcessRecID = utils.getUrlParam('bProcessRecID');
    window.getAllItems = utils.getUrlParam('getAllItems');
    initTable();
})