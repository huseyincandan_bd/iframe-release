﻿function btnAddClick() {
    $('#modalEdit').data('recID', utils.emptyGuid);
    $('#displayName').val('');
    $('#taskStatus').val('0');
    $('#actionType').val('0');
    actionTypeChange();
    $('#startTime').val(moment(new Date()).format('YYYY-MM-DDTHH:mm'));
    $('#periodType').val('0');
    periodTypeChange();
    $('#recurrencePeriod').val(1);
    $('#weekDays').selectpicker('val', []);
    $('#months').selectpicker('val', []);
    $('#monthlyRepeatType').val('0');
    monthlyRepeatTypeChange();
    $('#monthDays').selectpicker('val', []);
    $('#monthlyOnWeeks').selectpicker('val', []);
    $('#monthlyOnWeekDays').selectpicker('val', []);
    $('#repeatIntervalMinutes').val('0');
    repeatIntervalMinutesChange();
    $('#repeatDurationMinutes').val('15');
    $('#actionCode').val('');
    $('#modalEdit').modal('show');
}
async function btnEditClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];

    var result = await service.call('fScheduledTask/get', 'GET', { recID: recID });
    if (result) {
        $('#modalEdit').data('recID', result.recID);
        $('#displayName').val(result.displayName);
        $('#taskStatus').val(result.taskStatus.toString());
        $('#actionType').val(result.actionType.toString());
        await actionTypeChange();
        $('#action').val(result.actionID);
        $('#startTime').val(moment(result.startTime).format('YYYY-MM-DDTHH:mm'));
        $('#periodType').val(result.periodType);
        periodTypeChange();
        $('#recurrencePeriod').val(result.recurrencePeriod);
        $('#weekDays').selectpicker('val', getMultiselectSelectOptionsFromObject(result.weekDays));
        $('#months').selectpicker('val', getMultiselectSelectOptionsFromObject(result.months));
        $('#monthlyRepeatType').val(result.monthlyRepeatType);
        monthlyRepeatTypeChange();
        $('#monthDays').selectpicker('val', getMultiselectSelectOptionsFromObject(result.monthDays));
        $('#monthlyOnWeeks').selectpicker('val', getMultiselectSelectOptionsFromObject(result.monthlyOnWeeks));
        $('#monthlyOnWeekDays').selectpicker('val', getMultiselectSelectOptionsFromObject(result.monthlyOnWeekDays));
        $('#repeatIntervalMinutes').val(result.repeatIntervalMinutes);
        repeatIntervalMinutesChange();
        $('#repeatDurationMinutes').val(result.repeatDurationMinutes);
        $('#lastExecutionTime').val(moment(result.lastExecutionTime).format('YYYY-MM-DDTHH:mm'));
        if (moment(result.lastExecutionTime).year() == 1)
            $('#lastExecutionTime').val('');
        $('#nextExecutionTime').val(moment(result.nextExecutionTime).format('YYYY-MM-DDTHH:mm'));
        if (moment(result.nextExecutionTime).year() == 1)
            $('#nextExecutionTime').val('');
        $('#actionCode').val(result.actionCode);
        $('#modalEdit').modal('show');
    }
}
async function btnExecuteClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    var result = await service.call('fScheduledTask/execute', 'GET', { recID: recID });
    if (result) {
        utils.showToast('success', lngHelper.get('scheduledTask.executedSuccessfully'));
    }
}
function getMultiselectSelectOptionsFromObject(itemObject) {
    var options = [];
    if (itemObject) {
        for (const [key, value] of Object.entries(itemObject)) {
            if (value) {
                options.push(key);
            }
        }
    }
    return options;
}
function getObjectFromMultiSelect(selectElem) {
    var obj = {}
    var items = selectElem.selectpicker('val');
    for (var i = 0; i < items.length; i++) {
        obj[items[i]] = true;
    }
    return obj;
}
async function btnSaveClick() {
    if (!utils.formValidate($('#formEdit')[0]))
        return;
    var fScheduledTask = {
        recID: $('#modalEdit').data('recID'),
        displayName: $('#displayName').val(),
        taskStatus: parseInt($('#taskStatus').val()),
        actionType: parseInt($('#actionType').val()),
        actionID: $('#action').val(),
        periodType: parseInt($('#periodType').val()),
        startTime: $('#startTime').val(),
        recurrencePeriod: parseInt($('#recurrencePeriod').val()),
        weekDays: getObjectFromMultiSelect($('#weekDays')),
        months: getObjectFromMultiSelect($('#months')),
        monthlyRepeatType: parseInt($('#monthlyRepeatType').val()),
        monthDays: getObjectFromMultiSelect($('#monthDays')),
        monthlyOnWeeks: getObjectFromMultiSelect($('#monthlyOnWeeks')),
        monthlyOnWeekDays: getObjectFromMultiSelect($('#monthlyOnWeekDays')),
        repeatIntervalMinutes: parseInt($('#repeatIntervalMinutes').val()),
        repeatDurationMinutes: parseInt($('#repeatDurationMinutes').val()),
        actionCode: $('#actionCode').val()
    }
    var result = await service.call('fScheduledTask/save', 'POST', fScheduledTask);
    if (result) {
        window.dt.reload();
        $('#modalEdit').modal('hide');
    }
}
async function btnDeleteClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            service.call('fScheduledTask/delete', 'GET', { recID: selectedRow.data()[0] }).then(function (result) {
                window.dt.reload();
            }, function () { })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
async function actionTypeChange() {
    var actionType = parseInt($('#actionType').val());
    var result = null;
    switch (actionType) {
        case window.fEnums.FScheduledTaskActionType.BProcess:
            $('#action').parents('.form-group').show();
            $('#actionCodeContainer').hide();
            result = await service.call('fBProcess/getList', 'GET', {});
            if (result) {
                var options = [];
                for (var i = 0; i < result.length; i++) {
                    options.push({
                        value: result[i].recID,
                        text: result[i].displayName
                    })
                }
                utils.fillSelectOptions($('#action'), options);
            }
            break;
        case window.fEnums.FScheduledTaskActionType.ExecuteCode:
            $('#action').parents('.form-group').hide();
            $('#actionCodeContainer').show();
            break;
    }
}
function periodTypeChange() {
    var periodType = parseInt($('#periodType').val());
    switch (periodType) {
        case window.fEnums.FScheduledTaskPeriodType.Daily:
            $('#recurrencePeriod').parents('.form-group').show();            
            $('#weekDays').parents('.form-group').hide();
            $('#monthContainer').hide();
            break;
        case window.fEnums.FScheduledTaskPeriodType.Weekly:
            $('#recurrencePeriod').parents('.form-group').show();            
            $('#weekDays').parents('.form-group').show();
            $('#monthContainer').hide();
            break;
        case window.fEnums.FScheduledTaskPeriodType.Monthly:
            $('#recurrencePeriod').parents('.form-group').hide();            
            $('#weekDays').parents('.form-group').hide();
            $('#monthContainer').show();
            break;
    }
}
function repeatIntervalMinutesChange() {
    var repeatIntervalMinutes = parseInt($('#repeatIntervalMinutes').val());
    if (repeatIntervalMinutes == 0)
        $('#repeatDurationMinutes').parents('.form-group').hide();
    else
        $('#repeatDurationMinutes').parents('.form-group').show();
}
function monthlyRepeatTypeChange() {
    var monthlyRepeatType = parseInt($('#monthlyRepeatType').val());
    switch (monthlyRepeatType) {
        case window.fEnums.FScheduledTaskMonthlyRepeatType.MonthDays:
            $('#monthDays').parents('.form-group').show();
            $('#monthlyOnWeeks').parents('.form-group').hide();
            $('#monthlyOnWeekDays').parents('.form-group').hide();
            break;
        case window.fEnums.FScheduledTaskMonthlyRepeatType.WeekDays:
            $('#monthDays').parents('.form-group').hide();
            $('#monthlyOnWeeks').parents('.form-group').show();
            $('#monthlyOnWeekDays').parents('.form-group').show();
            break;
    }
}
function initTable() {
    window.dt = new dataTableHelper('dataTable', {
        ajax: '/fScheduledTask/getListDataTable',
        columns: [
            { title: "ID", visible: false },
            { title: lngHelper.get('scheduledTask.displayName') },
            {
                title: lngHelper.get('scheduledTask.taskStatus'), render: function (data, type, row) {
                    if (data == window.fEnums.FScheduledTaskStatus.Active.toString())
                        return lngHelper.get('scheduledTask.taskStatus_Active');
                    if (data == window.fEnums.FScheduledTaskStatus.Passive.toString())
                        return lngHelper.get('scheduledTask.taskStatus_Passive');
                    return data;
                }
            },
            {
                title: lngHelper.get('scheduledTask.actionType'), render: function (data, type, row) {
                    if (data == window.fEnums.FScheduledTaskActionType.BProcess.toString())
                        return lngHelper.get('scheduledTask.actionType_BProcess');
                    if (data == window.fEnums.FScheduledTaskActionType.ExecuteCode.toString())
                        return lngHelper.get('scheduledTask.actionType_ExecuteCode');
                    return data;
                }
            },
            { title: lngHelper.get('scheduledTask.action') },
            { title: lngHelper.get('scheduledTask.lastExecutionTime'), render: lngHelper.dataTableDateTimeColumnRender('dateTime') },
            { title: lngHelper.get('scheduledTask.nextExecutionTime'), render: lngHelper.dataTableDateTimeColumnRender('dateTime') }
        ]
    }, {
        buttons: [{
            text: lngHelper.get('generic.edit'),
            clickFunction: 'btnEditClick'
        }, {
            text: lngHelper.get('scheduledTask.execute'),
            clickFunction: 'btnExecuteClick'
        },{
            text: lngHelper.get('generic.delete'),
            clickFunction: 'btnDeleteClick'
        }],
        dblClickFunction: 'btnEditClick'
    });
}
function btnEditActionCodeInEditorClick() {
    var language = 'csharp';

    window.currentEditFormExecutablePage = utils.openPage('./codeEditor.html?language=' + language, null, {
        onLoad: function (iframe) {
            iframe = iframe[0];
            iframe.contentWindow.saveCallBack = function (code) {
                $('#actionCode').val(code);
                window.currentEditFormExecutablePage.modal('hide');
            }
            setTimeout(function () {   
                iframe.contentWindow.setCode($('#actionCode').val());
            }, 200);
        },
        noOpenInNewTab: true
    });
}
function initSelectOptions() {
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FScheduledTaskPeriodType)) {
        options.push({
            value: value,
            text: lngHelper.get('scheduledTask.periodType_' + key)
        })
    }
    utils.fillSelectOptions($('#periodType'), options, true);
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.WeekDay)) {
        options.push({
            value: key,
            text: lngHelper.get('scheduledTask.weekDay_' + key)
        })
    }
    utils.fillSelectOptions($('#weekDays'), options, true);
    $('#weekDays').selectpicker();
    utils.fillSelectOptions($('#monthlyOnWeekDays'), options, true);
    $('#monthlyOnWeekDays').selectpicker();
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.Month)) {
        options.push({
            value: key,
            text: lngHelper.get('scheduledTask.month_' + key)
        })
    }
    utils.fillSelectOptions($('#months'), options, true);
    $('#months').selectpicker();
    options = [];
    for (var i = 1; i <= 31; i++) {
        options.push({
            value: i.toString(),
            text: i.toString()
        })
    }
    options.push({
        value: 'last',
        text: lngHelper.get('scheduledTask.monthDayLast')
    })
    utils.fillSelectOptions($('#monthDays'), options, true);
    $('#monthDays').selectpicker();
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.FScheduledTaskMonthlyRepeatType)) {
        options.push({
            value: value,
            text: lngHelper.get('scheduledTask.monthlyRepeatType_' + key)
        })
    }
    utils.fillSelectOptions($('#monthlyRepeatType'), options, true);
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.FScheduledTaskMonthlyOnWeek)) {
        options.push({
            value: key,
            text: lngHelper.get('scheduledTask.monthlyOnWeek_' + key)
        })
    }
    utils.fillSelectOptions($('#monthlyOnWeeks'), options, true);
    $('#monthlyOnWeeks').selectpicker();
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.FScheduledTaskActionType)) {
        options.push({
            value: value,
            text: lngHelper.get('scheduledTask.actionType_' + key)
        })
    }
    utils.fillSelectOptions($('#actionType'), options, true);
    options = [{
        value: '0',
        text: lngHelper.get('scheduledTask.none')
    }, {
        value: '5',
        text: lngHelper.get('scheduledTask.minutes').replace(/%/,'5')
    }, {
        value: '10',
        text: lngHelper.get('scheduledTask.minutes').replace(/%/,'10')
    }, {
        value: '15',
        text: lngHelper.get('scheduledTask.minutes').replace(/%/,'15')
    }, {
        value: '30',
        text: lngHelper.get('scheduledTask.minutes').replace(/%/,'30')
    }, {
        value: '60',
        text: lngHelper.get('scheduledTask.hour').replace(/%/,'1')
    }];
    utils.fillSelectOptions($('#repeatIntervalMinutes'), options, true);
    options = [{
        value: '15',
        text: lngHelper.get('scheduledTask.minutes').replace(/%/, '15')
    }, {
        value: '30',
        text: lngHelper.get('scheduledTask.minutes').replace(/%/, '30')
    }, {
        value: '60',
        text: lngHelper.get('scheduledTask.hour').replace(/%/, '1')
    }, {
        value: '720',
        text: lngHelper.get('scheduledTask.hours').replace(/%/, '12')
    }, {
        value: '1440',
        text: lngHelper.get('scheduledTask.day').replace(/%/, '1')
    }];
    utils.fillSelectOptions($('#repeatDurationMinutes'), options, true);
    options = [];
    for (const [key, value] of Object.entries(window.fEnums.FScheduledTaskStatus)) {
        options.push({
            value: value,
            text: lngHelper.get('scheduledTask.taskStatus_' + key)
        })
    }
    utils.fillSelectOptions($('#taskStatus'), options, true);
}
$(document).on('initcompleted', function () {
    initTable();
    initSelectOptions();
})