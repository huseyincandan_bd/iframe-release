﻿function btnAddClick(e) {
    $('#modalEdit').data('recID', utils.emptyGuid);
    $('#displayName').val('');
    $('#modalEdit').modal('show');
}
function btnEditClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    $('#modalEdit').data('recID', selectedRow.data()[0]);
    $('#displayName').val(selectedRow.data()[1]);
    $('#modalEdit').modal('show');
}
function btnDeleteClick(e) {
    utils.confirmDelete(async () => {
        var selectedRow = dataTableHelper.getRowFromElem(e);
        var result = await service.call('fModule/delete', 'GET', { recID: selectedRow.data()[0] });
        if (result)
            window.dt.reload();
    })
}
async function btnSaveClick() {
    if (!utils.formValidate($('#displayName').parents('form')[0]))
        return;

    var fModule = {
        recID: $('#modalEdit').data('recID'),
        displayName: $('#displayName').val()
    };
    var result = await service.call('fModule/save', 'POST', fModule);
    if (result) {
        utils.showToast('success', lngHelper.get('generic.saved'));
        window.dt.reload();
        $('#modalEdit').modal('hide');
    }
}
async function btnPackClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var url = '/fModule/pack?recID=' + selectedRow.data()[0];
    utils.downloadFile(url);
}
async function btnUnpackClick() {
    $('#modalUnpack').modal('show');
}
async function btnUnpackOKClick() {
    var input = document.getElementById('unpackFileContent');
    var files = input.files;
    if (!files || files.length == 0) {
        utils.alertWarning(lngHelper.get('module.selectFileToUnpack'));
        return;
    }
    var fileName = input.files[0].name;
    var parts = fileName.split('.');
    var formData = new FormData();
    formData.append("file", files[0]);
    var result = await service.call('fModule/unpack', 'POST', formData, false, false, { contentType: false, processData: false, dataType: false });
    if (result.length == 0)
        utils.showToast('success', lngHelper.get('module.successfullyUnpacked'));
    else {
        var html = '<ul>';
        for (var i = 0; i < result.length; i++) {
            html += '<li>' + utils.htmlEscape(result[i]) + '</li>';
        }
        html += '</ul>';
        utils.alertError(html);
    }
}
async function btnShowContentClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var result = await service.call('fModule/getContent', 'GET', { recID: selectedRow.data()[0] });
    var html = '<ul>';
    for (const [key, value] of Object.entries(result)) {
        if (value && value.length > 0) {
            html += '<li>' + lngHelper.get('module.content_' + key) + '<ul>';
            for (var i = 0; i < value.length; i++) {
                html += '<li>' + utils.htmlEscape(value[i]) + '</li>';
            }
            html += '</ul></li>';
        }
    }
    html += '</ul>';
    utils.alertInfo(html);
}
function init() {
    window.dt = new dataTableHelper('dataTable', {
        ajax: '/fModule/getListDataTable',
        columns: [
            { title: "ID", visible: false },
            { title: lngHelper.get('module.displayName') }
        ],
        order: [[2, "asc"]]
    }, {
        buttons: [{
            text: lngHelper.get('generic.edit'),
            clickFunction: 'btnEditClick'
        }, {
            text: lngHelper.get('module.showContent'),
            clickFunction: 'btnShowContentClick'
        }, {
            text: lngHelper.get('module.pack'),
            clickFunction: 'btnPackClick'
        }, {
            text: lngHelper.get('generic.delete'),
            clickFunction: 'btnDeleteClick'
        }],
        dblClickFunction: 'btnEditClick'
    });
}
$(document).on('initcompleted', function () {
    init();
})