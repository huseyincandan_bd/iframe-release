﻿window.dropHelperElem = '<div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>';
function onDragStart(e) {
    e.dataTransfer.setData("text", e.target.id);
}
function updateTabAttributes(elem) {
    var id = elem.attr('id');
    elem.find('.nav-link').each((index, item) => {
        $(item).attr('id', id + '-' + (index + 1).toString());
        $(item).attr('href', '#' + id + '-c-' + (index + 1).toString());
        $(item).attr('aria-controls', id + '-c-' + (index + 1).toString());
    })
    elem.find('.tab-pane').each((index, item) => {
        $(item).attr('id', id + '-c-' + (index + 1).toString());
        $(item).attr('aria-labelledby', id + '-' + (index + 1).toString());
    })
}
function onDrop(e) {
    if (!e)
        e = window.event;
    var sender = e.srcElement || e.target;

    $('.drop-helper').removeClass('drag-over');
    e.stopPropagation();
    e.preventDefault();

    var data = e.dataTransfer.getData("text");
    var sourceElem = $('#' + data);
    var sourceParent = sourceElem.parent();
    var sourceRoot = sourceElem.parents('.root-panel');
    var targetElem = $(sender);

    if (sourceRoot.attr('id') == 'topLeft') {  // sol panelden geliyor
        var rootElem = sourceParent.parents('#propertyList');
        if (targetElem.hasClass('drop-helper')) { // sağda araya 
            if (sourceElem.hasClass('fform-element')) { // form alanı 
                var str = dropHelperElem + $('<div>').append(sourceElem.clone()).html() + dropHelperElem;
                targetElem.replaceWith(str);
                if (rootElem && rootElem.length > 0) {
                    sourceParent.remove();
                }
            } else {
                if (sourceElem.hasClass('fhtml-element')) { // html nesne
                    var newElem = $(sourceElem.data('fcontent'));
                    newElem.attr('id', generateHtmlElementID());
                    if (newElem.hasClass('tabs'))
                        updateTabAttributes(newElem);
                    newElem.attr('onclick', 'itemClick(event)');
                    newElem.attr('ondragstart', 'onDragStart(event)');
                    newElem.attr('draggable', true);
                    $(dropHelperElem + utils.getOuterHtml(newElem)).insertBefore(targetElem);
                    if (newElem.hasClass('allowed-once')) {
                        sourceParent.remove();
                    }
                }
            }
            $('#formContent').localize();
        }
    } else {  //sağ panelden geliyor 
        var sourceElemPrev = sourceElem.prev();
        if ((targetElem).hasClass('drop-helper')) {  //sağda araya 
            var str = dropHelperElem + $('<div>').append(sourceElem.clone()).html() + dropHelperElem;
            targetElem.replaceWith(str);
            if (sourceElemPrev.hasClass('drop-helper'))
                sourceElemPrev.remove();
            sourceElem.remove();
        } else {
            if (targetElem.parents('.root-panel') && targetElem.parents('.root-panel').attr('id') == 'topLeft') {  // sol panele
                if (sourceElem.hasClass('fform-element')) {
                    var str = "<li class='list-group-item'>" + $('<div>').append(sourceElem.clone()).html() + "</li>";
                    $('#propertyList').append(str);
                }
                if (sourceElemPrev.hasClass('drop-helper'))
                    sourceElemPrev.remove();
                sourceElem.remove();
                if (sourceElem.hasClass('allowed-once'))
                    fillHtmlElementList();
            }
        }
    }
}
function onDragOver(e) {
    $(e.target).addClass('drag-over');
    e.preventDefault();
}
function onDragLeave(e) {
    $(e.target).removeClass('drag-over');
    e.preventDefault();
}
function itemClick(e) {
    if (!e)
        e = window.event;
    var sender = $(e.srcElement || e.target);

    if (sender.hasClass('nav-link'))  //tabs
        return;

    while (true) {
        if (sender.prop('onclick'))
            break;
        sender = sender.parent();
    }
    if (sender.parents('.root-panel').attr('id') == 'topRight') {
        var state = sender.hasClass('fselected');
        $('.fselected').removeClass('fselected');
        if (state)
            sender.removeClass('fselected');
        else
            sender.addClass('fselected');
    }
    e.preventDefault();
    e.stopPropagation();
}
async function btnPropertyEditClick() {
    var selectedElem = $('.fselected');
    if (!selectedElem || selectedElem.length == 0) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('formDesign.selectProperty'));
        return;
    }
    window.currentElement = selectedElem;
    var setContentElems = window.currentElement.find('.set-content').first();
    if (window.currentElement.hasClass('set-content'))
        setContentElems = window.currentElement;
    if (setContentElems && setContentElems.length > 0) {
        $('#propertyContentContainer').show();
        $('#propertyContent').val(setContentElems.html());
    }
    else {
        if (window.currentElement.hasClass('fform-element')) {
            $('#propertyContentContainer').show();
            $('#propertyContent').val(window.currentElement.find('label').first().html());
            $('#modalEditPropertiesTitle').html(window.currentElement.attr('propertyname') ? window.currentElement.attr('propertyname') + ' | ' + lngHelper.get('formDesign.propertySettings') : lngHelper.get('formDesign.propertySettings'));
        } else
            $('#propertyContentContainer').hide();
    }
    $('#propertyStyle').val(window.currentElement.attr('style'));

    $('#selectParentParametersContainer').parents('.form-group').hide();
    $('#propertyParentSelectTable').parents('.form-group').hide();
    if (window.currentElement.hasClass('fform-element')) {
        var inputElem = window.currentElement.find('[fmodel]');
        if (inputElem) {
            $('#propertyDisabledContainer').show();
            $('#propertyRequiredContainer').show();
            $('#propertyHiddenContainer').show();
            $('#propertyDisabled').prop('checked', inputElem.prop('disabled'));
            $('#propertyRequired').prop('checked', inputElem.prop('required'));
            $('#propertyHidden').prop('checked', inputElem.attr('fhidden') == 'true');

            var fmodel = inputElem.attr('fmodel').match(/\{\{([^}]+)\}\}/)[1];
            if ((fmodel.match(/_/g) || []).length == 1) {
                $('#selectParentParametersContainer').parents('.form-group').show();
                var parentSelectParameters = selectedElem.find('[fmodel]').attr('parentSelectParameters');
                if (parentSelectParameters)
                    parentSelectParameters = JSON.parse(parentSelectParameters);
                else
                    parentSelectParameters = [];
                $('#modalEditProperties').data('parentSelectParameters', parentSelectParameters);
                fillTblParentSourceParameters();
            }
            if (fmodel.indexOf('_') > 0) {  //parenti varsa  
                for (var i = 0; i < window.currentPropertyDetails.length; i++) {
                    var propertyDetail = window.currentPropertyDetails[i];
                    if (fmodel == propertyDetail.propertyFullID) {
                        $('#propertyParentSelectTable').parents('.form-group').show();
                        var tables = await service.call('fTable/getList', 'GET', { fObjectID: propertyDetail.property.fObjectID });
                        var options = [];
                        for (var j = 0; j < tables.length; j++) {
                            options.push({
                                text: tables[j].displayName,
                                value: tables[j].recID
                            });
                        }
                        utils.fillSelectOptions($('#propertyParentSelectTable'), options, true);
                        if (inputElem.attr('table-id'))
                            $('#propertyParentSelectTable').val(inputElem.attr('table-id'));
                    }
                }
            }

            if (inputElem.is('table')) {   //IF-300
                $('#propertyDisabledContainer').hide();
                $('#propertyRequiredContainer').hide();
                $('#propertyHiddenContainer').hide();
            }
        }
    } else {
        $('#propertyDisabledContainer').hide();
        $('#propertyRequiredContainer').hide();
        $('#propertyHiddenContainer').hide();
    }

    if (window.currentElement.hasClass('tabs')) {
        var str = '';
        var index = 1;
        //var tmp = '';

        window.currentElement.find('.nav-link').each((index, item) => {
            str += '<li class="list-group-item" item-index="' + index.toString() + '"><div class="input-group"><div class="input-group-prepend"><i class="fas fa-ellipsis-v"></i></div><input type="text" class="form-control" value="' + utils.htmlEscape($(item).html()) + '"></input><div class="input-group-append"><button class="btn btn-outline-secondary" onclick="btnPropertyTabDeleteClick(event)"><i class="fas fa-times"></i></button></div></div></li>';
            index++;
            //if (tmp != '')
            //    tmp += '\n';
            //tmp += $(item).html();
        });
        //$('#propertyTabTitles').val(tmp);

        $('#propertyTabTitles').html(str);
        $('#propertyTabTitles').sortable();
        $('#propertyTabTitles').parents('.form-group').show();
    } else {
        $('#propertyTabTitles').parents('.form-group').hide();
    }

    var tableElem = window.currentElement.find('table');
    if (tableElem && tableElem.length > 0 && tableElem.attr('fmodel')) {
        var fmodelID = tableElem.attr('fmodel').match(/\{\{([^}]+)\}\}/)[1];
        var currentObjectRelation = null;
        for (var i = 0; i < window.currentObjectRelationList.length; i++) {
            if (window.currentObjectRelationList[i].relation.recID == fmodelID) {
                currentObjectRelation = window.currentObjectRelationList[i];
                break;
            }
        }
        if (currentObjectRelation != null) {  // relation
            var targetFormList = currentObjectRelation.childFormList;
            var targetTableList = currentObjectRelation.childTableList;
            if (currentObjectRelation.relation.relationType == window.fEnums.FObjectRelationType.ManyToMany && currentObjectRelation.childObject.recID == window.currentForm.fObjectID) {
                var targetFormList = currentObjectRelation.parentFormList;
                var targetTableList = currentObjectRelation.parentTableList;
            }
            var options = [];
            for (var j = 0; j < targetFormList.length; j++) {
                options.push({
                    value: targetFormList[j].recID.toString(),
                    text: targetFormList[j].displayName
                })
            }
            utils.fillSelectOptions($('#propertyForm'), options, true);
            if (tableElem.attr('form-id'))
                $('#propertyForm').val(tableElem.attr('form-id'));
            var options = [];
            for (var j = 0; j < targetTableList.length; j++) {
                options.push({
                    value: targetTableList[j].recID.toString(),
                    text: targetTableList[j].displayName
                })
            }
            utils.fillSelectOptions($('#propertyTable'), options, true);
            if (tableElem.attr('table-id'))
                $('#propertyTable').val(tableElem.attr('table-id'));
            $('#propertyTableContainer').show();
            $('#propertyForm').parents('.form-group').show();
        } else {
            $('#propertyTableContainer').hide();
            $('#propertyForm').parents('.form-group').hide();
        }

    } else {
        $('#propertyTableContainer').hide();
        $('#propertyForm').parents('.form-group').hide();
    }
    $('#propertyEventContainer').hide();
    $('#propertyEventContainer').html('');
    if (window.currentElement.attr('f-events')) {
        var events = JSON.parse(window.currentElement.attr('f-events'));
        var str = '';
        for (var i = 0; i < events.length; i++) {
            str += '<div class="form-group row" trigger="' + events[i].trigger + '">\
                            <label class="col-sm-2 col-form-label">' + lngHelper.get('formDesign.formExecutableTrigger_' + events[i].trigger) + '</label>\
                            <div class="col-sm-10">\
                                <select class="form-control"></select>\
                            </div>\
                        </div>';
        }
        $('#propertyEventContainer').html(str);
        var options = [];
        var executables = await service.call('fFormExecutable/getList', 'GET', { fFormID: window.currentForm.recID });
        for (var i = 0; i < executables.length; i++) {
            options.push({
                text: executables[i].displayName,
                value: executables[i].recID
            })
        }
        $('#propertyEventContainer').find('.form-group select').each((index, item) => {
            item = $(item);
            utils.fillSelectOptions(item, options);
        })
        $('#propertyEventContainer').find('.form-group').each((index, item) => {
            item = $(item);
            var trigger = item.attr('trigger');
            var selectElem = item.find('select');
            utils.fillSelectOptions(selectElem, options);
            for (var i = 0; i < events.length; i++) {
                if (events[i].trigger == trigger) {
                    selectElem.val(events[i].executableID);
                    break;
                }
            }
        })
        $('#propertyEventContainer').show();
    }
    var mapControl = selectedElem.find('.map-control');
    if (mapControl && mapControl.length > 0) {
        $('#propertyMapOptions').show();
        $('#propertyMapBaseLayers').html('');
        var baseLayers = mapControl.attr('base-layers');
        if (baseLayers)
            baseLayers = JSON.parse(baseLayers);
        else
            baseLayers = [];

        baseLayers.forEach((baseLayer) => {
            $('#baseMaps').data('allOptions').forEach((item) => {
                if (baseLayer == parseInt(item.value)) {
                    addLayerItem($('#propertyMapBaseLayers'), item.value, item.text);
                }
            })
        })
        $('#propertyMapDataLayers').html('');
        var dataLayers = mapControl.attr('data-layers');
        if (dataLayers)
            dataLayers = JSON.parse(dataLayers);
        else
            dataLayers = [];
        dataLayers.forEach((dataLayer) => {
            $('#dataMaps').data('allOptions').forEach((item) => {
                if (dataLayer == item.value) {
                    addLayerItem($('#propertyMapDataLayers'), item.value, item.text);
                }
            })
        })
        updateMapSelect($('#baseMaps'));
        updateMapSelect($('#dataMaps'));
    } else
        $('#propertyMapOptions').hide();
    $('#modalEditProperties').modal('show');
}
function btnPropertyTabDeleteClick(e) {
    var sender = utils.getEventSender(e);
    sender.parents('.list-group-item').remove();
}
function btnAddPropertyTabClick() {
    $('#propertyTabTitles').append($('<li class="list-group-item" item-index="-1"><div class="input-group"><div class="input-group-prepend"><i class="fas fa-ellipsis-v"></i></div><input type="text" class="form-control"></input><div class="input-group-append"><button class="btn btn-outline-secondary" onclick="btnPropertyTabDeleteClick(event)"><i class="fas fa-times"></i></button></div></div></li>'));    
}
function btnPropertySaveClick() {
    var setContentElems = window.currentElement.find('.set-content').first();
    if (setContentElems.length == 0) {
        if (window.currentElement.hasClass('fform-element')) {
            window.currentElement.find('label').first().html($('#propertyContent').val());
        } else {
            if (window.currentElement.hasClass('fhtml-element') && $('#propertyContent').is(':visible')) {
                window.currentElement.html($('#propertyContent').val());
            }
        }
    } else {
        if (window.currentElement.hasClass('set-content'))
            setContentElems = window.currentElement;
        if (setContentElems && setContentElems.length > 0) {
            setContentElems.html($('#propertyContent').val());
        }
    }
    window.currentElement.attr('style', $('#propertyStyle').val());
    if (window.currentElement.hasClass('fform-element')) {
        var inputElem = window.currentElement.find('[fmodel]');
        if (inputElem) {
            if ($('#propertyRequired').is(':checked'))
                inputElem.attr('required', 'required');
            else
                inputElem.removeAttr('required');
            if ($('#propertyDisabled').is(':checked'))
                inputElem.attr('disabled', 'disabled');
            else
                inputElem.removeAttr('disabled');
            if ($('#propertyHidden').is(':checked'))
                inputElem.attr('fhidden', 'true');
            else
                inputElem.removeAttr('fhidden');
        }
    }
    if ($('#propertyTableContainer').is(':visible')) {
        window.currentElement.find('table').attr('table-id', $('#propertyTable').val());
    }
    if ($('#propertyForm').parents('.form-group').is(':visible')) {
        window.currentElement.find('table').attr('form-id', $('#propertyForm').val());
    }
    if ($('#propertyTabTitles').parent().is(':visible')) {
        var contentPanes = window.currentElement.find('.tab-content .tab-pane');
        var newContent = '';
        var newTabs = '';
        $.each($('#propertyTabTitles .list-group-item'), function (index, item) {
            var index = parseInt($(item).attr('item-index'));
            var title = $(item).find('input').val();

            newTabs += '\
                        <li class="nav-item">\
                            <a class="nav-link" id="tab-{X}-2" data-toggle="tab" href="#tabcontent-{X}-2" role="tab" aria-controls="tabcontent-{X}-2" aria-selected="false">' + title + '</a>\
                        </li>';
            if (index == -1) {
                newContent += '\
                        <div class="tab-pane fade" id="tabcontent-{X}-2" role="tabpanel" aria-labelledby="tab-{X}-2">\
                            <div class="fhtml-container">\
                                <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                            </div>\
                        </div>';
            } else
                newContent += contentPanes[index].outerHTML;
        });
        window.currentElement.find('.nav-tabs').html(newTabs);
        window.currentElement.find('.tab-content').html(newContent);

        //var tabs = $('#propertyTabTitles').val().split('\n');
        //if (tabs.length == 0 || (tabs.length == 1 && tabs[0].trim() == ''))
        //    tabs = ['-'];
        //for (var i = window.currentElement.find('.nav-link').length; i > tabs.length; i--) {
        //    $(window.currentElement.find('.nav-link')[i - 1]).remove();
        //    $(window.currentElement.find('.tab-pane')[i - 1]).remove();
        //}
        //for (var i = window.currentElement.find('.nav-link').length; i < tabs.length; i++) {
        //    window.currentElement.find('.nav-tabs').append('\
        //                <li class="nav-item">\
        //                    <a class="nav-link" id="tab-{X}-2" data-toggle="tab" href="#tabcontent-{X}-2" role="tab" aria-controls="tabcontent-{X}-2" aria-selected="false">Tab</a>\
        //                </li>');
        //    window.currentElement.find('.tab-content').append('\
        //                <div class="tab-pane fade" id="tabcontent-{X}-2" role="tabpanel" aria-labelledby="tab-{X}-2">\
        //                    <div class="fhtml-container">\
        //                        <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
        //                    </div>\
        //                </div>');
        //}
        //for (var i = 0; i < tabs.length; i++) {
        //    $(window.currentElement.find('.nav-link')[i]).html(tabs[i]);
        //}

        updateTabAttributes(window.currentElement);
        window.currentElement.find('.nav-tabs .nav-item:first-child a').click();
    }
    if ($('#propertyParentSelectTable').parents('.form-group').is(':visible')) {
        var inputElem = window.currentElement.find('[fmodel]');
        inputElem.attr('table-id', $('#propertyParentSelectTable').val());
    }
    if (window.currentElement.attr('f-events')) {
        var events = JSON.parse(window.currentElement.attr('f-events'));
        $('#propertyEventContainer .form-group').each((index, item) => {
            item = $(item);
            var selectedOption = item.find('select').val();
            var trigger = item.attr('trigger');
            for (var i = 0; i < events.length; i++) {
                if (events[i].trigger == trigger) {
                    events[i].executableID = selectedOption;
                    break;
                }
            }
        })
        window.currentElement.attr('f-events', JSON.stringify(events));
    }
    if ($('#propertyMapOptions').is(':visible')) {
        var baseLayers = [];
        $('#propertyMapBaseLayers .list-item').each((index, item) => {
            baseLayers.push(parseInt($(item).attr('data-id')));
        })
        window.currentElement.find('[fmodel]').attr('base-layers', JSON.stringify(baseLayers));
        var dataLayers = [];
        $('#propertyMapDataLayers .list-item').each((index, item) => {
            dataLayers.push($(item).attr('data-id'));
        })
        window.currentElement.find('[fmodel]').attr('data-layers', JSON.stringify(dataLayers));
    }
    if ($('#selectParentParametersContainer').is(':visible')) {
        window.currentElement.find('[fmodel]').attr('parentSelectParameters', JSON.stringify($('#modalEditProperties').data('parentSelectParameters')));
    }
    $('#modalEditProperties').modal('hide');
}

async function btnSaveClick() {
    var cloneElem = $('#formContent').clone();
    cloneElem.find('.drop-helper').remove();
    cloneElem.find('.fform-element').removeAttr('onclick');
    cloneElem.find('.fform-element').removeAttr('ondragstart');
    cloneElem.find('.fform-element').removeAttr('draggable');
    cloneElem.find('.fform-element').removeClass('fselected');
    cloneElem.find('.fhtml-element').removeAttr('onclick');
    cloneElem.find('.fhtml-element').removeAttr('ondragstart');
    cloneElem.find('.fhtml-element').removeAttr('draggable');
    cloneElem.find('.fhtml-element').removeClass('fselected');

    if (window.currentJavascriptContent) {
        $('<script>')
            .attr('type', 'text/javascript')
            .text(window.currentJavascriptContent)
            .appendTo(cloneElem);
    }
    if (window.currentCSSContent) {
        $('<style>')
            .attr('type', 'text/css')
            .text(window.currentCSSContent)
            .prependTo(cloneElem);
    }

    var content = cloneElem.html();
    var fForm = window.currentForm;
    fForm.FormContent = content;

    var result = await service.call('fForm/save', 'POST', fForm);
    if (result) {
        window.currentForm = result;
        utils.showToast('success', lngHelper.get('generic.saved'));
    }
}
function btnClearClick() {
    $('#formContent').html(window.dropHelperElem);
    fillPropertyList();
    fillRelationList();
    fillHtmlElementList();
}
function addPropertyToPanel(property, recIDStr) {
    var displayName = property.displayName;

    var str = "<li class='list-group-item'>";
    //Pre 
    switch (property.editorType) {
        case window.fEnums.FObjectPropertyEditorType.InputText:
        case window.fEnums.FObjectPropertyEditorType.TextArea:
        case window.fEnums.FObjectPropertyEditorType.InputNumber:
        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
        case window.fEnums.FObjectPropertyEditorType.Date:
        case window.fEnums.FObjectPropertyEditorType.Select:
        case window.fEnums.FObjectPropertyEditorType.DateTime:
        case window.fEnums.FObjectPropertyEditorType.Time:
            str += "<div id='container-" + recIDStr + "' class='fform-element form-group row' draggable='true' ondragstart='onDragStart(event)' onclick='itemClick(event)' propertyname='" + displayName + "'>\
                                    <label for='" + recIDStr + "' class='col-4 col-form-label'>" + displayName + "</label>\
                                    <div class='col-8'>"
            break;
    }
    switch (property.editorType) {
        case window.fEnums.FObjectPropertyEditorType.InputText:
            str += "<input type='text' class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}' maxlength='" + (property.dataLength ? property.dataLength.toString() : '255') + "'>";
            break;
        case window.fEnums.FObjectPropertyEditorType.TextArea:
            str += "<textarea class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}' maxlength='" + (property.dataLength ? property.dataLength.toString() : '255') + "'></textarea>";
            break;
        case window.fEnums.FObjectPropertyEditorType.InputNumber:
            str += "<input type='number' class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}' onkeypress='return event.charCode >= 48 && event.charCode <= 57' min='-2147483647' max='2147483647'>";
            break;
        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
            var step = 1;
            if (property.decimalScale)
                step = (1 / Math.pow(10, property.decimalScale)).toString();
            str += "<input type='number' class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}' step='" + step + "' min='-100000000' max='100000000'>";
            break;
        case window.fEnums.FObjectPropertyEditorType.Date:
            str += "<input type='date' class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}'>";
            break;
        case window.fEnums.FObjectPropertyEditorType.Select:
            str += "<select class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}'></select>";
            break;
        case window.fEnums.FObjectPropertyEditorType.Checkbox:
            str += "<div id='container-" + recIDStr + "' class='fform-element form-group row' draggable='true' ondragstart='onDragStart(event)' onclick='itemClick(event)' propertyname='" + displayName + "'>\
                        <label class='col-form-label col-4' for='" + recIDStr + "'>" + displayName + "</label>\
                        <span class='col-8 switch switch-icon'>\
                            <label>\
                                <input class='form-check-input' type='checkbox' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}'>\
                                <span></span>\
                            </label>\
                        </span>\
                    </div>";
            break;
        case window.fEnums.FObjectPropertyEditorType.RadioButton:
            str += "<div id='container-" + recIDStr + "' class='fform-element form-group row' draggable='true' ondragstart='onDragStart(event)' onclick='itemClick(event)' propertyname='" + displayName + "'>\
                        <label class='col-4 col-form-label'>" + displayName + "</label>\
                        <div class='form-check col-8'>\
                            <div>\
                                <input class='form-check-input' type='radio' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}'>\
                                <label class='form-check-label' for='" + recIDStr + "'>" + displayName + "</label>\
                            </div>\
                        </div>\
                    </div>";
            break;
        case window.fEnums.FObjectPropertyEditorType.DateTime:
            str += "<input type='datetime-local' class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}'>";
            break;
        case window.fEnums.FObjectPropertyEditorType.Time:
            str += "<input type='time' class='form-control' id='" + recIDStr + "' name='" + recIDStr + "' fmodel='{{" + recIDStr + "}}'>";
            break;
        case window.fEnums.FObjectPropertyEditorType.GeoWKT:
            var dataLayer = '';
            for (var i = 0; i < window.mapDataLayers.length; i++) {
                if (window.mapDataLayers[i].fObject.recID == property.fObjectID && window.mapDataLayers[i].fObjectProperty.recID) {
                    dataLayer = window.mapDataLayers[i].fObject.displayName + '-' + window.mapDataLayers[i].fObjectProperty.displayName;
                    break;
                }
            }
            if (dataLayer)
                dataLayer = " data-layers='[\"" + dataLayer + "\"]'";
            str += "<div id='container-" + recIDStr + "' class='fform-element map-control-container form-group row' draggable='true' ondragstart='onDragStart(event)' onclick='itemClick(event)' propertyname='" + displayName + "'>\
                        <label class='col-4 col-form-label'>" + displayName + "</label>\
                        <div id='map-" + recIDStr + "' class='col-12 map-control' fmodel='{{" + recIDStr + "}}' base-layers='[0]'" + dataLayer + "></div>\
                    </div>";
            break;
    }
    //Post     
    switch (property.editorType) {
        case window.fEnums.FObjectPropertyEditorType.InputText:
        case window.fEnums.FObjectPropertyEditorType.TextArea:
        case window.fEnums.FObjectPropertyEditorType.InputNumber:
        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
        case window.fEnums.FObjectPropertyEditorType.Date:
        case window.fEnums.FObjectPropertyEditorType.Select:
        case window.fEnums.FObjectPropertyEditorType.DateTime:
        case window.fEnums.FObjectPropertyEditorType.Time:
            str += "</div></div>";
            break;
    }
    str += "</li>";
    return str;
}
function fillPropertyList() {
    var str = '';

    var options = [];
    //options.push({
    //    text: window.currentObject.displayName,
    //    value: ''
    //})

    var relations = [];

    for (var i = 0; i < window.currentPropertyDetails.length; i++) {
        var propertyDetail = window.currentPropertyDetails[i];
        var recIDStr = propertyDetail.propertyFullID;
        var checkElem = $('#formContent').find('[id=container-' + recIDStr + ']');
        if (!checkElem || checkElem.length == 0)
            str += addPropertyToPanel(propertyDetail.property, recIDStr);

        var relationID = utils.emptyGuid;
        var index = propertyDetail.propertyFullID.lastIndexOf('_');
        if (index > 0) {
            relationID = propertyDetail.propertyFullID.substring(0, index);
        }
        if (relations.indexOf(relationID) < 0) {
            relations.push(relationID);
            options.push({
                text: propertyDetail.relationFullName,
                value: relationID
            })
        }

        //TODO: Burası düzelecek
        //if (propertyDetail.childObject != null && parentArray.indexOf(propertyDetail.relationStr) < 0) {
        //    options.push({
        //        text: propertyDetail.parentObject.displayName + ' (-< ' + propertyDetail.childObject.displayName  + ')',
        //        value: propertyDetail.relationStr
        //    })
        //    parentArray.push(propertyDetail.relationStr);
        //}
    }
    utils.fillSelectOptions($('#parentObject'), options, true);

    $('#propertyList').html(str);
    parentObjectChange();
}
function fillRelationList() {
    if (!window.currentObjectRelationList)
        return;
    for (var i = 0; i < window.currentObjectRelationList.length; i++) {
        var or = window.currentObjectRelationList[i].relation;
        var checkElem = $('#formContent').find('[fmodel=\\{\\{' + or.recID.toString() + '\\}\\}]');
        if (checkElem && checkElem.length > 0)
            continue;
        switch (or.relationType) {
            case 0: // 1-n
                if (or.parentFObjectID == window.currentForm.fObjectID) {
                    var tableID = '';
                    if (window.currentObjectRelationList[i].childTableList && window.currentObjectRelationList[i].childTableList.length > 0)
                        tableID = window.currentObjectRelationList[i].childTableList[0].recID;
                    var str = "\
                                <li class='list-group-item'>\
                                    <div id='r-container" + or.recID.toString() + "'class='fform-element form-group' draggable='true' ondragstart='onDragStart(event)' onclick='itemClick(event)'>\
                                        <label for='rcontent-" + or.recID.toString() + "'>" + window.currentObjectRelationList[i].childObject.displayName + "</label>\
                                        <table id='" + or.recID.toString() + "' name='" + or.recID.toString() + "' fmodel='{{" + or.recID.toString() + "}}' table-id='" + tableID + "' class='relationTable'><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>\
                                    </div>\
                                </li>";
                    $('#propertyList').append(str);
                }
                break;
            case 1: // 1-1
                break;
            case 2: // n-n
                var displayName = window.currentObjectRelationList[i].childObject.displayName;
                var tableID = '';
                if (or.parentFObjectID == window.currentForm.fObjectID) {
                    if (window.currentObjectRelationList[i].childTableList && window.currentObjectRelationList[i].childTableList.length > 0)
                        tableID = window.currentObjectRelationList[i].childTableList[0].recID;
                } else {
                    displayName = window.currentObjectRelationList[i].parentObject.displayName;
                    if (window.currentObjectRelationList[i].parentTableList && window.currentObjectRelationList[i].parentTableList.length > 0)
                        tableID = window.currentObjectRelationList[i].parentTableList[0].recID;
                }
                var str = "\
                                <li class='list-group-item'>\
                                    <div id='r-container" + or.recID.toString() + "'class='fform-element form-group' draggable='true' ondragstart='onDragStart(event)' onclick='itemClick(event)'>\
                                        <label for='rcontent-" + or.recID.toString() + "'>" + displayName + "</label>\
                                        <table id='" + or.recID.toString() + "' name='" + or.recID.toString() + "' fmodel='{{" + or.recID.toString() + "}}' table-id='" + tableID + "' class='relationTable'><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>\
                                    </div>\
                                </li>";
                $('#propertyList').append(str);
                break;
        }
    }
}
function parentObjectChange() {
    var parentRelationID = $('#parentObject').val();
    if (parentRelationID == utils.emptyGuid)
        parentRelationID = '';
    if (parentRelationID)
        parentRelationID = 'container-' + parentRelationID;
    $('#propertyList .fform-element').each((index, item) => {
        var item = $(item);
        if (item.attr('id').indexOf('r-') == 0) {
            item.parents('li.list-group-item').show();
        } else {
            var relID = getRelationIDFromElemID(item.attr('id'));
            if (relID == parentRelationID)
                item.parents('li.list-group-item').show();
            else
                item.parents('li.list-group-item').hide();
        }
    })
}
function fillHtmlElementList() {
    $('#htmlElements').html('');
    var items = [];
    if ($('#formContent .attachment-container').length == 0) {
        items.push({
            title: lngHelper.get('formDesign.attachments'),
            content: '<div class="container attachment-container allowed-once">\
                        <div class="set-content">' + lngHelper.get('formDesign.attachments') + '</div>\
                        <table id="attachmentDataTable" class="table table-bordered dataTable" style="width:100%"><tr><th data-i18n="formDataEdit.attachmentName"></th><th data-i18n="formDataEdit.attachmentCreateTime"></th><th data-i18n="formDataEdit.attachmentCreateUser"></th></tr></table>\
                        </div>'
        })
    };
    items.push({
        title: lngHelper.get('formDesign.paragraph'),
        content: '<p class="set-content">' + lngHelper.get('formDesign.paragraph') + '</p>'
    });
    items.push({
        title: lngHelper.get('formDesign.header1'),
        content: '<h1 class="set-content">' + lngHelper.get('formDesign.header1') + '</h1>'
    });
    items.push({
        title: lngHelper.get('formDesign.header2'),
        content: '<h2 class="set-content">' + lngHelper.get('formDesign.header2') + '</h2>'
    });
    items.push({
        title: lngHelper.get('formDesign.header3'),
        content: '<h3 class="set-content">' + lngHelper.get('formDesign.header3') + '</h3>'
    });
    items.push({
        title: lngHelper.get('formDesign.twoColumns'),
        content: '\
                    <div class="container">\
                      <div class="row">\
                        <div class="fhtml-container col-lg-6 col-md-6 col-sm-6 col-xs-12">\
                          <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                        </div>\
                        <div class="fhtml-container col-lg-6 col-md-6 col-sm-6 col-xs-12">\
                          <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                        </div>\
                      </div>\
                    </div>'
    });
    items.push({
        title: lngHelper.get('formDesign.threeColumns'),
        content: '\
                    <div class="container">\
                      <div class="row">\
                        <div class="fhtml-container col-lg-4 col-md-6 col-sm-6 col-xs-12">\
                          <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                        </div>\
                        <div class="fhtml-container col-lg-4 col-md-6 col-sm-6 col-xs-12">\
                          <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                        </div>\
                        <div class="fhtml-container col-lg-4 col-md-6 col-sm-6 col-xs-12">\
                          <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                        </div>\
                      </div>\
                    </div>'
    });
    items.push({
        title: lngHelper.get('formDesign.card'),
        content: '\
                    <div class="card">\
                      <div class="card-header set-content"">' + lngHelper.get('formDesign.card') + '</div>\
                      <div class="card-body fhtml-container">\
                          <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                      </div>\
                    </div>'
    });
    items.push({
        title: lngHelper.get('formDesign.tabs'),
        content: '\
                    <div class="tabs">\
                        <ul class="nav nav-tabs" role="tablist">\
                          <li class="nav-item">\
                            <a class="nav-link active" id="tab-{X}-1" data-toggle="tab" href="#tabcontent-{X}-1" role="tab" aria-controls="tabcontent-{X}-1" aria-selected="true">Tab1</a>\
                          </li>\
                          <li class="nav-item">\
                            <a class="nav-link" id="tab-{X}-2" data-toggle="tab" href="#tabcontent-{X}-2" role="tab" aria-controls="tabcontent-{X}-2" aria-selected="false">Tab2</a>\
                          </li>\
                          <li class="nav-item">\
                            <a class="nav-link" id="tab-{X}-3" data-toggle="tab" href="#tabcontent-{X}-3" role="tab" aria-controls="tabcontent-{X}-3" aria-selected="false">Tab3</a>\
                          </li>\
                        </ul>\
                        <div class="tab-content">\
                            <div class="tab-pane fade show active" id="tabcontent-{X}-1" role="tabpanel" aria-labelledby="tab-{X}-1">\
                                <div class="fhtml-container">\
                                  <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                                </div>\
                            </div>\
                            <div class="tab-pane fade" id="tabcontent-{X}-2" role="tabpanel" aria-labelledby="tab-{X}-2">\
                                <div class="fhtml-container">\
                                  <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                                </div>\
                            </div>\
                            <div class="tab-pane fade" id="tabcontent-{X}-3" role="tabpanel" aria-labelledby="tab-{X}-3">\
                                <div class="fhtml-container">\
                                  <div class="drop-helper" ondragover="onDragOver(event)" ondragleave="onDragLeave(event)" ondrop="onDrop(event)"></div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>'
    });
    items.push({
        title: lngHelper.get('formDesign.seperator'),
        content: '<div class="fhtml-seperator-container"><hr/></div>'
    });
    items.push({
        title: lngHelper.get('formDesign.button'),
        content: '<div class="buttonContainer" f-events=\'[{"trigger":"click","executableID":""}]\'><button type="button" class="btn btn-outline-primary btn-sm mr-2 set-content">' + lngHelper.get('formDesign.button') + '</button></div>'
    })

    for (var i = 0; i < items.length; i++) {
        var item = $('<li class="list-group-item"></li>');
        var itemContent = $('<div>' + items[i].title + '</div>');
        itemContent.attr('id', 'chtml-' + i.toString());
        itemContent.addClass('fhtml-element');
        itemContent.attr('draggable', true);
        itemContent.attr('ondragStart', 'onDragStart(event)');
        itemContent.attr('onclick', 'itemClick(event)');
        var contentElem = $(items[i].content);
        contentElem.addClass('fhtml-element');
        itemContent.data('fcontent', utils.getOuterHtml(contentElem));
        item.append(itemContent);
        $('#htmlElements').append(item);
    }
}
function generateHtmlElementID() {
    var i = 1;
    while (true) {
        var id = 'elem-' + i.toString();

        if (!document.getElementById(id))
            return id;
        i++;
    }
}
function selectSourceObjectChange() {
    var fObjectID = $('#selectSourceObject').val();
    var options = [];
    var allOptions = $('#selectSourceTable').data('allOptions');
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].attributes && allOptions[i].attributes.fobjectid == fObjectID)
            options.push(allOptions[i]);
    }
    utils.fillSelectOptions($('#selectSourceTable'), options);
    options = [];
    var allOptions = $('#selectSourceProperty').data('allOptions');
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].attributes && allOptions[i].attributes.fobjectid == fObjectID)
            options.push(allOptions[i]);
    }
    utils.fillSelectOptions($('#selectSourceProperty'), options);
}
async function fillSelectSourceForm() {
    var elem = window.currentElement.find('[fmodel]');
    if ($('#selectSourceObject option').length == 0) {
        var tables = await service.call('fTable/getList', 'GET', {});
        var options = [];
        for (var i = 0; i < tables.length; i++) {
            options.push({
                value: tables[i].recID,
                text: tables[i].displayName,
                attributes: {
                    fobjectid: tables[i].fObjectID
                }
            })
        }
        utils.fillSelectOptions($('#selectSourceTable'), options);
        $('#selectSourceTable').data('allOptions', options);
        var properties = await service.call('fObjectProperty/getList', 'GET', {});
        options = [];
        for (var i = 0; i < properties.length; i++) {
            options.push({
                value: properties[i].recID,
                text: properties[i].displayName,
                attributes: {
                    fobjectid: properties[i].fObjectID
                }
            })
        }
        $('#selectSourceProperty').data('allOptions', options);
        $('#selectSourceParameterTargetObjectProperty').data('allOptions', options);
        utils.fillSelectOptions($('#selectSourceProperty'), options);
        utils.fillSelectOptions($('#selectSourceParameterTargetObjectProperty'), options);

        options = [];
        var objects = await service.call('fObject/getList', 'GET', {});
        for (var i = 0; i < objects.length; i++) {
            options.push({
                value: objects[i].recID,
                text: objects[i].displayName
            });
        }
        utils.fillSelectOptions($('#selectSourceObject'), options);
    }
    if ($('#selectSourceParameterSourceObjectProperty option').length == 0) {
        var options = [];
        for (var i = 0; i < window.currentObjectPropertyList.length; i++) {
            options.push({
                text: window.currentObjectPropertyList[i].displayName,
                value: window.currentObjectPropertyList[i].recID,
                attributes: {
                    fobjectid: window.currentObjectPropertyList[i].fObjectID
                }
            })
        }
        $('#selectSourceParameterSourceObjectProperty').data('allOptions', options);
    }
    var sourceObjectID = elem.attr('sourceobjectid');
    var sourceTableID = elem.attr('sourcetableid');
    var sourcePropertyID = elem.attr('sourcepropertyid');
    if (sourceObjectID && sourceObjectID != '0' && sourceTableID && sourceTableID != '0' && sourcePropertyID && sourcePropertyID != '0') {
        $('#selectSourceObject').val(sourceObjectID);
        selectSourceObjectChange();
        $('#selectSourceTable').val(sourceTableID);
        $('#selectSourceProperty').val(sourcePropertyID);
    } else {
        $('#selectSourceObject').val(0);
        selectSourceObjectChange();
        $('#selectSourceTable').val(0);
        $('#selectSourceProperty').val(0);
    }
    var parameters = [];
    if (elem.attr('sourceselectparameters'))
        parameters = JSON.parse(elem.attr('sourceselectparameters'));
    $('#modalEditSelectSource').data('parameters', parameters);
}
async function btnSelectSourceClick() {
    var selectedElem = $('.fselected');
    if (!selectedElem || selectedElem.length == 0 || !selectedElem.hasClass('fform-element')) {
        utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('formDesign.selectProperty'));
        return;
    }
    window.currentElement = selectedElem;
    await fillSelectSourceForm();
    fillTblSelectSourceParameters();
    $('#modalEditSelectSource').modal('show');
}
function fillTblSelectSourceParameters() {
    if (!window.tblSelectSourceParametersDT) {
        window.tblSelectSourceParametersDT = new dataTableHelper('tblSelectSourceParameters', {
            data: [],
            dom: '<"top">rt<"bottom"<p>><"clear">',
            columns: [
                { title: 'selectSourceParameterSourceObjectPropertyRecID', visible: false },
                { title: lngHelper.get('formDesign.selectSourceParameterSourceObjectProperty') },
                { title: 'selectSourceParameterTargetObjectPropertyRecID', visible: false },
                { title: lngHelper.get('formDesign.selectSourceParameterTargetObjectProperty') }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditSelectSourceParameterClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteSelectSourceParameterClick'
            }],
            dblClickFunction: 'btnEditSelectSourceParameterClick'
        });
    }
    var parameters = $('#modalEditSelectSource').data('parameters');
    var dataSet = [];
    for (var i = 0; i < parameters.length; i++) {
        var p = parameters[i];
        dataSet.push([p.sourceObjectPropertyRecID, p.sourceObjectPropertyDisplayName, p.targetObjectPropertyRecID, p.targetObjectPropertyDisplayName]);
    }
    window.tblSelectSourceParametersDT.updateData(dataSet);
}
function btnSelectSourceSaveClick() {
    var elem = window.currentElement.find('[fmodel]');
    var sourceObjectID = $('#selectSourceObject').val();
    var sourceTableID = $('#selectSourceTable').val();
    var sourcePropertyID = $('#selectSourceProperty').val();
    if (sourceObjectID == 0) {
        elem.removeAttr('sourceobjectid');
        elem.removeAttr('sourcetableid');
        elem.removeAttr('sourcepropertyid');
    } else {
        if (sourceTableID == 0 || sourcePropertyID == 0) {
            utils.openPopup('warning', lngHelper.get('generic.warning'), lngHelper.get('formDesign.selectSourceTableAndProperty'));
            return;
        } else {
            elem.attr('sourceobjectid', sourceObjectID);
            elem.attr('sourcetableid', sourceTableID);
            elem.attr('sourcepropertyid', sourcePropertyID);
        }
    }
    elem.attr('sourceselectparameters', JSON.stringify($('#modalEditSelectSource').data('parameters')));
    $('#modalEditSelectSource').modal('hide');
}
function initSelectSourceParameterModal() {
    var currentModel = window.currentElement.find('[fmodel]').attr('fmodel').match(/\{\{([^}]+)\}\}/)[1];
    var targetObjetID = $('#selectSourceObject').val();

    var options = [];
    var allOptions = $('#selectSourceParameterSourceObjectProperty').data('allOptions');
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].value != currentModel)
            options.push(allOptions[i]);
    }
    utils.fillSelectOptions($('#selectSourceParameterSourceObjectProperty'), options);

    options = [];
    allOptions = $('#selectSourceParameterTargetObjectProperty').data('allOptions');
    for (var i = 0; i < allOptions.length; i++) {
        if (allOptions[i].attributes && allOptions[i].attributes.fobjectid == targetObjetID)
            options.push(allOptions[i]);
    }
    utils.fillSelectOptions($('#selectSourceParameterTargetObjectProperty'), options);
}
function btnAddSelectSourceParameterClick() {
    initSelectSourceParameterModal();
    $('#modalEditSelectSourceParameter').data('parameterIndex',-1);
    $('#modalEditSelectSourceParameter').modal('show');
}
function btnEditSelectSourceParameterClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    initSelectSourceParameterModal();
    $('#modalEditSelectSourceParameter').data('parameterIndex', row.index());
    $('#selectSourceParameterSourceObjectProperty').val(row.data()[0]);
    $('#selectSourceParameterTargetObjectProperty').val(row.data()[2]);
    $('#modalEditSelectSourceParameter').modal('show');
}
function btnDeleteSelectSourceParameterClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var parameters = $('#modalEditSelectSource').data('parameters');
    parameters.splice(row.index(), 1);
    fillTblSelectSourceParameters();
}
function btnSelectSourceParameterSaveClick() {
    if ($('#selectSourceParameterSourceObjectProperty').val() == '0') {
        utils.alertWarning(lngHelper.get('formDesign.selectSourceParameterSourceObjectPropertyIsRequired'));
        return;
    }
    if ($('#selectSourceParameterTargetObjectProperty').val() == '0') {
        utils.alertWarning(lngHelper.get('formDesign.selectSourceParameterTargetObjectPropertyIsRequired'));
        return;
    }
    var parameterIndex = $('#modalEditSelectSourceParameter').data('parameterIndex');
    var parameter = {
        sourceObjectPropertyRecID: $('#selectSourceParameterSourceObjectProperty').val(),
        targetObjectPropertyRecID: $('#selectSourceParameterTargetObjectProperty').val(),
        sourceObjectPropertyDisplayName: $('#selectSourceParameterSourceObjectProperty option:selected').text(),
        targetObjectPropertyDisplayName: $('#selectSourceParameterTargetObjectProperty option:selected').text()
    }
    var parameters = $('#modalEditSelectSource').data('parameters');
    if (parameterIndex == -1)
        parameters.push(parameter);
    else
        parameters[parameterIndex] = parameter;
    $('#modalEditSelectSource').data('parameters', parameters);
    $('#modalEditSelectSourceParameter').modal('hide');
    fillTblSelectSourceParameters();
}
async function fillParentSourceParameterTargetObjectProperty() {
    var currentModel = window.currentElement.find('[fmodel]').attr('fmodel').match(/\{\{([^}]+)\}\}/)[1];
    var relationID = currentModel.substring(0, currentModel.lastIndexOf('_'));
    var result = await service.call('fObjectRelation/get', 'GET', { recID: relationID });
    result = await service.call('fObjectProperty/getList', 'GET', { fObjectID: result.parentFObjectID });
    var options = [];
    for (var i = 0; i < result.length; i++) {
        options.push({
            text: result[i].displayName,
            value: result[i].recID
        })
    }
    utils.fillSelectOptions($('#parentSourceParameterTargetObjectProperty'), options, true);
}
function fillTblParentSourceParameters() {
    if (!window.tblParentSourceParametersDT) {
        window.tblParentSourceParametersDT = new dataTableHelper('tblParentSourceParameters', {
            data: [],
            dom: '<"top">rt<"bottom"<p>><"clear">',
            columns: [
                { title: 'parentSourceParameterTargetObjectPropertyRecID', visible: false },
                { title: lngHelper.get('formDesign.parentSourceParameterTargetObjectProperty') },
                { title: 'parentSourceParameterValueType', visible: false },
                { title: lngHelper.get('formDesign.parentSourceParameterValueType') },
                { title: lngHelper.get('formDesign.parentSourceParameterValue') },
                { title: 'parentSourceParameterStaticValue', visible: false },
                { title: 'parentSourceParameterSourceObjectPropertyRecID', visible: false }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditParentSourceParameterClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteParentSourceParameterClick'
            }],
            dblClickFunction: 'btnEditParentSourceParameterClick'
        });
    }
    var parameters = $('#modalEditProperties').data('parentSelectParameters');
    var dataSet = [];
    for (var i = 0; i < parameters.length; i++) {
        var p = parameters[i];
        dataSet.push([
            p.targetObjectPropertyRecID,
            p.targetObjectPropertyDisplayName,
            p.parentSourceParameterValueType,
            (p.parentSourceParameterValueType == window.fEnums.FFormParentSourceParameterValueType.Static) ? lngHelper.get('formDesign.parentSourceParameterValueType_Static') : lngHelper.get('formDesign.parentSourceParameterValueType_PropertyValue'),
            (p.parentSourceParameterValueType == window.fEnums.FFormParentSourceParameterValueType.Static) ? p.parentSourceParameterStaticValue : p.targetObjectPropertyDisplayName,
            p.parentSourceParameterStaticValue,
            p.sourceObjectPropertyRecID
        ]);
    }
    window.tblParentSourceParametersDT.updateData(dataSet);
}
async function btnAddParentSourceParameterClick() {
    await fillParentSourceParameterTargetObjectProperty();
    $('#parentSourceParameterValueType').val(window.fEnums.FFormParentSourceParameterValueType.Static);
    parentSourceParameterValueTypeChange();
    $('#parentSourceParameterTStaticValue').val('');
    $('#modalEditParentSourceParameter').data('parameterIndex', -1);
    $('#modalEditParentSourceParameter').modal('show');
}
function btnParentSourceParameterSaveClick() {
    var item = {
        sourceObjectPropertyRecID: $('#parentSourceParameterSourceObjectProperty').val(),
        sourceObjectPropertyDisplayName: $('#parentSourceParameterSourceObjectProperty option:selected').text(),
        parentSourceParameterValueType: parseInt($('#parentSourceParameterValueType').val()),
        parentSourceParameterStaticValue: $('#parentSourceParameterStaticValue').val(),
        targetObjectPropertyRecID: $('#parentSourceParameterTargetObjectProperty').val(),
        targetObjectPropertyDisplayName: $('#parentSourceParameterTargetObjectProperty option:selected').text()
    };
    var parameterIndex = $('#modalEditParentSourceParameter').data('parameterIndex');
    var parentSelectParameters = $('#modalEditProperties').data('parentSelectParameters');
    if (!parentSelectParameters)
        parentSelectParameters = [];
    if (parameterIndex == -1)
        parentSelectParameters.push(item);
    else
        parentSelectParameters[parameterIndex] = item;
    $('#modalEditProperties').data('parentSelectParameters', parentSelectParameters);
    fillTblParentSourceParameters();
    $('#modalEditParentSourceParameter').modal('hide');
}
async function btnEditParentSourceParameterClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    await fillParentSourceParameterTargetObjectProperty();
    $('#modalEditParentSourceParameter').data('parameterIndex', row.index());
    $('#parentSourceParameterTargetObjectProperty').val(row.data()[0]);
    $('#parentSourceParameterSourceObjectProperty').val(row.data()[6]);
    $('#parentSourceParameterValueType').val(row.data()[2]);
    parentSourceParameterValueTypeChange();
    $('#parentSourceParameterStaticValue').val(row.data()[5]);
    $('#modalEditParentSourceParameter').modal('show');
}
function btnDeleteParentSourceParameterClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var parameters = $('#modalEditProperties').data('parentSelectParameters');
    parameters.splice(row.index(), 1);
    fillTblParentSourceParameters();
}
function parentSourceParameterValueTypeChange() {
    var valueType = parseInt($('#parentSourceParameterValueType').val());
    switch (valueType) {
        case window.fEnums.FFormParentSourceParameterValueType.Static:
            $('#parentSourceParameterStaticValue').parents('.form-group').show();
            $('#parentSourceParameterSourceObjectProperty').parents('.form-group').hide();
            break;
        case window.fEnums.FFormParentSourceParameterValueType.PropertyValue:
            $('#parentSourceParameterStaticValue').parents('.form-group').hide();
            $('#parentSourceParameterSourceObjectProperty').parents('.form-group').show();
            break;
    }
}
function btnFormExecutableClick() {
    if ($('#formExecutableCodeType option').length == 0) {
        var options = [];
        for (const [key, value] of Object.entries(window.fEnums.FormExecutableCodeType)) {
            options.push({
                value: value,
                text: lngHelper.get('formDesign.formExecutableCodeType_' + key)
            })
        }
        utils.fillSelectOptions($('#formExecutableCodeType'), options, true);
    }
    $('#modalFormExecutable').modal('show');
    fillFormExecutableTable();
}
function fillFormExecutableTable() {
    if (!window.formExecutableDT) {
        window.formExecutableDT = new dataTableHelper('formExecutableDataTable', {
            ajax: '/fFormExecutable/getListDataTable?fFormID=' + window.currentForm.recID,
            dom: '<"top">rt<"bottom"<>><"clear">',
            paging: false,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('formDesign.formExecutableDisplayName') },
                {
                    title: lngHelper.get('formDesign.formExecutableCodeType'),
                    render: function (data, type, row) {
                        for (const [key, value] of Object.entries(window.fEnums.FormExecutableCodeType)) {
                            if (value.toString() == data)
                                return lngHelper.get('formDesign.formExecutableCodeType_' + key);
                        }
                        return "";
                    }
                },
                { title: lngHelper.get('formDesign.formExecutableRunOnLoad') },
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditFormExecutableClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteFormExecutableClick'
            }],
            dblClickFunction: 'btnEditFormExecutableClick'
        });
    } else
        window.formExecutableDT.reload();
}
async function btnSaveFormExecutableCodeClick(close) {
    var formExecutable = {
        recID: $('#modalFormExecutable').data('recid'),
        fFormID: window.currentForm.recID,
        displayName: $('#formExecutableDisplayName').val(),
        codeType: parseInt($('#formExecutableCodeType').val()),
        code: $('#formExecutableCode').val(),
        runOnLoad: $('#formExecutableRunOnLoad').prop('checked') ? 1 : 0
    }
    var result = await service.call('fFormExecutable/save', 'POST', formExecutable);
    $('#modalFormExecutable').data('recid', result.recID);
    fillFormExecutableTable();
    if (close)
        $('#modalFormExecutableCode').modal('hide');
}
function btnAddFormExecutableCodeClick() {
    $('#modalFormExecutable').data('recid', utils.emptyGuid);
    $('#modalFormExecutableCode').modal('show');
    $('#formExecutableDisplayName').val('');
    $('#formExecutableCodeType').val('0');
    $('#formExecutableRunOnLoad').prop('checked', false);
    $('#formExecutableCode').val('');
}
async function btnEditFormExecutableClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var recID = row.data()[0];
    $('#modalFormExecutable').data('recid', recID);
    var result = await service.call('fFormExecutable/get', 'GET', { recID: recID });

    $('#formExecutableDisplayName').val(result.displayName);
    $('#formExecutableCodeType').val(result.codeType);
    $('#formExecutableRunOnLoad').prop('checked', result.runOnLoad);
    $('#formExecutableCode').val(result.code);
    $('#modalFormExecutableCode').modal('show');
}
function btnDeleteFormExecutableClick(e) {
    var row = dataTableHelper.getRowFromElem(e);
    var recID = row.data()[0];

    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: async function () {
            popup.modal('hide');
            var result = await service.call('fFormExecutable/delete', 'GET', { recID: recID });
            fillFormExecutableTable();
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}   
function btnEditFormExecutableInEditorClick() {
    var language = 'csharp';
    switch (parseInt($('#formExecutableCodeType').val())) {
        case window.fEnums.FormExecutableCodeType.ServerSide:
            language = 'csharp';
            break;
        case window.fEnums.FormExecutableCodeType.ClientSide:
            language = 'javascript';
            break;
    }
    window.currentCodeEditorContent = $('#formExecutableCode').val();
    window.currentCodeEditorSource = 'formExecutable';
    utils.openPage('./codeEditor.html?language=' + language, null);
}
function codeEditorSaveClick(code) {
    switch (window.currentCodeEditorSource) {
        case 'formExecutable':
            $('#formExecutableCode').val(code);
            btnSaveFormExecutableCodeClick(false);
            break;
        case 'css':
            window.currentCSSContent = code;
            break;
    }
}
function btnCSSEditClick() {
    window.currentCodeEditorContent = window.currentCSSContent;
    window.currentCodeEditorSource = 'css';
    utils.openPage('./codeEditor.html?language=css', null);
}
async function initForm() {
    window.currentForm = await service.call('fForm/get', 'GET', { recID: window.currentFormID });
    window.currentObjectPropertyList = await service.call('fObjectProperty/getList', 'GET', { fObjectID: window.currentForm.fObjectID });
    window.currentObjectRelationList = await service.call('fObjectRelation/getListWithDetail', 'GET', { fObjectID: window.currentForm.fObjectID });
    window.currentObject = await service.call('fObject/get', 'GET', { recID: window.currentForm.fObjectID });
    window.currentPropertyDetails = await service.call('fObject/getPropertyDetails?fObjectID=' + window.currentForm.fObjectID, 'POST', []);

    document.title = window.currentForm.displayName + ' | ' + lngHelper.get('formDesign.design');
    var tmpContent = $('<div>' + window.currentForm.formContent + '</div>');
    var scriptElems = tmpContent.find('script');
    if (scriptElems && scriptElems.length > 0) {
        window.currentJavascriptContent = scriptElems.html();
        scriptElems.remove();
    }
    var styleElems = tmpContent.find('style');
    if (styleElems && styleElems.length > 0) {
        window.currentCSSContent = styleElems.html();
        styleElems.remove();
    }
    $('#formContent').html(tmpContent.html());
    $('#formContent').localize();
    $('#formContent').find('.fform-element').attr('onclick', 'itemClick(event)');
    $('#formContent').find('.fform-element').attr('ondragstart', 'onDragStart(event)');
    $('#formContent').find('.fform-element').attr('draggable', true);
    $('#formContent').find('.fhtml-element').attr('onclick', 'itemClick(event)');
    $('#formContent').find('.fhtml-element').attr('ondragstart', 'onDragStart(event)');
    $('#formContent').find('.fhtml-element').attr('draggable', true);
    $(window.dropHelperElem).insertBefore($('#formContent').find('.fform-element'));
    $(window.dropHelperElem).insertBefore($('#formContent').find('.fhtml-element'));
    $('#formContent').append(window.dropHelperElem);
    $('.fhtml-container').append(window.dropHelperElem);

    if (!window.mapDataLayers)
        window.mapDataLayers = await service.call('fMap/getMapLayers', 'GET', {});

    fillPropertyList();
    fillRelationList();
    fillHtmlElementList();
    var options = [];
    for (var i = 0; i < window.currentPropertyDetails.length; i++) {
        var propertyDetail = window.currentPropertyDetails[i];
        if (propertyDetail.propertyFullID.indexOf('_') < 0) {
            options.push({
                text: propertyDetail.property.displayName,
                value: propertyDetail.property.recID
            });
        }
    }
    utils.fillSelectOptions($('#parentSourceParameterSourceObjectProperty'), options, true);
}
async function initPropertyMapProps() {
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FMapBaseLayers)) {
        options.push({
            value: value,
            text: lngHelper.get('formDesign.mapBaseLayer_' + key)
        })
    }
    $('#baseMaps').data('allOptions', options);
    utils.fillSelectOptions($('#baseMaps'), options, true);

    var results = await service.call('fMap/getMapLayers', 'GET', {});
    options = [];
    for (var i = 0; i < results.length; i++) {
        options.push({
            value: results[i].fObjectProperty.fObjectID + '-' + results[i].fObjectProperty.recID,
            text: results[i].layerName
        })
    }
    utils.fillSelectOptions($('#dataMaps'), options, true);
    $('#dataMaps').data('allOptions', options);

    $("#propertyMapOptions .sortable-list").sortable();
    $("#propertyMapOptions .sortable-list").disableSelection();
}
function updateMapSelect(selectElem) {
    var layers = selectElem.data('allOptions');
    var listElem = selectElem.parents('.form-group').find('.sortable-list');

    var options = [];
    for (var i = 0; i < layers.length; i++) {
        var elem = listElem.find('[data-id=' + layers[i].value + ']');
        if (!elem || elem.length == 0) {
            options.push(layers[i]);
        }
    }
    utils.fillSelectOptions(selectElem, options, true);
}
function addLayerItem(elem, value, text) {
    var html = '<div class="list-item" data-id="' + value + '" data-item-sortable-id="0" draggable="true" role="option" aria-grabbed="false">\
                    <div class="main-content">' + text + '</div>\
                    <div><button class="btn" onclick="btnDeleteMapLayerClick(event)"><i class="fas fa-times"></i></button></div>\
                </div>';
    elem.html(elem.html() + html);
}
function btnAddMapLayerClick(e) {
    var sender = utils.getEventSender(e);
    var selectElem = sender.parents('.form-group').find('select');
    var selectedOption = selectElem.find('option:selected');
    var listElem = selectElem.parents('.form-group').find('.sortable-list');
    if (!selectedOption || selectedOption.length == 0)
        return false;
    addLayerItem(listElem, selectedOption.attr('value'), selectedOption.html());
    updateMapSelect(selectElem);
}
function btnDeleteMapLayerClick(e) {
    var sender = utils.getEventSender(e);
    var selectElem = sender.parents('.form-group').find('select');
    sender.parents('.list-item').remove();
    updateMapSelect(selectElem);
}
$(document).on('initcompleted', function () {
    window.currentFormID = utils.getUrlParam('formID');
    initForm();
    initPropertyMapProps();
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FFormParentSourceParameterValueType)) {
        options.push({
            value: value,
            text: lngHelper.get('formDesign.parentSourceParameterValueType_' + key)
        })
    }
    utils.fillSelectOptions($('#parentSourceParameterValueType'), options, true);

})
