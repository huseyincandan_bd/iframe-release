﻿async function btnSaveClick() {
    var fObject = {
        RecID: window.fObjectID,
        DisplayName: $('#displayName').val(),
        Description: $('#description').val(),
        ModuleID: $('#module').val()
    };
    if (window.currentObjectIsExternal) {
        fObject.externalDatabaseID = $('#externalDatabase').val();
        fObject.tableType = parseInt($('#tableType').val());
        switch (fObject.tableType) {
            case window.fEnums.FObjectTableType.Table:
            case window.fEnums.FObjectTableType.View:
                fObject.externalTable = $('#externalTable').val();
                break;
            case window.fEnums.FObjectTableType.SQL:
                fObject.externalTable = $('#externalDatabaseSQL').val();
                break;
        }
        fObject.recIDColumnName = $('#recIDColumnName').val();
    }
    if (window.fObjectID && window.fObjectID != utils.emptyGuid) {
        var oldObject = await service.call('fObject/get', 'GET', { recID: window.fObjectID });
        fObject.authorizationQuery = oldObject.authorizationQuery;
    }
    var result = await service.call('fObject/save', 'POST', fObject);
    window.fObjectID = result.recID;
    getObjectDetails();

    document.title = result.displayName;
    if (window.parent && window.parent.childTitleChanged)
        window.parent.childTitleChanged();
    getObjectList();
    fillTableFormSelect();
    utils.callChildIFrameFunction(true, 'reloadObjectList');
    utils.showToast('success', lngHelper.get('generic.saved'));
}
function getPropertyList() {
    if (!window.propertyDT) {
        window.propertyDT = new dataTableHelper('propertyDataTable', {
            ajax: '/fObjectProperty/getListDataTable?fObjectID=' + window.fObjectID,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('objectEdit.name') },
                {
                    title: lngHelper.get('objectEdit.type'),
                    render: function (data, type, row) {
                        for (const [key, value] of Object.entries(window.fEnums.FObjectPropertyEditorType)) {
                            if (data == value.toString())
                                return lngHelper.get('object.objectPropertyEditorType_' + key);
                        }
                    }
                }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditPropertyClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeletePropertyClick'
            }],
            dblClickFunction: 'btnEditPropertyClick'
        });
    } else {
        window.propertyDT.reload();
    }
}
async function fillTableFormSelect() {
    var forms = await service.call('fForm/getList', 'GET', { fObjectID: window.fObjectID });
    var options = [];
    for (var i = 0; i < forms.length; i++) {
        options.push({
            text: forms[i].displayName,
            value: forms[i].recID
        })
    }
    utils.fillSelectOptions($('#tableForm'), options, true);
}
async function fillTableBProcessSelect() {
    var bProcesses = await service.call('fBProcess/getList', 'GET', {});
    var options = [];
    for (var i = 0; i < bProcesses.length; i++) {
        options.push({
            text: bProcesses[i].displayName,
            value: bProcesses[i].recID
        })
    }
    utils.fillSelectOptions($('#tableBProcess'), options, true);
    $('#tableBProcess').selectpicker();
}
function btnAddPropertyClick() {
    window.fObjectPropertyID = utils.emptyGuid;
    $('#propertyEditorType').attr('disabled', false);
    if ($('#propertyEditorType option') && $('#propertyEditorType option').length > 0) {
        $('#propertyEditorType option').attr('selected', false);
        $('#propertyEditorType option').first().attr('selected', true);
        propertyEditorTypeChange();
    }
    $('#propertyDisplayName').val('');
    $('#propertyUpdateMacro').val('');
    //$('#propertyEditorDataLength').val(10);
    $('#propertyEditorDecimalScale').val(2);
    $('#propertyGeometrySRID').val('4326');
    $('#propertyGeometryLineColor').val('rgb(52, 94, 235)');
    $('#propertyGeometryLineColor').trigger('change');
    $('#propertyGeometryLineWidth').val(1);
    $('#propertyGeometryFillColor').val('rgb(52, 94, 235)');
    $('#propertyGeometryFillColor').trigger('change');
    $('#propertyGeometryOutlineColor').val('rgb(52, 94, 235)');
    $('#propertyGeometryOutlineColor').trigger('change');
    $('#propertyGeometryOutlineWidth').val(1);
    //$('#propertyGeometryPointSize').val(5);
    $('#btnPropertyGeometryIcon img').attr('src', '/img/map-icon/default.png');
    $('#btnPropertyGeometryIconWidth').val(36);
    $('#btnPropertyGeometryIconHeight').val(36);
    $('#propertyGeometryLabelPropertyID').val(utils.emptyGuid);
    $('#modalEditProperty').modal({
        show: true,
        backdrop: 'static',
        keyboard: false
    });
}
function btnEditPropertyClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    $('#propertyEditorType').attr('disabled', true);
    window.fObjectPropertyID = selectedRow.data()[0];
    service.call('fObjectProperty/get', 'GET', { recID: window.fObjectPropertyID }).then(function (result) {
        $('#propertyDisplayName').val(result.displayName);
        $('#propertyEditorType option').prop('selected', false);
        $('#propertyEditorType').val(result.editorType.toString());
        $('#propertyUpdateMacro').val(result.updateMacro);
        propertyEditorTypeChange();
        if (result.editorType == 5 || result.editorType == 7) {
            $('#propertyOptionList').val(result.fOptionListID);
        }
        $('#propertyEditorDataLength').val(result.dataLength);
        $('#propertyEditorDecimalScale').val(result.decimalScale);
        $('#propertyGeometrySRID').val(result.geometrySRID);
        if (result.props) {
            var props = JSON.parse(result.props);
            $('#propertyGeometryLineColor').val(props.GeometryLineColor);
            $('#propertyGeometryLineColor').trigger('change');
            $('#propertyGeometryLineWidth').val(props.GeometryLineWidth);
            $('#propertyGeometryFillColor').val(props.GeometryFillColor);
            $('#propertyGeometryFillColor').trigger('change');
            $('#propertyGeometryOutlineColor').val(props.GeometryOutlineColor);
            $('#propertyGeometryOutlineColor').trigger('change');
            $('#propertyGeometryOutlineWidth').val(props.GeometryOutlineWidth);
            if (!props.GeometryIcon)
                props.GeometryIcon = 'default.png';
            $('#btnPropertyGeometryIcon img').attr('src', '/img/map-icon/' + props.GeometryIcon);
            $('#btnPropertyGeometryIconWidth').val(props.GeometryIconWidth);
            $('#btnPropertyGeometryIconHeight').val(props.GeometryIconHeight);
            $('#propertyGeometryLabelPropertyID').val(props.GeometryLabelPropertyID);
            //$('#propertyGeometryPointSize').val(props.GeometryPointSize);
        }
        $('#modalEditProperty').modal({
            show: true,
            backdrop: 'static',
            keyboard: false
        });
    })
}
function btnDeletePropertyClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            window.fObjectPropertyID = selectedRow.data()[0];
            service.call('fObjectProperty/delete', 'GET', { recID: window.fObjectPropertyID }).then(function (result) {
                window.propertyDT.reload();
            })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function btnPropertyUpdateMacroInEditorClick() {
    window.currentCodeEditorContent = $('#propertyUpdateMacro').val();
    window.currentCodeEditorSource = 'propertyUpdateMacro';
    utils.openPage('./codeEditor.html?language=csharp');
}
function btnTableCssInEditorClick() {
    window.currentCodeEditorContent = $('#tableCss').val();
    window.currentCodeEditorSource = 'tableCSS';
    utils.openPage('./codeEditor.html?language=css');
}
function btnTableScriptInEditorClick() {
    window.currentCodeEditorContent = $('#tableScript').val();
    window.currentCodeEditorSource = 'tableScript';
    utils.openPage('./codeEditor.html?language=javascript');
}
function codeEditorSaveClick(code) {
    switch (window.currentCodeEditorSource) {
        case 'propertyUpdateMacro':
            $('#propertyUpdateMacro').val(code);
            btnSavePropertyClick();
            break;
        case 'tableCSS':
            $('#tableCss').val(code);
            btnSaveTableClick();
            break;
        case 'tableScript':
            $('#tableScript').val(code);
            btnSaveTableClick();
            break;
    }
}
async function btnPropertyGeometryIconClick() {
    var result = await service.call('fMap/getMapIcons');
    var container = $('<div></div>');
    for (var i = 0; i < result.length; i++) {
        var button = $('<button class="btn" style="margin:10px;border:1px solid lightgray;border-radius:3px;" onclick="btnPropertyGeometryIconSelectClick(event)"><img src="/img/map-icon/' + result[i] + '" style="width:25px;height:25px;"></img></button>');
        button.on('click', function () {
            debugger;
        });
        container.append(button);
    }
    window.currentPropertyGeometryIconSelectPopup = utils.openPopup('', lngHelper.get('objectEdit.propertyGeometryIcon'), container.prop('outerHTML'));
}
function btnPropertyGeometryIconSelectClick(e) {
    var sender = utils.getEventSender(e);
    $('#btnPropertyGeometryIcon img').attr('src', sender.attr('src'));
    window.currentPropertyGeometryIconSelectPopup.modal('hide');
}
function btnSavePropertyClick() {
    var optionListID = utils.emptyGuid;
    var propertyEditorType = parseInt($('#propertyEditorType').val());
    if (propertyEditorType == 5 || propertyEditorType == 7)
        optionListID = $('#propertyOptionList').val();
    var icon = $('#btnPropertyGeometryIcon img').attr('src');
    icon = icon.substring(icon.lastIndexOf('/') + 1);
    var props = {};
    if (propertyEditorType == window.fEnums.FObjectPropertyEditorType.GeoWKT) {
        props = Object.assign(props, {
            GeometryLineColor: $('#propertyGeometryLineColor').val(),
            GeometryLineWidth: parseFloat($('#propertyGeometryLineWidth').val()),
            GeometryFillColor: $('#propertyGeometryFillColor').val(),
            GeometryOutlineColor: $('#propertyGeometryOutlineColor').val(),
            GeometryOutlineWidth: parseFloat($('#propertyGeometryOutlineWidth').val()),
            GeometryIcon: icon,
            GeometryIconWidth: parseInt($('#btnPropertyGeometryIconWidth').val()),
            GeometryIconHeight: parseInt($('#btnPropertyGeometryIconHeight').val()),
            GeometryLabelPropertyID: $('#propertyGeometryLabelPropertyID').val()
            //GeometryPointSize: parseFloat($('#propertyGeometryPointSize').val()) 
        });
    }
    var fObjectProperty = {
        RecID: window.fObjectPropertyID,
        FObjectID: window.fObjectID,
        DisplayName: $('#propertyDisplayName').val(),
        EditorType: propertyEditorType,
        FOptionListID: optionListID,
        UpdateMacro: $('#propertyUpdateMacro').val(),
        DataLength: parseInt($('#propertyEditorDataLength').val()),
        DecimalScale: parseInt($('#propertyEditorDecimalScale').val()),
        GeometrySRID: parseInt($('#propertyGeometrySRID').val()),
        Props: JSON.stringify(props)
    };
    if ((fObjectProperty.EditorType == window.fEnums.FObjectPropertyEditorType.Select || fObjectProperty.EditorType == window.fEnums.FObjectPropertyEditorType.RadioButton) &&
        (fObjectProperty.FOptionListID == '0' || fObjectProperty.FOptionListID == utils.emptyGuid)) {
        utils.alertWarning(lngHelper.get('objectEdit.listIsRequired'));
        return;
    }
    service.call('fObjectProperty/save', 'POST', fObjectProperty).then(function (result) {
        if (result) {
            window.fObjectPropertyID = result.recID;
            $('#modalEditProperty').modal('hide');
            window.propertyDT.reload();
        }
    });
}
function getFObjectPropertyEditorTypeList() {
    //service.call('fObjectProperty/getFObjectPropertyEditorTypeList', 'GET', {}).then(function (result) {
    //    $('#propertyEditorType').html('');
    //    var i = 0;
    //    while (true) {
    //        if (!result[i])
    //            break;
    //        $('#propertyEditorType').append('<option value="' + i.toString() + '">' + result[i] + '</option>')
    //        i++;
    //    }
    //    getPropertyList();
    //})
    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FObjectPropertyEditorType)) {
        options.push({
            text: lngHelper.get('object.objectPropertyEditorType_' + key),
            value: value
        })
    }
    utils.fillSelectOptions($('#propertyEditorType'), options, true);
    getPropertyList();
}
function getFormList() {
    if (!window.formDT) {
        window.formDT = new dataTableHelper('formDataTable', {
            ajax: '/fForm/getListDataTable?fObjectID=' + window.fObjectID,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('objectEdit.name') }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditFormClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteFormClick'
            }, {
                text: lngHelper.get('objectEdit.design'),
                clickFunction: 'btnDesignFormClick'
            }, {
                text: lngHelper.get('objectEdit.permissions'),
                clickFunction: 'btnFormAuthorizationClick'
            }, {
                text: lngHelper.get('objectEdit.createRecord'),
                clickFunction: 'btnFormDataEditClick'
            }, {
                text: lngHelper.get('generic.clone'),
                clickFunction: 'btnCloneFormClick'
            }],
            dblClickFunction: 'btnEditFormClick'
        });
    } else {
        window.formDT.reload();
    }
}
function btnAddFormClick() {
    window.fFormID = utils.emptyGuid;
    $('#formDisplayName').val('');
    $('#formDescription').val('');
    $('#formContent').val('');
    $('#formOperation_Save').prop('checked', true);
    $('#formOperation_Delete').prop('checked', false);
    $('#modalEditForm').modal('show');
}
async function btnCloneFormClick(e) {
    utils.alertConfirm(lngHelper.get('generic.confirmClone'), async () => {
        var selectedRow = dataTableHelper.getRowFromElem(e);
        var fForm = await service.call('fForm/clone', 'GET', { recID: selectedRow.data()[0] });
        window.formDT.reload();
    })
}
function btnEditFormClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.fFormID = selectedRow.data()[0];
    service.call('fForm/get', 'GET', { recID: window.fFormID }).then(function (result) {
        $('#formDisplayName').val(result.displayName);
        $('#formDescription').val(result.description);
        $('#formContent').val(result.formContent);
        $('#formOperation_Save').prop('checked', true);
        $('#formOperation_Delete').prop('checked', true);
        if (result.props) {
            var props = JSON.parse(result.props);
            if (props && !props.operation_Save)
                $('#formOperation_Save').prop('checked', false);
            if (props && !props.operation_Delete)
                $('#formOperation_Delete').prop('checked', false);
        }
        $('#modalEditForm').modal('show');
    })
}
function btnDeleteFormClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            service.call('fForm/delete', 'GET', { recID: selectedRow.data()[0] }).then(function (result) {
                fillTableFormSelect();
                window.formDT.reload();
            })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function btnDesignFormClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var url = './formDesign.html?formID=' + selectedRow.data()[0];
    utils.openPage(url);
}
function btnSaveFormClick() {
    var props = {
        operation_Save: $('#formOperation_Save').is(':checked'),
        operation_Delete: $('#formOperation_Delete').is(':checked')
    }
    var fForm = {
        RecID: window.fFormID,
        FObjectID: window.fObjectID,
        DisplayName: $('#formDisplayName').val(),
        Description: $('#formDescription').val(),
        FormContent: $('#formContent').val(),
        Props: JSON.stringify(props)
    };
    service.call('fForm/save', 'POST', fForm).then(function (result) {
        if (result) {
            $('#modalEditForm').modal('hide');
            window.formDT.reload();
            fillTableFormSelect();
        } else {
            utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.errorOccurred'));
        }
    });
}
function btnFormDataEditClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var fFormID = selectedRow.data()[0];
    utils.openPage('./formDataEdit.html?formID=' + fFormID + '&closeOnSave=true');
}
function getTableList() {
    if (!window.tableDT) {
        window.tableDT = new dataTableHelper('tableDataTable', {
            ajax: '/fTable/getListDataTable?fObjectID=' + window.fObjectID,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('objectEdit.name') }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditTableClick'
            }, {
                text: lngHelper.get('objectEdit.tableEditColumns'),
                clickFunction: 'btnTableEditColumnsClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteTableClick'
            }, {
                text: lngHelper.get('objectEdit.permissions'),
                clickFunction: 'btnTableAuthorizationClick'
            }, {
                text: lngHelper.get('objectEdit.showTable'),
                clickFunction: 'btnShowTableClick'
            }, {
                text: lngHelper.get('generic.clone'),
                clickFunction: 'btnCloneTableClick'
            }],
            dblClickFunction: 'btnEditTableClick'
        });
    } else
        window.tableDT.reload();
}
function getReportList() {
    if (!window.reportDT) {
        window.reportDT = new dataTableHelper('reportDataTable', {
            ajax: '/fReport/getListDataTable?fObjectID=' + window.fObjectID,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('objectEdit.name') },
                { title: 'extension', visible: false },
                { title: 'showOnTable', visible: false },
                { title: 'showOnForm', visible: false },
                { title: 'reportType', visible: false }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditReportClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteReportClick'
            }, {
                text: lngHelper.get('generic.download'),
                clickFunction: 'btnDownloadReportClick'
            }, {
                text: lngHelper.get('objectEdit.permissions'),
                clickFunction: 'btnReportAuthorizationClick'
            }],
            dblClickFunction: 'btnEditReportClick'
        });
    } else
        window.reportDT.reload();
}
function btnAddTableClick() {
    window.fTableID = utils.emptyGuid;
    $('#tableDisplayName').val('');
    $('#tableDescription').val('');
    $('#tableCss').val('');
    $('#tableScript').val('');
    $('#tableForm').val('');
    $('#modalEditTable').modal('show');
    $('#tableOperation_New').prop('checked', false);
    $('#tableOperation_Edit').prop('checked', false);
    $('#tableOperation_Delete').prop('checked', false);
    $('#tableOperation_Export').prop('checked', false);
    $('#tableOperation_Import').prop('checked', false);
    $('#tableOperation_AddRelationWithExistingItem').prop('checked', false);
    $('#tableBProcess').selectpicker('val', []);
    $('#tableBProcessUrlParameterRecordID').val('');
    $('#tableBProcessUrlParameterTableID').val('');
    $('#tableBProcessUrlParameterObjectID').val('');
    tableBProcessChange();
    //fillTableColumnLists();
}
async function btnCloneTableClick(e) {
    utils.alertConfirm(lngHelper.get('generic.confirmClone'), async () => {
        var selectedRow = dataTableHelper.getRowFromElem(e);
        var fTable = await service.call('fTable/clone', 'GET', { recID: selectedRow.data()[0] });
        window.tableDT.reload();
    })
}
function btnEditTableClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.fTableID = selectedRow.data()[0];
    service.call('fTable/get', 'GET', { recID: window.fTableID }).then(function (result) {
        $('#tableDisplayName').val(result.displayName);
        $('#tableDescription').val(result.description);
        $('#tableCss').val(result.css);
        $('#tableScript').val(result.script);
        $('#tableOperation_New').prop('checked', true);
        $('#tableOperation_Edit').prop('checked', true);
        $('#tableOperation_Delete').prop('checked', true);
        $('#tableOperation_Export').prop('checked', true);
        $('#tableOperation_Import').prop('checked', true);
        $('#tableOperation_AddRelationWithExistingItem').prop('checked', true);
        $('#tableBProcess').selectpicker('val', []);
        if (result.props) {
            var props = JSON.parse(result.props);
            $('#tableForm').val(props.formID);
            if (props) {
                if (!props.operation_New)
                    $('#tableOperation_New').prop('checked', false);
                if (!props.operation_Edit)
                    $('#tableOperation_Edit').prop('checked', false);
                if (!props.operation_Delete)
                    $('#tableOperation_Delete').prop('checked', false);
                if (!props.operation_Export)
                    $('#tableOperation_Export').prop('checked', false);
                if (!props.operation_Import)
                    $('#tableOperation_Import').prop('checked', false);
                if (!props.operation_AddRelationWithExistingItem)
                    $('#tableOperation_AddRelationWithExistingItem').prop('checked', false);
                if (props.bProcesses)
                    $('#tableBProcess').selectpicker('val', props.bProcesses);
                if (props.bProcessUrlParameters) {
                    $('#tableBProcessUrlParameterRecordID').val(props.bProcessUrlParameters.recID);
                    $('#tableBProcessUrlParameterTableID').val(props.bProcessUrlParameters.tableID);
                    $('#tableBProcessUrlParameterObjectID').val(props.bProcessUrlParameters.objectID);
                }
            }
        }
        tableBProcessChange();
        $('#modalEditTable').modal('show');
    });
    //fillTableColumnLists();
}
function btnDeleteTableClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            window.fTableID = selectedRow.data()[0];
            service.call('fTable/delete', 'GET', { recID: window.fTableID }).then(function (result) {
                window.tableDT.reload();
            })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function btnSaveTableClick() {
    var props = {
        formID: $('#tableForm').val(),
        operation_New: $('#tableOperation_New').is(':checked'),
        operation_Edit: $('#tableOperation_Edit').is(':checked'),
        operation_Delete: $('#tableOperation_Delete').is(':checked'),
        operation_Export: $('#tableOperation_Export').is(':checked'),
        operation_Import: $('#tableOperation_Import').is(':checked'),
        operation_AddRelationWithExistingItem: $('#tableOperation_AddRelationWithExistingItem').is(':checked'),
        bProcesses: $('#tableBProcess').selectpicker('val'),
        bProcessUrlParameters: {
            recID: $('#tableBProcessUrlParameterRecordID').val(),
            tableID: $('#tableBProcessUrlParameterTableID').val(),
            objectID: $('#tableBProcessUrlParameterObjectID').val()
        }
    }
    var fTable = {
        RecID: window.fTableID,
        FObjectID: window.fObjectID,
        DisplayName: $('#tableDisplayName').val(),
        Description: $('#tableDescription').val(),
        Css: $('#tableCss').val(),
        Script: $('#tableScript').val(),
        Props: JSON.stringify(props)
    };
    service.call('fTable/save', 'POST', fTable).then(function (result) {
        if (result) {
            window.fTableID = result.recID;
            $('#modalEditTable').modal('hide');
            window.tableDT.reload();
        } else {
            utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.errorOccurred'));
        }
    });
}
function btnAddReportClick() {
    window.fReportID = utils.emptyGuid;
    $('#reportDisplayName').val('');
    $('#reportType').val('0');
    $('#reportShowOnTable').prop('checked', false);
    $('#reportShowOnForm').prop('checked', false);
    $('#reportFileContent').val('');
    $('#reportHtmlContent').val('');
    reportTypeChange();
    $('#modalEditReport').modal('show');
}
async function btnEditReportClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.fReportID = selectedRow.data()[0];
    $('#reportDisplayName').val(selectedRow.data()[1]);
    $('#reportType').val(selectedRow.data()[5]);
    $('#reportShowOnTable').prop('checked', selectedRow.data()[3] == "1")
    $('#reportShowOnForm').prop('checked', selectedRow.data()[4] == "1")
    $('#reportFileContent').val('');
    $('#reportHtmlContent').val('');
    reportTypeChange();
    if ($('#reportHtmlContent').is(':visible')) {
        var result = await service.call('fReport/getHtmlTemplateString', 'GET', { recID: selectedRow.data()[0] });
        if (result && result.result)
            $('#reportHtmlContent').val(result.result);
    }
    $('#modalEditReport').modal('show');
}
async function btnDeleteReportClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.fReportID = selectedRow.data()[0];
    utils.confirmDelete(async () => {
        var result = await service.call('fReport/delete', 'GET', { recID: window.fReportID });
        window.reportDT.reload();
    })
}
async function btnSaveReportClick(leaveModalOpen) {
    var fReport = {
        RecID: window.fReportID,
        FObjectID: window.fObjectID,
        DisplayName: $('#reportDisplayName').val(),
        ShowOnTable: $('#reportShowOnTable').is(':checked'),
        ShowOnForm: $('#reportShowOnForm').is(':checked'),
        ReportType: parseInt($('#reportType').val())
    };
    var input = document.getElementById('reportFileContent');
    var files = input.files;
    if (files && files.length > 0) {
        var fileName = input.files[0].name;
        var parts = fileName.split('.');
        fReport.Extension = parts[parts.length - 1];
    }
    var result = await service.call('fReport/save', 'POST', fReport);
    if (result) {
        window.fReportID = result.recID;
        if (fReport.ReportType == window.fEnums.FReportType.Excel) {
            window.fReportID = result.recID;
            var formData = new FormData();
            if (files.length > 0) {
                formData.append("file", files[0]);
                result = await service.call('fReport/uploadFileContent?recID=' + window.fReportID, 'POST', formData, false, false, { contentType: false, processData: false, dataType: false });
                if (result) {
                    if (!leaveModalOpen)
                        $('#modalEditReport').modal('hide');
                }
                else
                    utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.errorOccurred'));
            } else {
                if (!leaveModalOpen)
                    $('#modalEditReport').modal('hide');
            }
        } else {
            result = await service.call('fReport/setHtmlTemplateString?recID=' + window.fReportID, 'POST', $('#reportHtmlContent').val(), false, false, { processData: false});
            if (result && !leaveModalOpen)
                $('#modalEditReport').modal('hide');
        }
        window.reportDT.reload();
    } else
        utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.errorOccurred'));
}
function btnReportAuthorizationClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    utils.openPage('authorization.html?category=report&authObjectRecID=' + selectedRow.data()[0]);
}
function btnDownloadReportClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.open('../fReport/download?recID=' + selectedRow.data()[0]);
}
function reportTypeChange() {
    var reportType = parseInt($('#reportType').val());
    if (reportType == window.fEnums.FReportType.Excel) {
        $('#reportFileContent').closest('.form-group').show();
        $('#reportHtmlContent').closest('.form-group').hide();
    } else {
        $('#reportFileContent').closest('.form-group').hide();
        $('#reportHtmlContent').closest('.form-group').show();
    }
}
async function btnEditHtmlReportContentClick() {
    window.currentHtmlEditorContent = $('#reportHtmlContent').val();
    if (window.fReportID == utils.emptyGuid)
        await btnSaveReportClick(true);
    utils.openPage('./reportDesign.html?reportID=' + window.fReportID, null);
}
function htmlEditorSaveClick(html) {
    //debugger;
    $('#reportHtmlContent').val(html);
    //btnSaveReportClick();
}
function getRelationList() {
    if (!window.relationDT) {
        window.relationDT = new dataTableHelper('relationDataTable', {
            ajax: '/fObjectRelation/getListDataTable?fObjectID=' + window.fObjectID,
            columns: [
                { title: "ID", visible: false },
                { title: "ParentFObjectID", visible: false },
                { title: lngHelper.get('objectEdit.parentObject') },
                { title: "ChildFObjectID", visible: false },
                { title: lngHelper.get('objectEdit.childObject') },
                { title: lngHelper.get('objectEdit.relationType'), className: 'dt-body-center' },
                { title: 'relationTypeID', visible:false },
                { title: lngHelper.get('objectEdit.relationAllowNullParent'), className:'dt-body-center' },
                { title: lngHelper.get('generic.name') },
                { title: lngHelper.get('objectEdit.relationCascadingDelete'), visible: false },
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditRelationClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteRelationClick'
            }],
            dblClickFunction: 'btnEditRelationClick'
        });
    } else
        window.relationDT.reload();
}
function btnAddRelationClick() {
    $('#modalEditRelation').data('recID', utils.emptyGuid);
    $('#relationType').val('0');
    $('#relationAllowNullParent').prop('checked', false);
    $('#relationCascadingDelete').prop('checked', false);
    $('#relationParentObject').attr('disabled', false);
    $('#relationChildObject').attr('disabled', false);
    $('#relationType').attr('disabled', false);
    $('#relationDisplayName').val('');
    $('#modalEditRelation').modal('show');
}
function btnEditRelationClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var rowData = selectedRow.data();
    $('#modalEditRelation').data('recID', rowData[0]);
    $('#relationParentObject').val(rowData[1]);
    $('#relationChildObject').val(rowData[3]);
    $('#relationType').val(rowData[6]);
    $('#relationAllowNullParent').prop('checked', rowData[7] == '1');
    $('#relationCascadingDelete').prop('checked', rowData[9] == '1');
    $('#relationParentObject').attr('disabled', true);
    $('#relationChildObject').attr('disabled', true);
    $('#relationType').attr('disabled', true);
    $('#relationDisplayName').val(rowData[8]);
    $('#modalEditRelation').modal('show');
}
function btnDeleteRelationClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var popup = utils.openPopup('warning', lngHelper.get('generic.confirm'), lngHelper.get('generic.confirmDelete'), [{
        text: lngHelper.get('generic.yes'),
        onclick: function () {
            popup.modal('hide');
            service.call('fObjectRelation/delete', 'GET', { recID: selectedRow.data()[0] }).then(function (result) {
                window.relationDT.reload();
                //window.relationDataTableObj.ajax.reload();
            })
        }
    }, {
        text: lngHelper.get('generic.no')
    }])
}
function btnSaveRelationClick() {
    var fObjectRelation = {
        RecID: $('#modalEditRelation').data('recID') ? $('#modalEditRelation').data('recID') : utils.emptyGuid,
        ParentFObjectID: $('#relationParentObject').find('option:selected').first().attr('value'),
        ChildFObjectID: $('#relationChildObject').find('option:selected').first().attr('value'),
        RelationType: parseInt($('#relationType').find('option:selected').first().attr('value')),
        AllowNullParent: $('#relationAllowNullParent').prop('checked'),
        CascadingDelete: $('#relationCascadingDelete').prop('checked'),
        DisplayName : $('#relationDisplayName').val()
    };

    if (fObjectRelation.ParentFObjectID != window.fObjectID && fObjectRelation.ChildFObjectID != window.fObjectID) {
        utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('objectEdit.relationShouldBeOnActiveObject'));
        return;
    }
    service.call('fObjectRelation/save', 'POST', fObjectRelation).then(function (result) {
        if (result) {
            $('#modalEditRelation').modal('hide');
            window.relationDT.reload();
        } else {
            utils.openPopup('danger', lngHelper.get('generic.error'), lngHelper.get('generic.errorOccurred'));
        }
    });
}
function relationObjectChange(e) {
    if (!e)
        e = window.event;
    var sourceSelectElem = e.srcElement || e.target;
    var targetSelectElem;
    if (sourceSelectElem.id == 'relationParentObject')
        targetSelectElem = document.getElementById('relationChildObject');
    else
        targetSelectElem = document.getElementById('relationParentObject');
    var sourceFObjectID = $(sourceSelectElem).find('option:selected').first().attr('value');
    if (sourceFObjectID != window.fObjectID) {
        $(targetSelectElem).find('option').prop('selected', false);
        $(targetSelectElem).find('option[value=' + window.fObjectID + ']').prop('selected', true);
    }
}
function getIndexList() {
    if (!window.indexDT) {
        window.indexDT = new dataTableHelper('indexDataTable', {
            ajax: '/fObjectIndex/getListDataTable?fObjectID=' + window.fObjectID,
            columns: [
                { title: "ID", visible: false },
                { title: lngHelper.get('objectEdit.indexDisplayName') },
                { title: lngHelper.get('objectEdit.indexIsUnique') }
            ]
        }, {
            buttons: [{
                text: lngHelper.get('generic.edit'),
                clickFunction: 'btnEditIndexClick'
            }, {
                text: lngHelper.get('generic.delete'),
                clickFunction: 'btnDeleteIndexClick'
            }],
            dblClickFunction: 'btnEditIndexClick'
        });
    } else
        window.indexDT.reload();
}
function btnAddIndexClick() {
    $('#modalEditIndex').data('recID', utils.emptyGuid);
    $('#indexDisplayName').val('');
    $('#indexIsUnique').prop('checked', false);
    $('#indexProperties').html('');
    fillIndexProperties(true);
    $('#modalEditIndex').modal('show');
}
async function btnEditIndexClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    var result = await service.call('fObjectIndex/get', 'GET', { recID: recID });
    $('#modalEditIndex').data('recID', recID);
    $('#indexDisplayName').val(result.displayName);
    $('#indexIsUnique').prop('checked', result.isUnique);
    $('#indexProperties').html('');
    await fillIndexProperties(true);
    var allOptions = $('#indexAllProperties').data('allOptions');
    var properties = result.properties.split(',');
    for (var i = 0; i < properties.length; i++) {
        for (var j = 0; j < allOptions.length; j++) {
            if (allOptions[j].value == properties[i]) {
                addIndexItem(allOptions[j].value, allOptions[j].text);
                break;
            }
        }
    }
    fillIndexProperties(false);
    $('#modalEditIndex').modal('show');
}
async function fillIndexProperties(reload) {
    if (reload) {
        var result = await service.call('fObjectProperty/getList', 'GET', { fObjectID: window.fObjectID });
        var options = [];
        for (var i = 0; i < result.length; i++) {
            options.push({
                text: result[i].displayName,
                value: result[i].recID
            })
        }
        $('#indexAllProperties').data('allOptions', options);
    }
    var allOptions = $('#indexAllProperties').data('allOptions');
    var options = [];
    for (var i = 0; i < allOptions.length; i++) {
        var elems = $('#indexProperties').find('[data-id=' + allOptions[i].value + ']');
        if (elems && elems.length > 0)
            continue;
        options.push(allOptions[i]);
    }
    utils.fillSelectOptions($('#indexAllProperties'), options, true);
}
function btnIndexAddPropertyClick(e) {
    var sender = utils.getEventSender(e);
    var selectElem = sender.parents('.form-group').find('select');
    var selectedOption = selectElem.find('option:selected');
    var listElem = selectElem.parents('.form-group').find('.list');
    if (!selectedOption || selectedOption.length == 0)
        return false;
    addIndexItem(selectedOption.attr('value'), selectedOption.html());
    fillIndexProperties();
}
function addIndexItem(value, text) {
    var html = '<div class="list-item" data-id="' + value + '" data-item-sortable-id="0" draggable="true" role="option" aria-grabbed="false">\
                    <div class="main-content">' + text + '</div>\
                    <div><button class="btn" onclick="btnDeleteIndexPropertyClick(event)"><i class="fas fa-times"></i></button></div>\
                </div>';
    var elem = $('#indexProperties');
    elem.html(elem.html() + html);
}
function btnDeleteIndexPropertyClick(e) {
    var sender = utils.getEventSender(e);
    var selectElem = sender.parents('.form-group').find('select');
    sender.parents('.list-item').remove();
    fillIndexProperties();
}
async function btnSaveIndexClick() {
    var indexProperties = [];
    $('#indexProperties').find('.list-item').each((index, item) => {
        indexProperties.push($(item).attr('data-id'));
    })
    if (indexProperties.length == 0) {
        utils.alertWarning(lngHelper.get('objectEdit.indexPropertyRequired'));
        return;
    }
    var objectIndex = {
        recID: $('#modalEditIndex').data('recID'),
        fObjectID: window.currentObject.recID,
        displayName: $('#indexDisplayName').val(),
        isUnique: $('#indexIsUnique').is(':checked'),
        properties: indexProperties.join(',')
    };
    if (!objectIndex.displayName) {
        utils.alertWarning(lngHelper.get('objectEdit.indexDisplayNameRequired'));
        return;
    }
    var result = await service.call('fObjectIndex/save', 'POST', objectIndex);
    if (result)
        utils.showToast('success', lngHelper.get('generic.saved'));
    $('#modalEditIndex').modal('hide');
    window.indexDT.reload();
}
async function btnDeleteIndexClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    var recID = selectedRow.data()[0];
    utils.confirmDelete(async () => {
        var result = await service.call('fObjectIndex/delete', 'GET', { recID: recID });
        window.indexDT.reload();
    })
}
function tabSetVisibility(tabID, show) {
    var tabContainer = $('#' + tabID).parents('.nav-item');
    if (show)
        tabContainer.removeClass('hidden-tab');
    else {
        if (!tabContainer.hasClass('hidden-tab'))
            tabContainer.addClass('hidden-tab');
    }
}
function getObjectDetails() {
    if (window.fObjectID != utils.emptyGuid) {
        getFObjectPropertyEditorTypeList();
        getFormList();
        getTableList();
        getReportList();
        getRelationList();
        getIndexList();
        $('#frmAuthorization').attr('src', 'authorization.html?category=object&authObjectRecID=' + window.fObjectID.toString());
        tabSetVisibility('property-tab', true);
        tabSetVisibility('form-tab', true);
        tabSetVisibility('table-tab', true);
        tabSetVisibility('report-tab', true);
        tabSetVisibility('relation-tab', true);
        tabSetVisibility('authorization-tab', true);
        tabSetVisibility('index-tab', true);
    } else {
        tabSetVisibility('property-tab', false);
        tabSetVisibility('form-tab', false);
        tabSetVisibility('table-tab', false);
        tabSetVisibility('report-tab', false);
        tabSetVisibility('relation-tab', false);
        tabSetVisibility('authorization-tab', false);
        tabSetVisibility('index-tab', false);
    }
}
function btnShowTableClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.fTableID = selectedRow.data()[0];
    utils.openPage('tableDataList.html?tableID=' + window.fTableID);
}
function btnTableEditColumnsClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    window.fTableID = selectedRow.data()[0];
    utils.openPage('tableEditColumns.html?fTableID=' + window.fTableID);
}
function getOptionList() {
    window.optionListList = [];
    $('#propertyOptionList').html('');
    return new Promise(function (resolve, reject) {
        service.call('fOptionList/getList', 'GET', {}).then(function (result) {
            window.optionListList = result;
            var options = [];
            for (var i = 0; i < result.length; i++) {
                options.push({
                    value: result[i].recID,
                    text: result[i].displayName
                })
            }
            utils.fillSelectOptions($('#propertyOptionList'), options);
            resolve();
        }, function () { reject(); })
    })
}
async function propertyEditorTypeChange() {
    //var selectedOption = $('#propertyEditorType option:selected');

    var editorType = parseInt($('#propertyEditorType').val());
    switch (editorType) {
        case window.fEnums.FObjectPropertyEditorType.Select:
        case window.fEnums.FObjectPropertyEditorType.RadioButton:
            $('#propertyOptionList').parents('.form-group').show();
            $('#propertyEditorDataLength').parents('.form-group').hide();
            $('#propertyEditorDecimalScale').parents('.form-group').hide();
            $('#propertyOptionList').parents('.form-group').show();
            $('#propertyGeoWKTContainer').hide();
            break;
        case window.fEnums.FObjectPropertyEditorType.InputDecimal:
            $('#propertyEditorDataLength').val('10');
            $('#propertyEditorDecimalScale').val('2');
            $('#propertyEditorDataLength').parents('.form-group').show();
            $('#propertyEditorDecimalScale').parents('.form-group').show();
            $('#propertyOptionList').parents('.form-group').hide();
            $('#propertyGeoWKTContainer').hide();
            break;
        case window.fEnums.FObjectPropertyEditorType.InputText:
        case window.fEnums.FObjectPropertyEditorType.TextArea:
            if (editorType == window.fEnums.FObjectPropertyEditorType.TextArea)
                $('#propertyEditorDataLength').val('512');
            else
                $('#propertyEditorDataLength').val('128');
            $('#propertyEditorDecimalScale').val('0');
            $('#propertyEditorDataLength').parents('.form-group').show();
            $('#propertyEditorDecimalScale').parents('.form-group').hide();
            $('#propertyOptionList').parents('.form-group').hide();
            $('#propertyGeoWKTContainer').hide();
            break;
        case window.fEnums.FObjectPropertyEditorType.GeoWKT:
            $('#propertyEditorDataLength').val('0');
            $('#propertyEditorDecimalScale').val('0');
            $('#propertyOptionList').parents('.form-group').hide();
            $('#propertyEditorDataLength').parents('.form-group').hide();
            $('#propertyEditorDecimalScale').parents('.form-group').hide();
            var result = await service.call('fObjectProperty/getList', 'GET', { fObjectID: window.fObjectID});
            var options = [];
            for (var i = 0; i < result.length; i++) {
                options.push({
                    text: result[i].displayName,
                    value: result[i].recID
                })
            }
            utils.fillSelectOptions($('#propertyGeometryLabelPropertyID'), options, false, utils.emptyGuid);            
            $('#propertyGeoWKTContainer').show();
            break;
        default:
            $('#propertyEditorDataLength').val('0');
            $('#propertyEditorDecimalScale').val('0');
            $('#propertyOptionList').parents('.form-group').hide();
            $('#propertyEditorDataLength').parents('.form-group').hide();
            $('#propertyEditorDecimalScale').parents('.form-group').hide();
            $('#propertyGeoWKTContainer').hide();
            break;
    }
}
function getObjectList() {
    window.objectList = [];
    $('#relationParentObject').html('');
    $('#relationChildObject').html('');
    return new Promise(function (resolve, reject) {
        service.call('fObject/getList', 'GET', {}).then(function (result) {
            window.objectList = result;
            var options = [];
            for (var i = 0; i < result.length; i++) {
                options.push({
                    text: result[i].displayName,
                    value: result[i].recID
                });
            }
            utils.fillSelectOptions($('#relationParentObject'), options, true);
            utils.fillSelectOptions($('#relationChildObject'), options, true);
            $('#relationParentObject').val(window.fObjectID.toString());
            resolve();
        }, function () { reject(); })
    })
}
function btnFormAuthorizationClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    utils.openPage('authorization.html?category=form&authObjectRecID=' + selectedRow.data()[0]);
}
function btnTableAuthorizationClick(e) {
    var selectedRow = dataTableHelper.getRowFromElem(e);
    utils.openPage('authorization.html?category=table&authObjectRecID=' + selectedRow.data()[0]);
}
function externalTableChange() {
    fillExternalColumnList();
}
async function fillExternalTableOrViewList() {
    var externalDatabaseID = $('#externalDatabase').val();
    var tableType = parseInt($('#tableType').val());

    var options = [];
    if (externalDatabaseID != utils.emptyGuid) {
        var result = [];
        switch (tableType) {
            case window.fEnums.FObjectTableType.Table:
                result = await service.call('fExternalDatabase/getTableNames', 'GET', { recID: externalDatabaseID, tables: true, views: false });
                break;
            case window.fEnums.FObjectTableType.View:
                result = await service.call('fExternalDatabase/getTableNames', 'GET', { recID: externalDatabaseID, tables: false, views: true });
                break;
        }
        if (result) {
            result.sort((a,b) => {
                return a.toLowerCase().localeCompare(b.toLowerCase());
            });
            for (var i = 0; i < result.length; i++) {
                options.push({
                    text: result[i],
                    value: result[i]
                })
            }
        }
    }
    utils.fillSelectOptions($('#externalTable'), options, true);
    fillExternalColumnList();
}
async function fillExternalColumnList() {
    var externalDatabaseID = $('#externalDatabase').val();
    var tableType = parseInt($('#tableType').val());

    var options = [];
    var result = [];
    switch (tableType) {
        case window.fEnums.FObjectTableType.Table:
        case window.fEnums.FObjectTableType.View:
            var tableName = $('#externalTable').val();
            if (tableName)
                result = await service.call('fExternalDatabase/getTableColumnNames', 'GET', { recID: externalDatabaseID, tableName: tableName});
            break;
        case window.fEnums.FObjectTableType.SQL:
            var tableName = $('#externalDatabaseSQL').val();
            if (tableName)
                result = await service.call('fExternalDatabase/getTableColumnNames', 'GET', { recID: externalDatabaseID, tableName: tableName, isSQL: true });
            break;
    }
    for (var i = 0; i < result.length; i++) {
        options.push({
            text: result[i].columnName,
            value: result[i].columnName
        })
    }
    utils.fillSelectOptions($('#recIDColumnName'), options, true);
}
async function btnExecuteExternalDatabaseSQLClick() {
    await fillExternalColumnList();
}
async function externalDatabaseChange() {
    await fillExternalTableOrViewList();
}
function tableTypeChange() {
    var tableType = parseInt($('#tableType').val());
    switch (tableType) {
        case window.fEnums.FObjectTableType.Table:
            $('#externalTable').parents('.form-group').find('label').html(lngHelper.get('objectEdit.externalTableType_Table'));
            $('#externalTable').parents('.form-group').show();
            $('#externalDatabaseSQL').parents('.form-group').hide();
            break;
        case window.fEnums.FObjectTableType.View:
            $('#externalTable').parents('.form-group').find('label').html(lngHelper.get('objectEdit.externalTableType_View'));
            $('#externalTable').parents('.form-group').show();
            $('#externalDatabaseSQL').parents('.form-group').hide();
            break;
        case window.fEnums.FObjectTableType.SQL:
            $('#externalTable').parents('.form-group').hide();
            $('#externalDatabaseSQL').parents('.form-group').show();
            break;
    }
    fillExternalTableOrViewList();
}
async function initExternalDatabaseParams() {
    if (window.fObjectID == utils.emptyGuid)
        window.currentObjectIsExternal = (utils.getUrlParam('type') == 'external');
    else
        window.currentObjectIsExternal = window.currentObject.externalDatabaseID && window.currentObject.externalDatabaseID != utils.emptyGuid;

    if (window.currentObjectIsExternal) {
        $('#externalDatabaseParamContainer').show();
        result = await service.call('fExternalDatabase/getList', 'GET', {});
        var options = [];
        if (result) {
            for (var i = 0; i < result.length; i++) {
                options.push({
                    text: result[i].displayName,
                    value: result[i].recID
                })
            }
            utils.fillSelectOptions($('#externalDatabase'), options, false, utils.emptyGuid);
        }
        var options = [];
        for (const [key, value] of Object.entries(window.fEnums.FObjectTableType)) {
            options.push({
                value: value,
                text: lngHelper.get('objectEdit.externalTableType_' + key)
            })
        }
        utils.fillSelectOptions($('#tableType'), options, true);

        if (window.currentObject) {
            $('#externalDatabase').val(window.currentObject.externalDatabaseID);
            await externalDatabaseChange();
            $('#tableType').val(window.currentObject.tableType);
            await tableTypeChange();
            externalTableChange();
            switch (window.currentObject.tableType) {
                case window.fEnums.FObjectTableType.Table:
                    $('#externalTable').val(window.currentObject.externalTable);
                    break;
                case window.fEnums.FObjectTableType.View:
                    $('#externalTable').val(window.currentObject.externalTable);
                    break;
                case window.fEnums.FObjectTableType.SQL:
                    $('#externalDatabaseSQL').val(window.currentObject.externalTable);
                    break;
            }
            $('#recIDColumnName').val(window.currentObject.recIDColumnName);
        } else
            tableTypeChange();
    } else {
        $('#externalDatabaseParamContainer').hide();
        $('#externalDatabase').val(utils.emptyGuid);
        $('#tableType').val(window.fEnums.FObjectTableType.Table);
    }
}
function tableBProcessChange() {
    var bProcesses = $('#tableBProcess').selectpicker('val');
    if (bProcesses && bProcesses.length > 0)
        $('#tableBProcessUrlParametersContainer').show();
    else
        $('#tableBProcessUrlParametersContainer').hide();
}
async function getModules() {
    var result = await service.call('fModule/getList');
    var options = [];
    for (var i = 0; i < result.length; i++) {
        options.push({
            text: result[i].displayName,
            value: result[i].recID
        })
    }
    utils.fillSelectOptions($('#module'), options, false, utils.emptyGuid);
}
async function init() {
    await getModules();
    if (window.fObjectID != utils.emptyGuid) {
        var result = await service.call('fObject/get', 'GET', { recID: fObjectID });
        window.currentObject = result;
        $('#displayName').val(result.displayName);
        $('#description').val(result.description);
        $('#module').val(result.moduleID);
        utils.changeTitleFromInside(result.displayName);
        initExternalDatabaseParams();
    } else
        initExternalDatabaseParams();
    getObjectDetails();
    getOptionList();
    getObjectList();
    fillTableFormSelect();
    fillTableBProcessSelect();
}
$(document).on('initcompleted', function () {
    window.fObjectID = utils.getUrlParam('recID');
    if (!window.fObjectID)
        window.fObjectID = utils.emptyGuid;
    init();

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
    });

    $('#propertyGeometryFillColor').parents('.input-group').colorpicker({ format: 'rgb' });
    $('#propertyGeometryLineColor').parents('.input-group').colorpicker({ format: 'rgb' });
    $('#propertyGeometryOutlineColor').parents('.input-group').colorpicker({ format: 'rgb' });

    var options = [];
    for (const [key, value] of Object.entries(window.fEnums.FReportType)) {
        options.push({
            text: lngHelper.get('objectEdit.reportType_' + key),
            value: value
        })
    }
    utils.fillSelectOptions($('#reportType'), options, true);

})